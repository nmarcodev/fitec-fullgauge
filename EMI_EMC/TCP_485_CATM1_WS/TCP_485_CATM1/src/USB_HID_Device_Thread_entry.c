#include "USB_HID_Device_Thread.h"
#include "Includes/ApplicationUSB.h"
#include "Includes/ConfDefault.h"
#include "Includes/Thread_Monitor_entry.h"
#include "Includes/MainManagerThreadEntry.h"
#include "Includes/ApplicationTcp485.h"
#include "Includes/common_util.h"
#include "Serial_thread.h"
#include "Includes/Serial_thread_entry.h"

extern void g_ux_device_class_register_hid(void);
void USB_Message_Framework_NotifyCallback(TX_QUEUE *USB_HID_Device_Thread_message_queue);

void TimeoutTCP_Open(void);
void StartTimeoutTcp(timer_size_t TimeOut);
void RestartTimeoutTcp(timer_size_t TimeOut);
void StopTimeoutTcp(void);
bool        TestSerial485(uint8_t AddressInst485);
void Timer3_Open(void);
void StopTimerMain(void);
void StartTimerMain(void);
void send_gsm_status(uint8_t);


/********* Menssage Framework *************/
void RtcPost(_USB_CONTROL *USBData);
void SerialPost(_USB_CONTROL *USBData);

void  usb_actived(void *hid_instance);
void  usb_deactivate(void *hid_instance);

#define HID_FLAG ((ULONG)0x0001)

ULONG status;

//UX_SLAVE_DEVICE*    device;
//UX_SLAVE_INTERFACE* interface;
UX_SLAVE_CLASS_HID* g_hid;

UX_SLAVE_CLASS_HID_EVENT hid_event;
void  usb_actived(void *hid_instance)
{
    /* Save the CDC instance.  */
	g_hid = (UX_SLAVE_CLASS_HID *)hid_instance;
    tx_event_flags_set(&USB_Flags, HID_FLAG, TX_OR);
	//printf("USB actived\n");
}

void  usb_deactivate(void *hid_instance)
{
	SSP_PARAMETER_NOT_USED(hid_instance);

    tx_event_flags_set(&USB_Flags, ~HID_FLAG, TX_AND);
    g_hid = UX_NULL;
	//printf("USB deactivate\n");
}

bool TestSerial485(uint8_t AddressInst485)
{
//    ULONG SerialFlag = 0;
//    uint32_t AddressInst_485;
//    AddressInst_485 = AddressInst485;

	SSP_PARAMETER_NOT_USED(AddressInst485);
	tx_event_flags_set (&Serial_event_flags, SERIAL_TEST_485, TX_OR);
//    tx_queue_send(&QueueTest485, &AddressInst_485, TX_WAIT_FOREVER);   //envia para a thread Serial
    tx_thread_relinquish();
    tx_thread_sleep(200);
//    tx_event_flags_get(&EventFlagsSerialTestF, 0x01, TX_OR_CLEAR, &SerialFlag, TIME_2S);
//    USBControl.Rs485Status = !test_serial485?(uint8_t)1:(uint8_t)0;
    USBControl.Rs485Status = test_serial485;
    return test_serial485;
}


_USB_RETURN SendPacketUSB(_USB_CONTROL *USBData, uint16_t len)
{
	uint8_t Addr = 1;
	bool result_serial = true;

	SSP_PARAMETER_NOT_USED(len);

	if(g_hid != NULL)
	{
		//printf("set buffer\n");
		memset(&hid_event.ux_device_class_hid_event_buffer[0], 0, sizeof(hid_event.ux_device_class_hid_event_buffer));
		tx_thread_sleep(20);
		hid_event.ux_device_class_hid_event_length = 64;//len;
		memcpy(&hid_event.ux_device_class_hid_event_buffer[0], &USBData->BufferUSB.HeaderPayload[0], len);

		status = ux_device_class_hid_event_set(g_hid, &hid_event);
		if(status != TX_SUCCESS)
		{
			printf("Error at ux_device_class_hid_event_set: %d", status);
			while(1){}
		}
		//printf("Event set\n");
//		SerialTest();//TODO: VERSÃO EMI/EMC
		tx_thread_sleep(10);
		result_serial = TestSerial485(Addr);
		tx_thread_sleep(10);
		//printf("resultado serial end %d: %d\n",Addr,result_serial);
	}

	return UX_SUCCESS;
}

/* USB HID Device Thread entry function */
void USB_HID_Device_Thread_entry(void)
{
	ULONG actual_flags;
	static _STATUS_FLAGS StatusFlags;

	//tx_thread_sleep(TIME_5S); //TIME_1S

	g_ux_device_class_register_hid();

	//printf("Inicio da Thread USB HID\n");

	/* Get the pointer to the device.  */
	//device =  &_ux_system_slave->ux_system_slave_device;

	/* reset the HID event structure.  */
	//ux_utility_memory_set(&hid_event, 0, sizeof(UX_SLAVE_CLASS_HID_EVENT));

	g_sce_0.p_api->open(g_sce_0.p_ctrl, g_sce_0.p_cfg);
	g_sce_aes_1.p_api->open(g_sce_aes_1.p_ctrl, g_sce_aes_1.p_cfg); //Open AES_0

	//USBControl.FlagFAC = GetUSBFAC(); //Verifica se o modulo esta em fac
	//USBOpen(&USBControl, &StatusFlags);

	//Timer3_Open();
	//TimeoutTCP_Open();

    while (1)
	{

    	ux_utility_memory_set(&hid_event, 0, sizeof(UX_SLAVE_CLASS_HID_EVENT));
    	/* Is the device configured ? */
    	/* Check if a HID device is connected */
		status = tx_event_flags_get(&USB_Flags, HID_FLAG,  TX_OR, &actual_flags,  TX_WAIT_FOREVER);
		if(status != TX_SUCCESS)
		{
			//printf("Error at tx_event_flags_get: %d", status);
			while(1){}
		}


		while (g_hid != NULL)
		{
			g_ux_device_class_register_hid();
			    /* Get the pointer to the device.  */
			//device =  &_ux_system_slave->ux_system_slave_device;
			/* Then wait.  */
			tx_thread_sleep(10);
		}

//		if(USBControl.USBStatesMachine != NULL)
//    		printf("%d\n",USBControl.USBStatesMachine);
		/* Until the device stays configured.  */
//    	if(USBControl.USBStatesMachine != 0)
		if (g_hid != NULL)
		//		while (device->ux_slave_device_state == UX_DEVICE_CONFIGURED)
		{
			//printf("\nHID\n");

			//tx_thread_sleep(10);



			NetStateMachine(&USBControl, &StatusFlags);

			StatusSitrad(&StatusFlags);

			if(TX_SUCCESS == tx_event_flags_set (&g_main_event_flags, NetStatusModule(&USBControl, &StatusFlags), TX_OR))
			{
	            ThreadMonitor_SendKeepAlive(T_MONITOR_USB_THREAD);
			}

		}


	}
}

void send_gsm_status(uint8_t gsm_flag)
{
	USBControl.CellularStatus = gsm_flag;
	//printf("CELLULAR_FLAG: %d\n",USBControl.CellularStatus);
//	SerialTest();
}

//*************************************************************************************************
//* Configures the Timer03 and created semaphore for its uses
//*************************************************************************************************
void Timer3_Open(void)
{
    g_timer3.p_api->open(g_timer3.p_ctrl, g_timer3.p_cfg);

    g_timer3.p_api->periodSet( g_timer3.p_ctrl, 1, TIMER_UNIT_PERIOD_SEC );
    g_timer3.p_api->start(g_timer3.p_ctrl);
}

//*************************************************************************************************
//* Configures the Timer04 and created semaphore for its uses
//*************************************************************************************************
void TimeoutTCP_Open(void)
{
    g_timer4.p_api->open(g_timer4.p_ctrl, g_timer4.p_cfg);
}

void StartTimeoutTcp(timer_size_t TimeOut)
{
    g_timer4.p_api->periodSet( g_timer4.p_ctrl, TimeOut, TIMER_UNIT_PERIOD_SEC );
    g_timer4.p_api->start(g_timer4.p_ctrl);
}

void StartTimerMain(void)
{
    g_timer6Main.p_api->open(g_timer6Main.p_ctrl, g_timer6Main.p_cfg);
    g_timer6Main.p_api->periodSet( g_timer6Main.p_ctrl, 1, TIMER_UNIT_PERIOD_SEC );
    g_timer6Main.p_api->start(g_timer6Main.p_ctrl);
}

void StopTimerMain(void)
{
    g_timer4.p_api->stop(g_timer6Main.p_ctrl);
}

void RestartTimeoutTcp(timer_size_t TimeOut)
{
    g_timer4.p_api->periodSet( g_timer4.p_ctrl, TimeOut, TIMER_UNIT_PERIOD_SEC );
    g_timer4.p_api->reset(g_timer4.p_ctrl);
}

void StopTimeoutTcp(void)
{
    g_timer4.p_api->stop(g_timer4.p_ctrl);
}

//Callback que sinaliza que o timer3 estourou
void UserTimer3Callback (timer_callback_args_t *p_args)
{
    if (p_args->event == TIMER_EVENT_EXPIRED)
    {
        //Sinaliza que o evento ocorreu
        tx_semaphore_put(&USBStatus_Semaphore);

        g_timer3.p_api->periodSet( g_timer3.p_ctrl, 1, TIMER_UNIT_PERIOD_SEC );
        g_timer3.p_api->start(g_timer3.p_ctrl);
    }
}

//Callback que sinaliza que o TimeOutTCPCallback (timer4)
void TimeOutTCPCallback (timer_callback_args_t *p_args)
{
    if (p_args->event == TIMER_EVENT_EXPIRED)
    {
        tx_event_flags_set (&USB_Flags, FLAG_TIMEOUT_USB, TX_OR);
    }
}

void TimerMainCallback(timer_callback_args_t *p_args)
{
    if (p_args->event == TIMER_EVENT_EXPIRED)
    {
        tx_event_flags_set (&USB_Flags, FLAG_TIMEOUT_TIMER_MAIN, TX_OR);
    }
}


//Callback de recebimento de Message Framework
void USB_Message_Framework_NotifyCallback(TX_QUEUE *USB_HID_Device_Thread_message_queue)
{
    tx_event_flags_set (&USB_Flags, FLAG_MFRAMEWORK, TX_OR);
    SSP_PARAMETER_NOT_USED(USB_HID_Device_Thread_message_queue);
}


