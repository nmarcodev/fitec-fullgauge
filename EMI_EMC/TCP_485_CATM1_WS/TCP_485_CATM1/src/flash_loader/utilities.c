/*
 * utilities.c
 */
#include "common_data.h"
#include "utilities.h"
#include "app_info.h"
#include "Includes/internal_flash.h"
#include "Includes/TypeDefs.h"

_FG_ERR boot_request(const flash_instance_t *p_flash, uint8_t *p_data, uint32_t size)
{
    _FG_ERR FG_Status;

    SSP_PARAMETER_NOT_USED(p_flash);
    SSP_PARAMETER_NOT_USED(p_data);
    SSP_PARAMETER_NOT_USED(size);

    FG_Status = Flash_BootRequest();
    return FG_Status;
}

