/*
 * http_setup.h
 *
 *  Created on: 8 de abr de 2021
 *      Author: rhenanbezerra
 */

#ifndef INCLUDES_HTTP_SETUP_H_
#define INCLUDES_HTTP_SETUP_H_

#include <Cellular_thread.h>

void delete_ip_instance(void);
ULONG get_response(char * receive_buffer, ULONG size_of_buffer, ULONG *recieved_bytes);
void http_setup(void);

#endif /* INCLUDES_HTTP_SETUP_H_ */
