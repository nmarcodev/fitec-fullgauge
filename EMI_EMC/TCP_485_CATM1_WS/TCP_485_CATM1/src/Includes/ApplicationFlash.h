/*
 * AplicationFlash.h
 *
 *  Created on: 23 de abr de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_APPLICATIONFLASH_H_
#define INCLUDES_APPLICATIONFLASH_H_

#include "Includes/utility.h"
#include "Includes/USBTypes.h"

extern void SetConfigDefault(_USB_CONF *UsbConfig);

#endif /* INCLUDES_APPLICATIONFLASH_H_ */
