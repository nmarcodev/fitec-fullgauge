/*
 * wifi_app_thread_entry.h
 *
 *  Created on: 15 de mar de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_WIFI_APP_THREAD_ENTRY_H_
#define INCLUDES_WIFI_APP_THREAD_ENTRY_H_

#define TIME3_DIFF_SEC   2 ///> Valor para o timer 3 em segundos

typedef enum
{
   WIFI_S_IDLE,
   WIFI_S_INIT,
   WIFI_S_READ_FLASH,
   WIFI_S_READ_FLASH_DEFAULT,
   WIFI_S_WAIT_NET,
   WIFI_S_WAIT_QUEUE,
   WIFI_S_RESTART_IP,
   WIFI_S_RESTART_DEVICE,
   WIFI_S_PROVISION,
   WIFI_S_SCAN_USER,      ///> SCAN COM A SUPERVISAO DO USUARIO
   WIFI_S_DHCP,
   WIFI_S_SCAN,           ///> SCAN AUTOMATICO
   WIFI_S_VIEW_IP,
   WIFI_S_SAVE_NET,
   WIFI_S_EVENT_QUEUE,

}_WIFI_STATE;

typedef enum
{
   WIFI_FLAGS_NONE              = 0,
   // eventos
   WIFI_FLAG_UDP_RECEIVED       = 0x00000001,
   WIFI_FLAG_TCP_RECEIVED       = 0x00000002,
   WIFI_FLAG_FREE               = 0x00000004,
   WIFI_FLAG_FREE1              = 0x00000008,

   WIFI_FLAG_FREE2              = 0x00000010,
   WIFI_FLAG_FREE3              = 0x00000020,
   WIFI_FLAG_TCP_DISCONNECT     = 0x00000040,
   WIFI_FLAG_FREE5              = 0x00000080,

   WIFI_FLAG_TCP_CONNECT        = 0x00000100,
   WIFI_FLAG_FREE7              = 0x00000200,
   WIFI_FLAG_FREE8              = 0x00000400,
   WIFI_FLAG_QUEUE_CONSOLE      = 0X00000800,

   WIFI_FLAG_MFRAMEWORK         = 0x00001000,

   WIFI_UDPTCP_FLAGS_ALL        = 0xFFFFFFFF,

}_WIFI_TCP_UDP_FLAGS;

typedef enum
{
   WIFI_NONE,
   // eventos

   WIFI_TCP_SEND,
   WIFI_TCP_REQUESTED,
   WIFI_TCP_RELISTEN,

}_WIFI_RETURN;


#endif /* INCLUDES_WIFI_APP_THREAD_ENTRY_H_ */
