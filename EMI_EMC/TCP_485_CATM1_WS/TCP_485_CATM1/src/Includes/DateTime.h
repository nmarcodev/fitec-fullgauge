#ifndef __DATE_TIME_H
#define __DATE_TIME_H

#include "r_rtc_api.h"

typedef enum
{
    DATE_TIME_DAY    = 0,
    DATE_TIME_MONTH  = 1,
    DATE_TIME_YEAR   = 2,
    DATE_TIME_HOUR   = 3,
    DATE_TIME_MINUTE = 4,
    DATE_TIME_SECOND = 5,
    DATE_TIME_SIZE   = 6,

}_DATE_TIME_LIST;

typedef enum
{
    DT_GET    = 1,
    DT_SET    = 0,

}_DATE_TIME_EVENT;

extern void SetDateTimerX(uint8_t *DateTimeValue);
extern void GetDateTimerX(uint8_t *DateTimeValue);
extern void SetFlagEclo(uint8_t StatusEclo);
extern uint8_t GetFlagEclo(void);

//extern void GetDateTimerX(uint8_t *DateTimeValue, _DATE_TIME_EVENT DataTimeEvent);
#endif
