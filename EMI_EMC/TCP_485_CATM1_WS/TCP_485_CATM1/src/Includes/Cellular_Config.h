/*
 * Cellular_Config.h
 *
 *  Created on: 19 de mar de 2021
 *      Author: rhenanbezerra
 *
 *  Description  : This file contains Cellular specific Data structures and Defines.
 */

#ifndef INCLUDES_CELLULAR_CONFIG_H_
#define INCLUDES_CELLULAR_CONFIG_H_

#include "Cellular_Thread.h"
#include "common_util.h"

// Lista de APNs
#define APN_TIM   						"zap.vivo.com.br"//"timbrasil.br"
#define APN_VIVO  						"zap.vivo.com.br"
#define APN_CLARO 						"java.claro.com.br"

#define DEFAULT_APN_NAME            	"internet"
#define DEFAULT_CONTEXT_ID          	(1)
#define DEFAULT_PDP_TYPE            	(SF_CELLULAR_PDP_TYPE_IP)

#define AT_CMD_SIZE                 	(64)
#define AT_CMD_RSP_SIZE             	(100)

#define CELLULAR_MODULE_RESET_PIN       (IOPORT_PORT_04_PIN_08)
#define CELLULAR_MODULE_POWER_PIN       (IOPORT_PORT_03_PIN_07)
#define CELLULAR_MODULE_SUSPEND_PIN     (IOPORT_PORT_03_PIN_03)


#define PPP_LINK_TIMEOUT        		(60 * TX_TIMER_TICKS_PER_SECOND)
#define PPP_LINK_UP             		(1<<0)
#define PPP_LINK_DOWN           		(1<<1)

#define CELLULAR_UPDATE_DELAY      		(500)

#define SYS_EV_CELLULAR_UPDATE          (0x1)
#define SYS_EV_RX_UPDATE                (0x2)
#define SYS_EV_TIME_UPDATE              (0x4)
#define SYS_EV_USB_START               (0x8)
#define SYS_EV_USB_STOP                (0x10)
#define SYS_EV_CELLULAR_DISCONN         (0x20)
#define SYS_EV_ALL                      (0xFF)

ssp_err_t config_init_cellular(UINT interface_index);

#endif /* INCLUDES_CELLULAR_CONFIG_H_ */
