/*
 * Thread_Datalogger_entry.h
 *
 *  Created on: 22 de mai de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_THREAD_DATALOGGER_ENTRY_H_
#define INCLUDES_THREAD_DATALOGGER_ENTRY_H_

#include "Includes/ExternalFlash.h"
#include "Includes/TypeDefs.h"
#include "Includes/DataLoggerFlash.h"

typedef enum
{
    DL_FLAGS_NONE               = 0,
   // eventos
    DL_FLAG_CLEAR               = 0x00000001,
    DL_FLAG_READ_PAGE           = 0x00000002,
    DL_DO_DATALOGGER            = 0x00000004,
    DL_FLAG_QUEUE               = 0x00000008,

    DL_FLAG_FIRM_STARTING       = 0x00000010,
    DL_FLAG_FIRM_NEXT           = 0x00000020,
    DL_FLAG_FIRM_EVENT          = 0x00000030,
    DL_FLAG_M_FRAMEWORK         = 0x00000040,
    FREE0011                    = 0x00000080,


    DL_FLAG_TIMER_EXP           = 0x00000100,

    DL_FLAG_ALL                 = 0xFFFFFFFF,

}_DATALOGGER_FLAGS;

extern TX_EVENT_FLAGS_GROUP DataloggerEventFlags;
extern _FG_ERR SPI_Read_EEPROM(uint8_t *EepromData, uint8_t SizeEepromData);
extern _FG_ERR SPI_Write_EEPROM(uint8_t *EepromData, uint8_t SizeEepromData, uint8_t Address);
extern bool FlashReadPageData(uint32_t Address, uint16_t QntPag, uint8_t *DataFlash);
extern void ClearSectorFlashQSPI(uint16_t Sector);
extern _FG_ERR DataLoggerPost_485(_DATALOGGER_DATA *DataLoggerData);
extern _FG_ERR DataLoggerPend(_DATALOGGER_DATA *DataLoggerData);
extern void DataLoggerWriteData(_DATALOGGER_DATA *DatalogData);
extern void ClearSectorLogEEPROM(void);
extern void Datalogger_ProcessFlags(_DATALOGGER_DATA *DataloggerData);

#if RECORD_DATALOGGER_EVENTS
extern void DataLoggerRegisterEvent(const _DATALOGGER_EVENTS event, const uint16_t info, const bool status);
#endif

#endif /* INCLUDES_THREAD_DATALOGGER_ENTRY_H_ */
