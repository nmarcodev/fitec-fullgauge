/*
 * Eth_thread_entry.h
 *
 *  Created on: 6 de fev de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_ETH_THREAD_ENTRY_H_
#define INCLUDES_ETH_THREAD_ENTRY_H_

#define TIME4_DIFF_SEC      2   ///> Valor para o Timer 4

typedef enum
{
    ETH_IDLE,
    ETH_INIT,
    ETH_READ_FLASH,
    ETH_WAIT_INTERFACE,
    ETH_WAIT_QUEUE,
    ETH_EVENT_FLAG,
    ETH_DHCP,
    ETH_RESTART_IP,
    ETH_VIEW_IP,
}_ETH_STATE;

typedef enum
{
    UDP_TCP_FLAGS_NONE          = 0,
    // eventos
    UDP_RECEIVED                = 0x00000001,
    TCP_RECEIVED                = 0x00000002,
    UDP_SEND                    = 0x00000004,
    TCP_SEND                    = 0x00000008,
    UDP_REQUESTED               = 0x00000010,
    TCP_REQUESTED               = 0x00000020,
    TCP_DISCONNECT              = 0x00000040,
    TCP_RELISTEN                = 0x00000080,
    TCP_CONNECT                 = 0x000000FF,

    ETH_EVENT_PEND_UDP          = 0x00000100,
    ETH_EVENT_PEND_TCP          = 0x00000200,
    ETH_EVENT_CONSOLE           = 0X00000400,
    ETH_EVENT_QUEUE_ALL         = 0x00000FFF,

    ETH_UDPTCP_FLAGS_ALL        = 0xFFFFFFFF,

}_TCP_UDP_FLAGS;

#endif /* INCLUDES_ETH_THREAD_ENTRY_H_ */
