///*
// * UpdateOTA.h
// *
// *  Created on: 22 de ago de 2019
// *      Author: leonardo.ceolin
// */
//
//#ifndef INCLUDES_UPDATEOTA_H_
//#define INCLUDES_UPDATEOTA_H_
//
//#include "Includes/Eth_Wifi_Thread_Entry.h"
//#include "Includes/Define_IOs.h"
///* Start GET request. */
//#define HTTP_CLIENT_HOST_IP_ADDRESS     (IP_ADDRESS(192,168,248,2)) //http://192.168.248.5:666/TCP_WIFI_485_NB_01_00.bin
///* Path to resource */
//#define HTTP_CLIENT_PATH "TCP_WIFI_485_NB_01_02.bin"//"TCP_WIFI_485_NB_01_00.bin"//"TCP_WIFI_V02CRC.bin" //"teste2048.hex"//"m2m_ota_3a0.bin"//"teste2048.hex"//"/RCK-602.ffg"//"teste.bin" //"/RCK-602.ffg"
///* Wait option for HTTP Client operations */ /* https://registration.sitrad.com/TCP485WifiLogV2/TCP_WIFI_485_NB_01_00.bin */
//#define HTTP_CLIENT_WAIT_OPTION 10000
///* Receive buffer size */
//#define RECEIVE_BUFFER_SIZE 8192
//
//#define PORT_HTTP 80
///* Receive buffer */
//
// typedef struct
// {
//    uint8_t     ReceiveBuffer[RECEIVE_BUFFER_SIZE];
//    uint16_t    BytesCopied;
//
//    sf_message_header_t                 *Header;
//    _MESSAGE_FRAMEWORK_CFG              MessageCfg; //sk
//
//    _FLASHEXTERNAL_PAYLOAD              FlashExternalPayload;
// }_ControlUpdateOTA;
//
//
// //extern void UpdateOTAHttp(void);
//extern void UpdateOTAHttp(_ETH_WIFI_CONTROL *EthWifiData);
//extern void http_thread_entry_update(_ETH_WIFI_CONTROL *EthWifiData);
//extern uint8_t TesteWebHttp(_ETH_WIFI_CONTROL *EthWifiData);
//#endif /* INCLUDES_UPDATEOTA_H_ */
