/*
 * SPI_Application.h
 *
 *  Created on: 5 de jun de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_SPI_APPLICATION_H_
#define INCLUDES_SPI_APPLICATION_H_

typedef enum
{
    ETH_SPI_NONE          = 0,
    // eventos
    ETH_SPI_DONE                = 0x00000001,
    ETH_SPI_FREE1               = 0x00000002,
    ETH_SPI_FREE2               = 0x00000004,
    ETH_SPI_FREE3               = 0x00000008,
    ETH_SPI_FREE4               = 0x00000010,
    ETH_SPI_FREE5               = 0x00000020,
    ETH_SPI_FREE6               = 0x00000040,
    ETH_SPI_FREE7               = 0x00000080,
    ETH_SPI_ALL                 = 0x000000FF,

}_ETH_SPI_FLAGS;


#endif /* INCLUDES_SPI_APPLICATION_H_ */
