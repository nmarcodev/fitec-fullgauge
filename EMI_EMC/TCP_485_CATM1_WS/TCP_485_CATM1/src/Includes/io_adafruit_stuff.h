#ifndef INCLUDES_IO_ADAFRUIT_STUFF_H_
#define INCLUDES_IO_ADAFRUIT_STUFF_H_

#define IO_USERNAME         ""
#define FEED_NAME_POST      "count"
#define FEED_NAME_GET       "switch"
#define IO_KEY              ""

#define ID_SITRAD_SERVER    "hlg-introducer.sitrad.com"
#define X_AIO_KEY           "X-AIO-Key"

#define API_V2              "/api/v2/"
#define FEEDS               "/feeds/"
#define DATA                "/data"
#define DATA_LAST           "/data/last"

#define X_CONTENT_TYPE      "Content-Type"
#define CONTENT_TYPE        "application/json"

#define REQ_RESPONSE_KEY    "value"



#endif /* INCLUDES_IO_ADAFRUIT_STUFF_H_ */
