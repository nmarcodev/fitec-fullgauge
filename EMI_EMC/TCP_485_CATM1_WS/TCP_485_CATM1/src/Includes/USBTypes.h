/*
 * USBTypes.h
 *
 *  Created on: 9 de jul de 2021
 *      Author: rhenanbezerra
 */

#ifndef INCLUDES_USBTYPES_H_
#define INCLUDES_USBTYPES_H_

#include "Includes/TypeDefs.h"
#include "common_data.h" // include obrigatório

//--------------------- DEFINES ---------------------//
#define MODEL_CONV_SIZE         20
#define NAME_CONV_SIZE          30
#define VERSION_SIZE            20
#define BAUD_RATE_485_SIZE      4
#define TIME_OUT_485_SIZE       2
#define PASSWORD_SIZE           8
#define LISTS_INSTRUMENTS_SIZE  247

#define BASE_STATION_SIZE       8
#define VERSION_EXS82_SIZE      20
#define APN_ACTUAL_SIZE         100
#define APN_DEFAULT_SIZE        100
#define APN_ALTERNATIVE_SIZE    100
#define APN_MANUAL_SIZE         100
#define APN_USER_SIZE           30
#define APN_PASSWORD_SIZE            30
#define ID_SITRAD_SIZE          10
#define IMEI_SIZE               20

typedef struct
{
    uint8_t ConfDefault;//1 bytes
    uint8_t ModelConv[MODEL_CONV_SIZE]; //20bytes
    uint8_t NameConv[NAME_CONV_SIZE]; //30bytes
    uint8_t EnablePassword;//1 bytes
    uint8_t Version[VERSION_SIZE];  //20bytes
    uint8_t BaudRate485[BAUD_RATE_485_SIZE];//4bytes
    uint8_t TimeOutRs485[TIME_OUT_485_SIZE];//2bytes
    uint8_t Password[PASSWORD_SIZE];//8bytes
    uint8_t StatusSitrad;

    uint8_t BaseStation[BASE_STATION_SIZE];
    uint8_t StatusNetCell;
    uint8_t VersionEXS82[VERSION_EXS82_SIZE];      //20bytes
    uint8_t ApnMode;
    uint8_t ApnActual[APN_ACTUAL_SIZE];         //100bytes
	uint8_t ApnDefault[APN_DEFAULT_SIZE];       //100bytes
	uint8_t ApnAlternative[APN_ALTERNATIVE_SIZE];    //100bytes
	uint8_t ApnManual[APN_MANUAL_SIZE];         //100bytes
	uint8_t ApnUser[APN_USER_SIZE];           //30bytes
	uint8_t ApnPassword[APN_PASSWORD_SIZE];            //30bytes
	uint8_t IdSitrad[ID_SITRAD_SIZE];          //10bytes
	uint8_t Imei[IMEI_SIZE];               //20bytes

}_USB_CONF;

typedef enum
{
    ID_MODEL              = 0x06,
    ID_CONVERTER_NAME     = 0x07,
	ID_ENABLE_PASSWORD    = 0x08,
	ID_VERSION            = 0x0A,
    ID_BAUD_RATE          = 0x0F,
	ID_TIME_OUT_RS485     = 0x10,
	ID_PASSWORD           = 0x12,
	ID_TIME_LOG           = 0x16,//
	ID_LIST_INSTRUMENTS   = 0x17,//
	ID_STATUS_SITRAD      = 0X29,
	ID_BASE_STATION       = 0x30,
    ID_STATUS_NET_CELL    = 0x31,
    ID_VERSION_EXS82      = 0x32,
    ID_APN_MODE           = 0x33,
    ID_APN_ACTUAL         = 0x34,
    ID_APN_DEFAULT        = 0x35,
    ID_APN_ALTERNATIVE    = 0x36,
    ID_APN_MANUAL         = 0x37,
    ID_APN_USER           = 0x38,
    ID_APN_PASSWORD       = 0x39,
    ID_SITRAD             = 0x40,
    ID_IMEI               = 0x41,
	ID_END                = ID_STATUS_SITRAD,
}_PACKET_USB_CONF;

#endif /* INCLUDES_USBTYPES_H_ */
