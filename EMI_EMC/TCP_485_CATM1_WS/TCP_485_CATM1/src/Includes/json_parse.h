/*
 * json_parse.h
 *
 *  Created on: 8 Sep 2018
 *      Author: jimbo
 */

#ifndef INCLUDES_JSON_PARSE_H_
#define INCLUDES_JSON_PARSE_H_

#define NUM_JSON_TOKENS        (30UL)

ssp_err_t parse_json_response(char * json_buffer, ULONG size_of_json_response, char * required_key, char * key_string, ULONG size_of_key_string_buffer);

#endif /* INCLUDES_JSON_PARSE_H_ */
