/*
 * ApplicationTcp.h
 *
 *  Created on: 6 de jun de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_APPLICATIONTCP_H_
#define INCLUDES_APPLICATIONTCP_H_

extern void TcpStart(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad);
extern uint8_t TcpAcceptConnect(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad);
extern _TCP_RETURN TcpEvent(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad);
extern void TcpSend(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad);
extern void TcpFinish(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad);
extern void TcpDisconnect(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad);
extern void TcpRefugeeConnection(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad);

#endif /* INCLUDES_APPLICATIONTCP_H_ */
