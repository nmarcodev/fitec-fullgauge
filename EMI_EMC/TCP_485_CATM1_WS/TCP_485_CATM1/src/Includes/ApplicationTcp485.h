/*
 * ApplicationTcp485.h
 *
 *  Created on: 4 de nov de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_APPLICATIONTCP485_H_
#define INCLUDES_APPLICATIONTCP485_H_

#define WF_SSID    "HOST431 4359"
#define WF_KEY     "sitrad123"
#define MAX_ATTEMPTS     5
//#define DEBUG_WIFI

#include "common_data.h"
#include "Includes/Eth_Wifi_Thread_Entry.h"
#include "Includes/MainManagerThreadEntry.h"
#include "Includes/ApplicationIp.h"
#include "Includes/ApplicationTcpPayload.h"
#include "Includes/ApplicationMeFramw.h"
#include "Includes/ApplicationWiFi.h"
#include "Includes/ApplicationCommonModule.h"
#include "Eth_Wifi_Thread.h"
#include "Thread_Datalogger.h"
#include "Includes/utility.h"
#include "Includes/MainManagerThreadEntry.h"
#include "Includes/ApplicationAP.h"
#include "Includes/internal_flash.h"
#include "Includes/ApplicationUdp.h"
#include "Includes/ApplicationTcp.h"
#include "Includes/ApplicationLedsStatus.h"
#include "Includes/Define_IOs.h"
#include "Includes/ApplicationTimerMain.h"
#include "Includes/ApplicationUSB.h"

extern void InitEth(_USB_CONTROL *USBControl);
extern void WaitLinkEth(_USB_CONTROL *USBControl);
extern void InitAp(_USB_CONTROL *USBControl);
extern void ChangeMode(_USB_CONTROL *USBControl);
extern void ReadFlashNetDefault(_USB_CONTROL *USBControl);

extern void InitDhcp(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX);
extern void StaticIp(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX);

extern void IdleInit(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX);
extern void InitWifi(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX);
extern void WifiScan(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX);
extern void NetStateMachine(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX);
//extern void WifiProvision(_USB_CONTROL *USBControl);
//extern void WifiProvisionFail(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX);
extern _MAIN_MANAGER_FLAGS NetStatusModule(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX);
extern void StatusSitrad(_STATUS_FLAGS *StatusFlagsX);
extern bool GetStatusSitrad(void);

#endif /* INCLUDES_APPLICATIONTCP485_H_ */
