/*
 * ApplicationCellular.h
 *
 *  Created on: 3 de abr de 2021
 *      Author: rhenanbezerra
 */

#ifndef INCLUDES_APPLICATIONCELLULAR_H_
#define INCLUDES_APPLICATIONCELLULAR_H_

#include "Includes/console_config.h"

ssp_err_t cellular_is_module_init(const sf_cellular_at_cmd_set_t *cellular_at_cmds);
ssp_err_t cellular_at_commands_executioner(const sf_cellular_at_cmd_set_t *cellular_cmds);
UINT cellular_setup(net_input_cfg_t *net_cfg);

net_input_cfg_t net_cfg;

//typedef enum {
//	CONNECTION_CELULLAR_ERROR,
//	TRYING_CONNECTION,
//	PING_SUCESSFULL
//}_CELULLAR_STATUS_FLAGS;

#endif /* INCLUDES_APPLICATIONCELLULAR_H_ */
