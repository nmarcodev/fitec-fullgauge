/*
 * CRCModbus.h
 *
 *  Created on: 27 de mai de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_CRCMODBUS_H_
#define INCLUDES_CRCMODBUS_H_

extern uint16_t CrcModbusCalc(uint8_t *pu8LDataCrc, uint16_t NumBytes, bool F_Firmware);//uint16_t CrcModbusCalc(uint8_t *pu8LDataCrc, uint16_t NumBytes);

#endif /* INCLUDES_CRCMODBUS_H_ */
