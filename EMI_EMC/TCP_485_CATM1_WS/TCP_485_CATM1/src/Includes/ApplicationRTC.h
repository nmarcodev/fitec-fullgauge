#include "Includes/ApplicationTimer.h"

typedef enum
{
    RT_FLAGS_NONE               = 0,
   // eventos
	RT_UPDATE_DATE_TIME,

}_RTC_FLAGS;
typedef struct
{

    // timer de refresh da interface
    _FG_TIMER                      RTC_Timer_RefreshTimer;
    rtc_time_t                     ActualTime;
    // parametros de configuração de     mensagens via message Framework
    _MESSAGE_FRAMEWORK_CFG          Configs_messageCfg;
    _DATE_TIME_MESSAGE_PAYLOAD      *mf_DateTimePayload;

}_RTC_CONTROL;
