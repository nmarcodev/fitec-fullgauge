/*
 * ApplicationWiFi.h
 *
 *  Created on: 7 de jun de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_APPLICATIONWIFI_H_
#define INCLUDES_APPLICATIONWIFI_H_

extern _FG_ERR ScanConfig(_ETH_WIFI_CONTROL *EthWifiData);
extern uint8_t ModuleProvision(_ETH_WIFI_CONTROL *EthWifiData);
extern uint8_t FlashReadNets(_ETH_WIFI_CONTROL *WifiData);
extern void GetStatusWiFi(_ETH_WIFI_CONTROL *EthWifiData);
extern _FG_ERR ScanNets(_ETH_WIFI_CONTROL *EthWifiData);
extern bool MacAddressList(uint8_t *MacAdd, bool AddTRemoveF);

#endif /* INCLUDES_APPLICATIONWIFI_H_ */
