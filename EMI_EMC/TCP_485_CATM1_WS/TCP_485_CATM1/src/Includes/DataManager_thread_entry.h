/*
 * DataManager_thread_entry.h
 *
 *  Created on: 6 de fev de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_DATAMANAGER_THREAD_ENTRY_H_
#define INCLUDES_DATAMANAGER_THREAD_ENTRY_H_

#include "TypeDefs.h"

typedef union{
    uint8_t stFlagByte;
    struct{
        bool    bit0    :1;
        bool    bit1    :1;
        bool    bit2    :1;
        bool    bit3    :1;
        bool    bit4    :1;
        bool    bit5    :1;
        bool    bit6    :1;
        bool    bit7    :1;
    }stFlagsBit;
}UFlags;

typedef enum
{
    DM_FLAGS_NONE           = 0,
    // eventos
    DM_UDP_IN               = 0x00000001,
    DM_UDP_OUT              = 0x00000002,
    DM_TCP_IN               = 0x00000004,
    DM_TCP_OUT              = 0x00000008,
    DM_RTC                  = 0x00000010,
    DM_REQ_485              = 0x00000020,
    DM_REC_485              = 0x00000040,
    DM_TCP_OUT_VOID         = 0x00000080,
    DM_ALL                  = 0x000000FF,
}_DATA_MANAGER_FLAGS;

typedef enum
{
    TCP_CONFIG,
    TCP_CONTROL,
    TCP_VOID ,

}_TCP_RET;


#endif /* INCLUDES_DATAMANAGER_THREAD_ENTRY_H_ */
