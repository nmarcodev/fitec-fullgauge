///*
// * ApplicationTCP.c
// *
// *  Created on: 5 de jun de 2019
// *      Author: leonardo.ceolin
// */
//
//#include "common_data.h" // include obrigatório
//#include "Includes/Eth_Wifi_Thread_Entry.h"
//#include "Includes/ApplicationTcp.h"
//#include "Includes/ApplicationUdp.h"
//#include "Includes/Define_IOs.h"
//
//#if RECORD_DATALOGGER_EVENTS
//#include "Includes/Thread_Datalogger_entry.h"
//#endif
//
//#define SIZE_BUFFER_TCP 2100
//#define TCP_SOCKET_DISCONNECTION_TIMEOUT (100)
//
//void TcpStart(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad);
//uint8_t TcpAcceptConnect(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad);
//_TCP_RETURN TcpEvent(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad);
//void TcpSend(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad);
//void TcpRelisten(_ETH_WIFI_CONTROL *EthWifiData, NX_TCP_SOCKET * SocketTCP, uint16_t PortSitrad, UINT timeout);
//void TcpFinish(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad);
//void TcpDisconnect(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad);
//bool FullgaugeValidaIP_Remoto(uint32_t RemoteIP);
//void TcpRefugeeConnection(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad);
//
//
//void TcpStart(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad)
//{
//    NX_TCP_SOCKET *SocketTCP;
//
//    // Send to client. verifica a porta para a resposta
//    if(PortSitrad == EthWifiData->IpStruct.Tcp_Port_Conf)
//    {
//        SocketTCP = & EthWifiData->TcpSocketConf;
//    }
//    else
//    {
//        if(EthWifiData->FlagFAC) //não inicializa a porta de comunicacao em caso de FAC.
//            return;
//
//        SocketTCP = & EthWifiData->TcpSocket;
//    }
//
//    nx_tcp_socket_create( EthWifiData->NxIP, SocketTCP, "TCP Socket",
//                          NX_IP_NORMAL, NX_FRAGMENT_OKAY, NX_IP_TIME_TO_LIVE, SIZE_BUFFER_TCP, NX_NULL,
//                          (PortSitrad == EthWifiData->IpStruct.Tcp_Port_Conf) ?
//                                  EthWifi_TCP_DisconnectRequestConf : EthWifi_TCP_DisconnectRequest);
//
//
//    // Setup server listening on port Sitrad.
//    nx_tcp_server_socket_listen(   EthWifiData->NxIP, (UINT)PortSitrad, SocketTCP, 0,
//                                    (PortSitrad == EthWifiData->IpStruct.Tcp_Port_Conf) ? EthWifi_TCP_ConnectRequestConf : EthWifi_TCP_ConnectRequest );
//
//    //Setup a receive packet callback function for the "client_socket" socket.
//    nx_tcp_socket_receive_notify(SocketTCP, (PortSitrad == EthWifiData->IpStruct.Tcp_Port_Conf) ?
//            EthWifi_TCP_ReceiveNotifyConf : EthWifi_TCP_ReceiveNotify);
//
//}
//
//uint8_t TcpAcceptConnect(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad)
//{
//    UINT    StatusRet;
//    NX_TCP_SOCKET *SocketTCP;
//
//    // Send to client. verifica a porta para a resposta
//    if(PortSitrad == EthWifiData->IpStruct.Tcp_Port_Conf)
//    {
//        SocketTCP = & EthWifiData->TcpSocketConf;
//        StatusRet = nx_tcp_server_socket_accept(SocketTCP, 200);
//    }
//    else
//    { //porta de comunicacao TCP -> Apenas essa tem direito de refugar IP
//        SocketTCP = & EthWifiData->TcpSocket;
//        if(!FullgaugeValidaIP_Remoto((uint32_t)(SocketTCP->nx_tcp_socket_connect_ip.nxd_ip_address.v4)))
//        {
//            // Se o cliente deve ser negado por causa do IP, o unico jeito e aceitar a conexao e depois fechar o socket imediatamente.
//            // https://stackoverflow.com/questions/16590847/how-can-i-refuse-a-socket-connection-in-c
//            // O sitrad, mesmo recebendo o FIN, tenta enviar comandos. O Nx Duo vai dar Ack nesse comando e vai chamar a callback de recepcao
//            // Ao tentar receber o pacote, vai retornar erro Not Bound.
//            StatusRet = nx_tcp_server_socket_accept(SocketTCP, 200);
//            StatusRet = 1U;
//        }
//        else
//        {
//            StatusRet = nx_tcp_server_socket_accept(SocketTCP, 200);
//        }
//    }
//
//    nx_tcp_server_socket_unlisten(EthWifiData->NxIP, (UINT)PortSitrad);
//    if (StatusRet != NX_SUCCESS)
//    {
//        TcpRelisten(EthWifiData, SocketTCP, PortSitrad, 2 * TCP_SOCKET_DISCONNECTION_TIMEOUT);
//
//        return 1U; //reporta problema
//    }
//
//    return 0U; //reporta sucesso
//}
//
//void TcpRefugeeConnection(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad)
//{
//    NX_TCP_SOCKET *SocketTCP;
//
//    // Send to client. verifica a porta para a resposta
//    if(PortSitrad == EthWifiData->IpStruct.Tcp_Port_Conf)
//    {
//        SocketTCP = & EthWifiData->TcpSocketConf;
//    }
//    else
//    {
//        SocketTCP = & EthWifiData->TcpSocket;
//    }
//
//    TcpRelisten(EthWifiData, SocketTCP, PortSitrad, 2 * TCP_SOCKET_DISCONNECTION_TIMEOUT);
//}
//
//bool FullgaugeValidaIP_Remoto(uint32_t RemoteIP)
//{
//   bool Result = true;
//   _UDP_CONF   UdpConfFG;
//   uint32_t    IpInit = 0,
//               IpEnd  = 0;
//
//   GetPacketUdpCuston(&UdpConfFG);
//
//   if ( UdpConfFG.EnableIpFilter )
//   {
//       IpInit =                      (uint32_t)(UdpConfFG.InitFilterIp[0] << 24  );
//       IpInit =  (uint32_t)IpInit |  (uint32_t)(UdpConfFG.InitFilterIp[1] << 16  );
//       IpInit =  (uint32_t)IpInit |  (uint32_t)(UdpConfFG.InitFilterIp[2] << 8   );
//       IpInit =  (uint32_t)IpInit |  (uint32_t)(UdpConfFG.InitFilterIp[3]        );
//
//       IpEnd =                        (uint32_t)(UdpConfFG.EndFilterIp[0] << 24  );
//       IpEnd =  (uint32_t)IpEnd     | (uint32_t)(UdpConfFG.EndFilterIp[1] << 16  );
//       IpEnd =  (uint32_t)IpEnd     | (uint32_t)(UdpConfFG.EndFilterIp[2] << 8   );
//       IpEnd =  (uint32_t)IpEnd     | (uint32_t)(UdpConfFG.EndFilterIp[3]        );
//
//
//      Result = (( RemoteIP >= IpInit ) &&
//                ( RemoteIP <= IpEnd ));
//
//   }
//
//   return Result;
//}
//
//////Funcao que trata do recebimento TCP e valida o pacote recebido comunicacao
//_TCP_RETURN TcpEvent(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad)
//{
//    NX_TCP_SOCKET   * SocketTCP;
//    NX_PACKET       * PacketReceive;
//    UINT Status;
//
//    if(PortSitrad == EthWifiData->IpStruct.Tcp_Port_Conf)
//    {
//        SocketTCP       = &EthWifiData->TcpSocketConf;
//        PacketReceive   = (NX_PACKET*) &EthWifiData->PacketReceiveTCPConf;
//    }
//    else
//    {
//        SocketTCP       = &EthWifiData->TcpSocket;
//        PacketReceive   = (NX_PACKET*) &EthWifiData->PacketReceiveTCP;
//    }
//
//    Status = nx_tcp_socket_receive(SocketTCP, (NX_PACKET**)PacketReceive, 1);
//
//    if (NX_SUCCESS == Status)
//    {
//        return EW_TCP_REQUESTED;
//    }
//    else if(NX_NOT_BOUND == Status)
//    {
//        // Caso o a conexao tenha sido negada, o socket era aberto com accept e fechado logo em seguida.
//        // Mesmo assim o sitrad pode enviar um pacote, e vai cair aqui com esse erro. A conexao ja esta fechada, e nao pode ser fechada novamente.
//    }
//    else if(NX_NO_PACKET != Status)
//    {
//        TcpRelisten(EthWifiData, SocketTCP, PortSitrad, 5 * TCP_SOCKET_DISCONNECTION_TIMEOUT);
//    }
//
//#if RECORD_DATALOGGER_EVENTS
//    DataLoggerRegisterEvent(DATALOGGER_EVENT_TCP_ERROR, (uint16_t)(0x100 + Status), PortSitrad == EthWifiData->IpStruct.Tcp_Port_Com);
//#endif
//
//   return EW_TCP_NONE;
//}
//
//
//
//void TcpSend(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad)
//{
//    ULONG SizeTcpSend;
//    static NX_PACKET *PacketTCP_Temp;
//    NX_TCP_SOCKET *SocketTCP;
//
//    // Allocate a packet for the Send message.
//    if (NX_SUCCESS != nx_packet_allocate(&g_packet_pool0, &PacketTCP_Temp, NX_TCP_PACKET, 1))//50
//    {
//        return;
//    }
//
//    SizeTcpSend = (ULONG)(SIZE_HEADER_TCP + ((EthWifiData->BufferTcp.HP.Header[1] << 8) | EthWifiData->BufferTcp.HP.Header[2])); //parte +signficativa do tamanho
//
//    if (NX_SUCCESS != nx_packet_data_append(PacketTCP_Temp, EthWifiData->BufferTcp.HeaderPayload, SizeTcpSend, &g_packet_pool0, 1))//50
//    {
//        nx_packet_release(PacketTCP_Temp);
//        return;
//    }
//
//    // Send to client. verifica a porta para a resposta
//    if(PortSitrad == EthWifiData->IpStruct.Tcp_Port_Conf)
//    {
//        SocketTCP = & EthWifiData->TcpSocketConf;
//    }
//    else
//    {
//        SocketTCP = & EthWifiData->TcpSocket;
//    }
//
//    UINT result = nx_tcp_socket_send(SocketTCP, PacketTCP_Temp, 5); //500
//    if (NX_SUCCESS != result)
//    {
//
//#if RECORD_DATALOGGER_EVENTS
//        DataLoggerRegisterEvent(DATALOGGER_EVENT_TCP_ERROR, (uint16_t)result, PortSitrad == EthWifiData->IpStruct.Tcp_Port_Com);
//#endif
//
//        if (NX_WINDOW_OVERFLOW == result)
//        {
//            // tenta de novo
//            result = nx_tcp_socket_send(SocketTCP, PacketTCP_Temp, 5); //500
//        }
//        if (NX_SUCCESS != result)
//        {
//
//#if RECORD_DATALOGGER_EVENTS
//        DataLoggerRegisterEvent(DATALOGGER_EVENT_TCP_ERROR, (uint16_t)result, PortSitrad == EthWifiData->IpStruct.Tcp_Port_Com);
//#endif
//
//            nx_packet_release(PacketTCP_Temp);
//            TcpRelisten(EthWifiData, SocketTCP, PortSitrad, 5 * TCP_SOCKET_DISCONNECTION_TIMEOUT);
//        }
//    }
//
//}
//
//void TcpRelisten(_ETH_WIFI_CONTROL *EthWifiData, NX_TCP_SOCKET * SocketTCP, uint16_t PortSitrad, UINT timeout)
//{
//
//#if RECORD_DATALOGGER_EVENTS
//    DataLoggerRegisterEvent(DATALOGGER_EVENT_SOCKET_SHUTDOWN, (uint16_t)timeout, PortSitrad == EthWifiData->IpStruct.Tcp_Port_Com);
//#endif
//
//    // Now disconnect the server socket from the client.
//    nx_tcp_socket_disconnect(SocketTCP, timeout);
//
//    // Unaccept the server socket. Note that unaccept is called even if disconnect or accept fails.
//    nx_tcp_server_socket_unaccept(SocketTCP);
//
//    /* Setup server socket for listening with this socket again. */
//    nx_tcp_server_socket_listen(   EthWifiData->NxIP, (UINT)PortSitrad, SocketTCP, 0,
//                                       (PortSitrad == EthWifiData->IpStruct.Tcp_Port_Conf) ? EthWifi_TCP_ConnectRequestConf : EthWifi_TCP_ConnectRequest );
//}
//
//void TcpFinish(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad)
//{
//    UINT port;
//    NX_TCP_SOCKET *SocketTCP;
//
//    // Send to client. verifica a porta para a resposta
//    if(PortSitrad == EthWifiData->IpStruct.Tcp_Port_Conf)
//    {
//        SocketTCP = & EthWifiData->TcpSocketConf;
//    }
//    else
//    {
//        SocketTCP = & EthWifiData->TcpSocket;
//    }
//
//    nx_tcp_server_socket_unlisten(EthWifiData->NxIP, PortSitrad);
//
//    if(!nx_tcp_client_socket_port_get(SocketTCP, &port))
//    {
//        nx_tcp_socket_disconnect(SocketTCP, 100);
//
//        nx_tcp_server_socket_unaccept(SocketTCP);
//    }
//
//    nx_tcp_socket_delete(SocketTCP);
//}
//
//void TcpDisconnect(_ETH_WIFI_CONTROL *EthWifiData, uint16_t PortSitrad)
//{
//    UINT port;
//
//    NX_TCP_SOCKET *SocketTCP;
//
//    // Send to client. verifica a porta para a resposta
//    if(PortSitrad == EthWifiData->IpStruct.Tcp_Port_Conf)
//    {
//        SocketTCP = & EthWifiData->TcpSocketConf;
//    }
//    else
//    {
//        SocketTCP = & EthWifiData->TcpSocket;
//    }
//
//    if(!nx_tcp_client_socket_port_get(SocketTCP, &port))
//    {
//        TcpRelisten(EthWifiData, SocketTCP, PortSitrad, 15 * TCP_SOCKET_DISCONNECTION_TIMEOUT);
//
//    }
//
//
//
//}

