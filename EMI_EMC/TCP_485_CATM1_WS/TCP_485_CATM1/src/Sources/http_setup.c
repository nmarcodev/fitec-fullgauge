/*
 * http_setup.c
 *
 *  Created on: 8 de abr de 2021
 *      Author: rhenanbezerra
 */

#include "Includes/http_setup.h"
#include "Includes/tls_setup.h"
#include <Cellular_thread.h>
#include "Includes/utility.h"
#include "Includes/server_data.h"

//#define BUFFER_SIZE            (150)
//#define NETWORK_TIMEOUT        (60 * TX_TIMER_TICKS_PER_SECOND)
//
//float g_count = 42.4f;
//char  g_receive_buffer[1024];
//
//
//ULONG get_response(char * receive_buffer, ULONG size_of_buffer, ULONG *recieved_bytes)
//{
//    /* This function assumes the size of the buffer passed in is big enough to hold the complete response */
//    ULONG       get_status = NX_SUCCESS;
//    NX_PACKET   *receive_packet;
//    ULONG       status;
//
//    /* At this point, we need to handle the response from the server */
//    /*by repeatedly calling nx_web_http_client_response_body_get()   */
//    /*until the entire response is retrieved. */
//
//    do{
//      get_status = nx_web_http_client_response_body_get(&g_web_http_client1, &receive_packet, NETWORK_TIMEOUT);
//      if (NX_SUCCESS == get_status)
//      {
//          /* Process response packets */
//          *recieved_bytes = 0;
//          status = nx_packet_data_extract_offset(receive_packet, 0, receive_buffer,  size_of_buffer - 1, recieved_bytes );
//          nx_packet_release (receive_packet);
//          if (NX_SUCCESS != status)
//          {
//             return status;
//          }
//      }
//      else if (get_status != NX_WEB_HTTP_GET_DONE)
//      {
//          return get_status;
//      }
//
//    }while(get_status != NX_WEB_HTTP_GET_DONE);
//
//    return NX_SUCCESS;
//}
//
//
//void delete_ip_instance(void)
//{
//    UINT status;
//
//    status = nx_web_http_client_delete(&g_web_http_client1);
//    if (NX_SUCCESS != status)
//    {
//        while(1);
//    }
//
//    status = nx_dns_delete(&g_dns1);
//    if (NX_SUCCESS != status)
//    {
//        while(1);
//    }
//
////    status = nx_dhcp_stop(&g_dhcp_client0);
////    if (NX_SUCCESS != status)
////    {
////        while(1);
////    }
////
////    status = nx_dhcp_delete(&g_dhcp_client0);
////    if (NX_SUCCESS != status)
////    {
////        while(1);
////    }
//
//    status = nx_ip_delete(&g_ip0);
//    if (NX_SUCCESS != status)
//    {
//        while(1);
//    }
//
//    printf("\r\n Invalid Context Id!!! \r\n");
//}
//
//void http_process(void)
//{
//	UINT         status;
//	NXD_ADDRESS  server_ip;
//	CHAR         http_request[BUFFER_SIZE];
//	CHAR         resource_for_post[BUFFER_SIZE];
//	CHAR         resource_for_get[BUFFER_SIZE];
//	NX_PACKET    *http_packet;
//	ULONG        actual_size;
//	ssp_err_t    err;
//	char         key_string[16];
//
//	printf("Attempting to resolve Host IP address for %s using DNS.\n\r", ID_SITRAD_SERVER);
//
//	/*Attempting to resolve Host IP address for server using DNS.*/
//	server_ip.nxd_ip_version = NX_IP_VERSION_V4;
//
//	status = nx_dns_host_by_name_get(&g_dns1, (UCHAR *)ID_SITRAD_SERVER,
//									 &(server_ip.nxd_ip_address.v4),
//									 NETWORK_TIMEOUT);
//	if (NX_SUCCESS != status)
//	{
//		delete_ip_instance();
//		while(1);
//	}
//
//
//	printf("Host IP address = %d.%d.%d.%d \r\n", (int)(server_ip.nxd_ip_address.v4>>24 & 0xFF),
//												 (int)(server_ip.nxd_ip_address.v4>>16 & 0xFF),
//												 (int)(server_ip.nxd_ip_address.v4>>8  & 0xFF),
//												 (int)(server_ip.nxd_ip_address.v4>>0  & 0xFF));
//
//    /* Connect to a remote HTTP(S) server. */
//    status = nx_web_http_client_secure_connect(&g_web_http_client1, &server_ip, NX_WEB_HTTPS_SERVER_PORT, tls_setup_callback, NETWORK_TIMEOUT);
//    if (NX_SUCCESS != status)
//    {
//        delete_ip_instance();
//        while(1);
//    }
//
//    /* Set up the strings for the HTTP requests */
//    memset(http_request, 0, sizeof(http_request));
//    snprintf(http_request, BUFFER_SIZE, "{\"value\": \"%2.1f\"}",g_count);
//
//    memset(resource_for_post, 0, sizeof(resource_for_post));
//    snprintf(resource_for_post, BUFFER_SIZE, "%s%s%s%s%s", API_V2, IO_USERNAME, FEEDS, FEED_NAME_POST, DATA);
//
//    status = nx_web_http_client_request_initialize(&g_web_http_client1,
//                                                 NX_WEB_HTTP_METHOD_POST,
//                                                 resource_for_post,
//                                                 ID_SITRAD_SERVER,
//                                                 strlen(http_request),
//                                                 NX_FALSE,
//                                                 NX_NULL, /* username */
//                                                 NX_NULL, /* password */
//                                                 NETWORK_TIMEOUT);
//    if (NX_SUCCESS != status)
//    {
//        delete_ip_instance();
//        while(1);
//    }
//
//    /* Add AIO key header */
//    status = nx_web_http_client_request_header_add(&g_web_http_client1,
//                                                 X_AIO_KEY,
//                                                 strlen(X_AIO_KEY),
//                                                 IO_KEY,
//                                                 strlen(IO_KEY),
//                                                 NETWORK_TIMEOUT);
//    if (NX_SUCCESS != status)
//    {
//        delete_ip_instance();
//        while(1);
//    }
//
//    /* Add Content type header:- Content-Type: application/json */
//    status = nx_web_http_client_request_header_add(&g_web_http_client1,
//                                                 X_CONTENT_TYPE,
//                                                 strlen(X_CONTENT_TYPE),
//                                                 CONTENT_TYPE,
//                                                 strlen(CONTENT_TYPE),
//                                                 NETWORK_TIMEOUT);
//    if (NX_SUCCESS != status)
//    {
//        delete_ip_instance();
//        while(1);
//    }
//
//    status = nx_web_http_client_request_send(&g_web_http_client1, NETWORK_TIMEOUT);
//    if (NX_SUCCESS != status)
//    {
//        delete_ip_instance();
//        while(1);
//    }
//
//    /* Create a new data packet request on the HTTP(S) client instance. */
//    status = nx_web_http_client_request_packet_allocate(&g_web_http_client1, &http_packet, NETWORK_TIMEOUT);
//    if (NX_SUCCESS != status)
//    {
//         delete_ip_instance();
//         while(1);
//    }
//
//    status = nx_packet_data_append(http_packet, http_request, strlen(http_request), &g_packet_pool0, NETWORK_TIMEOUT);
//    if (NX_SUCCESS != status)
//    {
//         delete_ip_instance();
//         while(1);
//    }
//
//    status = nx_web_http_client_request_packet_send(&g_web_http_client1, http_packet, 0, NETWORK_TIMEOUT);
//    if (NX_SUCCESS != status)
//    {
//         delete_ip_instance();
//         while(1);
//    }
//
//    memset(g_receive_buffer, 0, sizeof(g_receive_buffer));
//    status = get_response(g_receive_buffer, sizeof(g_receive_buffer), &actual_size);
//    if (NX_SUCCESS != status)
//    {
//         delete_ip_instance();
//         while(1);
//    }
//    printf("\r\nResponse from HTTP POST :-\r\n%s\r\n", g_receive_buffer);
//
//    memset(resource_for_get, 0, sizeof(resource_for_get));
//    snprintf(resource_for_get, BUFFER_SIZE, "%s%s%s%s%s", API_V2, IO_USERNAME, FEEDS, FEED_NAME_GET, DATA_LAST);
//
//    status = nx_web_http_client_request_initialize(&g_web_http_client1,
//                                                NX_WEB_HTTP_METHOD_GET,
//                                                resource_for_get,
//                                                ID_SITRAD_SERVER,
//                                                NX_NULL,
//                                                NX_FALSE, /* Chunked */
//                                                NX_NULL, /* username */
//                                                NX_NULL, /* password */
//                                                NETWORK_TIMEOUT);
//    if (NX_SUCCESS != status)
//    {
//         delete_ip_instance();
//         while(1);
//    }
//
//    /* Add AIO key header */
//    status = nx_web_http_client_request_header_add(&g_web_http_client1,
//                                                X_AIO_KEY,
//                                                strlen(X_AIO_KEY),
//                                                IO_KEY,
//                                                strlen(IO_KEY),
//                                                NETWORK_TIMEOUT);
//    if (NX_SUCCESS != status)
//    {
//         delete_ip_instance();
//         while(1);
//    }
//
//    status = nx_web_http_client_request_send(&g_web_http_client1, NETWORK_TIMEOUT);
//    if (NX_SUCCESS != status)
//    {
//         delete_ip_instance();
//         while(1);
//    }
//
//    memset(g_receive_buffer, 0, sizeof(g_receive_buffer));
//    status = get_response(g_receive_buffer, sizeof(g_receive_buffer), &actual_size);
//    if (NX_SUCCESS != status)
//    {
//         delete_ip_instance();
//         while(1);
//    }
//    printf("\r\nResponse from HTTP GET :-\r\n%s\r\n", g_receive_buffer);
//
//    memset(key_string, 0, sizeof(key_string));
//    err = parse_json_response(g_receive_buffer, actual_size, REQ_RESPONSE_KEY, key_string, sizeof(key_string));
//    if(SSP_SUCCESS != err)
//    {
//        printf("Parsing JSON response failed\r\n");
//    }
//    else
//    {
//        printf("\r\nValue of key \"%s\" is %s\r\n\r\n", REQ_RESPONSE_KEY, key_string);
//    }
//
//    if (0 == strcmp(key_string, "OFF"))
//    {
////        g_ioport.p_api->pinWrite(leds.p_leds[GREEN_LED],  IOPORT_LEVEL_LED_OFF);
////        g_ioport.p_api->pinWrite(leds.p_leds[ORANGE_LED], IOPORT_LEVEL_LED_OFF);
////        g_ioport.p_api->pinWrite(leds.p_leds[RED_LED],    IOPORT_LEVEL_LED_ON);
//    }
//    else if (0 == strcmp(key_string, "ON"))
//    {
////        g_ioport.p_api->pinWrite(leds.p_leds[GREEN_LED],  IOPORT_LEVEL_LED_ON);
////        g_ioport.p_api->pinWrite(leds.p_leds[ORANGE_LED], IOPORT_LEVEL_LED_ON);
////        g_ioport.p_api->pinWrite(leds.p_leds[RED_LED],    IOPORT_LEVEL_LED_OFF);
//    }
//
//
//   /* We have finished, so close everything down */
//   printf("finished\r\n");
//   delete_ip_instance();
//
//}
