/*
 * AplicationTimer.c
 *
 *  Created on: 12 de dez de 2018
 *      Author: felipe.pohren
 */
#include "common_data.h" // include obrigatório

#include "Includes/ApplicationTimer.h"

_FG_ERR  AplicationTimer_TimerVerify(_FG_TIMER* timer);


//*************************************************************************************************
//* Verifica se o timer expirou
//*
//* @param[in] timer: ponteiro para a estrutura do timer
//*
//*************************************************************************************************
_FG_ERR AplicationTimer_TimerVerify(_FG_TIMER* timer)
{
    //obtem informação do timer
    tx_timer_info_get(&timer->timerControlBlock,
                      &timer->name,
                      &timer->active,
                      &timer->remaining_ticks,
                      &timer->reschedule_ticks,
                      &timer->next_timer);

    // retorna estado do timer
    if(timer->active == TX_FALSE)
        return FG_TIMER_EXPIRED;
    else
        return FG_TIMER_ACTIVE;
}
