#include "Cellular_thread.h"
#include <stdio.h>
#include <stdlib.h>
#include "Includes\jsmn.h"
#include "Includes\json_parse.h"

/* based on code from https://github.com/alisdair/jsmn-example */


bool json_token_streq(char *js, jsmntok_t *t, char *s);
char * json_token_tostr(char *js, jsmntok_t *t);
ULONG json_parse(jsmntok_t *tokens, char *js, int num_tokens, char * key, char * return_str, ULONG size_of_return_str);



bool json_token_streq(char *js, jsmntok_t *t, char *s)
{
    return ((strncmp(js + t->start, s, (size_t)(t->end - t->start)) == 0)  && (strlen(s) == (size_t) (t->end - t->start)));
}

char * json_token_tostr(char *js, jsmntok_t *t)
{
    js[t->end] = '\0';
    return js + t->start;
}

ULONG json_parse(jsmntok_t *tokens, char *js, int num_tokens, char * key, char * return_str, ULONG size_of_return_str)
{

    typedef enum { START, KEY, PRINT, SKIP, STOP } parse_state;
    parse_state state = START;

    size_t object_tokens = 0;
    size_t sub_tokens = 0;

    for (size_t i = 0, j = (size_t)num_tokens; j > 0; i++, j--)
    {
        jsmntok_t *t = &tokens[i];

        /* Should never reach uninitialised tokens */
        if(t->start == -1 && t->end == -1)
        {
            //printf("assert\r\n");
            return 1;
        }

        switch (state)
        {
            case START:
                if (t->type != JSMN_OBJECT)
                {
                   // printf("Invalid response: root element must be an object.\r\n");
                    return 1;
                }

                state = KEY;
                object_tokens = (size_t)t->size;

                if (object_tokens == 0)
                    state = STOP;
                break;

            case KEY:
                if (t->type != JSMN_STRING)
                {
                    //printf("Invalid response: object keys must be strings.\r\n");
                    return 1;
                }

                object_tokens--;
                state = SKIP;
                sub_tokens = (size_t)t->size;

                if (json_token_streq(js, t, key))
                {
                    //printf("%s: ", key);
                    state = PRINT;
                    break;
                }

                break;

            case SKIP:
                if (t->type != JSMN_STRING && t->type != JSMN_PRIMITIVE)
                {
                    //printf("Invalid response: object values must be strings or primitives.\r\n");
                    return 1;
                }

                sub_tokens--;
                if (sub_tokens == 0)
                {
                    state = KEY;
                }

                if (object_tokens == 0)
                {
                    state = STOP;
                }

                break;

            case PRINT:
                if (t->type != JSMN_STRING && t->type != JSMN_PRIMITIVE)
                {
                    //printf("Invalid response: object values must be strings or primitives.\r\n");
                    return 1;
                }

                char *str = json_token_tostr(js, t);
                //puts(str);
                if(((t->end - t->start) + 1) > (int)size_of_return_str)
                {
                    //printf("Buffer too small for string.\r\n");
                    return 1;

                }
                strcpy(return_str, str);

                sub_tokens--;
                if (sub_tokens == 0)
                {
                    state = KEY;
                }

                if (object_tokens == 0)
                {
                    state = STOP;
                }

                break;

            case STOP:
                // Just consume the tokens
                break;

            default:
                //printf("Invalid state %u\r\n", state);
                return 1;
        }
    }

    return 0;
}

ssp_err_t parse_json_response(char * json_buffer, ULONG size_of_json_response, char * required_key, char * key_string, ULONG size_of_key_string_buffer)
{
    ssp_err_t err = SSP_SUCCESS;
    jsmntok_t    tokens[NUM_JSON_TOKENS];
    jsmn_parser  parser;
    int          jsmn_status;
    UINT         status;

    /* Intialise the jsmn json parser MIT license https://github.com/zserge/jsmn */
    jsmn_init(&parser);

    if (0 < size_of_json_response)
    {
        jsmn_status = jsmn_parse(&parser, json_buffer, size_of_json_response, tokens, NUM_JSON_TOKENS);
        if (0 > jsmn_status)
        {
            err = SSP_ERR_INTERNAL;
        }
        else
        {
            status = json_parse(tokens, json_buffer, jsmn_status, required_key, key_string, size_of_key_string_buffer);
            if (0 != status)
            {
                err = SSP_ERR_INTERNAL;
            }
        }

    }

    return err;
}
