/*
 * ApplicationIp.c
 *
 *  Created on: 7 de jun de 2019
 *      Author: leonardo.ceolin
 */
#include "common_data.h" // include obrigatório
//#include "Includes/Eth_Wifi_Thread_Entry.h"
#include "Includes/ApplicationUSB.h"
//#include "Eth_Wifi_Thread.h"
#include "Includes/Eeprom25AA02E48.h"
//#include "Includes/ApplicationIp.h"
#include "Includes/Define_IOs.h"
//#include "Includes/ApplicationUdp.h"
//#include "Includes/ApplicationAP.h"
//#include "Includes/ApplicationUdp.h"
#include <string.h>

#if RECORD_DATALOGGER_EVENTS
#include "Includes/Thread_Datalogger_entry.h"
#endif

void USBOpen(_USB_CONTROL *USBData, _STATUS_FLAGS *StatusFlagsX);
//void set_mac_address (nx_mac_address_t*_pMacAddress);
//void SetStatusLinkEth(uint8_t StatusEth);

//uint8_t StatusLinkEth(uint8_t StatusEth, uint8_t SetGet);
//uint8_t EthWifiWaitForLink(_USB_CONTROL *USBData);
//uint8_t EthWifiStartDhcpClient (_USB_CONTROL *USBData);
//uint8_t EthWifiStartStaticIP(NX_IP * NxIP, ULONG Address, ULONG Mask, ULONG Gtw, ULONG Dns);
//uint8_t EthWifiGetStatusIp(_USB_CONTROL *USBData);
//UINT NetStop (_USB_CONTROL *USBData);

//void mydhcp_Wifi(CHAR *name);
//void mydhcp_Eth(CHAR *name);
void ReplaceNonAlphanumericByDash(CHAR *Str);

#define ARP_RENEWAL_PERIOD (60 * _CLOCKS_PER_SEC_)
#define ARP_CHECK_PERIOD (30 * _CLOCKS_PER_SEC_)

void USBOpen(_USB_CONTROL *USBData, _STATUS_FLAGS *StatusFlagsX)
{
    //Message Framework: Used in Acquire
    USBData->MessageCfg.acquireCfg.buffer_keep = true;   // não desaloca o buffer

    //Message Framework: Used in Post
    USBData->MessageCfg.postCfg.priority = SF_MESSAGE_PRIORITY_NORMAL;
    USBData->MessageCfg.postCfg.p_callback = NULL; // não cria callback

    //Message Framework: Cria BufferAcquire para postar para Serial 485
    g_sf_message0.p_api->bufferAcquire( g_sf_message0.p_ctrl,
                                        (sf_message_header_t **) &USBData->SerialDataInPayload,
                                        &USBData->MessageCfg.acquireCfg,
                                        300);

    //Message Framework: Cria BufferAcquire para postar para RTC
    g_sf_message0.p_api->bufferAcquire(  g_sf_message0.p_ctrl,
                                         (sf_message_header_t **) &USBData->DateTimeMessagePayload,
                                         &USBData->MessageCfg.acquireCfg,
                                         300);

    //Message Framework: Cria BufferAcquire para postar para Firmware
    g_sf_message0.p_api->bufferAcquire(  g_sf_message0.p_ctrl,
                                         (sf_message_header_t **) &USBData->FlashExternalPaylod,
                                         &USBData->MessageCfg.acquireCfg,
                                         300);

    //Cria a notificacao entre uma mensagem recebida com o Data_Manager_Message_Framework_NotifyCallback
    //tx_queue_send_notify (&USB_HID_Device_Thread_message_queue, USB_HID_Device_Thread_message_queue);

    StatusFlagsX->StateSitrad           = false;
    StatusFlagsX->FlagNetScan           = false;
    StatusFlagsX->FlagNumAttempts       = false;
    StatusFlagsX->FlagStartUdpTcp       = false;
    StatusFlagsX->FlagNetConnected      = false;
    StatusFlagsX->FlagSitradConnected   = false;

    StatusFlagsX->FlagErrorIp         = false;
    StatusFlagsX->FlagErrorPassWrd      = false;

    USBData->FlagConnect            = false;
    USBData->ValidAccess            = false;
    USBData->ValidAccessConf        = false;
    USBData->BigFrame            = false;
    USBData->FlagOTAFinished        = false;
    USBData->FlagOTARun             = false;

    USBData->TimerBigFrame          = 0;
    USBData->TimerRestartModule     = 0;
    USBData->TimerOTACheckCRC       = 0;
    USBData->TimerTimeOutOTA        = 0;
    USBData->TimerRestoreFactory    = 0;
    USBData->TimerInactivity        = 0;

    g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_HIGH); // DESLIGA O LED DE ENVIO
    g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_HIGH); // DESLIGA O LED DE ENVIO
    g_ioport.p_api->pinWrite(LED_ETH, IOPORT_LEVEL_HIGH);

    USBData->USBStatesMachine = SM_IDLE;
}

//
//uint8_t StatusLinkEth(uint8_t StatusEth, uint8_t SetGet)
//{
//    static uint8_t LinkEth=0;
//
//    if(SetGet)
//    {
//        LinkEth = StatusEth;
//    }
//
//     return LinkEth;
//}
//
//void SetStatusLinkEth(uint8_t StatusEth)
//{
//    StatusLinkEth(StatusEth, 1U);
//}
//
//uint8_t GetStatusLinkEth(void)
//{
//    return StatusLinkEth(0, 0);
//}

#define PRIMARY_INTERFACE (0)
//VOID LinkStatusChangeNotifyEth(NX_IP *ip_ptr, UINT interface_index, UINT link_up);
//VOID LinkStatusChangeNotifyEth(NX_IP *ip_ptr, UINT interface_index, UINT link_up)
//{
//    SSP_PARAMETER_NOT_USED(ip_ptr);
//
//    if (PRIMARY_INTERFACE == interface_index)
//    {
//        if (NX_TRUE == link_up)
//        {
//            g_ioport.p_api->pinWrite(LED_ETH, IOPORT_LEVEL_LOW);
//        }
//        else if (NX_FALSE == link_up)
//        {
//            g_ioport.p_api->pinWrite(LED_ETH, IOPORT_LEVEL_HIGH);
//        }
//
//        SetStatusLinkEth((uint8_t)link_up);
//
//#if RECORD_DATALOGGER_EVENTS
//        DataLoggerRegisterEvent(DATALOGGER_EVENT_ETHERNET_CONNECTION_STATUS_CHANGE, 0, link_up);
//#endif
//
//    }
//}

/***********************************************************************************************************************
 * @brief  wait_for_link function
 *
 * This function check the network link status
 ***********************************************************************************************************************/
//uint8_t EthWifiWaitForLink(_USB_CONTROL *USBData)
//{
//    UINT        status = 0;
//    ULONG       ip_status;
//    uint8_t     retry_cnt = 0;
//
//    do {
//         status = nx_ip_interface_status_check(USBData->NxIP, 0, NX_IP_LINK_ENABLED, &ip_status, 50);
//         retry_cnt++;
//    }while((status != NX_SUCCESS) && (retry_cnt < MAX_RETRY_CNT));
//
//    if(retry_cnt >= MAX_RETRY_CNT)
//    {
//        return 1U;
//    }
//    else
//    {
//        return 0U;
//    }
//
//    nx_ip_link_status_change_notify_set(USBData->NxIP, LinkStatusChangeNotifyEth);
//}


/***********************************************************************************************************************
 * @brief  start_dhcp_client function
 *
 * This function starts the dhcp client and obtaines the IP address
 ***********************************************************************************************************************/
//uint8_t EthWifiStartDhcpClient (_USB_CONTROL *USBData)
//{
//   ULONG   WifiIpAddress = 0,
//           WifiMaskAddress = 0;
//   ULONG   NeededStatus = 0;
//   UINT    StatusRet = 0;
//
//   uint8_t retry_cnt = 0;
//
//   static _UDP_CONF ConfSitradUdp;
//   ReadPacketUdpCuston(&ConfSitradUdp);
//   CHAR NameForDhcp[sizeof ConfSitradUdp.ModelName];
//   strcpy(NameForDhcp, (CHAR*)ConfSitradUdp.ModelName);
//   ReplaceNonAlphanumericByDash(NameForDhcp);
//
//
//   if(USBData->ConfNetBasic.InterfaceType == WIFI)
//   {
//       mydhcp_Wifi(NameForDhcp);
//   }
//   else if(USBData->ConfNetBasic.InterfaceType == ETHERNET)
//   {
//       mydhcp_Eth(NameForDhcp);
//   }
//
//   if(NX_SUCCESS != nx_dhcp_stop(USBData->NxDHCP))
//   {
//       return 1U;
//   }
//
//   if(NX_SUCCESS != nx_dhcp_reinitialize(USBData->NxDHCP))
//   {
//       return 1U;
//   }
//
//   WifiMaskAddress = 0;
//   WifiIpAddress = 0;
//
//   if (NX_SUCCESS != nx_ip_interface_address_set(USBData->NxIP, 0, WifiIpAddress, WifiMaskAddress))
//   {
//       return 1U;
//   }
//
//   if(NX_SUCCESS != nx_ip_interface_status_check(USBData->NxIP, 0, NX_IP_LINK_ENABLED, &NeededStatus, TX_WAIT_FOREVER))
//   {
//       return 1U;
//   }
//
//   if(NX_SUCCESS != nx_dhcp_start(USBData->NxDHCP))
//   {
//       return 1U;
//   }
//
//   do{
//        StatusRet = nx_ip_status_check(USBData->NxIP, NX_IP_ADDRESS_RESOLVED, &NeededStatus, 200);
//        retry_cnt++;
//   }while(StatusRet && (retry_cnt < 5));
//
//   if(StatusRet)
//   {
//       return 1U;
//   }
//
//   return 0U;
//}


/*********************************************************************************************************************
 * @brief EthWifiStartStaticIP
 *
 * This function sets the IP mode to static mode
 ********************************************************************************************************************/
//uint8_t EthWifiStartStaticIP(NX_IP * NxIP, ULONG Address, ULONG Mask, ULONG Gtw, ULONG Dns)
//{
//    UINT status;
//    SSP_PARAMETER_NOT_USED(Dns);
//
//    status = nx_ip_address_set(NxIP, Address, Mask);
//    if (NX_SUCCESS != status) {
//        return 1U;
//    }
//
//    status = nx_ip_gateway_address_set(NxIP, Gtw);
//    if (NX_SUCCESS != status) {
//        return 1U;
//    }
//
//    return 0;
//}

//uint8_t EthWifiGetStatusIp(_USB_CONTROL *USBData)
//{
//    uint8_t MacAdressWiFi[6];
//    USBData->IpStruct.IpGateway = 0;
//    USBData->IpStruct.IpAddress = 0;
//    USBData->IpStruct.IpMask = 0;
//    /* retrieve the IP address */
//    if (NX_SUCCESS != nx_ip_interface_address_get(USBData->NxIP, 0, &USBData->IpStruct.IpAddress, &USBData->IpStruct.IpMask))
//        return 1U;
//
//    if(NX_SUCCESS != nx_ip_gateway_address_get(USBData->NxIP, &USBData->IpStruct.IpGateway))
//    {
//        if(!USBData->IpStruct.IpGateway)
//        {
//            USBData->IpStruct.IpGateway = USBData->IpStruct.IpAddress & USBData->IpStruct.IpMask;
//            USBData->IpStruct.IpGateway++;
//        }
//    }
//
//    if(NX_SUCCESS != nx_ip_interface_physical_address_get(USBData->NxIP, 0UL, &USBData->IpStruct.IpMacAdressMs, &USBData->IpStruct.IpMacAdressLs))
//    {
//        return 1U;
//    }
//
//    MacAdressWiFi[5] = (uint8_t)((USBData->IpStruct.IpMacAdressMs >>8)   & 0xFF);
//    MacAdressWiFi[4] = (uint8_t)((USBData->IpStruct.IpMacAdressMs)       & 0xFF);
//    MacAdressWiFi[3] = (uint8_t)((USBData->IpStruct.IpMacAdressLs>>24)   & 0xFF);
//    MacAdressWiFi[2] = (uint8_t)((USBData->IpStruct.IpMacAdressLs>>16)   & 0xFF);
//    MacAdressWiFi[1] = (uint8_t)((USBData->IpStruct.IpMacAdressLs>>8)    & 0xFF);
//    MacAdressWiFi[0] = (uint8_t)( USBData->IpStruct.IpMacAdressLs        & 0xFF);
//
//    SetMACValueWF(&MacAdressWiFi[0]); //Seta o MACAddress na RAM
//
//
//    if(USBData->IpStruct.IpGateway == USBData->IpStruct.IpAddress)
//    {
//        return 1U;
//    }
//    //seta a strutura do IP para a ram -> UdpOndemand
//    SetUdpIp(&USBData->IpStruct);
//
//    return 0U;
//}

//UINT NetStop (_USB_CONTROL *USBData)
//{
//    if(USBData->ConfNetBasic.AddrMode != MODE_STATIC)
//    {
//        nx_dhcp_stop(USBData->NxDHCP);
//        nx_dhcp_delete(USBData->NxDHCP);
//    }
//
//    memset(&g_dhcp_Wifi, 0, sizeof(g_dhcp_Wifi));
//    memset(&g_dhcp_Eth, 0, sizeof(g_dhcp_Eth));
//
//    nx_ip_delete(USBData->NxIP);
//
//    memset(&g_ip_wifi, 0, sizeof(g_ip_wifi));
//    memset(&g_ip_eth, 0, sizeof(g_ip_eth));
//
//    nx_packet_pool_delete(&g_packet_pool0);
//
//    memset(&g_packet_pool0, 0, sizeof(g_packet_pool0));
//
//    return NX_SUCCESS;
//}


/*********************************************************************************************************************
 * @brief  set_mac_address function
 *
 * Sets the unique Mac Address of the device using the FMI unique ID.
 ********************************************************************************************************************/
//void set_mac_address (nx_mac_address_t*_pMacAddress)
//{
//    fmi_unique_id_t id;
//    _FG_ERR Status;
//    ULONG lowerHalfMac;
//    uint8_t ZEepromMacAdress[6];
//
//    Status = GetMACValue(&ZEepromMacAdress[0]);
//
//    if(FG_ERROR == Status)
//    {
//        /* Read FMI unique ID */
//        g_fmi.p_api->uniqueIdGet(&id);
//
//        /* REA's Vendor MAC range: 00:30:55:xx:xx:xx */
//        lowerHalfMac = ((0x55000000) | (id.unique_id[0] & (0x00FFFFFF)));
//
//        /* Return the MAC address */
//        _pMacAddress->nx_mac_address_h=0x0030;
//        _pMacAddress->nx_mac_address_l=lowerHalfMac;
//    }
//    else
//    {
//        /* Return the MAC address */
//        _pMacAddress->nx_mac_address_h = (ULONG)(   ((ZEepromMacAdress[0]&(0x000000FF)) << 8) | (ZEepromMacAdress[1]&(0x000000FF)));
//        _pMacAddress->nx_mac_address_l = (ULONG)(   ((ZEepromMacAdress[2]&(0x000000FF)) << 24) | ((ZEepromMacAdress[3]&(0x000000FF)) << 16) |
//                                                    ((ZEepromMacAdress[4]&(0x000000FF)) << 8) | (ZEepromMacAdress[5]));
//    }
//}

//void mydhcp_Wifi(CHAR *name)
//{
//
//    UINT g_dhcp_Wifi_err;
//    /* Create DHCP client. */
//    g_dhcp_Wifi_err = nx_dhcp_create (&g_dhcp_Wifi, &g_ip_wifi, name);
//    if (NX_SUCCESS != g_dhcp_Wifi_err)
//    {
//        g_dhcp_Wifi_err_callback ((void *) &g_dhcp_Wifi, &g_dhcp_Wifi_err);
//    }
//
//#if DHCP_USR_OPT_ADD_ENABLE_g_dhcp_Wifi
//    /* Set callback function to add user options.  */
//    g_dhcp_Wifi_err = nx_dhcp_user_option_add_callback_set(&g_dhcp_Wifi,
//            dhcp_user_option_add_client_Wifi);
//
//    if (NX_SUCCESS != g_dhcp_Wifi_err)
//    {
//        g_dhcp_Wifi_err_callback((void *)&g_dhcp_Wifi,&g_dhcp_Wifi_err);
//    }
//#endif
//}

//void mydhcp_Eth(CHAR *name)
//{
//    UINT g_dhcp_Eth_err;
//    /* Create DHCP client. */
//    g_dhcp_Eth_err = nx_dhcp_create (&g_dhcp_Eth, &g_ip_eth, name);
//    if (NX_SUCCESS != g_dhcp_Eth_err)
//    {
//        g_dhcp_Eth_err_callback ((void *) &g_dhcp_Eth, &g_dhcp_Eth_err);
//    }
//
//#if DHCP_USR_OPT_ADD_ENABLE_g_dhcp_Eth
//    /* Set callback function to add user options.  */
//    g_dhcp_Eth_err = nx_dhcp_user_option_add_callback_set(&g_dhcp_Eth,
//            dhcp_user_option_add_client0);
//
//    if (NX_SUCCESS != g_dhcp_Eth_err)
//    {
//        g_dhcp_Eth_err_callback((void *)&g_dhcp_Eth,&g_dhcp_Eth_err);
//    }
//#endif
//}

void ReplaceNonAlphanumericByDash(CHAR *Str)
{
    CHAR *ptr = Str;
    while(*ptr)
    {
        if((!isalnum(*ptr)) && (*ptr != '-') && (*ptr != '.') )
        {
            *ptr = '-';
        }
        ptr++;
    }

}

//void EthWifiCheckConnectivity(_USB_CONTROL *EthWifiControl, _STATUS_FLAGS *StatusFlagsX)
//{
//    static ULONG LastCheck = 0;
//    static bool Sent = false;
//    static bool IpIsValid = false; // controle para ver se o ip foi validado ao menos uma vez. Se essa flag esta, nao pode gerar erro de ip por aqui
//
//    ULONG Now = tx_time_get();
//
//    if(EthWifiControl->EthWifiStatesMachine == EW_SM_DONE)
//    {
//        // limpa controle
//        Sent = false;
//        IpIsValid = false;
//        if(EthWifiControl->ConfNetBasic.AddrMode == MODE_STATIC)
//        {
//            LastCheck = Now - ARP_RENEWAL_PERIOD;
//        }
//        else
//        {
//            LastCheck = Now;
//        }
//        return;
//    }
//    if((Now - LastCheck) > ARP_RENEWAL_PERIOD)
//    {
//        LastCheck = Now;
//        Sent = true;
//        nx_arp_dynamic_entries_invalidate(EthWifiControl->NxIP);
//        nx_arp_dynamic_entry_set(EthWifiControl->NxIP, EthWifiControl->IpStruct.IpGateway, 0, 0);
//        nx_arp_gratuitous_send(EthWifiControl->NxIP, NULL);
//    }
//    else if(Sent && ((Now - LastCheck) > (ARP_CHECK_PERIOD)))
//    {
//        Sent = false;
//        ULONG msb, lsb;
//        if (NX_SUCCESS != nx_arp_hardware_address_find(EthWifiControl->NxIP, EthWifiControl->IpStruct.IpGateway, &msb, &lsb))
//        {
//            // no modo estatico, acusa erro de ip
//            if ((EthWifiControl->ConfNetBasic.AddrMode == MODE_STATIC) && (!IpIsValid)) // se ainda nao validou ip no modo estatico, assume erro de ip
//            {
//                StatusFlagsX->FlagErrorIp = true;
//            }
//
//#if RECORD_DATALOGGER_EVENTS
//            DataLoggerRegisterEvent(DATALOGGER_EVENT_IP_ERROR, EthWifiControl->EthWifiStatesMachine, EthWifiControl->ConfNetBasic.AddrMode);
//#endif
//
//            EthWifiControl->EthWifiStatesMachine = EW_SM_RESTART_IP;
//        }
//        else
//        {
//            StatusFlagsX->FlagErrorIp = false;//limpa erro de ip independente do caso
//            IpIsValid = true; // marca para que nao assumir mais erro de ip
//        }
//    }
//}


