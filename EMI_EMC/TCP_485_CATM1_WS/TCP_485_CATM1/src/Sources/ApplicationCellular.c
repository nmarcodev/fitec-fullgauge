///*
// * ApplicationCellular.c
// *
// *  Created on: 15 de mar de 2021
// *      Author: rhenanbezerra
// *
// *  Description  : This file contains network related interface functions.
// */
//
//
//#include "Includes/Cellular_Config.h"
//#include "Includes/internal_flash.h"
//#include "Includes/console_config.h"
//#include "Includes/ApplicationCellular.h"
//
//#define SF_CELLULAR_QCTLCATM1_INIT_DELAY                       (2000U)   ///< Modem initialization delay in millisecond
//
//#define RX_BUFFER_LEN 256
//
//extern net_input_cfg_t net_cfg;
//
//
//char rxBufferEXS[RX_BUFFER_LEN];
//uint8_t indexEXS = 0;
//uart_event_t uart_eventEXS = UART_EVENT_RX_CHAR;
//
///* These global variables are used within the context of Cellular Thread */
//NX_IP   *g_active_ip = NULL;                /* Pointer to active IP interface */
//extern sf_cellular_instance_t g_sf_cellular0;
//
///* Function Prototype declaration */
//UINT init_network_interface(net_input_cfg_t *net_cfg);
//UINT deinit_network_service(net_input_cfg_t net_cfg);
//UINT reset_network_interface(net_input_cfg_t net_cfg);
//
//
///*AT Commands*/
//const sf_cellular_at_cmd_set_t g_init_commands[]=
//{
//     /** AT Command: To check module init */
//     {
//         .p_cmd = (uint8_t *) "AT\r\n",
//         .p_success_resp = (uint8_t *) "OK",
//         .max_resp_length = SF_CELLULAR_STR_LEN_32,
//         .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
//         .retry = SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_RETRY_COUNT,
//         .retry_delay = SF_CELLULAR_DELAY_500MS
//     },
////     {
////         .p_cmd = (uint8_t *) "at^scfg?\r\n",
////         .p_success_resp = (uint8_t *) "OK",
////         .max_resp_length = SF_CELLULAR_STR_LEN_32,
////         .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
////         .retry = (uint8_t) SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_RETRY_COUNT,
////         .retry_delay = (uint16_t) SF_CELLULAR_DELAY_500MS
////     },
////	 {
////		  .p_cmd = (uint8_t *) "at+cops=0\r\n", //"AT+COPS=0\r\n"
////		  .p_success_resp = (uint8_t *) "OK",
////		  .max_resp_length = SF_CELLULAR_STR_LEN_32,
////		  .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_WAITTIME_5000MS,
////		  .retry = SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_COPS_AUTO_RETRY_COUNT,
////		  .retry_delay = SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_COPS_AUTO_RETRY_DELAY
////	 },
//	 {
//		  .p_cmd = (uint8_t *) "AT+CGDCONT=1,\"IPV4V6\",\"timbrasil.br\"\r\n",
//		  .p_success_resp = (uint8_t *) "OK",
//		  .max_resp_length = SF_CELLULAR_STR_LEN_32,
//		  .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
//		  .retry = SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_RETRY_COUNT,
//		  .retry_delay = SF_CELLULAR_DELAY_500MS
//	 },
//     {
//    	 //Attach Rede
//         .p_cmd = (uint8_t *) "AT+CGATT=1\r\n",
//         .p_success_resp = (uint8_t *) "OK",
//         .max_resp_length = SF_CELLULAR_STR_LEN_32,
//         .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
//         .retry = SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_RETRY_COUNT,
//         .retry_delay = SF_CELLULAR_DELAY_500MS
//     },
//     {
//    	 //Habilita Serviço Internet
//         .p_cmd = (uint8_t *) "AT^SICA=1,1\r\n",
//         .p_success_resp = (uint8_t *) "OK",
//         .max_resp_length = SF_CELLULAR_STR_LEN_32,
//         .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
//         .retry = SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_RETRY_COUNT,
//         .retry_delay = SF_CELLULAR_DELAY_500MS
//     },
//	 {
//		 //Habilita Serviço Internet
//		  .p_cmd = (uint8_t *) "AT+CGPADDR=1\r\n",
//		  .p_success_resp = (uint8_t *) "OK",
//		  .max_resp_length = SF_CELLULAR_STR_LEN_128,
//		  .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
//		  .retry = SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_RETRY_COUNT,
//		  .retry_delay = SF_CELLULAR_DELAY_500MS
//	  },
//     {
//    	 //Habilita Serviço Internet
//         .p_cmd = (uint8_t *) "AT^SISX=Ping,1,\"8.8.8.8\",5,5000\r\n",
//         .p_success_resp = (uint8_t *) "OK",
//         .max_resp_length = SF_CELLULAR_STR_LEN_512,
//         .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_WAITTIME_2000MS,
//         .retry = SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_RETRY_COUNT,
//         .retry_delay = SF_CELLULAR_DELAY_500MS
//     },
//     {
//         .p_cmd = NULL
//     },
//};
//
//
//UINT reset_network_interface(net_input_cfg_t net_cfg)
//{
//    UINT status = NX_SUCCESS;
//    //NX_IP_DRIVER    driver_request;
//
//    //TODO: rever escrita do código
//
//    if(SSP_SUCCESS != int_storage_read((uint8_t *)&net_cfg, sizeof(net_cfg), CONF_NET_BASIC))
//    {
//        printf("Failed to read network user input configuration from internal storage \r\n");
//        APP_ERR_TRAP(SSP_ERR_ABORTED);
//    }
//
//
//    status = nx_ip_address_set(g_active_ip, 0 , 0);
//    if(NX_SUCCESS != status)
//    {
//        return status;
//    }
//
//    status = nx_ip_interface_detach(g_active_ip, 0);
//    if(NX_SUCCESS != status)
//    {
//        return status;
//    }
//
//    return status;
//}
//
//
//UINT deinit_network_service(net_input_cfg_t net_cfg)
//{
//    UINT status = NX_SUCCESS;
//    char str[128] = {0};
//
//    printf("\r\nDe-initializing  N/W Interface... ");
//
//
//	UINT index = 0;
//	for(index = 0; index < NX_MAX_PHYSICAL_INTERFACES; index++)
//	{
//		if(strcmp(g_active_ip->nx_ip_interface[index].nx_interface_name, "Cellular") == 0)
//		{
//			status = nx_ip_interface_detach(g_active_ip, index);
//			if(NX_SUCCESS != status)
//			{
//				snprintf(str,sizeof(str),"nx_ip_interface_detach failed!!!, ret = 0x%x\r\n", status);
//				printf(str);
//				return status;
//			}
//
//			status = g_sf_cellular0.p_api->networkDisconnect(g_sf_cellular0.p_ctrl);
//			if(NX_SUCCESS != status)
//			{
//				snprintf(str, sizeof(str), "networkDisconnect failed!!!, ret = 0x%x\r\n", status);
//				printf(str);
//				return status;
//			}
//			break;
//		}
//	}
//	if(index >= NX_MAX_PHYSICAL_INTERFACES)
//	{
//		printf("Not able to find Cellular interface index!!!\r\n");
//		printf("Failed\r\n");
//	}
//
//	printf("Done\r\n");
//    return status;
//}
//
//
///* PPP Link Down Notification Callback
// * @param ppp_ptr
// */
//void ppp_link_down_callback(NX_PPP * ppp_ptr)
//{
//    (void)ppp_ptr;
//    printf ("PPP Link is Down\r\n");
//    tx_event_flags_set(&g_cellular_event_flags, PPP_LINK_DOWN, TX_OR);
//}
//
///**
// * PPP Link UP Notification callback
// * @param ppp_ptr
// */
//void ppp_link_up_callback(NX_PPP * ppp_ptr)
//{
//    (void)ppp_ptr;
//    printf ("PPP Link is UP\r\n");
//    tx_event_flags_set(&g_cellular_event_flags, PPP_LINK_UP, TX_OR);
//}
//
//
///**
// * ProvisioningSet Callback
// * @param p_args[in]    Pointer to Callback argument structure
// * @return
// *  SSP_SUCCESS                     Provisioning Set successful
// *  SSP_ERR_CELLULAR_FAILED         Provisioning Set Failed
// *  SSP_ERR_NOT_OPEN                Framework not open
// *  SSP_ERR_CELLULAR_INVALID_STATE  Data Mode is on
// */
//ssp_err_t celr_prov_callback(sf_cellular_callback_args_t * p_args)
//{
//    ssp_err_t ssp_err = SSP_ERR_CELLULAR_FAILED;
//    net_input_cfg_t net_cfg;
//    static uint8_t cell_init = 0;
//    char str[128] = {0};
//
//    if (p_args->event == SF_CELLULAR_EVENT_PROVISIONSET)
//    {
//        printf("Provisioning Cellular Modem: ");
//
//        ssp_err = int_storage_read((uint8_t *)(&net_cfg) , sizeof(net_cfg), CONF_NET_BASIC);
//        if (ssp_err != SSP_SUCCESS)
//        {
//            printf ("Failed to read cellular provisioning info from flash.\r\n");
//            printf ("Please reconfigure cellular connection.\r\n");
//            return ssp_err;
//        }
//
//        ssp_err = g_sf_cellular0.p_api->provisioningSet (g_sf_cellular0.p_ctrl, &net_cfg.cell_prov);
//        if (ssp_err != SSP_SUCCESS)
//        {
//            snprintf(str, sizeof(str), "Failed, ret=0x%x\r\n",ssp_err);
//
//            printf (str);
//        }
//        else
//        {
//            printf ("done\r\n");
//
//            if(cell_init == 0)
//            {
//                cell_init = 1;
//
//                printf("Initializing cellular connection:");
//
//                /* Initialize & Configure cellular module */
//                ssp_err_t result = SSP_SUCCESS;
//                const sf_cellular_at_cmd_set_t *init_at_cmds = g_init_commands;
//
//                /* Initialize the UART port of Cellular modem to send Cellular init commands to Thalles module */
////                result = g_uart_cell.p_api->open(g_uart_cell.p_ctrl, g_uart_cell.p_cfg);
////                if(result != SSP_SUCCESS && result != SSP_ERR_IN_USE)
////                {
////                    printf("Failed to initialize UART for cellular module!!!\r\n");
////                    return result;
////                }
//
//                result = cellular_is_module_init(init_at_cmds);
//                if(result != SSP_SUCCESS)
//                {
//                    printf("Cellular module Init Failed !!!! \r\n");
//                    return result;
//                }
//
//                /* Goto next AT command */
//                init_at_cmds++;
//
//                /* Delay for module to get registered */
//                sf_cellular_msec_delay(SF_CELLULAR_QCTLCATM1_INIT_DELAY);
//
//                /* send GPS AT command set to BG96 module */
//                //todo [rbdev] : mudar função para a nossa aplicação
//                //result = gps_at_commands_executioner(init_at_cmds);
//                if (SSP_SUCCESS == result)
//                {
//                    printf("done\r\n");
//                }
//                else
//                {
//                    printf("failed\r\n");
//                }
//            }
//        }
//    }
//
//    return ssp_err;
//}
//
//
//
//
///*********************************************************************************************************************
// * @brief  init_network_interface function
// *
// * This function initializes the network interface
// ********************************************************************************************************************/
//UINT init_network_interface(net_input_cfg_t *net_cfg)
//{
//    UINT   status = NX_SUCCESS;
//    ULONG  actual_events = 0;
//    ULONG  actual_status;
//    ULONG  dns_ip;
//    ULONG  ip_address;
//	ULONG  mask;
//	ssp_err_t result = SSP_SUCCESS;
//
//
//    if(NULL == net_cfg)
//	{
//		printf("\r\n NULL pointer in function init_network_interface\r\n");
//		return 1;
//	}
//
//    printf("\r\nInitializing Network Interface...\r\n");
//
//    result = g_sf_cellular0.p_api->open(g_sf_cellular0.p_ctrl, g_sf_cellular0.p_cfg);
//	if(result != SSP_SUCCESS)
//		printf("Failed!!!\r\n");
//	else
//	{
//		printf("done\r\n");
//	}
//
//	result = g_sf_cellular0.p_api->close(g_sf_cellular0.p_ctrl);
//	if(result != SSP_SUCCESS)
//		printf("Failed to close Cellular Module instance!!!!\r\n");
//	else
//	{
//		printf("done\r\n");
//	}
//
//
//	status = nx_ip_interface_attach(g_active_ip, "Cellular", IP_ADDRESS(0, 0, 0, 0), IP_ADDRESS(0, 0, 0, 0), g_sf_el_nx_cellular);
//	if((NX_SUCCESS != status) && (NX_UNHANDLED_COMMAND != status))
//	{
//		//err = SSP_ERR_INTERNAL;
//		//return err;
//		return status;
//	}
//
//	printf("Waiting for PPP link: ");
//
//	/* Wait for the PPP Link up event */
//	status = tx_event_flags_get(&g_cellular_event_flags, (PPP_LINK_UP | PPP_LINK_DOWN), TX_OR, &actual_events, PPP_LINK_TIMEOUT);
//	//if ((TX_SUCCESS != status) || ((actual_events & PPP_LINK_DOWN) == PPP_LINK_DOWN))
//	if (TX_SUCCESS != status)
//	{
// 		printf("PPP Link Event Flags failed!!!\r\n");
// 		//err = SSP_ERR_INTERNAL;
// 		//return err;
// 		return status;
//	}
//
//	// TODO: Check interface index value
////	status = nx_ip_interface_status_check(&g_ip0, 0, NX_IP_LINK_ENABLED, &actual_status, PPP_LINK_TIMEOUT);
////    //status = nx_ip_interface_status_check(&g_ip0, interface_index, NX_IP_LINK_ENABLED, &actual_status, PPP_LINK_TIMEOUT);
////	if (NX_SUCCESS != status)
////	{
////		printf("IP interface status check failed!!!\r\n");
////		//err = SSP_ERR_INTERNAL;
////		//return err;
////		return status;
////	}
//
//	dns_ip = 0;
//
//	// Get the DNS IP assigned during the IPCP handshake
//	status = nx_ppp_dns_address_get(&g_nx_ppp0, &dns_ip);
//	if ((NX_SUCCESS != status) || (0 == dns_ip))
//	{
//		dns_ip = IP_ADDRESS(8, 8, 8, 8);
//	}
//
////	if (0 != dns_ip)
////	{
////		// Add an IPv4 server address to the Client list.
////		status = nx_dns_server_add(&g_dns1, dns_ip);
////		if ((NX_SUCCESS != status) && (NX_DNS_DUPLICATE_ENTRY != status))
////		{
////			printf("nx_dns_server_add failed!!!\r\n");
////			//err = SSP_ERR_INTERNAL;
////			//return err;
////			return status;
////		}
////	}
//
//
////	dns_ip = 0;
////
////	/* Get the secondary DNS IP assigned during the IPCP handshake */
////	status = nx_ppp_secondary_dns_address_get(&g_nx_ppp0, &dns_ip);
////	if ((NX_SUCCESS != status) || (0 == dns_ip))
////	{
////		dns_ip = IP_ADDRESS(8, 8, 4, 4);
////	}
////
////	if (0 != dns_ip)
////	{
////		/* Add an IPv4 server address to the Client list. */
////		status = nx_dns_server_add(&g_dns0, dns_ip);
////		if ((NX_SUCCESS != status) && (NX_DNS_DUPLICATE_ENTRY != status))
////		{
////			printf("nx_dns_server_add failed!!!\r\n");
////	        //err = SSP_ERR_INTERNAL;
////	        //return err;
////			return status;
////		}
////	}
////
////	/* Print out the IP address if connected to a debugger */
////	status = nx_ip_interface_address_get(&g_ip0, 0, &ip_address, &mask);
////	if (NX_SUCCESS != status)
////	{
////		printf("IP interface address get failed!!!\r\n");
////	    //err = SSP_ERR_INTERNAL;
////	    //return err;
////		return status;
////	}
//
//	printf("Successfully setup Cellular interface\r\n");
//
//	/*printf("IP address = %d.%d.%d.%d on interface %d\r\n", (int)(ip_address>>24 & 0xFF),
//	                                                           (int)(ip_address>>16 & 0xFF),
//	                                                           (int)(ip_address>>8 &  0xFF),
//	                                                           (int)(ip_address>>0 &  0xFF),
//	                                                           interface_index);*/
//
//    return NX_SUCCESS;
//}
//
//
//UINT cellular_setup(net_input_cfg_t *netcfg)
//{
//	const sf_cellular_at_cmd_set_t *init_at_cmds = g_init_commands;
//	ssp_err_t ssp_err = SSP_ERR_CELLULAR_FAILED;
//	static uint8_t cellular_init = 0;
//
//	SSP_PARAMETER_NOT_USED(netcfg);
//
//
//	if(cellular_init == 0)
//	{
//		cellular_init = 1;
//
//		printf("Initializing Cellular");
//
//		/* Initialize & Configure the Cellular module */
//		ssp_err_t result = SSP_SUCCESS;
//
//		ssp_err = g_sf_comms_cell.p_api->open(g_sf_comms_cell.p_ctrl, g_sf_comms_cell.p_cfg);
//		if(SSP_SUCCESS != ssp_err && SSP_ERR_IN_USE != ssp_err)
//		{
//			printf("Failed to open g_sf_comms_cell instance!!!");
//
//		}
//		else
//		{
//			printf("\ndone g_sf_comms_cell\r\n");
//		}
//
//		/* Initialize the UART port of Cellular modem to send init commands to EXS82 module */
//		result = g_uart_cell.p_api->open(g_uart_cell.p_ctrl, g_uart_cell.p_cfg);
//		if(result != SSP_SUCCESS && result != SSP_ERR_IN_USE)
//		{
//			printf("Failed to initialize UART for cellular module!!!\r\n");
//			return result;
//		}
//		else
//		{
//			printf("UART cell starts!!!\r\n");
//		}
//
//		ssp_err = cellular_is_module_init(init_at_cmds);
//		if(ssp_err != SSP_SUCCESS)
//		{
//			printf("Cellular module Init Failed !!!! \r\n");
//			return ssp_err;
//		}
//		else
//		{
//			printf("Cellular module Init Success !!!! \r\n");
//		}
//
//		/* Goto next AT command */
//		init_at_cmds++;
//
//		/* Delay for module to get registered */
//		sf_cellular_msec_delay(SF_CELLULAR_QCTLCATM1_INIT_DELAY);
//
//		/* send init AT command set to EXS82 module */
//		ssp_err = cellular_at_commands_executioner(init_at_cmds);
//		if (SSP_SUCCESS == ssp_err)
//		{
//			printf("done\r\n");
//		}
//		else
//		{
//			printf("failed\r\n");
//		}
//
//		ssp_err = g_sf_comms_cell.p_api->close(g_sf_comms_cell.p_ctrl);
//		if(SSP_SUCCESS != ssp_err)
//		{
//			printf("Failed to close g_sf_comms_cell instance!!!");
//
//		}
//		else
//		{
//			printf("done\r\n");
//		}
//
//
//	}
//
//	return ssp_err;
//}
//
//
//ssp_err_t cellular_is_module_init(const sf_cellular_at_cmd_set_t *cellular_at_cmds)
//{
//    ssp_err_t result = SSP_SUCCESS;
//    sf_cellular_cmd_resp_t send_cmd, reci_cmd;
//    uint8_t rx_data[16];
//    uint8_t retry_cnt = 0;
//
//    /* Initialize send and receive buffers */
//    send_cmd.p_buff = (uint8_t*)cellular_at_cmds->p_cmd;
//    send_cmd.buff_len = (uint16_t)strlen((const char*)cellular_at_cmds->p_cmd);
//
//    reci_cmd.p_buff = rx_data;
//    reci_cmd.buff_len = sizeof(rx_data);
//
//    do {
//        /* Send AT command to EXS82 modem */
//        result = g_sf_comms_cell.p_api->write(g_sf_comms_cell.p_ctrl, send_cmd.p_buff, send_cmd.buff_len,SF_CELLULAR_SERIAL_READ_TIMEOUT_TICKS);
//        if(result == SSP_SUCCESS)
//        {
//            /* Receive AT command response from EXS82 modem */
//            result = g_sf_comms_cell.p_api->read(g_sf_comms_cell.p_ctrl,reci_cmd.p_buff,reci_cmd.buff_len,SF_CELLULAR_SERIAL_READ_TIMEOUT_TICKS);
//            if((result == SSP_SUCCESS) || (result == SSP_ERR_TIMEOUT))
//            {
//                /* Parse response of Modem */
//                if(sf_cellular_is_str_present((const char *)reci_cmd.p_buff,(const char *)(uint8_t*)cellular_at_cmds->p_success_resp) == SF_CELLULAR_TRUE)
//                {
//                    result = SSP_SUCCESS;
//                    break;
//                }
//            }
//            else
//            {
//                printf("Failed to read modem response !!! \r\n");
//            }
//        }
//        else
//        {
//        	printf("Failed to send AT command!!! \r\n");
//        }
//
//        /* Delay before next retry */
//        sf_cellular_msec_delay(cellular_at_cmds->retry_delay);
//
//        ++retry_cnt;
//        printf("\nretry_cnt: %d\n", retry_cnt);
//
//    }while(retry_cnt < cellular_at_cmds->retry);
//
//    if(result != SSP_SUCCESS)
//    	printf("Cellular AT command failed!!!\r\n");
//
//    return result;
//}
//
//ssp_err_t cellular_at_commands_executioner(const sf_cellular_at_cmd_set_t *cellular_cmds)
//{
//    ssp_err_t result = SSP_SUCCESS;
//    sf_cellular_cmd_resp_t send_cmd, reci_cmd;
//    uint8_t rx_data[256];
//    uint8_t retry_cnt = 0;
//
//    reci_cmd.p_buff = rx_data;
//    reci_cmd.buff_len = sizeof(rx_data);
//
//    while(cellular_cmds->p_cmd != NULL)
//    {
//        send_cmd.p_buff = (uint8_t*)cellular_cmds->p_cmd;
//        send_cmd.buff_len = (uint16_t)strlen((const char*)cellular_cmds->p_cmd);
//
//        do {
//            /* Send AT command to EXS82 modem */
//            result = g_sf_comms_cell.p_api->write(g_sf_comms_cell.p_ctrl, send_cmd.p_buff, send_cmd.buff_len,SF_CELLULAR_SERIAL_READ_TIMEOUT_TICKS);
//            if(result == SSP_SUCCESS)
//            {
//                /* Receive AT command response from BG96 modem */
//                result = g_sf_comms_cell.p_api->read(g_sf_comms_cell.p_ctrl,reci_cmd.p_buff,reci_cmd.buff_len,cellular_cmds->resp_wait_time);//SF_CELLULAR_SERIAL_READ_TIMEOUT_TICKS
//                if((result == SSP_SUCCESS) || (result == SSP_ERR_TIMEOUT))
//                {
//                    /* Parse response of Modem */
//                    if(sf_cellular_is_str_present((const char *)reci_cmd.p_buff,(const char *)(uint8_t*)cellular_cmds->p_success_resp) == SF_CELLULAR_TRUE)
//                    {
//                    	printf("\nComando: %s\n", send_cmd.p_buff);
//                    	printf("\nResposta: %s\n", reci_cmd.p_buff);
//                        result = SSP_SUCCESS;
//                        break;
//                    }
//                    else
//                    {
//                    	// TODO: Alterar
//                    	printf("\nComando: %s\n", send_cmd.p_buff);
//                    	printf("\nResposta: %s\n", reci_cmd.p_buff);
//                    	result = SSP_SUCCESS;
//						break;
//                    }
//                }
//                else
//                {
//                	printf("Failed to read modem response !!! \r\n");
//                }
//            }
//            else
//            {
//            	printf("Failed to send AT command!!! \r\n");
//            }
//
//            /* Delay before next retry */
//            sf_cellular_msec_delay(cellular_cmds->retry_delay);
//
//            ++retry_cnt;
//            printf("\nretry_cnt: %d\n", retry_cnt);
//        }while(retry_cnt < cellular_cmds->retry);
//
//        if(result != SSP_SUCCESS)
//        {
//        	printf("Cellular AT command failed!!!\r\n");
//            break;
//        }
//
//        /* Goto next command */
//        cellular_cmds++;
//        printf("\nGoto next command\n");
//
//        retry_cnt = 0;
//    }
//
//    return result;
//}
//
////void user_uart_callback(uart_callback_args_t *p_args)
////{
////    switch(p_args->event)
////    {
////        case UART_EVENT_RX_CHAR:
////            tx_queue_send(&g_cell_queue,&(p_args->data),TX_NO_WAIT);
////            break;
////        case UART_EVENT_ERR_PARITY:
////        case UART_EVENT_ERR_FRAMING:
////        case UART_EVENT_BREAK_DETECT:
////        case UART_EVENT_ERR_OVERFLOW:
////        case UART_EVENT_ERR_RXBUF_OVERFLOW:
////            break;
////        default:
////            break;
////    }
////}
//
//
//void user_uart_callback(uart_callback_args_t *p_args)
//{
//    __NOP();
//    if(p_args->event == UART_EVENT_RX_CHAR)
//    {
//        //piscaLed( LED_ST_B,  500U);
//        rxBufferEXS[indexEXS] = (char) p_args->data;
//        indexEXS++;
//    }
//
//    else if (p_args->event == UART_EVENT_TX_COMPLETE)
//    {
//        indexEXS = 0;
//        //uart_eventEXS = UART_EVENT_TX_COMPLETE;
//
//        if(strcmp (rxBufferEXS ,"\r\n^SYSSTART\r\n") != 0)
//        {
//            uart_eventEXS = UART_EVENT_TX_COMPLETE;
//        }
//    }
//}
//
//
//
//void user_rts_callback(uint32_t channel, uint32_t level)
//{
//    __NOP();
//    if(channel == g_uart_cell.p_cfg->channel)
//    {
//        switch(level)
//        {
//            case 0U:
//                g_ioport.p_api->pinWrite((ioport_port_pin_t)IOPORT_PORT_06_PIN_08, IOPORT_LEVEL_LOW);
//                break;
//            case 1U:
//                g_ioport.p_api->pinWrite((ioport_port_pin_t)IOPORT_PORT_06_PIN_08, IOPORT_LEVEL_HIGH);
//                break;
//            default:
//                break;
//        }
//    }
//}
//
