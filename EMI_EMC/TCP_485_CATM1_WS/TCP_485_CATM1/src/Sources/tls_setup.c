#include <Cellular_thread.h>
#include "Includes/tls_setup.h"
#include "Includes/certificates.h"
#include "Includes/io_adafruit_stuff.h"
#include "Includes/AmazonRootCA1.h"


//#ifdef USE_TLS
//
//#define NO_OF_REMOTE_CERTS          (4)
//
///* Typical certificates with RSA keys of 2048 bits
//using SHA-256 for signatures are in the range of 1000-2000 bytes. The
//buffer should be large enough to at least hold that size, but depending on
//the remote host certificates may be significantly smaller or larger. Note
//that if the buffer is too small to hold the incoming certificate, the TLS
//handshake will end with an error. */
//#define SIZE_OF_REMOTE_CERT_BUF     (2 * 1024)
//
///* nx_secure_tls_session_packet_buffer_set() associates a packet re-assembly buffer to a TLS session.
//In order to decrypt and parse incoming TLS records, the data in each record
//must be assembled from the underlying TCP packets. TLS records can be
//up to 16KB in size (though typically are much smaller) so may not fit into a
//single TCP packet.
//If you know the incoming message size will be smaller than the TLS
//record limit of 16KB, the buffer size can be made smaller, but in order to
//handle unknown incoming data the buffer should be made as large as
//possible. If an incoming record is larger than the supplied buffer, the TLS
//session will end with an error.*/
//#define SIZE_OF_TLS_PACKET_BUF      (16 * 1024)
//
//#if (NX_CRYPTO_MAX_RSA_MODULUS_SIZE == 4096)
//#ifdef NX_SECURE_ENABLE_PSK_CIPHERSUITES
//#define SIZE_OF_META_BUFFER         (12496) /* calculated with nx_secure_tls_metadata_size_calculate() for nx_crypto_tls_ciphers_synergys7, RSA Modulus 4096 */
//#else
//#define SIZE_OF_META_BUFFER         (12192) /* calculated with nx_secure_tls_metadata_size_calculate() for nx_crypto_tls_ciphers_synergys7, RSA Modulus 4096 */
//#endif
//#else
//#ifdef NX_SECURE_ENABLE_PSK_CIPHERSUITES
//#define SIZE_OF_META_BUFFER         (3200)  /* calculated with nx_secure_tls_metadata_size_calculate() for nx_crypto_tls_ciphers_synergys7, RSA Modulus 1024 */
//#else
//#define SIZE_OF_META_BUFFER         (2896)  /* calculated with nx_secure_tls_metadata_size_calculate() for nx_crypto_tls_ciphers_synergys7, RSA Modulus 1024 */
//#endif
//#endif
//
//#ifdef HAVE_TRUSTED_CERTIFICATE
//const TRUSTED_CERTIFCATE_INFO_STRUCT g_trust_certs[] =
//{
//    [0]
//        {
//         .pointer_to_cert = (UCHAR *)(AmazonRootCA1),
//         .sizeof_cert = sizeof(AmazonRootCA1),
//        },
//};
//
//#define NO_OF_TRUSTED_CERTS         sizeof(g_trust_certs)/sizeof(TRUSTED_CERTIFCATE_INFO_STRUCT)
//
//NX_SECURE_X509_CERT g_trusted_certificate[NO_OF_TRUSTED_CERTS];
//#endif
//
//NX_SECURE_X509_CERT g_remote_cert[NO_OF_REMOTE_CERTS];
//
//UCHAR               g_tls_packet_buffer[SIZE_OF_TLS_PACKET_BUF];
//UCHAR               g_remote_cert_buffer[NO_OF_REMOTE_CERTS][SIZE_OF_REMOTE_CERT_BUF];
//
//CHAR                g_tls_session_meta_data[SIZE_OF_META_BUFFER];
//
//extern const NX_SECURE_TLS_CRYPTO nx_crypto_tls_ciphers_synergys7;
//
//
//UINT tls_setup_callback(NX_WEB_HTTP_CLIENT *p_client_ptr, NX_SECURE_TLS_SESSION *p_tls_session)
//{
//    UINT ret = NX_SUCCESS;
//    UINT        status;
//    uint32_t    i;
//
//    SSP_PARAMETER_NOT_USED(p_client_ptr);
//
//    /* Create a TLS session */
//    memset(g_tls_session_meta_data, 0, sizeof(g_tls_session_meta_data));
//    status = nx_secure_tls_session_create (p_tls_session, &nx_crypto_tls_ciphers_synergys7 ,
//                                                       g_tls_session_meta_data,
//                                                       sizeof(g_tls_session_meta_data));
//    if (NX_SUCCESS != status)
//    {
//        return status;
//    }
//
//    /* Allocate space for packet re-assembly */
//    memset(g_tls_packet_buffer, 0, sizeof(g_tls_packet_buffer));
//    status = nx_secure_tls_session_packet_buffer_set(p_tls_session, g_tls_packet_buffer, sizeof(g_tls_packet_buffer));
//    if (NX_SUCCESS != status)
//    {
//        return status;
//    }
//
//    for( i = 0; i < NO_OF_TRUSTED_CERTS; i++)
//    {
//        /* Add a CA Certificate to our trusted store for verifying incoming server certificates. */
//        memset(&g_trusted_certificate[i], 0, sizeof(NX_SECURE_X509_CERT));
//        status = nx_secure_x509_certificate_initialize(&g_trusted_certificate[i],
//                                                       g_trust_certs[i].pointer_to_cert,
//                                                       g_trust_certs[i].sizeof_cert,
//                                                       NX_NULL,
//                                                       0,
//                                                       NX_NULL,
//                                                       0,
//                                                       NX_SECURE_X509_KEY_TYPE_NONE
//                                                       );
//        if (NX_SUCCESS != status)
//        {
//            return status;
//        }
//
//        status = nx_secure_tls_trusted_certificate_add(p_tls_session, &g_trusted_certificate[i]);
//        if (NX_SUCCESS != status)
//        {
//            return status;
//        }
//    }
//
//    for( i = 0; i < NO_OF_REMOTE_CERTS; i++)
//    {
//        /* Allocate buffers for in-coming certificates */
//        memset(&g_remote_cert_buffer[i][0], 0, SIZE_OF_REMOTE_CERT_BUF);
//        memset(&g_remote_cert[i], 0, sizeof(NX_SECURE_X509_CERT));
//        status = nx_secure_tls_remote_certificate_allocate(p_tls_session,
//                                                           &g_remote_cert[i],
//                                                           &g_remote_cert_buffer[i][0],
//                                                           SIZE_OF_REMOTE_CERT_BUF);
//        if (NX_SUCCESS != status)
//        {
//            return status;
//        }
//    }
//
//    return ret;
//}
//
//#endif /* #ifdef USE_TLS */

