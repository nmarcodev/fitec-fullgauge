/*
 * CrcModbus.c
 *
 *  Created on: 27 de mai de 2019
 *      Author: leonardo.ceolin
 */

#include "common_data.h" // include obrigatório
#include "Includes/CRCModbus.h"

//--------------------- FUNCTIONS ---------------------//
//uint16_t CrcModbusCalc(uint8_t *pu8LDataCrc, uint16_t NumBytes);
uint16_t CrcModbusCalc(uint8_t *pu8LDataCrc, uint16_t NumBytes, bool F_Firmware);
//----------------------------------------------------//

/*****************************************************************************************************/
/**
* @brief <b>Descricao:</b> Esta funcao calcula o CRC16 para um vetor de bytes
*
* @param pu8LDataCrc - Ponteiro para o inicio dos dados que serao udados para deterninar o CRC
* @param NumBytes - Tamanho do vetor de bytes contidos no buffer
* F_Firmware - Se setado, faz o CRC do dado recebido do configurador, inibe o retorno do FFFF
*
* @return uint16_t - 2 bytes como o valor do CRC16 ou 0xFFFF se todo o buffer estava apagado
*
*/
/*****************************************************************************************************/
uint16_t CrcModbusCalc(uint8_t *pu8LDataCrc, uint16_t NumBytes, bool F_Firmware)
{
    uint8_t     CrcL,
                CrcH,
                Right,
                Left,
                Byte,
                BufferVoid,
                *pu8LBufferCrc;
    uint16_t    Index,
                SizeBuffer;

    SizeBuffer = NumBytes;

    pu8LBufferCrc = pu8LDataCrc;
    CrcL = 0xFF;
    CrcH = 0xFF;
    BufferVoid = 0xFF;
    for (Index=0; Index < SizeBuffer; Index++)
    {
        Byte = *pu8LBufferCrc;
        BufferVoid = BufferVoid & Byte;                                //Verifica se é endereço não usado
        Right = (uint8_t)(Byte ^ CrcL);
        Right = (uint8_t)(Right ^ ((Right&0x7F)<<1));
        Right = (uint8_t)(Right ^ ((Right&0x3F)<<2));
        Left = (uint8_t)(Right ^ ((Right&0xF )<<4));
        Right = (uint8_t)(((Left & 3)<<6)|((Left&0xFC)>>2));
        CrcL = (uint8_t)(CrcH ^ (Right & 0xC0));
        CrcL = (uint8_t)(CrcL ^ ((Left & 0x80)>>7));
        CrcH = (uint8_t)(Left ^ (Right & 0x3F));
        pu8LBufferCrc++;
    }

    if( (BufferVoid == 0xFF) && (!F_Firmware))                              //Se mtodo o buffer testado estava apagado
    {
        return 0xFFFF;                                                      //Retorna informação de endereço não usado
    }
    else
    {
        return((uint16_t)(CrcH*256+CrcL));
    }
}
