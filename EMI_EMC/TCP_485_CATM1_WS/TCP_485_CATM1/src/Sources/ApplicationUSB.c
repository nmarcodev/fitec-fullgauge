/*
 * ApplicationUSB.c
 *
 *  Created on: 27 de mai de 2021
 *      Author: rhenanbezerra
 */

#include "usb_hid_device_thread.h"
#include "Includes/ApplicationUSB.h"
#include "Includes/ApplicationCommonModule.h"
#include "Includes/TypeDefs.h"
#include "ux_api.h"
#include "Includes/FlashQSPI.h"
#include "common_data.h"
#include "Includes/Define_IOs.h"
#include "Includes/ExternalFlash.h"
#include "stdio.h"
#include "Includes/CRCModbus.h"
#include "Includes/Thread_Monitor_entry.h"
#include "Includes/Thread_Datalogger_entry.h"
#include "Includes/MainManagerThreadEntry.h"
#include "Includes/RX_8571LC.h"
#include "Includes/Serial_thread_entry.h"
#include "Includes/USBTypes.h"
#include "Includes/ApplicationFlash.h"
#include "Includes/common_util.h"

#define HANDLE_FLASH_ERRORS (0)
extern _USB_CONTROL USBControl;

TX_THREAD   USB_Thread;
uint32_t    Rx_Qtde;
uint32_t    Rx_Lim;
uint32_t    Rx_Cnt;
uint16_t    SizeRxFrame;

#if FIRMWARE_VERSION == 99
const uint8_t aes_key2[]   = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F};
#else
#error no firmware version defined!
#endif
const uint8_t IVc2[]   = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

_USB_RETURN AppPacketUSB(_USB_CONTROL *USBData);
int cont = 2;
int j = 0;
uint16_t AESEnc(uint8_t * PMessage, uint8_t *EncMessage, uint16_t InputOctets);
uint16_t AESDec(uint8_t * PMessage, uint8_t *DecMessage, uint16_t InputOctets);

uint16_t ProcessPayloadUSB(_USB_CONTROL *USBData);
uint16_t GetConfSitrad(uint8_t id, uint8_t * GetConfData, _USB_CONTROL *USBData, bool FlagReadConf);
void     SetConfSitrad(uint8_t * SetConfData, _USB_CONTROL *USBData, uint16_t SizeRequest);

uint16_t RequesitaInfoConf(_USB_CONTROL *USBData, uint16_t RandomNumber);
uint16_t DownloadFirmware(_USB_CONTROL *USBData, uint16_t RandomNumber);
uint16_t AtualizaInfoConf(_USB_CONTROL *USBData, uint16_t RandomNumber);
uint16_t UpdadeDateHour(_USB_CONTROL *USBData, uint16_t RandomNumber);
uint16_t ReadPgDatalogger(_USB_CONTROL *USBData, uint16_t RandomNumber);
uint16_t GetStatus(_USB_CONTROL *USBData, uint16_t RandomNumber);
uint16_t AnswerDefault(_USB_CONTROL *USBData, uint16_t RandomNumber);
uint16_t FactoryTest(_USB_CONTROL *USBData, uint16_t RandomNumber);
uint16_t LoginDataResponse(_USB_CONTROL *USBData, uint16_t RandomNumber);

bool     TestQSPI(void);
//bool     TestSerial485(uint8_t AddressInst485);
uint8_t  TestStatusRssid(uint8_t Rssid_In);

void ResetRxBuffer(void);
void LoadRxBuffer(uint8_t *BufferInput, _USB_CONTROL *USBData, uint16_t Len);
void ProcessRxBuffer( _USB_CONTROL *USBData);
void ClearRxBuffer(_USB_CONTROL *USBData);
void ClearTxBuffer(_USB_CONTROL *USBData);

void ReadPacketUSBCuston(_USB_CONF *ConfSitradUSB);
#define HID_FLAG ((ULONG)0x0001)

UINT ux_hid_device_callback(UX_SLAVE_CLASS_HID* hid, UX_SLAVE_CLASS_HID_EVENT* hid_event)
{
    /* Does nothing. */
//    SSP_PARAMETER_NOT_USED(hid);



    //printf("Pacote recebido\n");

    memcpy(&USBControl.BufferUSB.HeaderPayload[0],&hid_event->ux_device_class_hid_event_buffer[0], SIZE_HEADER_USB + SIZE_PAYLOAD_USB);
    memcpy(&USBControl.BufferUSB.HP.Header[0],&hid_event->ux_device_class_hid_event_buffer[0], SIZE_HEADER_USB);
    memcpy(&USBControl.BufferUSB.HP.Payload[0],&hid_event->ux_device_class_hid_event_buffer[3], SIZE_PAYLOAD_USB);

    AppPacketUSB(&USBControl);

    return UX_SUCCESS;
}

/**
 * Trata um pacote USB recebido.
 */
_USB_RETURN AppPacketUSB(_USB_CONTROL *USBData)
{
	_BUFFER_USB     *BufferUSB;
	ConvBytesToInt  CmdSize;
	uint8_t         BufferRxUSBSitradOut[SIZE_PAYLOAD_USB]; ///> Buffer de envio
	uint16_t        PayloadLength;

	SizeRxFrame = 0;

	BufferUSB = &USBData->BufferUSB;

	 //Comando inválido
	if(BufferUSB->HP.Header[0] < 0x21 || BufferUSB->HP.Header[0] > 0x29)
	{
		//printf("\nCOMANDO INVALIDO: %d\n", BufferUSB->HP.Header[0]);
		return USB_RELISTEN;
	}

	CmdSize.Bytes[0] = BufferUSB->HP.Header[2];
	CmdSize.Bytes[1] = BufferUSB->HP.Header[1];
	PayloadLength    = CmdSize.Word16;
	if(!PayloadLength) //se o payload tem tamanho zero
	{
		return USB_RELISTEN;
	}
//	for(int i = 0; i < 16; i++) {
//	    	if(i == 0|| i == 16)printf("\n %d",BufferUSB->HP.Payload[i]);
//	    	else printf(" %d",BufferUSB->HP.Payload[i]);
//	    }


	// MODO DEV/TEST DESCOMENTAR LINHAS ABAIXO
//	AESEnc(&BufferUSB->HP.Payload[0], &BufferUSB->HP.Payload[0], PayloadLength);
//	printf("\nCRIPT\n");
//	for(int i = 0; i < 16; i++)
//	{
//	    	if(i == 0|| i == 16)printf("\n %d",BufferUSB->HP.Payload[i]);
//	    	else printf(" %d",BufferUSB->HP.Payload[i]);
//	}

	// Todos os pacotes sao criptografados com uma chave AES
	memset(&BufferRxUSBSitradOut[0], 0, sizeof(BufferRxUSBSitradOut));
	SizeRxFrame = AESDec(&BufferUSB->HP.Payload[0], &BufferRxUSBSitradOut[0], PayloadLength);
//	printf("\nHEADER: %d %d %d %d",BufferUSB->HP.Header[0],BufferUSB->HP.Header[1],BufferUSB->HP.Header[2],BufferUSB->HP.Header[3]);
//	printf("\nPAYLOAD:");
//	for(int i = 0; i < 16; i++) {
//	    	if(i == 0|| i == 16)printf("\n %d",BufferRxUSBSitradOut[i]);
//	    	else printf(" %d",BufferRxUSBSitradOut[i]);
//	    }
	if(!SizeRxFrame)
	{
		return USB_RELISTEN;
	}

	switch(BufferUSB->HP.Header[0])
	{
	   // Quando toda a informação chega em um ínico pacote
	   case CNV_TXPACKT:
		   if (!Rx_Lim )//|| (BufferRxUSBSitradOut[1]!=4 && BufferRxUSBSitradOut[1]!=5 &&BufferRxUSBSitradOut[1]!=0x0b))//1,2,3,6,7,9,0a,0c
		   {
			   LoadRxBuffer(&BufferRxUSBSitradOut[0], USBData,PayloadLength);
			   ProcessRxBuffer(USBData);
			   ResetRxBuffer();
			   return USB_SEND;
		   }
	   break;

	   // Quando a informação chega em vários pacotes
	   //CNV_INIT_TXBUFF: Pacote com início do payload
	   //CNV_BDY_TXBUFF: Pacote com as informações intermediarias do payload
	   //CNV_END_TXBUFF: Pacote com o final do payload
	   case CNV_INIT_TXBUFF:
		   if (!Rx_Lim)
		   {
		       LoadRxBuffer(&BufferRxUSBSitradOut[0], USBData,PayloadLength);
			   ResetRxBuffer();
		       return USB_BUFFERING;
		   }
	   break;

	   case CNV_BDY_TXBUFF:
		   if (!Rx_Lim)
		   {
			   LoadRxBuffer(&BufferRxUSBSitradOut[0], USBData,PayloadLength);
			   return USB_BUFFERING;
		   }
	   break;

	   case CNV_END_TXBUFF:
        if (!Rx_Lim)
        {
            LoadRxBuffer(&BufferRxUSBSitradOut[0], USBData,PayloadLength);
            ProcessRxBuffer(USBData);
            ResetRxBuffer();
            return USB_SEND;
        }
	   break;

	   //Implementação futura, se necessário
	   //case CNV_RXBUFF:
	   //break;

	   //case CNV_FLUSH_USBBUFF:
	   //break;

	   //case CNV_RD_RXSIZE:
	   //break;
	}

	return USB_SEND;
}

void ProcessRxBuffer( _USB_CONTROL *USBData)
{
	uint16_t        PayloadLengthBuffer;

	Rx_Lim = Rx_Qtde;
	Rx_Cnt = 0;
	 uint8_t ret = -1;

	uint8_t cnt = 0;

	PayloadLengthBuffer = ProcessPayloadUSB(USBData);

	if (( PayloadLengthBuffer % 16U ) != 0 )
	{
		uint8_t tmp;
		tmp = (uint8_t)((uint8_t)(PayloadLengthBuffer / 16U) + (uint8_t)1U);
		PayloadLengthBuffer = (uint8_t)(tmp*16U);
	}

	if(PayloadLengthBuffer <= SIZE_PAYLOAD_USB) // Payload len zero indica que nenhuma resposta deve ser enviada.
	{
		memset(&USBData->BufferUSB.HeaderPayload[0], 0, sizeof(USBData->BufferUSB.HeaderPayload));

		USBData->BufferUSB.HeaderPayload[cnt++] = CNV_TXPACKT_OK;
		USBData->BufferUSB.HeaderPayload[cnt++] =  (uint8_t)((PayloadLengthBuffer >>  8) & 0xFF);//(uint8_t)(PayloadLengthBuffer >> 8);
		USBData->BufferUSB.HeaderPayload[cnt++] =  (uint8_t)((PayloadLengthBuffer >>  0) & 0xFF);//(uint8_t)(PayloadLengthBuffer^0xFF);

//		printf("\nDADOS_PACOTE(SEM CRIPT): ");
//		for(j = 0; j< PayloadLengthBuffer; j++){
//			printf("%x ",USBData->Buffer_Tx[j]);
//		}
//		j = 0;

		// Todos os pacotes sao criptografados com uma chave AES
		SizeRxFrame = AESEnc(&USBData->Buffer_Tx[0], &USBData->BufferUSB.HeaderPayload[cnt], PayloadLengthBuffer);
		ret = SendPacketUSB(USBData,(uint16_t)(SizeRxFrame+3U));
//			printf("\nDADOS_PACOTE(CRIPT): ");
//			for(j = 0; j< PayloadLengthBuffer; j++){
//				printf("%x ",USBData->BufferUSB.HeaderPayload[j]);
//			}
//			j = 0;


			//		printf("USB_TX_DATA: %d %d %d %d %d %d %d %d\n", USBData->Buffer_Tx[0],USBData->Buffer_Tx[1],USBData->Buffer_Tx[2],USBData->Buffer_Tx[3],USBData->Buffer_Tx[4],USBData->Buffer_Tx[5],USBData->Buffer_Tx[6],USBData->Buffer_Tx[7]);
//		printf("USB_BUFF_DATA: %d %d %d %d %d %d %d %d", USBData->BufferUSB.HeaderPayload[0],USBData->BufferUSB.HeaderPayload[1],USBData->BufferUSB.HeaderPayload[2],USBData->BufferUSB.HeaderPayload[3],USBData->BufferUSB.HeaderPayload[4],USBData->BufferUSB.HeaderPayload[5],USBData->BufferUSB.HeaderPayload[6],USBData->BufferUSB.HeaderPayload[7]);
				//printf("\nSEND_PKT_USB: %d", ret);
	}
	else if(PayloadLengthBuffer > SIZE_PAYLOAD_USB)
	{
		uint16_t NumSendPackets = 0,
				 num = 0,
			     pos = 0; // Posicão do payload no USBControl.Buffer_Tx

		uint8_t  BufferEncTx[PayloadLengthBuffer]; // Buffer de envio encriptado

		memset(&BufferEncTx[0], 0, sizeof(BufferEncTx));
		//Encripta o payload de envio
		SizeRxFrame = AESEnc(&USBData->Buffer_Tx[0], &BufferEncTx[0], PayloadLengthBuffer);

		num = SizeRxFrame % SIZE_PAYLOAD_USB;

		if (num!= 0)
		{
			NumSendPackets = (uint16_t)((uint16_t)(SizeRxFrame/SIZE_PAYLOAD_USB)+ (uint16_t)1U);
		}
		else
		{
			NumSendPackets = (uint16_t)(SizeRxFrame/SIZE_PAYLOAD_USB);
		}

		for(uint16_t i=1; i<=NumSendPackets; i++)
		{
			cnt = 0;

			if(i==1)
			{
				USBData->BufferUSB.HeaderPayload[cnt++] = CNV_INIT_TXBUFF_OK;


				USBData->BufferUSB.HeaderPayload[cnt++] = (uint8_t)((SIZE_PAYLOAD_USB >>  8) & 0xFF);//(uint8_t)(SIZE_PAYLOAD_USB >> 8);
				USBData->BufferUSB.HeaderPayload[cnt++] = (uint8_t)((SIZE_PAYLOAD_USB >>  0) & 0xFF);//(uint8_t)(SIZE_PAYLOAD_USB^0xFF);
			    memcpy(&USBControl.BufferUSB.HeaderPayload[cnt], &BufferEncTx[0], SIZE_PAYLOAD_USB);
			    pos = SIZE_PAYLOAD_USB;
//			    printf("BUFFER_TX_ENC: %d %d %d %d %d %d %d %d %d %d",BufferEncTx[0],BufferEncTx[1],BufferEncTx[2],BufferEncTx[3],BufferEncTx[4],BufferEncTx[5],BufferEncTx[6],BufferEncTx[7],BufferEncTx[8],BufferEncTx[9]);

			    ret = SendPacketUSB(USBData,(uint16_t)(SIZE_HEADER_USB + SIZE_PAYLOAD_USB));
			    //printf("SEND_FIRST_PKT: %d", ret);
			}
			else if (i == NumSendPackets)
			{
				memset(&USBData->BufferUSB.HeaderPayload[0], 0, SIZE_HEADER_USB + SIZE_PAYLOAD_USB);
				USBData->BufferUSB.HeaderPayload[cnt++] = CNV_END_TXBUFF_OK;
				USBData->BufferUSB.HeaderPayload[cnt++] = (uint8_t)(num >> 8);
				USBData->BufferUSB.HeaderPayload[cnt++] = (uint8_t)(num^0xFF);
				memcpy(&USBControl.BufferUSB.HeaderPayload[cnt], &BufferEncTx[pos], SIZE_PAYLOAD_USB);

				ret = SendPacketUSB(USBData,num);
//				printf("\nDADOS_PACOTE(CRIPT): ");
//				for( j = 0; j< PayloadLengthBuffer; j++){
//					printf("%x ",USBData->BufferUSB.HeaderPayload[j]);
//				}
//
//				j = 0;
				//printf("SEND_LAST_PKT: %d", ret);
			}
			else
			{
				USBData->BufferUSB.HeaderPayload[cnt++] = CNV_BDY_TXBUFF_OK;
				USBData->BufferUSB.HeaderPayload[cnt++] = (uint8_t)((SIZE_PAYLOAD_USB >>  8) & 0xFF);//(uint8_t)(SIZE_PAYLOAD_USB >> 8);
				USBData->BufferUSB.HeaderPayload[cnt++] = (uint8_t)((SIZE_PAYLOAD_USB >>  0) & 0xFF);//(uint8_t)(SIZE_PAYLOAD_USB^0xFF);
				memcpy(&USBControl.BufferUSB.HeaderPayload[cnt], &BufferEncTx[pos], SIZE_PAYLOAD_USB);
				pos = (uint16_t)(pos + SIZE_PAYLOAD_USB);

				ret = SendPacketUSB(USBData,(uint16_t)(SIZE_HEADER_USB + SIZE_PAYLOAD_USB));
				//printf("SEND_%d_PKT: %d",i, ret);
			}
//		    printf("BUFFER_TX_ENC: %d %d %d %d %d %d %d %d %d %d",BufferEncTx[0],BufferEncTx[1],BufferEncTx[2],BufferEncTx[3],BufferEncTx[4],BufferEncTx[5],BufferEncTx[6],BufferEncTx[7],BufferEncTx[8],BufferEncTx[9]);

		}

	}

}


uint16_t ProcessPayloadUSB(_USB_CONTROL *USBData)
{
	#define TypeCom 2
    int RNumber = rand();
    uint16_t Size = 0;
    //printf("\nCOMANDO: %d\n",USBData->Buffer_Rx[TypeCom]);
	switch(USBData->Buffer_Rx[TypeCom])
	{

		case CMD_LOGIN_CONVERSOR:
			printf("\nLOGIN\n");
			Size = LoginDataResponse(USBData, (uint16_t)RNumber);
			break;

		case CMD_REINICIA_CONVERSOR: // REINICIA O CONVERSOR (REBOOT)
			printf("\nREINICIA_CONVERSOR\n");
			Size = AnswerDefault(USBData, (uint16_t)RNumber);
			//USBData->TimerRestartModule = 3; //INICIALIZA A VARIAVEL QUE VAI RESETAR O CONVERSOR
			USBControl.USBStatesMachine = SM_RESTART_DEVICE;
			break;

		case CMD_CONF_FABRICA:
			printf("\nCONF_FABRICA\n");
			Size = AnswerDefault(USBData, (uint16_t)RNumber);
			//USBData->TimerRestoreFactory = 3; //Carrega contador para Restaurar o padrao de fabrica
			USBControl.USBStatesMachine = SM_RESTORE_FACTORY_DEVICE;
			break;

		case CMD_ATUALIZA_CONF:
			printf("\nATT_CONF\n");
			Size = AtualizaInfoConf(USBData, (uint16_t)RNumber);
			break;

		case CMD_CMD_REQ_CONF:
			printf("\nREQ_CONF\n");
			Size = RequesitaInfoConf(USBData, (uint16_t)RNumber);
			break;

		case CMD_DESC_CONVERSOR:
			printf("\nDESC_CONVERSOR\n");
			Size = AnswerDefault(USBData, (uint16_t)RNumber);
		    tx_event_flags_set (&USB_Flags, FLAG_USB_DISCONNECTED, TX_OR);
			break;

		case CMD_LIMPAR_DATALOGGER:
			printf("\nLIMPAR_DATALOGGER\n");
			//tx_event_flags_set (&DataloggerEventFlags, DL_FLAG_CLEAR, TX_OR); // Seta a flag para a thread datalogger apagar o registro de logs.
			USBControl.USBStatesMachine = SM_CLEAN_DATALLOGER;
			Size = AnswerDefault(USBData, (uint16_t)RNumber);
			break;

		case CMD_LEITURA_LOG:
			printf("\nLEITURA_LOG\n");
			Size = ReadPgDatalogger(USBData, (uint16_t)RNumber);
			break;

		case CMD_AJUSTE_DATA_HORA:
			printf("\nAJUSTA_DATA_HORA\n");
			Size = UpdadeDateHour(USBData, (uint16_t)RNumber);
			//seta o flag primeiro.
			USBControl.USBStatesMachine = SM_UPDATE_DATE_TIME;
			//tx_event_flags_set (&USB_Flags, FLAG_RTC_SEND, TX_OR); // Seta a flag para a thread do RTC ajustar o horario
			break;

		case CMD_STATUS_CONVERSOR:
			printf("\nSTATUS_CONVERSOR\n");
			Size = GetStatus(USBData, (uint16_t)RNumber);
			break;

		case CMD_ATUALIZACAO_FW:
			printf("\nATUALIZAÇÃO_FW\n");
			Size = DownloadFirmware(USBData, (uint16_t)RNumber);
			break;

		case CMD_TESTE_FABRICA:
			printf("\nTESTE_FABRICA\n");
			Size = FactoryTest(USBData, (uint16_t)RNumber);
			break;

		default:
			USBData->Buffer_Tx[1] = 0;
			USBData->Buffer_Tx[2] = 0;
			Size = 0;
		    break;
	}

	return Size;
}

uint16_t ReadPgDatalogger(_USB_CONTROL *USBData, uint16_t RandomNumber)
{
    /* Teste ...  08 00 03 00 00 01 08 08 08 08 08 08 08 08*/
    /* Pagina(d39 = 0x27) 08 00 03 00 27 01 08 08 08 08 08 08 08 08
    * */
    //print_to_console("\n\r [DM] SITRAD TCP -> 0x08 - READ_PG_DATALOGGER!");
    //if(S25FL064ChipEraseFinished() == 0)
    // 0   1   2   3  4  5 6   7  ....
    //RDM RDL 08  00  03 x y   z  (X || Y): Pagina a Ler  z: Qntidade de paginas.
    uint16_t Size = 0;
    uint16_t Temp;

    Temp = (uint16_t)((USBData->Buffer_Rx[5] <<  8) | (USBData->Buffer_Rx[6]));         //captura o numero da pagina a ler
    uint8_t QtdPages = USBData->Buffer_Rx[7];

    //-------------------Payload-----------------------
    USBData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber >> 8   );
    USBData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber ^  0xFF);
    USBData->Buffer_Tx[Size++] = USBData->Buffer_Rx[TypeCom] | 0x80;
    USBData->Buffer_Tx[Size++] = 0x00;
    USBData->Buffer_Tx[Size++] = 0x00;

    if((QtdPages > MAX_PAGES_TO_READ) || (QtdPages < 1))
    {
#if HANDLE_FLASH_ERRORS
    	USBData->Buffer_Tx[2] = USBData->Buffer_Rx[TypeCom] | 0xA0;

#endif
        Size = 5; //tamanho do payload
        USBData->Buffer_Tx[3] = 0; //ok
        USBData->Buffer_Tx[4] = 0; //ok
    }
    else // tudo ok, pode ler as paginas
    {

        static _DATA_LOGGER_DATA DataLoggerData;
        DataLoggerData.OpCod = REQUEST_PAG; //Sinaliza o Cod da operacao
        DataLoggerData.QntPg = QtdPages; //numero de paginas que deve ser lida
        DataLoggerData.SizeTemp = Temp; //primeira pagina a ser lida
        DataLoggerData.Payload = &(USBData->Buffer_Tx[Size]);

        tx_queue_send(&Datalogger_Flash, &DataLoggerData, TX_WAIT_FOREVER); //envia para a thread Datalogger
        tx_thread_relinquish();//TODO tratar o retorno da funcao da flash datalogger
        tx_queue_receive(&USB_Flash, &DataLoggerData, TX_WAIT_FOREVER); //recebe da thread Datalogger
        //todo verificar variável EthWifi_Flash

#if HANDLE_FLASH_ERRORS
        if(!DataLoggerData.status)//erro de leitura da flash qspi
        {
        	USBData->Buffer_Tx[2] = USBData->Buffer_Rx[TypeCom] | 0xC0;

            Size = 8;
            USBData->Buffer_Tx[3] = 0; //ok
            USBData->Buffer_Tx[4] = 0; //ok
        }
        else
#endif
        {
            uint16_t u16LQtd;
            u16LQtd = (uint16_t)((uint16_t)(QtdPages*PAGE_SIZE) + (uint16_t)(CRC_SIZE));

            USBData->Buffer_Tx[3] = (uint8_t)((u16LQtd >>  8) & 0xFF); //ok
            USBData->Buffer_Tx[4] = (uint8_t)((u16LQtd >>  0) & 0xFF); //ok

            u16LQtd = (uint16_t)(u16LQtd + (uint16_t)(HEADER_SIZE));

            Size = (uint16_t)(u16LQtd - (uint16_t)CRC_SIZE);

            Temp = (uint16_t)(CrcModbusCalc((uint8_t*)&USBData->Buffer_Tx[2], Size, false));

            Size = (uint16_t)(Size + (uint16_t)5);

            USBData->Buffer_Tx[Size++] = (uint8_t)((Temp >>  8) & 0xFF);
            USBData->Buffer_Tx[Size++] = (uint8_t)((Temp >>  0) & 0xFF);
        }
    }

    return Size;
}


uint16_t DownloadFirmware(_USB_CONTROL *USBData, uint16_t RandomNumber)
{
    bool                FSend       = false;
    uint16_t            Size        = 0,
                        SizePayload = 0,
                        SizeFile    = 0,
                        CRC_Calc    = 0,
                        CompFile    = 0,
                        FragFile    = 0,
                        CRC_Recv    = 0;
    static uint32_t     AddressFirmware = 0;
    static uint16_t     ExpFragFile = 0;
    uint16_t QtdBytesToEndPage;
    USBData->TimerTimeOutOTA = 8;

    ThreadMonitor_SendKeepAlive(T_MONITOR_STOP_MONITORING); //para o monitorando

    USBData->FlagOTARun = true;
    UINT OldPrority;
    tx_thread_priority_change(&USB_Thread, 1, &OldPrority);

    SizePayload = (uint16_t)((USBData->Buffer_Rx[3] <<  8) | (USBData->Buffer_Rx[4])); //captura o tamanho do payload
    SizeFile    = (uint16_t)(SizePayload - 6);

    FragFile    = (uint16_t)((USBData->Buffer_Rx[5] <<  8) | (USBData->Buffer_Rx[6])); //captura o fragmento do arquivo
    CompFile    = (uint16_t)((USBData->Buffer_Rx[7] <<  8) | (USBData->Buffer_Rx[8])); //captura o numero de partes do arqvivo

    CRC_Calc = (uint16_t)(CrcModbusCalc((uint8_t*)&USBData->Buffer_Rx[9], SizeFile, true));
    CRC_Recv = (uint16_t)((USBData->Buffer_Rx[SizePayload+3] <<  8) | (USBData->Buffer_Rx[SizePayload+4]));

    if(!FragFile) //Sinaliza que vai comecar a enviar o firmware;
    {

#if RECORD_DATALOGGER_EVENTS
        DataLoggerRegisterEvent(DATALOGGER_EVENT_FOTA_STATUS_CHANGE, 0, true);
#endif

        _DATA_LOGGER_DATA DataLoggerData;
        DataLoggerData.OpCod = START_OTA; //Avisa a thread datalogger que uma atualizacao vai comecar.
        tx_queue_send(&Datalogger_Flash, &DataLoggerData, TX_WAIT_FOREVER); //envia para a thread Datalogger

        ExpFragFile = 1;
        AddressFirmware = 0;
        ClearSectorFlashQSPIFirm((uint16_t)AddressFirmware);
        if(!USBData->FlagConnectSitrad)
		{
			FSend = true;
		}
    }

    else if((CRC_Calc == CRC_Recv) && (FragFile <= CompFile))
    {
       if(FragFile != ExpFragFile)
       {
           g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_LOW);
           FSend = false;
       }
       else
       {
           g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_HIGH);

           USBData->FlashExternalPaylod->SizeData = (uint16_t)SizeFile;
           USBData->FlashExternalPaylod->FragFile = (uint16_t)FragFile;
           USBData->FlashExternalPaylod->CompFile = (uint16_t)CompFile;
           USBData->FlashExternalPaylod->DataFlash = &USBData->Buffer_Rx[9];
           QtdBytesToEndPage = (uint16_t)(PAGE_FLASH_SIZE - (AddressFirmware - 0));
           AddressFirmware = SaveFirmwareQSPI(USBData->FlashExternalPaylod, AddressFirmware);
//           FlashReadFirmwareData(AddressFirmware + QSPI_DEVICE_ADDRESS,
//           			                                QtdBytesToEndPage,
//           			                                USBData->FlashExternalPaylod);
           ExpFragFile ++;
           FSend = true;
       }

       if(CompFile == FragFile)
       {
           if(!FSend)
           {
               USBData->TimerTimeOutOTA = 3;
           }
           else
           {
               USBData->TimerOTACheckCRC = 2;
           }
       }
    }


    //------------------+ Payload +------------------------
    USBData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber   >>  8   );
    USBData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber   ^   0xFF);
    USBData->Buffer_Tx[Size++] = (uint8_t)USBData->Buffer_Tx[TypeCom] | 0x80;
    USBData->Buffer_Tx[Size++] = 0;//tamanho
    //BufferUSB->HeaderPayload[Size++] = 1;//tamanho todo corrigir protocolo
    if(FSend)
    {
    	USBData->Buffer_Tx[Size++] = 0;
        FSend = false;
    }
    else
    {
    	USBData->Buffer_Tx[Size++] = 1;
    	USBData->Buffer_Tx[Size++] = (uint8_t)ExpFragFile;
    }

    g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_HIGH);

    return Size;
}


uint16_t FactoryTest(_USB_CONTROL *USBData, uint16_t RandomNumber)
{
	bool     FSend = true;
	uint16_t Size = 0;

    //------------------+ Payload +------------------------
	USBData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber   >>  8   );
	USBData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber   ^   0xFF);
	USBData->Buffer_Tx[Size++] = (uint8_t)USBData->Buffer_Rx[TypeCom] | 0x80;
	USBData->Buffer_Tx[Size++] = 0x00;//06
	USBData->Buffer_Tx[Size++] = 0x02;//7
	USBData->Buffer_Tx[Size++] = USBData->Buffer_Rx[5]; //8 sub id
    Size++;

    // verifica se pode entrar em modo teste
    if((!USBData->FlagFAC))
    {
        return Size;
    }

    switch(USBData->Buffer_Rx[5])
    {
		case FAC_TEST_START:
		{
			USBData->FlagFAC = true;

            _DATA_LOGGER_DATA DataLoggerData;
            DataLoggerData.OpCod = TEST_START; //Avisa a thread datalogger que uma atualizacao vai comecar.
            tx_queue_send(&Datalogger_Flash, &DataLoggerData, TX_WAIT_FOREVER); //envia para a thread Datalogger

			ThreadMainManagerSetFlag(MM_FAC_MODE);

		}break;

        case FAC_TEST_SUPER_CAP_ON:
        {
            g_ioport.p_api->pinWrite(RTC_FAC_PIN,IOPORT_LEVEL_HIGH);

            tx_thread_sleep(TIME_200MS);

            InitI2C_RTC();

            tx_thread_sleep(TIME_200MS);

            // inicializa o RTC Externo
            RX8571LC_Init();

            // verifica estado de ECLO
            if(FG_SUCCESS != RX8571LC_VerifyEclo())
            {
                FSend = false; //foi mal.
            }

            ThreadMonitor_SendKeepAlive(T_MONITOR_START_MONITORING);
        }break;

        case FAC_TEST_SUPER_CAP_OFF:
        {
        	ThreadMonitor_SendKeepAlive(T_MONITOR_STOP_MONITORING);

            CloseI2C_RTC();

            g_ioport.p_api->pinWrite(RTC_FAC_PIN,IOPORT_LEVEL_LOW);

        }break;

        case FAC_TEST_QSPI:
        {
            FSend = TestQSPI(); //ret=true ok, ret false, nok

        }break;

        case FAC_TEST_LEDS:
        {
        	ThreadMainManagerSetLedColor(USBData->Buffer_Rx[6]);
            ThreadMainManagerSetFlag(MM_FAC_TEST_LEDs);

        }break;

        case FAC_TEST_485:
        {
            //FSend = TestSerial485(USBData->Buffer_Rx[6]);

        }break;

        case FAC_TEST_KEY:
        {
            FSend = ThreadMainManagerGetStatusButton();
        }break;

        case FAC_TEST_DO_FAC:
        {
        	_FLAGS_EEPROM FlagsEeprom;
        	FlagsEeprom.Bites.bCalibration = true;
        	SetValueFacEEPROM(&FlagsEeprom.Bytes);
            USBData->TimerRestartModule = 3; //INICIALIZA A VARIAVEL QUE VAI RESETAR O CONVERSOR
        }break;

        case FAC_TEST_END:
        {
        	ThreadMainManagerSetLedColor(12);
        	ThreadMainManagerSetFlag(MM_FAC_TEST_LEDs);
        }break;

		default:
			break;
	}

    if(FSend)
	{
    	USBData->Buffer_Tx[6] = 0;
	}
	else
	{
		USBData->Buffer_Tx[6] = 1;
	}

    return Size++;
}


//bool TestSerial485(uint8_t AddressInst485)
//{
//    ULONG SerialFlag = 0;
//    uint32_t AddressInst_485;
//    AddressInst_485 = AddressInst485;
//
//    tx_queue_send(&QueueTest485, &AddressInst_485, TX_WAIT_FOREVER);   //envia para a thread Serial
//    tx_thread_relinquish();
//
//    tx_event_flags_get(&EventFlagsSerialTestF, 0x01, TX_OR_CLEAR, &SerialFlag, TIME_2S);
//
//    return (bool)SerialFlag;
//}

bool TestQSPI(void)
{
    uint8_t Tve[BUFFER_LENGTH-1];
    uint8_t InTve[BUFFER_LENGTH];
    bool FlagTestMem = false;

    memset(&InTve[0], 0xFF, BUFFER_LENGTH);

    FlashReadFirmwareData(0, 1, &InTve[0]);
    for(uint8_t i=0; i < sizeof(Tve); i++)
    {
        if(InTve[i] == 0xFF)
        {
            FlagTestMem = true; //se a flash vazia
        }
    }

    if(FlagTestMem)
    {
        ClearSectorFlashQSPIFirm(0);
        memset(&InTve, 0, sizeof(InTve));

        for(uint8_t i=0; i < sizeof(Tve); i++)
        {
            Tve[i] = (uint8_t)rand();
        }

        FlashWritePageData(QSPI_DEVICE_ADDRESS, sizeof(Tve), &Tve[0]);
        FlashReadFirmwareData(0, 1, &InTve[0]);
        for(uint8_t i=0; i < sizeof(Tve); i++)
        {
            if(InTve[i] != Tve[i])
            {
                return false;
            }
        }
    }

    return true;
}


uint16_t AtualizaInfoConf(_USB_CONTROL *USBData, uint16_t RandomNumber)
{
    //0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28
    //32 92 04 00 18 26 0c 54 50 2d 4c 49 4e 4b 5f 44 36 38 32 27 08 38 30 31 30 31 30 36 32
    //print_to_console("\n\r [DM] SITRAD TCP -> 0x04 - ATUALIZ_INFO_CONF!");
    //0  1  2  3  4  5 6  7  8  9  10 11 12 13 14 15 ....
    //0d 20 04 00 fd 16 02 00 0f 17 f7 1e 20 21 22 00
	uint16_t Temp = 0;
	uint16_t Size = 0;

    //-------------------Payload-----------------------
	USBData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber    >>     8);     //RANDOM1_IND
	USBData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber    ^   0xFF);     //RANDOM2_IND
	USBData->Buffer_Tx[Size++] = USBData->Buffer_Rx[2] |    0x80;         //COMMAND_IND
	USBData->Buffer_Tx[Size++] = 0x00;          //SIZE
	USBData->Buffer_Tx[Size++] = 0x00;          //SIZE

	if(USBData->Buffer_Rx[4] == 0xFD) //compatibilidade da versao antiga
	{
		Temp = (uint16_t)((USBData->Buffer_Rx[7] <<  8) | (USBData->Buffer_Rx[8]));           //captura o tempo de log
		if(Temp < 15)//se for menor que 15 segundos, seta em 15s;
			Temp = 15;

		DataloggerFlashSetCmdSitrad(Temp, &USBData->Buffer_Rx[11], USBData->Buffer_Rx[10]);
	}
	else
	{
		uint16_t SizeRequest = (uint16_t)( USBData->Buffer_Rx[3] << 8 | USBData->Buffer_Rx[4]);
		SetConfSitrad(&USBData->Buffer_Rx[5], USBData, SizeRequest); //envia os parametros recebidos do configurador para a memoria flash.
	}

    return Size;
}


uint16_t RequesitaInfoConf(_USB_CONTROL *USBData, uint16_t RandomNumber)
{
    // exemplo 85 00 fd 16 02 00 1e 17 f7
    //A principio o sitrad deve enviar o range dos comandos que o conversor de responder.
    //Caso A: 0x00 0x00 = Respondo tudo, estilo UDP
    //Caso B: 0x01 0x05 = Respondo o intervalo, do 0x01 ao 0x05
    //Caso C: 0x03 0x03 = Respondo apenas o 0x03
	uint8_t cnt = 0;
    uint16_t Size = 0;

    bool     FlagReadConf = false;

    //-------------------Payload-----------------------
	USBData->Buffer_Tx[Size++]  = (uint8_t)(RandomNumber >> 8  );
	USBData->Buffer_Tx[Size++]  = (uint8_t)(RandomNumber ^ 0xFF);
	USBData->Buffer_Tx[Size++]  = USBData->Buffer_Rx[2] | 0x80;
	USBData->Buffer_Tx[Size++]  = 0x00;
	USBData->Buffer_Tx[Size++]  = 0x00;
    if((!USBData->Buffer_Rx[3]) && (!USBData->Buffer_Rx[4]))
    {
//    	printf("\nPRIMEIRO IF\n");
        //varre o intervalo
        uint16_t    TmpRet,
                    SizePayload = 0;

        for(uint8_t i = 0x16; i <= 0x17; i++)
        {
        	TmpRet = (uint16_t)GetConfSitrad(i, &USBData->Buffer_Tx[Size], USBData, FlagReadConf);
            FlagReadConf = true;
            if(TmpRet)
            {
                SizePayload = (uint16_t)(SizePayload + TmpRet);
                Size = (uint16_t)(TmpRet + Size);
            }
        }

        USBData->Buffer_Tx[3] = (uint8_t)(    ((SizePayload) >>  8) & 0xFF);
        USBData->Buffer_Tx[4] = (uint8_t)(    ((SizePayload) >>  0) & 0xFF);

        Size = (uint16_t)(SizePayload  + (uint16_t)5); //ajustar o payload

        //uint8_t TempSize = (uint8_t)(Size / 16);

        //TempSize++;

        //USBData->BufferUSB.HeaderPayload[2] = (uint8_t)(0x10 * TempSize);

        //Size = (uint16_t)(Size + (uint16_t)3);
    }
    else
    {
        //varre o intervalo
//    	printf("\nELSE\n");
        uint16_t    TmpRet,
                    SizePayload = 0,
                    SizeRequest;
        SizeRequest = (uint16_t)(USBData->Buffer_Rx[3]);

        if(!USBData->Buffer_Rx[5]) //verifica se eh range de IDs!
        {
            if(!USBData->Buffer_Rx[6] && !USBData->Buffer_Rx[7]) // 00 00 :> que todos os IDS
            {
            	USBData->Buffer_Rx[6] = ID_MODEL;    //Primeiro ID
            	USBData->Buffer_Rx[7] = ID_END;            //Ultimo ID
            }

            for(uint8_t i = USBData->Buffer_Rx[6]; i <= USBData->Buffer_Rx[7]; i++)
            {
                //TmpRet = (uint16_t)GetConfSitrad(i, &BufferUSB->HeaderPayload[Size], USBData, FlagReadConf);
                FlagReadConf = true;
                if(TmpRet)
                {
                    SizePayload = (uint16_t)(SizePayload + TmpRet);
                    Size = (uint16_t)(TmpRet + Size);
                }
            }
        }
        else //Neste caso, o Sitrad quer os IDs individuais.
        {
            if(SizeRequest)
            {
                SizeRequest = (uint16_t)(SizeRequest - 1); //remove o sinalizador

                uint8_t IndexBufferInput = 6;
                for(uint8_t i = IndexBufferInput; i <= (SizeRequest+IndexBufferInput); i++)
                {
                    TmpRet = (uint16_t)GetConfSitrad(USBData->Buffer_Rx[i], &USBData->Buffer_Tx[Size], USBData, FlagReadConf);
                    FlagReadConf = true;
                    if(TmpRet)
                    {
                        SizePayload = (uint16_t)(SizePayload + TmpRet);
                        Size = (uint16_t)(TmpRet + Size);
                    }
                }
            }
        }


        USBData->Buffer_Tx[3] = (uint8_t)(    ((SizePayload) >>  8) & 0xFF); //CMD_SIZE1_IND
        USBData->Buffer_Tx[4] = (uint8_t)(    ((SizePayload) >>  0) & 0xFF); //CMD_SIZE2_IND

        Size = (uint16_t)(SizePayload + (uint16_t)5); //ajustar o payload

        //uint16_t DifTemp = (Size % 16);
        //BufferUSB->HeaderPayload[1] = (uint8_t)(((uint16_t)(Size + (16 - DifTemp)) >>  8) & 0xFF);
        //BufferUSB->HeaderPayload[2] = (uint8_t)(((uint16_t)(Size + (16 - DifTemp)) >>  0) & 0xFF);
        //Size = (uint16_t)(Size + (uint16_t)3);
    }

    return Size;
}

uint16_t UpdadeDateHour(_USB_CONTROL *USBData, uint16_t RandomNumber)
{
    uint16_t Size = 0;

    ClearTxBuffer(USBData);

    //-------------------Payload-----------------------
    USBData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber >> 0x08);
    USBData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber ^ 0xFF);
    USBData->Buffer_Tx[Size++] = (USBData->Buffer_Rx[2] | 0x80);
    USBData->Buffer_Tx[Size++] = 0x00;
    USBData->Buffer_Tx[Size++] = 0x01;
    USBData->Buffer_Tx[Size++] = 0x00;


    USBData->DateTimeValues[0] = USBData->Buffer_Rx[3];
    USBData->DateTimeValues[1] = USBData->Buffer_Rx[4];
    USBData->DateTimeValues[2] = USBData->Buffer_Rx[5];
    USBData->DateTimeValues[3] = USBData->Buffer_Rx[6];
    USBData->DateTimeValues[4] = USBData->Buffer_Rx[7];
    USBData->DateTimeValues[5] = USBData->Buffer_Rx[8];

    return Size;
}


uint16_t AnswerDefault(_USB_CONTROL *USBData, uint16_t RandomNumber)
{
    uint16_t Size = 0;

    ClearTxBuffer(USBData);

    //-------------------Payload-----------------------
    USBData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber >> 0x08);
    USBData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber ^ 0xFF);
    USBData->Buffer_Tx[Size++] = (USBData->Buffer_Rx[2] | 0x80 );
    USBData->Buffer_Tx[Size++] = 0x00;
    USBData->Buffer_Tx[Size++] = 0x01;
    USBData->Buffer_Tx[Size++] = 0x00;

    return Size;
}


uint16_t GetStatus(_USB_CONTROL *USBData, uint16_t RandomNumber)
{
    uint8_t     DateTimeValue[DATE_TIME_SIZE];
    uint16_t    Temp;
    uint16_t    Size = 0;

    ClearTxBuffer(USBData);

    GetDateTimerX(DateTimeValue);
    //printf("BERFORE SEND CELLULAR_FLAG: %d\n",USBData->CellularStatus);
    //-------------------Payload-----------------------
    USBData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber >> 8);
    USBData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber ^ 0xFF);
    USBData->Buffer_Tx[Size++] = (uint8_t)(USBData->Buffer_Rx[TypeCom] | 0x80);
    USBData->Buffer_Tx[Size++] = (uint8_t)0x00; //TODO: VERSÃO EMI/EMC
    USBData->Buffer_Tx[Size++] = (uint8_t)0x10; //TODO: VERSÃO EMI/EMC
    USBData->Buffer_Tx[Size++] = (uint8_t)USBData->CellularStatus;
    USBData->Buffer_Tx[Size++] = USBData->Rs485Status;
    USBData->Buffer_Tx[Size++] = (uint8_t)DataloggerGetFlags();
    USBData->Buffer_Tx[Size++] = (uint8_t)GetFlagEclo();
    USBData->Buffer_Tx[Size++] = (uint8_t)DateTimeValue[DATE_TIME_DAY];
    USBData->Buffer_Tx[Size++] = (uint8_t)DateTimeValue[DATE_TIME_MONTH];
    USBData->Buffer_Tx[Size++] = (uint8_t)DateTimeValue[DATE_TIME_YEAR];
    USBData->Buffer_Tx[Size++] = (uint8_t)0x00;//WeekOfTheMonth
    USBData->Buffer_Tx[Size++] = (uint8_t)DateTimeValue[DATE_TIME_HOUR];
    USBData->Buffer_Tx[Size++] = (uint8_t)DateTimeValue[DATE_TIME_MINUTE];
    USBData->Buffer_Tx[Size++] = (uint8_t)DateTimeValue[DATE_TIME_SECOND];
    Temp = DataloggerGetActualPage();
    USBData->Buffer_Tx[Size++] = (uint8_t)((Temp >>  8) & 0xFF);
    USBData->Buffer_Tx[Size++] = (uint8_t)((Temp >>  0) & 0xFF);
    USBData->Buffer_Tx[Size++] = (uint8_t)((PAGE_FLASH_SIZE >>  8) & 0xFF);
    USBData->Buffer_Tx[Size++] = (uint8_t)((PAGE_FLASH_SIZE >>  0) & 0xFF);
    USBData->Buffer_Tx[Size++] = (uint8_t)((NUMBER_OF_PAGES_DATALOGGER >>  8) & 0xFF);
    USBData->Buffer_Tx[Size++] = (uint8_t)((NUMBER_OF_PAGES_DATALOGGER >>  0) & 0xFF);

    USBData->Buffer_Tx[Size++] = (uint8_t)(USBData->FlagConnectSitrad); //novo

    //Retorna tamanho do payload
    return Size;
}

uint16_t GetConfSitrad(uint8_t id, uint8_t * GetConfData, _USB_CONTROL *USBData, bool FlagReadConf) //(_PACKET_UDP *PacketUdp, _IPSTRUCT *PonterIpStruct)
{
	static _USB_CONF ConfSitradUSB;
    uint16_t SizeReturn = 0;

    //Todo: Verificar se sera necessario retirar
    SSP_PARAMETER_NOT_USED(USBData);

	if(!FlagReadConf)
	{
      ReadPacketUSBCuston(&ConfSitradUSB);
    }

	switch(id)
	{
        case ID_MODEL:
            *GetConfData++ = id;
            *GetConfData++ = (uint8_t)strlen((const char *)MODEL);
            strcpy((char *)GetConfData, (const char *)MODEL);
            SizeReturn = (uint16_t) (strlen((const char *)MODEL) + HEADER_CONF_SIZE); //size do comando + 2bytes header
            break;

        case ID_CONVERTER_NAME:
            *GetConfData++ = id;
            *GetConfData++ = (uint8_t)strlen((const char *)ConfSitradUSB.NameConv);
            strcpy((char *)GetConfData, (const char *)ConfSitradUSB.NameConv);
            SizeReturn = (uint16_t) (strlen((const char *)ConfSitradUSB.NameConv) + HEADER_CONF_SIZE); //size do comando + 2bytes header
            break;

        case ID_ENABLE_PASSWORD:
            *GetConfData++ = id;
            *GetConfData++ = FLAG_PASSWORD_SIZE;
            *GetConfData++ = (uint8_t)ConfSitradUSB.EnablePassword;
            SizeReturn = (uint16_t) (FLAG_PASSWORD_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
            break;

        case ID_VERSION:
            *GetConfData++ = id;
            *GetConfData++ = (uint8_t)strlen((const char *)VERSION);//VERSION_SIZE;
            strcpy((char *)GetConfData, (const char *)VERSION);
            SizeReturn = (uint16_t) ((uint8_t)strlen((const char *)VERSION) + HEADER_CONF_SIZE); //size do comando + 2bytes header
            break;

        case ID_BAUD_RATE:
            *GetConfData++ = id;
            *GetConfData++ = BAUD_RATE_485_SIZE;
            *GetConfData++ = (uint8_t)(BAUD_RATE_RS485  >> 8 ) & 0xFF;
            *GetConfData++ = (uint8_t)(BAUD_RATE_RS485  >> 8 ) & 0xFF;
            *GetConfData++ = (uint8_t)(BAUD_RATE_RS485  >> 8 ) & 0xFF;
            *GetConfData++ = (uint8_t)(BAUD_RATE_RS485       ) & 0xFF;
            SizeReturn = (uint16_t) (BAUD_RATE_485_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
            break;

        case ID_TIME_OUT_RS485://           = 0x10,
            *GetConfData++ = id;
            *GetConfData++ = TIME_OUT_485_SIZE;
            *GetConfData++ = (uint8_t)(TIMER_OUT_WAIT_RX_RS485 >> 8 ) & 0xFF;
            *GetConfData++ = (uint8_t)(TIMER_OUT_WAIT_RX_RS485      ) & 0xFF;
            SizeReturn = (uint16_t)(TIME_OUT_485_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
            break;

        case ID_TIME_LOG://                 = 0x16,
            *GetConfData++ = id;
            *GetConfData++ = TIME_OUT_LOG_SIZE;
            uint16_t TempS;
            TempS = DataloggerGetTimeToLog();
            *GetConfData++ = (uint8_t)(TempS  >> 8 ) & 0xFF;
            *GetConfData++ = (uint8_t)(TempS       ) & 0xFF;
            SizeReturn = (uint16_t)(TIME_OUT_LOG_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
            break;

        case ID_LIST_INSTRUMENTS:
            *GetConfData++ = id;
            uint8_t DataTemp[0xFF],
                    SizeTemp = 0;
            SizeTemp = (uint8_t)DataloggerGetListInstrumentsFG(&DataTemp[0]);
            if(!SizeTemp)
            {
                SizeTemp++;
                *GetConfData++ = SizeTemp;
                *GetConfData++ = 0;
            }
            else
            {
                *GetConfData++ = SizeTemp;
                memcpy(GetConfData, &DataTemp[0], SizeTemp);
            }
            SizeReturn = (uint16_t)(SizeTemp + HEADER_CONF_SIZE); //size do comando + 2bytes header
            break;

            //TODO: colocar demais IDs

        default:
            SizeReturn = 0;
            break;


	}

    return SizeReturn;
}


void SetConfSitrad(uint8_t * SetConfData, _USB_CONTROL *USBData, uint16_t SizeRequest) //
{
    _USB_CONF            ConfSitradUSB;

    //Todo: Verificar se sera necessario
    SSP_PARAMETER_NOT_USED(USBData);

    int_storage_read((uint8_t *)&ConfSitradUSB, sizeof(_UDP_CONF), CONF_USB_SITRAD);


    uint16_t    SizeCmd = 0,
                SizeCmdHeader,
                TimeDatalogger = 0;
    uint8_t     Id,
                Temp,
                BufInstruments[MAX_NUM_INSTRUMENTS];

    for(SizeCmdHeader = 0; SizeCmdHeader < SizeRequest; )
    {
        //    i = (uint8_t)(i + SetConfSitrad(&BufferInput[i]));
        Id = *SetConfData++;
        SizeCmd = *SetConfData++;
        switch(Id)
        {
        	case ID_MODEL:
        		memset(ConfSitradUSB.ModelConv,'\0', MODEL_CONV_SIZE );
        		memcpy((char *)ConfSitradUSB.ModelConv, (const char *)SetConfData, SizeCmd);
        		SetConfData = SetConfData + SizeCmd;
        		break;

            case ID_CONVERTER_NAME:
                memset(ConfSitradUSB.NameConv,'\0', NAME_CONV_SIZE );
                memcpy((char *)ConfSitradUSB.NameConv, (const char *)SetConfData, SizeCmd);
                SetConfData = SetConfData + SizeCmd;
                break;

            case ID_ENABLE_PASSWORD:
            	ConfSitradUSB.EnablePassword =  *SetConfData++;
                break;

            case ID_VERSION:
            	memset(ConfSitradUSB.Version,'\0', VERSION_SIZE );
            	memcpy((char *)ConfSitradUSB.Version, (const char *)SetConfData, SizeCmd);
            	SetConfData = SetConfData + SizeCmd;
            	break;

            case ID_BAUD_RATE://
            	ConfSitradUSB.BaudRate485[3] = (uint8_t)*SetConfData++;
            	ConfSitradUSB.BaudRate485[2] = (uint8_t)*SetConfData++;
            	ConfSitradUSB.BaudRate485[1] = (uint8_t)*SetConfData++;
            	ConfSitradUSB.BaudRate485[0] = (uint8_t)*SetConfData++;
                break;

            case ID_TIME_OUT_RS485:
            	ConfSitradUSB.TimeOutRs485[1] = (uint8_t)*SetConfData++;
            	ConfSitradUSB.TimeOutRs485[0] = (uint8_t)*SetConfData++;
				break;

            case ID_PASSWORD:
				memset(ConfSitradUSB.Password, 0 , PASSWORD_SIZE );
				memcpy((char *)ConfSitradUSB.Password, (const char *)SetConfData, (uint8_t)SizeCmd);
				SetConfData = SetConfData + SizeCmd;
                break;

            case ID_TIME_LOG:
                TimeDatalogger = (uint16_t)(*SetConfData++ << 8);
                TimeDatalogger = (uint16_t)(TimeDatalogger | (uint8_t)*SetConfData++);
                if(TimeDatalogger < 15) //tempo minimo para log. em segundos
                    TimeDatalogger = 15;
                SetLogInterval(TimeDatalogger);
                break;

            case ID_LIST_INSTRUMENTS:
				Temp = (uint8_t)SizeCmd; //numero de instrumentos para log

				if(Temp > MAX_NUM_INSTRUMENTS)
					Temp = MAX_NUM_INSTRUMENTS;

				memset(&BufInstruments[0], 0xFF, MAX_NUM_INSTRUMENTS);

				for(uint8_t i=0; i < Temp; i++)
				{
					BufInstruments[i] = *SetConfData++;
				}

				DataloggerFlashSetCmdSitrad(0, &BufInstruments[0], Temp);
				break;

            case ID_STATUS_SITRAD:
            	ConfSitradUSB.StatusSitrad =  *SetConfData++;
            	break;

            case ID_BASE_STATION:
            	memset(ConfSitradUSB.BaseStation,'\0', BASE_STATION_SIZE );
				memcpy((char *)ConfSitradUSB.BaseStation, (const char *)SetConfData, SizeCmd);
				SetConfData = SetConfData + SizeCmd;
				break;

            case ID_STATUS_NET_CELL:
            	ConfSitradUSB.StatusNetCell =  *SetConfData++;
            	break;

            case ID_VERSION_EXS82:
            	memset(ConfSitradUSB.VersionEXS82,'\0', VERSION_EXS82_SIZE );
				memcpy((char *)ConfSitradUSB.VersionEXS82, (const char *)SetConfData, SizeCmd);
				SetConfData = SetConfData + SizeCmd;
				break;

            case ID_APN_MODE:
            	ConfSitradUSB.ApnMode =  *SetConfData++;
            	break;

            case ID_APN_ACTUAL:
            	memset(ConfSitradUSB.ApnActual,'\0', APN_ACTUAL_SIZE );
				memcpy((char *)ConfSitradUSB.ApnActual, (const char *)SetConfData, SizeCmd);
				SetConfData = SetConfData + SizeCmd;
				break;

            case ID_APN_DEFAULT:
            	memset(ConfSitradUSB.ApnDefault,'\0', APN_DEFAULT_SIZE );
				memcpy((char *)ConfSitradUSB.ApnDefault, (const char *)SetConfData, SizeCmd);
				SetConfData = SetConfData + SizeCmd;
				break;

            case ID_APN_ALTERNATIVE:
            	memset(ConfSitradUSB.ApnAlternative,'\0', APN_ALTERNATIVE_SIZE );
            	memcpy((char *)ConfSitradUSB.ApnAlternative, (const char *)SetConfData, SizeCmd);
				SetConfData = SetConfData + SizeCmd;
				break;

            case ID_APN_MANUAL:
            	memset(ConfSitradUSB.ApnManual,'\0', APN_MANUAL_SIZE );
				memcpy((char *)ConfSitradUSB.ApnManual, (const char *)SetConfData, SizeCmd);
				SetConfData = SetConfData + SizeCmd;
				break;

            case ID_APN_USER:
            	memset(ConfSitradUSB.ApnUser,'\0', APN_USER_SIZE );
				memcpy((char *)ConfSitradUSB.ApnUser, (const char *)SetConfData, SizeCmd);
				SetConfData = SetConfData + SizeCmd;
				break;

            case ID_APN_PASSWORD:
            	memset(ConfSitradUSB.ApnPassword,'\0', APN_PASSWORD_SIZE );
				memcpy((char *)ConfSitradUSB.ApnPassword, (const char *)SetConfData, SizeCmd);
				SetConfData = SetConfData + SizeCmd;
				break;

            case ID_SITRAD:
            	memset(ConfSitradUSB.IdSitrad,'\0', ID_SITRAD_SIZE );
				memcpy((char *)ConfSitradUSB.IdSitrad, (const char *)SetConfData, SizeCmd);
				SetConfData = SetConfData + SizeCmd;
				break;

            case ID_IMEI:
            	memset(ConfSitradUSB.Imei,'\0', IMEI_SIZE );
				memcpy((char *)ConfSitradUSB.Imei, (const char *)SetConfData, SizeCmd);
				SetConfData = SetConfData + SizeCmd;
				break;

            default:
                //SizeReturn = 0;
                break;
        }

        SizeCmdHeader = (uint16_t)(SizeCmdHeader + (uint16_t)(SizeCmd + HEADER_CONF_SIZE));

        if(     (Id == ID_MODEL)            ||
                (Id == ID_CONVERTER_NAME)   ||
                (Id == ID_ENABLE_PASSWORD)  ||
                (Id == ID_VERSION)          ||
                (Id == ID_BAUD_RATE)        ||
                (Id == ID_TIME_OUT_RS485)   ||
                (Id == ID_PASSWORD)         ||
                (Id == ID_STATUS_SITRAD)    ||
                (Id == ID_BASE_STATION)     ||
                (Id == ID_STATUS_NET_CELL)  ||
				(Id == ID_VERSION_EXS82)    ||
				(Id == ID_APN_MODE)         ||
				(Id == ID_APN_ACTUAL)       ||
				(Id == ID_APN_DEFAULT)      ||
				(Id == ID_APN_ALTERNATIVE)  ||
				(Id == ID_APN_MANUAL)       ||
				(Id == ID_APN_USER)         ||
				(Id == ID_APN_PASSWORD)     ||
				(Id == ID_SITRAD)           ||
                (Id == ID_IMEI))
        {
            ConfSitradUSB.ConfDefault = FLASH_NUMBER_CUSTON;
        }

    }

    int_storage_write((uint8_t *)&ConfSitradUSB,    sizeof(_USB_CONF),  CONF_USB_SITRAD);
}


uint16_t LoginDataResponse(_USB_CONTROL *USBData, uint16_t RandomNumber)
{
    uint16_t Size = 0;

    ClearTxBuffer(USBData);

    //Todo: Adicionar método de validação de senha;

    //-------------------Payload-----------------------
    USBData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber >> 0x08);
    USBData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber ^ 0xFF);
    USBData->Buffer_Tx[Size++] = ( USBData->Buffer_Rx[2] | 0x80 );
    USBData->Buffer_Tx[Size++] = 0x00;
    USBData->Buffer_Tx[Size++] = 0x01;
    USBData->Buffer_Tx[Size++] = 0x00;

	return Size;
}


uint16_t AESEnc (uint8_t * PMessage, uint8_t *EncMessage, uint16_t InputOctets)
{
    uint32_t  MessageLen  = 0;

    if (( EncMessage == NULL ) || ( InputOctets <= 0 ))
    {
       return 0;
    }

    if (( InputOctets % 16U ) != 0 )
    {
       return 0;
    }

    MessageLen = (uint32_t)(InputOctets / 4U);

    g_sce_aes_1.p_api->open(g_sce_aes_1.p_ctrl, g_sce_aes_1.p_cfg); //Open AES_0

    g_sce_aes_1.p_api->encrypt(g_sce_aes_1.p_ctrl, (uint32_t *)aes_key2, (uint32_t *)IVc2, MessageLen, (uint32_t *)PMessage, (uint32_t *)EncMessage);

    // Close AES driver
    g_sce_aes_1.p_api->close(g_sce_aes_1.p_ctrl);

    return InputOctets;
}


uint16_t AESDec (uint8_t * PMessage, uint8_t *DecMessage, uint16_t InputOctets)
{
   //uint16_t NumBlocks;
   uint32_t  MessageLen  = 0;

   if (( DecMessage == NULL ) || ( InputOctets <= 0 ))
   {
      return 0;
   }

   if (( InputOctets % 16U ) != 0 )
   {
      return 0;
   }

   //NumBlocks = ( InputOctets / 16U );//280219

   MessageLen = (uint32_t)(InputOctets / 4U);
//   g_sce.p_api->open(g_sce.p_ctrl, g_sce.p_cfg);
//   g_sce_aes_1.p_api->open(g_sce_aes_1.p_ctrl, g_sce_aes_1.p_cfg); //Open AES_0

   g_sce_aes_1.p_api->decrypt(g_sce_aes_1.p_ctrl, (uint32_t *)aes_key2, (uint32_t *)IVc2, MessageLen, (uint32_t *)PMessage, (uint32_t *)DecMessage);//(uint32_t *)tempOut);

   /* Close AES driver */
//   g_sce_aes_1.p_api->close(g_sce_aes_1.p_ctrl);
//   g_sce.p_api->close(g_sce.p_ctrl);

//   uint16_t padLen = DecMessage[(NumBlocks*16) - 1];
//
//   if (( padLen <= 0 ) || ( padLen > 16 ))
//   {
//       return 0;
//   }
//
//   //Verifica o padding do pacote
//   for ( uint16_t i = (uint16_t)(16 - padLen); i < 16; i++ )
//   {
//       if(DecMessage[((NumBlocks*16)-16) + i] != padLen)
//       {
//             return 0;
//       }
//   }
//
//   uint16_t OutSizePayload;
//   OutSizePayload = (uint16_t)(( NumBlocks*16 ) - padLen);
//
//   return OutSizePayload;
   return InputOctets;
}


void ReadPacketUSBCuston(_USB_CONF *ConfSitradUSB)
{
    _USB_CONF ConfSitrad_USB;
    //Le a memoria em busca das configuracoes de UDP
    int_storage_read((uint8_t *)&ConfSitrad_USB, sizeof(_USB_CONF), CONF_USB_SITRAD);

    if((ConfSitrad_USB.ConfDefault != FLASH_NUMBER_CUSTON) && (ConfSitrad_USB.ConfDefault != FLASH_NUMBER_DEFAULT))
    {   //Caso nunca tenha sido adicionado uma conf, usa o default.
        SetConfigDefault(&ConfSitrad_USB);

        ConfSitrad_USB.ConfDefault = FLASH_NUMBER_DEFAULT;
        int_storage_write((uint8_t *)&ConfSitrad_USB, sizeof(_UDP_CONF), CONF_USB_SITRAD);
    }

    memcpy(ConfSitradUSB, &ConfSitrad_USB, sizeof(_UDP_CONF));
}


//------------------------------------------------------------------------------------------
// reseta buffer
//------------------------------------------------------------------------------------------
void ResetRxBuffer(void)
{
	Rx_Lim = Rx_Qtde = 0;

}

//------------------------------------------------------------------------------------------
// Carrega Buffer
//------------------------------------------------------------------------------------------
void LoadRxBuffer(uint8_t *BufferInput,_USB_CONTROL *USBData, uint16_t Len)
{
	uint32_t counter = 0;
	if(USBData->BufferUSB.HP.Header[0]==35||USBData->BufferUSB.HP.Header[0]==37)counter = 3;
	for (; counter < Len; counter++) {
		USBData->Buffer_Rx[Rx_Qtde] = BufferInput[counter];
		Rx_Qtde++;
	}
//		printf("\nBUFFER COMPLETO: ");
//	for(uint32_t i=0; i< Rx_Qtde; i++){
//		if(i==0)printf("\n%d",USBData->Buffer_Rx[i]);
//		else if(i==Rx_Qtde)printf(" %d\n",USBData->Buffer_Rx[i]);
//		else printf(" %d",USBData->Buffer_Rx[i]);
//	}
}

void ClearRxBuffer(_USB_CONTROL *USBData)
{
	memset(&USBData->Buffer_Rx[0], 0, sizeof(USBData->Buffer_Rx));
}

void ClearTxBuffer(_USB_CONTROL *USBData)
{
	memset(&USBData->Buffer_Tx[0], 0, sizeof(USBData->Buffer_Tx));
}


