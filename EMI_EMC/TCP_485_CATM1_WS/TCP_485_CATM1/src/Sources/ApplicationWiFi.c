///*
// * ApplicationWiFi.c
// *
// *  Created on: 7 de jun de 2019
// *      Author: leonardo.ceolin
// */
//#include "common_data.h" // include obrigatório
//#include "Includes/Eth_Wifi_Thread_Entry.h"
//#include "Includes/ApplicationWiFi.h"
//#include "Includes/ApplicationCommonModule.h"
//#include "Includes/internal_flash.h"
//#include "Includes/DateTime.h"
//#include "Includes/ApplicationLedsStatus.h"
//#include "Includes/Define_IOs.h"
//
//#define MAX_NUM_NET_SCAN    BUFFER_SIZE_RX_WINDS
//#define MAX_NUM_MAC_LIST    5
//
//uint8_t ModuleProvision(_ETH_WIFI_CONTROL *EthWifiData);
//void GetStatusWiFi(_ETH_WIFI_CONTROL *EthWifiData);
//
//_FG_ERR ScanNets(_ETH_WIFI_CONTROL *EthWifiData);
//void SaveNet(sf_wifi_provisioning_t *ProvisionWifi);
//bool MacAddressList(uint8_t *MacAdd, bool AddTRemoveF);
//void WifiLink_UpDown_NotifyCallback(sf_wifi_callback_args_t * p_args);
//void StrToHex(char *dest, char * src);
//
//void GetStatusWiFi(_ETH_WIFI_CONTROL *EthWifiData)
//{
//    static uint16_t Sample_N1 = 0;
//    sf_wifi_info_t WifiInfo;
//
//    memset(&WifiInfo, 0, sizeof(WifiInfo));
//
//    if(SSP_SUCCESS == g_sf_wifi0.p_api->infoGet(g_sf_wifi0.p_ctrl, &WifiInfo))
//    {
//        Sample_N1 = (uint16_t)((Sample_N1 + WifiInfo.rssi) / 2);
//        EthWifiData->WifiRssid = (uint8_t)(Sample_N1);
//    }
//
//    SSP_PARAMETER_NOT_USED(EthWifiData);
//}
//
//_FG_ERR ScanNets(_ETH_WIFI_CONTROL *EthWifiData)
//{
//    uint8_t AP_found = MAX_NUM_NET_SCAN,
//            Index    = MAX_NUM_NET_SCAN;
//    bool FlagNet = false;
//    ssp_err_t error;
//    uint8_t ssid_hidden[SF_WIFI_SSID_LENGTH + 1];
//
//    sf_wifi_scan_t aps[AP_found];
//
//    memset(&aps, 0 ,sizeof(aps));
//    memset(&ssid_hidden, 0 ,sizeof(ssid_hidden));
//
//    error = g_sf_wifi0.p_api->scan(g_sf_wifi0.p_ctrl, aps, &AP_found);
//    if(error != SSP_SUCCESS)
//    {
//        g_sf_wifi0.p_api->close(g_sf_wifi0.p_ctrl);
//        tx_thread_sleep(100);
//        g_sf_wifi0.p_api->open(g_sf_wifi0.p_ctrl, g_sf_wifi0.p_cfg);
//        return FG_WIFI_ERRO_SCAN;
//    }
//
//    SortStruct(&aps[0], AP_found);  //organiza as redes pela intensidade
//
//    for (Index = 0; (Index < AP_found) && (!FlagNet) && (aps[Index].rssi>0); Index++)
//    {
//        if(strlen((char *)aps[Index].ssid) == strlen((char *)EthWifiData->SfProvisionWifi->ssid))
//        {
//            if(!strcmp((char *)aps[Index].ssid, (char *)EthWifiData->SfProvisionWifi->ssid))
//            {
//                FlagNet = true;
//            }
//        }
//        else if(EthWifiData->ConfNetBasic.HiddenNet)//caso a rede seja oculta
//        {
//            if(strlen((char *)aps[Index].ssid) == strlen((char *)ssid_hidden))
//            {
//                if(!strcmp((char *)aps[Index].ssid, (char *)ssid_hidden))
//                {
//                    if(!MacAddressList(&aps[Index].bssid[0], true))//consulta se a rede ja foi tentada
//                    {
//                        FlagNet = true; //seta o flag se encontrou uma rede com o mesmo ssid e ainda nao foi colocada na lista de mac
//                    }
//                }
//            }
//        }
//    }
//
//    if(FlagNet)// || (Index != MAX_NUM_NET_SCAN))
//    {
//        EthWifiData->SfProvisionWifi->channel    = aps[Index-1].channel;
//        EthWifiData->SfProvisionWifi->encryption = aps[Index-1].encryption;
//        EthWifiData->SfProvisionWifi->security   = aps[Index-1].security;
//        EthWifiData->SfProvisionWifi->mode       = SF_WIFI_INTERFACE_MODE_CLIENT;
//        return FG_SUCCESS; //rede encontrada
//    }
//    else
//    {
//        return FG_WIFI_NO_NETS_PROV;
//    }
//}
//
//uint8_t ModuleProvision(_ETH_WIFI_CONTROL *EthWifiData)
//{
//    EthWifiData->SfProvisionWifi->p_callback = WifiLink_UpDown_NotifyCallback;
//
//    sf_wifi_provisioning_t *Provisioning = EthWifiData->SfProvisionWifi;
//    //Autenticacao tipo WEP precisa de tratamento na senha antes de ser enviada para o provisionametno
//    if(EthWifiData->SfProvisionWifi->security == SF_WIFI_SECURITY_TYPE_WEP)
//    {
//        sf_wifi_provisioning_t ProvisionWifi = *EthWifiData->SfProvisionWifi;
//        ProvisionWifi.key[0] = 1; //Sempre usa chave #1, como os celulares android e iOS fazem
//        size_t KeySize = strlen((char*)Provisioning->key);
//        //Se o tamanho da senha for o padrao de um dos tipos de chave, copia direto, pois o usuario entrou a senha em hex
//        //Senao, converte a chave para hex pois o usuario entrou com a senha em texto
//        if(KeySize == WEP_40_KEY_STRING_SIZE || KeySize == WEP_104_KEY_STRING_SIZE)
//        {
//            ProvisionWifi.key[1] = (uint8_t)(KeySize + 1);
//            strcpy((char*)&ProvisionWifi.key[2], (char*)Provisioning->key);
//        }
//        else if(KeySize == (WEP_40_KEY_STRING_SIZE / 2) || KeySize == (WEP_104_KEY_STRING_SIZE / 2))
//        {
//            ProvisionWifi.key[1] = (uint8_t)(KeySize * 2 + 1);
//            StrToHex((char*)&ProvisionWifi.key[2], (char*)Provisioning->key);
//        }
//        else
//        {
//            return 1U;
//        }
//        Provisioning = &ProvisionWifi;
//    }
//
//
//    if(g_sf_wifi0.p_api->provisioningSet(g_sf_wifi0.p_ctrl, Provisioning))
//    {
//        return 1U;
//    }
//    else
//    {
//        tx_thread_sleep(100);
//        return 0U;
//    }
//}
//
////Salva na ram 5 mac de redes ocultas
//bool MacAddressList(uint8_t *MacAdd, bool AddTRemoveF)
//{
//    static uint8_t MacAddress[MAX_NUM_MAC_LIST][6],
//                   SizeList,
//                   NumAtt;
//    static bool FlagErase = false;
//
//    if(SizeList == MAX_NUM_MAC_LIST)
//    {
//        FlagErase = 0;
//    }
//
//    if(!FlagErase)
//    {
//        memset(&MacAddress, 0, sizeof(MacAddress));
//        SizeList = 0;
//        NumAtt = 0;
//        FlagErase = true;
//    }
//
//    if(AddTRemoveF)
//    {
//        uint8_t j=0;
//        for(uint8_t i=0; i<10 && i<SizeList ; i++)
//        {
//            if( (MacAddress[i][j]   == MacAdd[j]) &&
//                (MacAddress[i][j+1] == MacAdd[j+1]) &&
//                (MacAddress[i][j+2] == MacAdd[j+2]) &&
//                (MacAddress[i][j+3] == MacAdd[j+3]) &&
//                (MacAddress[i][j+4] == MacAdd[j+4]) &&
//                (MacAddress[i][j+5] == MacAdd[j+5]))
//            {
//                NumAtt++;
//                if(NumAtt > MAX_NUM_MAC_LIST)
//                {
//                    NumAtt = 0;
//                    SizeList = MAX_NUM_MAC_LIST;
//                }
//                return true; //caso ja esteja salvo o mac
//            }
//        }
//        for(uint8_t i=0; i<6; i++)
//        {
//            MacAddress[SizeList][i] = *MacAdd++;
//        }
//        SizeList++;
//    }
//    else
//    {
//        memset(&MacAddress, 0, sizeof(MacAddress));
//        SizeList = 0;
//    }
//
//    return false;
//}
//
//void SaveNet(sf_wifi_provisioning_t *ProvisionWifi)
//{
//    bool WriteFlash = true;
//    _NET_WIFI_CONF NetWifiConf;
//    int_storage_read((uint8_t *)&NetWifiConf, sizeof(_NET_WIFI_CONF), CONF_WIFI_CLIENT);
//
//    if(strlen((char *)NetWifiConf.ApSsid) == strlen((char *)ProvisionWifi->ssid)) //compara o tamanho da atual com a ssid da memoria
//    {
//       if(!strcmp((char *)NetWifiConf.ApSsid, (char *)ProvisionWifi->ssid)) //compara o igualdade das SSID
//       {
//           if(NetWifiConf.SecurityType == ProvisionWifi->security)
//           {
//               WriteFlash = false;
//           }
//       }
//    }
//
//    if(WriteFlash)
//    {
//        strcpy((char *)NetWifiConf.ApSsid, (char*)ProvisionWifi->ssid);
//        strcpy((char *)NetWifiConf.ApPwd, (char*)ProvisionWifi->key);
//        NetWifiConf.SecurityType = ProvisionWifi->security;
//
//        int_storage_write((uint8_t *)&NetWifiConf,      sizeof(_NET_WIFI_CONF),     CONF_WIFI_CLIENT);
//    }
//}
//
//void WifiLink_UpDown_NotifyCallback(sf_wifi_callback_args_t * p_args)
//{
//    switch(p_args->event)
//    {
//        case SF_WIFI_EVENT_AP_CONNECT:          ///< Device Associated Successfully with AP
//            tx_event_flags_set (&EthWifi_Flags, EW_FLAG_WIFI_CONNECTED, TX_OR);
//            break;
//
//        case SF_WIFI_EVENT_AP_DISCONNECT:       ///< Device Disconnected with AP
//            tx_event_flags_set (&EthWifi_Flags, EW_FLAG_WIFI_DISCONNECTED, TX_OR);
//            break;
//
//        case SF_WIFI_EVENT_RX:                  ///< Packet received event
//        case SF_WIFI_EVENT_CLIENT_DISCONNECT:   ///< Client Disconnected from device AP
//        case SF_WIFI_EVENT_CLIENT_CONNECT:      ///< Client Associated Successfully with device AP
//        default:
//            tx_event_flags_set (&EthWifi_Flags, EW_FLAG_FREE7, TX_OR);
//            break;
//    }
//}
//
//
//void StrToHex(char *dest, char * src)
//{
//    size_t len = strlen(src);
//    strcpy(dest,"");
//    for(size_t i = 0; i < len; i++){
//        char aux[3];
//        sprintf(aux, "%.2x", src[i]);
//        strcat(dest, aux);
//    }
//}
