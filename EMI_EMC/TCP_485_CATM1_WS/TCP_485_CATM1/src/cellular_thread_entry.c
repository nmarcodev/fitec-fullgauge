#include "cellular_thread.h"
#include "Includes/common_util.h"
#include "sf_cellular_common_private.h"
#include  "Includes/ApplicationUSB.h"

#define SF_CELLULAR_QCTLCATM1_INIT_DELAY                       (2000U)
#define BUFFER_SIZE             (64)
#define PPP_LINK_UP             (1<<0)
#define PPP_LINK_DOWN           (1<<1)
#define TIM
#define NETWORK_SCAN_MODE_CMD   ("AT+QCFG=\"nwscanmode\",1,1\r\n")
//#define NETWORK_SCAN_SEQ_CMD    ("AT+QCFG=\"nwscanseq\",010203,1\r\n")
//#define NETWORK_ROAM_SERV_CMD   ("AT+QCFG=\"roamservice\",2,1\r\n")
//#define CMD_RESPONSE            ("\r\nOK\r\n")
//#define CMD_TIMEOUT             (300)

#define NETWORK_SCAN_SEQ_CMD    ("AT^SXRAT=9,0\r\n")//("AT+QCFG=\"nwscanseq\",010203,1\r\n")
#define NETWORK_ROAM_SERV_CMD   ("at+creg=0\r\n")//("AT+QCFG=\"roamservice\",2,1\r\n")
#define CMD_RESPONSE            ("\r\nOK\r\n")
#define CMD_TIMEOUT             (300)

#define PWRKEY_PIN              (IOPORT_PORT_01_PIN_02)
#define PPP_LINK_TIMEOUT        (60 * TX_TIMER_TICKS_PER_SECOND)
#define DNS_SERVER_1_ADDRESS         (IP_ADDRESS(8,8,8,8)) /* Google Public DNS servers as a backup */
#define DNS_SERVER_2_ADDRESS         (IP_ADDRESS(8,8,4,4)) /* Google Public DNS servers as a backup */
#define CELLULAR_MODULE_RESET_PIN       (IOPORT_PORT_04_PIN_08)
#define CELLULAR_MODULE_POWER_PIN       (IOPORT_PORT_03_PIN_07)
#define ON_EXS82       					(IOPORT_PORT_04_PIN_14)

#define CELLULAR_CONNECTION_ERROR (0)
#define CELLULAR_MODULE_ON  (1)
#define CELLULAR_TRYING_CONNECTION  (2)
#define CELLULAR_PING_SUCESSFULL  (3)

uint8_t status_gsm = 0;
uint8_t cellular_control = 0;
extern _USB_CONTROL USBControl;

void sf_cellular_debug_log(uint8_t const * const p_cmd_buf, uint32_t cmd_buf_len, sf_cellular_log_buffer_type_t cmd_buf_type);
UINT reinit_network(void);

ssp_err_t cellular_is_module_init(const sf_cellular_at_cmd_set_t *cellular_at_cmds);
ssp_err_t cellular_at_commands_executioner(const sf_cellular_at_cmd_set_t *cellular_cmds);


ssp_err_t cellular_setup(UINT interface_index);
extern void send_gsm_status(uint8_t);


void EXS82_init(void);

/*AT Commands*/
const sf_cellular_at_cmd_set_t g_init_commands[]=
{
     /** AT Command: To check module init */
     {
         .p_cmd = (uint8_t *) "AT\r\n",
         .p_success_resp = (uint8_t *) "OK",
         .max_resp_length = SF_CELLULAR_STR_LEN_32,
         .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
         .retry = SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_RETRY_COUNT,
         .retry_delay = SF_CELLULAR_DELAY_500MS
     },
//     {
//         .p_cmd = (uint8_t *) "at^scfg?\r\n",
//         .p_success_resp = (uint8_t *) "OK",
//         .max_resp_length = SF_CELLULAR_STR_LEN_32,
//         .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
//         .retry = (uint8_t) SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_RETRY_COUNT,
//         .retry_delay = (uint16_t) SF_CELLULAR_DELAY_500MS
//     },
//	 {
//		  .p_cmd = (uint8_t *) "at+cops=0\r\n", //"AT+COPS=0\r\n"
//		  .p_success_resp = (uint8_t *) "OK",
//		  .max_resp_length = SF_CELLULAR_STR_LEN_32,
//		  .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_WAITTIME_5000MS,
//		  .retry = SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_COPS_AUTO_RETRY_COUNT,
//		  .retry_delay = SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_COPS_AUTO_RETRY_DELAY
//	 },
	 {
		  .p_cmd = (uint8_t *) "AT+CGDCONT=1,\"IPV4V6\",\"zap.vivo.com.br\"\r\n",
		  .p_success_resp = (uint8_t *) "OK",
		  .max_resp_length = SF_CELLULAR_STR_LEN_32,
		  .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
		  .retry = SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_RETRY_COUNT,
		  .retry_delay = SF_CELLULAR_DELAY_500MS
	 },
     {
    	 //Attach Rede
         .p_cmd = (uint8_t *) "AT+CGATT=1\r\n",
         .p_success_resp = (uint8_t *) "OK",
         .max_resp_length = SF_CELLULAR_STR_LEN_32,
         .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
         .retry = SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_RETRY_COUNT,
         .retry_delay = SF_CELLULAR_DELAY_500MS
     },
     {
    	 //Habilita Serviço Internet
         .p_cmd = (uint8_t *) "AT^SICA=1,1\r\n",
         .p_success_resp = (uint8_t *) "OK",
         .max_resp_length = SF_CELLULAR_STR_LEN_32,
         .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
         .retry = SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_RETRY_COUNT,
         .retry_delay = SF_CELLULAR_DELAY_500MS
     },
	 {
		 //Habilita Serviço Internet
		  .p_cmd = (uint8_t *) "AT+CGPADDR=1\r\n",
		  .p_success_resp = (uint8_t *) "OK",
		  .max_resp_length = SF_CELLULAR_STR_LEN_128,
		  .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
		  .retry = SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_RETRY_COUNT,
		  .retry_delay = SF_CELLULAR_DELAY_500MS
	  },
     {
    	 //Habilita Serviço Internet
         .p_cmd = (uint8_t *) "AT^SISX=Ping,1,\"8.8.8.8\",5,5000\r\n",
         .p_success_resp = (uint8_t *) "OK",
         .max_resp_length = SF_CELLULAR_STR_LEN_512,
         .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_WAITTIME_2000MS,
         .retry = SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_RETRY_COUNT,
         .retry_delay = SF_CELLULAR_DELAY_500MS
     },
     {
         .p_cmd = NULL
     },
};

#ifdef TIM
/* Provisioning information for TIM SIM */
const sf_cellular_provisioning_t cellular_prov_info =
{
    .airplane_mode = (SF_CELLULAR_AIRPLANE_MODE_OFF),
    .auth_type      = (SF_CELLULAR_AUTH_TYPE_NONE),
    .username       = "",
    .password       = "",
    .apn            = "zap.vivo.com.br",
    .context_id     = (1),
    .pdp_type       = (SF_CELLULAR_PDP_TYPE_IP),
};
#endif

void sf_cellular_debug_log(uint8_t const * const p_cmd_buf, uint32_t cmd_buf_len, sf_cellular_log_buffer_type_t cmd_buf_type)
{
    char buffer[BUFFER_SIZE];
    SSP_PARAMETER_NOT_USED(cmd_buf_type);

    if(BUFFER_SIZE < cmd_buf_len)
    {
        cmd_buf_len = BUFFER_SIZE;
    }
    memset(buffer, 0, sizeof(buffer));
    snprintf(buffer, cmd_buf_len, (char *)p_cmd_buf);
    printf("%s\r\n", buffer);
}

void ppp_link_down_callback(NX_PPP * ppp_ptr)
{
    SSP_PARAMETER_NOT_USED(ppp_ptr);

    printf("PPP Link down\r\n");
    tx_event_flags_set(&g_cellular_event_flags, PPP_LINK_DOWN, TX_OR);

}

void ppp_link_up_callback(NX_PPP * ppp_ptr)
{
    SSP_PARAMETER_NOT_USED(ppp_ptr);

    printf("PPP Link up\r\n");
    tx_event_flags_set(&g_cellular_event_flags, PPP_LINK_UP, TX_OR);

}

ssp_err_t celr_prov_callback(sf_cellular_callback_args_t * p_args)
{
    ssp_err_t err = SSP_SUCCESS;

    printf("Cellular Provisioning Callback\n");

    if(SF_CELLULAR_EVENT_PROVISIONSET == p_args->event)
    {

        sf_cellular_ctrl_t * p_ctrl = (sf_cellular_ctrl_t *)(p_args->p_context);


#ifdef TIM /* Force the modem to GSM RAT */
        sf_cellular_cmd_resp_t  input_at_command;
        sf_cellular_cmd_resp_t  output;
        uint8_t                 response_buffer[64];
        uint8_t                 atcommand_buffer[64];
        int                     result;

        output.p_buff = response_buffer;
        output.buff_len = sizeof(response_buffer);

        input_at_command.p_buff = atcommand_buffer;
        input_at_command.buff_len = sizeof(atcommand_buffer);

        /* RAT search sequence GSM>LTE Cat M1>LTE Cat NB1 */
        memset(response_buffer, 0, sizeof(response_buffer));
        memset(atcommand_buffer, 0, sizeof(atcommand_buffer));
        snprintf((char *)atcommand_buffer, sizeof(atcommand_buffer), NETWORK_SCAN_SEQ_CMD);

        err = g_sf_cellular0.p_api->commandSend(p_ctrl, &input_at_command , &output, CMD_TIMEOUT);
        if (SSP_SUCCESS != err)
        {
            return err;
        }

        output.p_buff[output.buff_len] = 0;
        result = strcmp((char *)output.p_buff, CMD_RESPONSE);
        if (0 != result)
        {
            return SSP_ERR_INTERNAL;
        }

        /* Roam service*/
        memset(response_buffer, 0, sizeof(response_buffer));
        memset(atcommand_buffer, 0, sizeof(atcommand_buffer));
        snprintf((char *)atcommand_buffer, sizeof(atcommand_buffer), NETWORK_ROAM_SERV_CMD);

        err = g_sf_cellular0.p_api->commandSend(p_ctrl, &input_at_command , &output, CMD_TIMEOUT);
        if (SSP_SUCCESS != err)
        {
            return err;
        }

        output.p_buff[output.buff_len] = 0;
        result = strcmp((char *)output.p_buff, CMD_RESPONSE);
        if (0 != result)
        {
            return SSP_ERR_INTERNAL;
        }
#endif
        err = g_sf_cellular0.p_api->provisioningSet (p_ctrl, &cellular_prov_info);
        if (SSP_SUCCESS != err)
        {
            printf ("Modem Provisioning Failed:");
            cellular_control = 1;
        }
        else
        {
            printf ("Modem Provisioning Successful\r\n");
        	send_gsm_status(CELLULAR_PING_SUCESSFULL);
        	cellular_control = 1;
//
//        	USBControl.CellularStatus = CELLULAR_PING_SUCESSFULL;
//        	printf("CELLULAR_FLAG: %d\n",USBControl.CellularStatus);
        }
    }
    return err;
}

void user_rts_callback(uint32_t channel, uint32_t level)
{
    __NOP();
    if(channel == g_uart_cell.p_cfg->channel)
    {
        switch(level)
        {
            case 0U:
                g_ioport.p_api->pinWrite((ioport_port_pin_t)IOPORT_PORT_06_PIN_08, IOPORT_LEVEL_LOW);
                break;
            case 1U:
                g_ioport.p_api->pinWrite((ioport_port_pin_t)IOPORT_PORT_06_PIN_08, IOPORT_LEVEL_HIGH);
                break;
            default:
                break;
        }
    }
}

/*  Sets the unique Mac Address of the device using the FMI unique ID */
//void user_mac_address_set(nx_mac_address_t *p_mac_config)
//{
//    fmi_unique_id_t id;
//    ULONG lowerHalfMac;
//
//    /* Read FMI unique ID */
//    g_fmi.p_api->uniqueIdGet(&id);
//
//    /* REA's Vendor MAC range: 00:30:55:xx:xx:xx */
//    lowerHalfMac = ((0x55000000) | (id.unique_id[0] & (0x00FFFFFF)));
//
//    /* Return the MAC address */
//    p_mac_config->nx_mac_address_h=0x0030;
//    p_mac_config->nx_mac_address_l=lowerHalfMac;
//}

ssp_err_t cellular_setup(UINT interface_index)
{
    ssp_err_t err = SSP_SUCCESS;
    UINT      status;
    ULONG     actual_status;
    ULONG     dns_ip;
    ULONG     actual_events;
    ULONG     ip_address;
    ULONG     mask;

    printf("Setting up Cellular on interface %d\r\n", interface_index);

    /* the STATUS pin is incorrectly wired on the BG96 module, it always outputs a HIGH level */
    /* So it cannot be used to determine the status of the module */
    /* Set the PWRKEY_PIN to a high level to ensure the module powers up */
    //g_ioport.p_api->pinWrite(PWRKEY_PIN, IOPORT_LEVEL_HIGH);

    //EXS82_init();

    printf("\r\nInitializing Network Interface...\r\n");



	//sf_comms_init_cell();

	//EXS82_init();

    status = nx_ip_interface_attach(&g_ip0, "Cellular", IP_ADDRESS(0,0,0,0), IP_ADDRESS(0,0,0,0), g_sf_el_nx_cellular);
    /* NX_UNHANDLED_COMMAND is returned as the status if IPV6 support is enabled in NetX, (FEATURE_NX_IPV6 is defined) */
    /* and NX_DISABLE_ICMPV6_ROUTER_SOLICITATION is not defined, as multicast command is not supported by cellular NSAL */
    if ((NX_SUCCESS != status) && (NX_UNHANDLED_COMMAND != status))
    {
        err = SSP_ERR_INTERNAL;
        return err;
    }

    /* Wait for the PPP Link up event */
    status = tx_event_flags_get(&g_cellular_event_flags, (PPP_LINK_UP | PPP_LINK_DOWN), TX_OR, &actual_events, PPP_LINK_TIMEOUT);
    if ((TX_SUCCESS != status) || ((actual_events & PPP_LINK_DOWN) == PPP_LINK_DOWN))
    {
        err = SSP_ERR_INTERNAL;
        return err;
    }

    status = nx_ip_interface_status_check(&g_ip0, interface_index, NX_IP_LINK_ENABLED, &actual_status, PPP_LINK_TIMEOUT);
    if (NX_SUCCESS != status)
    {
        err = SSP_ERR_INTERNAL;
        return err;
    }


    dns_ip = 0;

    /* Get the DNS IP assigned during the IPCP handshake */
    status = nx_ppp_dns_address_get(&g_nx_ppp0, &dns_ip);
    if ((NX_SUCCESS != status) || (0 == dns_ip))
    {
        dns_ip = DNS_SERVER_1_ADDRESS;
    }

    if (0 != dns_ip)
    {
        /* Add an IPv4 server address to the Client list. */
        status = nx_dns_server_add(&g_dns0, dns_ip);
        if ((NX_SUCCESS != status) && (NX_DNS_DUPLICATE_ENTRY != status))
        {
            err = SSP_ERR_INTERNAL;
            return err;
        }
    }

    dns_ip = 0;

    /* Get the secondary DNS IP assigned during the IPCP handshake */
    status = nx_ppp_secondary_dns_address_get(&g_nx_ppp0, &dns_ip);
    if ((NX_SUCCESS != status) || (0 == dns_ip))
    {
        dns_ip = DNS_SERVER_2_ADDRESS;
    }

    if (0 != dns_ip)
    {
        /* Add an IPv4 server address to the Client list. */
        status = nx_dns_server_add(&g_dns0, dns_ip);
        if ((NX_SUCCESS != status) && (NX_DNS_DUPLICATE_ENTRY != status))
        {
            err = SSP_ERR_INTERNAL;
            return err;
        }
    }

    /* Print out the IP address if connected to a debugger */
    status = nx_ip_interface_address_get(&g_ip0, interface_index, &ip_address, &mask);
    if (NX_SUCCESS != status)
    {
        err = SSP_ERR_INTERNAL;
        return err;
    }

    printf("Successfully setup Cellular interface\r\n");

    printf("IP address = %d.%d.%d.%d on interface %d\r\n", (int)(ip_address>>24 & 0xFF),
                                                           (int)(ip_address>>16 & 0xFF),
                                                           (int)(ip_address>>8 &  0xFF),
                                                           (int)(ip_address>>0 &  0xFF),
                                                           interface_index);

    return err;
}

/*********************************************************************************************************************
 * @brief  cellular_module_reset function
 *
 * This function resets cellular module.
 ********************************************************************************************************************/
static void cellular_module_reset(ioport_port_pin_t pin_reset)
{
    g_ioport.p_api->pinWrite(pin_reset, IOPORT_LEVEL_HIGH);
    sf_cellular_msec_delay(SF_CELLULAR_MODULE_RESET_DELAY_MS);

    g_ioport.p_api->pinWrite(pin_reset, IOPORT_LEVEL_LOW);
    sf_cellular_msec_delay(SF_CELLULAR_MODULE_RESET_DELAY_MS);
}

static void cellular_module_powerup(ioport_port_pin_t pwr_pin)
{
    g_ioport.p_api->pinWrite(pwr_pin, IOPORT_LEVEL_LOW);
    sf_cellular_msec_delay(1000); //200
    g_ioport.p_api->pinWrite(pwr_pin, IOPORT_LEVEL_HIGH);

//    sf_cellular_msec_delay(100); //200
//
//    g_ioport.p_api->pinWrite(pwr_pin, IOPORT_LEVEL_LOW);
//    sf_cellular_msec_delay(100); //200
//    g_ioport.p_api->pinWrite(pwr_pin, IOPORT_LEVEL_HIGH);
//
//    sf_cellular_msec_delay(100); //200
//
//    g_ioport.p_api->pinWrite(pwr_pin, IOPORT_LEVEL_LOW);
//    sf_cellular_msec_delay(100); //200
//    g_ioport.p_api->pinWrite(pwr_pin, IOPORT_LEVEL_HIGH);
}

void EXS82_init(void)
{
    /* Power Cellular Module */
    cellular_module_powerup(CELLULAR_MODULE_POWER_PIN);

    /* Hard reset cellular module */
    //cellular_module_reset(CELLULAR_MODULE_RESET_PIN);
}


UINT reinit_network(void)
{
	const sf_cellular_at_cmd_set_t *init_at_cmds = g_init_commands;
	ssp_err_t ssp_err = SSP_ERR_CELLULAR_FAILED;
	static uint8_t cellular_init = 0;

	//if(cellular_init == 0)
	//{
		//cellular_init = 1;

		printf("Reinit network\n");

		/* Initialize & Configure the Cellular module */
		ssp_err_t result = SSP_SUCCESS;

//		ssp_err = g_sf_comms_cell.p_api->open(g_sf_comms_cell.p_ctrl, g_sf_comms_cell.p_cfg);
//		if(SSP_SUCCESS != ssp_err && SSP_ERR_IN_USE != ssp_err)
//		{
//			printf("Failed to open g_sf_comms_cell instance!!!");
//
//		}
//		else
//		{
//			printf("\ndone g_sf_comms_cell\r\n");
//		}
//
//		/* Initialize the UART port of Cellular modem to send init commands to EXS82 module */
//		result = g_uart_cell.p_api->open(g_uart_cell.p_ctrl, g_uart_cell.p_cfg);
//		if(result != SSP_SUCCESS && result != SSP_ERR_IN_USE)
//		{
//			printf("Failed to initialize UART for cellular module!!!\r\n");
//			return result;
//		}
//		else
//		{
//			printf("UART cell starts!!!\r\n");
//		}
//        status_gsm = 1;
//        send_gsm_status(status_gsm);
		ssp_err = cellular_is_module_init(init_at_cmds);
		if(ssp_err != SSP_SUCCESS)
		{
			printf("Cellular module Init Failed !!!! \r\n");
//	        USBControl.CellularStatus = CELLULAR_CONNECTION_ERROR;
//	        printf("CELLULAR_FLAG: %d\n",USBControl.CellularStatus);
			send_gsm_status(CELLULAR_CONNECTION_ERROR);
			return ssp_err;
		}
		else
		{
//			send_gsm_status(1);
			printf("Cellular module Init Success !!!! \r\n");
//	        USBControl.CellularStatus = CELLULAR_MODULE_ON;
//	        printf("CELLULAR_FLAG: %d\n",USBControl.CellularStatus);
			send_gsm_status(CELLULAR_MODULE_ON);
		}

		/* Goto next AT command */
		init_at_cmds++;

		/* Delay for module to get registered */
		sf_cellular_msec_delay(SF_CELLULAR_QCTLCATM1_INIT_DELAY);

		/* send init AT command set to EXS82 module */
//		USBControl.CellularStatus = CELLULAR_TRYING_CONNECTION;
//		printf("CELLULAR_FLAG: %d\n",USBControl.CellularStatus);
		send_gsm_status(CELLULAR_TRYING_CONNECTION);
		ssp_err = cellular_at_commands_executioner(init_at_cmds);
		if (SSP_SUCCESS == ssp_err)
		{
			printf("done\r\n");
//			status_gsm = 3;
//			send_gsm_status(status_gsm);
//	        USBControl.CellularStatus = CELLULAR_PING_SUCESSFULL;
//	        printf("CELLULAR_FLAG: %d\n",USBControl.CellularStatus);
			send_gsm_status(CELLULAR_PING_SUCESSFULL);
			return ssp_err;
		}
		else
		{
			printf("failed\r\n");
//			status_gsm = 3;
//			send_gsm_status(status_gsm);
//	        USBControl.CellularStatus = CELLULAR_CONNECTION_ERROR;
//	        printf("CELLULAR_FLAG: %d\n",USBControl.CellularStatus);
			send_gsm_status(CELLULAR_CONNECTION_ERROR);
		}

//		ssp_err = g_sf_comms_cell.p_api->close(g_sf_comms_cell.p_ctrl);
//		if(SSP_SUCCESS != ssp_err)
//		{
//			printf("Failed to close g_sf_comms_cell instance!!!");
//
//		}
//		else
//		{
//			printf("done\r\n");
//		}


	//}

	return ssp_err;
}


ssp_err_t cellular_is_module_init(const sf_cellular_at_cmd_set_t *cellular_at_cmds)
{
    ssp_err_t result = SSP_SUCCESS;
    sf_cellular_cmd_resp_t send_cmd, reci_cmd;
    uint8_t rx_data[16];
    uint8_t retry_cnt = 0;

    /* Initialize send and receive buffers */
    send_cmd.p_buff = (uint8_t*)cellular_at_cmds->p_cmd;
    send_cmd.buff_len = (uint16_t)strlen((const char*)cellular_at_cmds->p_cmd);

    reci_cmd.p_buff = rx_data;
    reci_cmd.buff_len = sizeof(rx_data);

    do {
        /* Send AT command to EXS82 modem */
        result = g_sf_comms_cell.p_api->write(g_sf_comms_cell.p_ctrl, send_cmd.p_buff, send_cmd.buff_len,SF_CELLULAR_SERIAL_READ_TIMEOUT_TICKS);
        if(result == SSP_SUCCESS)
        {
            /* Receive AT command response from EXS82 modem */
            result = g_sf_comms_cell.p_api->read(g_sf_comms_cell.p_ctrl,reci_cmd.p_buff,reci_cmd.buff_len,SF_CELLULAR_SERIAL_READ_TIMEOUT_TICKS);
            if((result == SSP_SUCCESS) || (result == SSP_ERR_TIMEOUT))
            {
                /* Parse response of Modem */
                if(sf_cellular_is_str_present((const char *)reci_cmd.p_buff,(const char *)(uint8_t*)cellular_at_cmds->p_success_resp) == SF_CELLULAR_TRUE)
                {
                    result = SSP_SUCCESS;
                    break;
                }
            }
            else
            {
                printf("Failed to read modem response !!! \r\n");
            }
        }
        else
        {
        	printf("Failed to send AT command!!! \r\n");
        }

        /* Delay before next retry */
        sf_cellular_msec_delay(cellular_at_cmds->retry_delay);

        ++retry_cnt;
        printf("\nretry_cnt: %d\n", retry_cnt);

    }while(retry_cnt < cellular_at_cmds->retry);

    if(result != SSP_SUCCESS)
    	printf("Cellular AT command failed!!!\r\n");

    return result;
}

ssp_err_t cellular_at_commands_executioner(const sf_cellular_at_cmd_set_t *cellular_cmds)
{
    ssp_err_t result = SSP_SUCCESS;
    sf_cellular_cmd_resp_t send_cmd, reci_cmd;
    uint8_t rx_data[256];
    uint8_t retry_cnt = 0;

    status_gsm = 2;

    reci_cmd.p_buff = rx_data;
    reci_cmd.buff_len = sizeof(rx_data);

    while(cellular_cmds->p_cmd != NULL)
    {
        send_cmd.p_buff = (uint8_t*)cellular_cmds->p_cmd;
        send_cmd.buff_len = (uint16_t)strlen((const char*)cellular_cmds->p_cmd);

        do {
            /* Send AT command to EXS82 modem */
            result = g_sf_comms_cell.p_api->write(g_sf_comms_cell.p_ctrl, send_cmd.p_buff, send_cmd.buff_len,SF_CELLULAR_SERIAL_READ_TIMEOUT_TICKS);
            if(result == SSP_SUCCESS)
            {
                /* Receive AT command response from BG96 modem */
                result = g_sf_comms_cell.p_api->read(g_sf_comms_cell.p_ctrl,reci_cmd.p_buff,reci_cmd.buff_len,cellular_cmds->resp_wait_time);//SF_CELLULAR_SERIAL_READ_TIMEOUT_TICKS
                if((result == SSP_SUCCESS) || (result == SSP_ERR_TIMEOUT))
                {
                    /* Parse response of Modem */
                    if(sf_cellular_is_str_present((const char *)reci_cmd.p_buff,(const char *)(uint8_t*)cellular_cmds->p_success_resp) == SF_CELLULAR_TRUE)
                    {
                    	printf("\nComando: %s\n", send_cmd.p_buff);
                    	printf("\nResposta: %s\n", reci_cmd.p_buff);
                        result = SSP_SUCCESS;
                        break;
                    }
                    else
                    {
                    	// TODO: Alterar
                    	printf("\nComando: %s\n", send_cmd.p_buff);
                    	printf("\nResposta: %s\n", reci_cmd.p_buff);
                    	result = SSP_SUCCESS;
						break;
                    }
                }
                else
                {
                	printf("Failed to read modem response !!! \r\n");
                }
            }
            else
            {
            	printf("Failed to send AT command!!! \r\n");
            }

            /* Delay before next retry */
            sf_cellular_msec_delay(cellular_cmds->retry_delay);

            ++retry_cnt;
            printf("\nretry_cnt: %d\n", retry_cnt);
        }while(retry_cnt < cellular_cmds->retry);

        if(result != SSP_SUCCESS)
        {
        	printf("Cellular AT command failed!!!\r\n");
            break;
        }

        /* Goto next command */
        cellular_cmds++;
        printf("\nGoto next command\n");
//        send_gsm_status(1);
//        USBControl.CellularStatus = CELLULAR_TRYING_CONNECTION;
//        printf("CELLULAR_FLAG: %d\n",USBControl.CellularStatus);
        send_gsm_status(CELLULAR_TRYING_CONNECTION);
        retry_cnt = 0;
    }

    return result;
}

/* Cellular Thread entry function */
void cellular_thread_entry(void)
{
	UINT interface_index = 0;
	ssp_err_t   err = SSP_SUCCESS;
	//tx_thread_sleep(TIME_1S5);
	tx_thread_sleep(110);


	tx_thread_sleep(TIME_3S);

	g_ioport.p_api->pinWrite(ON_EXS82, IOPORT_LEVEL_LOW);
	sf_cellular_msec_delay(100); //200
	g_ioport.p_api->pinWrite(ON_EXS82, IOPORT_LEVEL_HIGH);

	tx_thread_sleep(TIME_2S);
	EXS82_init();
    printf("Thread celular inicializada\r\n");

//    interface_index++;
//    err = cellular_setup(interface_index);
//    if (SSP_SUCCESS != err)
//    {
//      printf("Error configuring the Cellular interface\r\n");
//      while(1);
//    }



	while (1) {

		if(cellular_control)
		{

		}
		tx_thread_sleep(60);
		__NOP();
	}
}
