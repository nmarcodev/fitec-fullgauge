/***********************************************************************************************************************
 * Copyright [2015-2017] Renesas Electronics Corporation and/or its licensors. All Rights Reserved.
 *
 * This file is part of Renesas SynergyTM Software Package (SSP)
 *
 * The contents of this file (the "contents") are proprietary and confidential to Renesas Electronics Corporation
 * and/or its licensors ("Renesas") and subject to statutory and contractual protections.
 *
 * This file is subject to a Renesas SSP license agreement. Unless otherwise agreed in an SSP license agreement with
 * Renesas: 1) you may not use, copy, modify, distribute, display, or perform the contents; 2) you may not use any name
 * or mark of Renesas for advertising or publicity purposes or in connection with your use of the contents; 3) RENESAS
 * MAKES NO WARRANTY OR REPRESENTATIONS ABOUT THE SUITABILITY OF THE CONTENTS FOR ANY PURPOSE; THE CONTENTS ARE PROVIDED
 * "AS IS" WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT; AND 4) RENESAS SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, OR
 * CONSEQUENTIAL DAMAGES, INCLUDING DAMAGES RESULTING FROM LOSS OF USE, DATA, OR PROJECTS, WHETHER IN AN ACTION OF
 * CONTRACT OR TORT, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THE CONTENTS. Third-party contents
 * included in this file may be subject to different terms.
 **********************************************************************************************************************/

/**********************************************************************************************************************
 * File Name    : sf_cellular_qctlcatm1.c
 * Description  : RTOS-integrated Cellular Framework example. Implementation is Quectel CATM1 Cellular Modem interface.
 **********************************************************************************************************************/
/* Cellular Framework Instance specific header file */
#include "sf_cellular_api.h"
#include "sf_cellular_common_api.h"
#include "sf_cellular_common_private.h"
/* Configuration header files for this package */
#include "sf_cellular_qctlcatm1_cfg.h"
/* Framework header files for this package */
#include "sf_cellular_qctlcatm1.h"

#include "sf_cellular_serial.h"
/* Private header files for this package */
#include "sf_cellular_qctlcatm1_private_api.h"
/* Serial UART interface */
#if SF_CELLULAR_QCTLCATM1_CFG_ONCHIP_STACK_SUPPORT
#include "sf_cellular_socket_api.h"
#include "sf_cellular_qctlcatm1_socket_private_api.h"
#endif

/**********************************************************************************************************************
 * Macro definitions
 **********************************************************************************************************************/
#define SF_CELLULAR_QCTLCATM1_COPS_STRING      ((const char *) "+COPS: ")     ///< Response for operator information
#define SF_CELLULAR_QCTLCATM1_QNWINFO_STRING   ((const char *) "+QNWINFO: ")  ///< Response for network information
#define SF_CELLULAR_ACCESS_TECH_UNKNOWN        ((const char *) "NONE")        ///< Unknown network
#define SF_CELLULAR_QCTLCATM1_COMMA_ASCII      ((uint8_t) 0x2C)               ///< Char ','
#define SF_CELLULAR_QCTLCATM1_OP_NAME_INDEX                    (2U)      ///< Operator name index in COPS response
#define SF_CELLULAR_QCTLCATM1_OP_CODE_INDEX                    (1U)      ///< Operator code index qwninfo response
 #define SF_CELLULAR_QCTLCATM1_ACCESS_TECH_INDEX               (0U)      ///< Access Technology index qwninfo response
#define SF_CELLULAR_QCTLCATM1_CLCK_RESP_WAIT_TIME_MS           (5000U)   ///< Response wait time in ms for setting lock functionality
#define SF_CELLULAR_QCTLCATM1_CPIN_RESP_WAIT_TIME_MS           (5000U)   ///< Response wait time in ms for setting sim Pin functionality
#define SF_CELLULAR_QCTLCATM1_DATMODE_RESP_WAIT_TIME_MS        (5000U)   ///< Response wait time in ms for entering data mode
#define SF_CELLULAR_QCTLCATM1_CFUN_RESP_WAIT_TIME_MS           (15000U)  ///< Response wait time in ms for setting phone functionality command
#define SF_CELLULAR_QCTLCATM1_COPS_RESP_WAIT_TIME_MS           (180000U) ///< Response wait time in ms to set Operator mode  command
#define SF_CELLULAR_QCTLCATM1_CONTEXT_CMDRSP_WAITITME          (150000U) ///< Response wait time in ms to set context setting command
#define SF_CELLULAR_QCTLCATM1_INIT_DELAY                       (2000U)   ///< Modem initialization delay in millisecond

/* NBIOT Bands */
#define LTE_NBIOT_BAND_B1       (0x1U)
#define LTE_NBIOT_BAND_B2       (0x2U)
#define LTE_NBIOT_BAND_B3       (0x4U)
#define LTE_NBIOT_BAND_B4       (0x8U)
#define LTE_NBIOT_BAND_B5       (0x10U)
#define LTE_NBIOT_BAND_B8       (0x80U)
#define LTE_NBIOT_BAND_B12      (0x800U)
#define LTE_NBIOT_BAND_B13      (0x1000U)
#define LTE_NBIOT_BAND_B18      (0x20000U)
#define LTE_NBIOT_BAND_B19      (0x40000U)
#define LTE_NBIOT_BAND_B20      (0x80000U)
#define LTE_NBIOT_BAND_B26      (0x2000000U)
#define LTE_NBIOT_BAND_B28      (0x8000000U)

/**********************************************************************************************************************
 * Private Function Declaration
 **********************************************************************************************************************/
static ssp_err_t sf_cellular_qctlcatm1_provision_set(sf_cellular_ctrl_t * p_ctrl,
        sf_cellular_provisioning_t const * p_cellular_provisioning);
static ssp_err_t sf_cellular_qctlcatm1_get_network_status_info(sf_cellular_instance_cfg_t * p_celr_instance,
        sf_cellular_network_status_t * p_network_status_info);
static ssp_err_t sf_cellular_qctlcatm1_get_operator_info(sf_cellular_instance_cfg_t * p_celr_instance,
        sf_cellular_network_status_t * p_network_status_info);
static ssp_err_t sf_cellular_qctlcatm1_cops_status_response_handling(uint8_t * p_token,
        sf_cellular_network_status_t * p_network_status_info);
static void sf_cellular_qctlcatm1_cops_response_switch_handling(sf_cellular_network_status_t * p_network_status_info, uint16_t loc,
        uint8_t * p_subtoken);
static ssp_err_t sf_cellular_qctlcatm1_parse_cops_status_response(uint8_t * p_token,
        sf_cellular_network_status_t * p_network_status_info);
static ssp_err_t sf_cellular_qctlcatm1_get_network_status(sf_cellular_instance_cfg_t * p_celr_instance,
        sf_cellular_network_status_t * p_network_status_info);
static ssp_err_t sf_cellular_qctlcatm1_network_status_response_handling(uint8_t * p_token,
        sf_cellular_network_status_t * p_network_status_info);
static void sf_cellular_qctlcatm1_network_response_switch_handling(sf_cellular_network_status_t * p_network_status_info,
        uint16_t loc, uint8_t * p_subtoken);
static ssp_err_t sf_cellular_qctlcatm1_parse_network_status_response(uint8_t * p_token,
        sf_cellular_network_status_t * p_network_status_info);
static ssp_err_t sf_cellular_qctlcatm1_info_get(sf_cellular_instance_cfg_t * p_celr_instance, sf_cellular_info_t * const p_cellular_info);
static ssp_err_t sf_cellular_qctlcatm1_driver_init(sf_cellular_instance_cfg_t * p_celr_instance, uint8_t * const p_sim_pin,
        uint8_t * const p_puk_pin);
static ssp_err_t sf_cellular_qctlcatm1_module_open(sf_cellular_ctrl_t * p_ctrl, sf_cellular_cfg_t const * const p_cfg);
//static ssp_err_t sf_cellular_set_fallback_sequence(sf_cellular_instance_cfg_t * const p_celr_instance);
ssp_err_t sf_cellular_qctlcatm1_get_imsi(sf_cellular_instance_cfg_t * p_celr_instance, uint8_t * p_imsi);
static ssp_err_t sf_cellular_check_network_registration_status(sf_cellular_instance_cfg_t * p_celr_instance);
//static ssp_err_t sf_cellular_set_sim_priority_effect(sf_cellular_instance_cfg_t * const p_celr_instance);
static ssp_err_t sf_cellular_read_network_registration_response(sf_cellular_instance_cfg_t * p_celr_instance,
        uint8_t * p_resp_buff, uint16_t * p_bytes_read_write, uint32_t resp_wait_ticks);
ssp_err_t sf_cellular_qctlcatm1_config_set(sf_cellular_ctrl_t * p_ctrl, sf_cellular_cfg_t const * p_cfg);
static ssp_err_t sf_cellular_qctlcatm1_module_open_and_config(sf_cellular_ctrl_t *p_ctrl, sf_cellular_cfg_t const * const p_cfg);

/***********************************************************************************************************************
 * Global variables
 **********************************************************************************************************************/
/** Name of module used by error logger macro */
#if BSP_CFG_ERROR_LOG != 0
static const char g_module_name[] = "sf_cellular_qctlcatm1";
#endif

#if defined(__GNUC__)
/* This structure is affected by warnings from a GCC compiler bug. This pragma suppresses the warnings in this
 * structure only.*/
/*LDRA_INSPECTED 69 S */
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif
/** Version data structure used by error logger macro. */
static const ssp_version_t g_sf_cellular_qctlcatm1_version =
{
    .api_version_minor = SF_CELLULAR_API_VERSION_MINOR,
    .api_version_major = SF_CELLULAR_API_VERSION_MAJOR,
    .code_version_major = SF_CELLULAR_QCTLCATM1_CODE_VER_MAJOR,
    .code_version_minor = SF_CELLULAR_QCTLCATM1_CODE_VER_MINOR
};
#if defined(__GNUC__)
/* Restore warning settings for 'missing-field-initializers' to as specified on command line. */
/*LDRA_INSPECTED 69 S */
#pragma GCC diagnostic pop
#endif

/* NBIOT Band Bitmap Lookup Table */
uint32_t band_lookup[29] = {    0,
                                LTE_NBIOT_BAND_B1,
                                LTE_NBIOT_BAND_B2,
                                LTE_NBIOT_BAND_B3,
                                LTE_NBIOT_BAND_B4,
                                LTE_NBIOT_BAND_B5,
                                0,0,
                                LTE_NBIOT_BAND_B8,
                                0,0,0,
                                LTE_NBIOT_BAND_B12,
                                LTE_NBIOT_BAND_B13,
                                0,0,0,0,
                                LTE_NBIOT_BAND_B18,
                                LTE_NBIOT_BAND_B19,
                                LTE_NBIOT_BAND_B20,
                                0,0,0,0,0,
                                LTE_NBIOT_BAND_B26,
                                0,
                                LTE_NBIOT_BAND_B28
                        };

/** Modifiable AT Command sets. AT commands for which retry count and delay can be
 * modified at run time are present in this array */
sf_cellular_at_cmd_set_t g_sf_cellular_qctlcatm1_modifiable_cmd_set[] =
{
 /** AT Command set to read registration status */
 [SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_CREG] =
 {
     .p_cmd = (uint8_t *) "AT+CGREG?\r\n",
     .p_success_resp = (uint8_t *) "+CGREG: 0,1",
     .max_resp_length = SF_CELLULAR_STR_LEN_32,
     .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
     .retry = (uint8_t) SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_RETRY_COUNT,
     .retry_delay = (uint16_t) SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_RETRY_DELAY
 },

 /** AT Command set to read EPS registration status */
 [SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_CEREG] =
 {
    .p_cmd = (uint8_t *) "AT+CEREG?\r\n",
    .p_success_resp = (uint8_t *) "+CEREG: 0,1",
    .max_resp_length = SF_CELLULAR_STR_LEN_32,
    .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
    .retry = (uint8_t) SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_RETRY_COUNT,
    .retry_delay = (uint16_t) SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_RETRY_DELAY
 },

 /** AT Command set to operator selection mode to auto */
 [SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_COPS_AUTO_SET] =
 {
     .p_cmd = (uint8_t *) "AT+COPS=0\r\n",
     .p_success_resp = (uint8_t *) "OK",
     .max_resp_length = SF_CELLULAR_STR_LEN_32,
     .resp_wait_time = SF_CELLULAR_QCTLCATM1_COPS_RESP_WAIT_TIME_MS,
     .retry = (uint8_t) SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_COPS_AUTO_RETRY_COUNT,
     .retry_delay = (uint16_t) SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_COPS_AUTO_RETRY_DELAY
 },

 /** AT Command set to set operator selection mode to manual */
 [SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_COPS_MANUAL_SET] =
 {
     .p_cmd = (uint8_t *) "AT+COPS=1,",
     .p_success_resp = (uint8_t *) "OK",
     .max_resp_length = SF_CELLULAR_STR_LEN_32,
     .resp_wait_time = SF_CELLULAR_QCTLCATM1_COPS_RESP_WAIT_TIME_MS,
     .retry = (uint8_t) SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_COPS_MANUAL_RETRY_COUNT,
     .retry_delay = (uint16_t) SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_COPS_MANUAL_RETRY_DELAY
 },

 /** AT Command set to read SIM PIN status */
[SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_CPIN_STATUS_GET] =
{
    .p_cmd = (uint8_t *) "AT+CPIN?\r\n",
    .p_success_resp = (uint8_t *) "+CPIN",
    .max_resp_length = SF_CELLULAR_STR_LEN_32,
    .resp_wait_time = SF_CELLULAR_QCTLCATM1_CPIN_RESP_WAIT_TIME_MS,
    .retry = (uint8_t) SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_SIM_PIN_RETRY_COUNT,
    .retry_delay = (uint16_t) SF_CELLULAR_QCTLCATM1_CFG_AT_CMD_SIM_PIN_RETRY_DELAY
},
};

/** AT Command sets */
/*LDRA_INSPECTED 27 D This structure must be accessible in user code. It cannot be static. */
const sf_cellular_at_cmd_set_t g_sf_cellular_qctlcatm1_cmd_set[] =
{
    /** AT Command: To check module init */
    [SF_CELLULAR_AT_CMD_INDEX_AT] =
    {
        .p_cmd = (uint8_t *) "AT\r\n",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_10,
        .retry_delay = SF_CELLULAR_DELAY_500MS
    },
    /** AT Command to soft reset the module */
    [SF_CELLULAR_AT_CMD_INDEX_ATZ0] =
    {
        .p_cmd = (uint8_t *) "ATZ0\r\n",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },

    /** AT Command set to switch back to Data mode */
    [SF_CELLULAR_AT_CMD_INDEX_AT_IPR_SET] =
    {
        .p_cmd = (uint8_t *) "AT+IPR=",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },

    /** AT Command set to switch back to Data mode */
    [SF_CELLULAR_AT_CMD_INDEX_AT_SWITCH_BACK_TO_DATA_MODE] =
    {
        .p_cmd = (uint8_t *) "AT&D2\r\n",//"ATO0\r\n",
        .p_success_resp = (uint8_t *) "OK",//"CONNECT ",
        .max_resp_length = (uint16_t) sizeof("\r\nCONNECT xxxxxxxxx\r\n"),
		.resp_wait_time = SF_CELLULAR_AT_CMD_RESP_WAITTIME_2000MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },

    /** AT Command set to disable registration notification */
    [SF_CELLULAR_AT_CMD_INDEX_AT_CREG_SET_0] =
    {
        .p_cmd = (uint8_t *) "AT+CGREG=0\r\n",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_0,
        .retry_delay = SF_CELLULAR_DELAY_0
    },

    /** AT Command to set Network scan sequence */
    [SF_CELLULAR_AT_CMD_INDEX_AT_NWSCANSEQ_SET] =
    {
        .p_cmd = (uint8_t *) "AT+QCFG=\"nwscanseq\",",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
		.resp_wait_time = SF_CELLULAR_AT_CMD_RESP_WAITTIME_2000MS,
        .retry = SF_CELLULAR_RETRY_VALUE_0,
        .retry_delay = SF_CELLULAR_DELAY_100MS
    },

    /** AT Command to set network scan mode */
    [SF_CELLULAR_AT_CMD_INDEX_AT_NWSCANMODE_SET] =
    {
        .p_cmd = (uint8_t *) "AT+QCFG=\"nwscanmode\",0,1\r\n",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_WAITTIME_2000MS,
        .retry = SF_CELLULAR_RETRY_VALUE_0,
        .retry_delay = SF_CELLULAR_DELAY_100MS
    },

    /** AT Command to set search mode for LTE */
    [SF_CELLULAR_AT_CMD_INDEX_AT_IOTOPMODE_SET] =
    {
        .p_cmd = (uint8_t *) "AT+QCFG=\"iotopmode\",2,1\r\n",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_WAITTIME_2000MS,
        .retry = SF_CELLULAR_RETRY_VALUE_0,
        .retry_delay = SF_CELLULAR_DELAY_100MS
    },

    /** AT Command set to disable the error details */
    [SF_CELLULAR_AT_CMD_INDEX_AT_CMEE_SET_0] =
    {
        .p_cmd = (uint8_t *) "AT+CMEE=0\r\n",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_100MS
    },

    /** AT Command set to set echo off */
    [SF_CELLULAR_AT_CMD_INDEX_AT_ECHO] =
    {
        .p_cmd = (uint8_t *) "ATE0\r\n",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_100MS,
    },
    /** AT Command set to save the current setting */
    [SF_CELLULAR_AT_CMD_INDEX_AT_SAVE] =
    {
        .p_cmd = (uint8_t *) "AT&W\r\n",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },

    /** AT Command set to enter SIM pin to unlock SIM */
    [SF_CELLULAR_AT_CMD_INDEX_AT_ENTER_CPIN] =
    {
        .p_cmd = (uint8_t *) "AT+CPIN=\"",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_QCTLCATM1_CPIN_RESP_WAIT_TIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_100MS
    },
    /** AT Command set to change the SIM Pin used for lock/unlock SIM */
    [SF_CELLULAR_AT_CMD_INDEX_AT_CPIN_SET] =
    {
        .p_cmd = (uint8_t *) "AT+CPWD=\"SC\",\"",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_QCTLCATM1_CPIN_RESP_WAIT_TIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_300MS
    },
    /** AT Command set define PDP context */
    [SF_CELLULAR_AT_CMD_INDEX_AT_CGDCONT_SET] =
    {
        .p_cmd = (uint8_t *) "AT+CGDCONT=",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_10,
        .retry_delay = SF_CELLULAR_DELAY_500MS
    },
    /** AT Command set to set preferred operator list */
    [SF_CELLULAR_AT_CMD_INDEX_AT_CPOL_SET] =
    {
        .p_cmd = (uint8_t *) "AT+CPOL=",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_100MS
    },

    /** AT Command: To check module init */
       [SF_CELLULAR_AT_CMD_INDEX_AT_BAUD_CHECK] =
       {
           .p_cmd = (uint8_t *) "AT\r\n",
           .p_success_resp = (uint8_t *) "OK",
           .max_resp_length = SF_CELLULAR_STR_LEN_32,
           .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
           .retry = SF_CELLULAR_RETRY_VALUE_10,
           .retry_delay = SF_CELLULAR_DELAY_1000MS
       },

    /** AT Command set to set full function mode */
    [SF_CELLULAR_AT_CMD_INDEX_AT_AIRPLANE_OFF] =
    {
        .p_cmd = (uint8_t *) "AT+CFUN=1,0\r\n",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_QCTLCATM1_CFUN_RESP_WAIT_TIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_1,
        .retry_delay = SF_CELLULAR_DELAY_100MS
    },

    /** AT Command set to set airplane mode on */
    [SF_CELLULAR_AT_CMD_INDEX_AT_AIRPLANE_ON] =
    {
        .p_cmd = (uint8_t *) "AT+CFUN=4,0\r\n",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_QCTLCATM1_CFUN_RESP_WAIT_TIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_2000MS,
    },
    /** AT Command set to activate the PDP context */
    [SF_CELLULAR_AT_CMD_INDEX_AT_CONTEXT_ACTIVE] =
    {
        .p_cmd = (uint8_t *) "AT+CGACT=1,",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_QCTLCATM1_CONTEXT_CMDRSP_WAITITME,
        .retry = SF_CELLULAR_RETRY_VALUE_10,
        .retry_delay = SF_CELLULAR_DELAY_500MS
    },
    /** AT Command set to disable the PDP context */
    [SF_CELLULAR_AT_CMD_INDEX_AT_CONTEXT_DEACTIVE] =
    {
        .p_cmd = (uint8_t *) "AT+CGACT=0,",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_QCTLCATM1_CONTEXT_CMDRSP_WAITITME,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_300MS
    },
    /** AT Command set to start Data mode */
    [SF_CELLULAR_AT_CMD_INDEX_AT_CGDATA_ACTIVE] =
    {
        .p_cmd = (uint8_t *) "AT+CGDATA=",
        .p_success_resp = (uint8_t *) "CONNECT",
        .max_resp_length = (uint16_t) sizeof("CONNECT XXXXXXXXXX\r\nOK\r\n"),
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_500MS
    },
    /** AT Command set to close Data mode */
    [SF_CELLULAR_AT_CMD_INDEX_AT_CGDATA_DEACTIVE] =
    {
        .p_cmd = (uint8_t *) "+++",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_500MS
    },
    /** AT Command set to get the Cellular information */
    [SF_CELLULAR_AT_CMD_INDEX_AT_CSQ_GET] =
    {
        .p_cmd = (uint8_t *) "AT+CSQ\r\n",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },
    /** AT Command set to get the Cellular version */
    [SF_CELLULAR_AT_CMD_INDEX_AT_VER_GET] =
    {
        .p_cmd = (uint8_t *) "AT+GMR\r\n",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_64,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },
    /** AT Command set to get the Cellular chipset number */
    [SF_CELLULAR_AT_CMD_INDEX_AT_CHIPSET_GET] =
    {
        .p_cmd = (uint8_t *) "AT+CGMM\r\n",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },

    /** AT Command set to get the Cellular IMEI number */
    [SF_CELLULAR_AT_CMD_INDEX_AT_IMEI_GET] =
    {
        .p_cmd = (uint8_t *) "AT+GSN\r\n",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_64,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },
    /** AT Command set to get the Cellular manufacture name */
    [SF_CELLULAR_AT_CMD_INDEX_AT_MANF_NAME_GET] =
    {
        .p_cmd = (uint8_t *) "AT+GMI\r\n",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },
    /** AT Command set to get the Cellular SIM ID */
    [SF_CELLULAR_AT_CMD_INDEX_AT_SIMID_GET] =
    {
        .p_cmd = (uint8_t *) "AT+QCCID\r\n",
        .p_success_resp = (uint8_t *) "\r\n+QCCID: ",
        .max_resp_length = SF_CELLULAR_STR_LEN_64,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },
    /** AT Command set to get the Cellular network status info  */
    [SF_CELLULAR_AT_CMD_INDEX_AT_NET_STATUS_GET] =
    {
        .p_cmd = (uint8_t *) "AT+QNWINFO\r\n",
        .p_success_resp = (uint8_t *) "+QNWINFO: ",
        .max_resp_length = SF_CELLULAR_STR_LEN_128,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },

    /** AT Command set to get the current operator info */
    [SF_CELLULAR_AT_CMD_INDEX_AT_COPS_GET] =
    {
        .p_cmd = (uint8_t *) "AT+COPS?\r\n",
        .p_success_resp = (uint8_t *) "+COPS: ",
        .max_resp_length = SF_CELLULAR_STR_LEN_128,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },
    /** AT Command set to Lock the SIM */
    [SF_CELLULAR_AT_CMD_INDEX_AT_LOCK_SIM] =
    {
        .p_cmd = (uint8_t *) "AT+CLCK=\"SC\",1,\"",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_QCTLCATM1_CLCK_RESP_WAIT_TIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },
    /** AT Command set to Unlock the SIM */
    [SF_CELLULAR_AT_CMD_INDEX_AT_UNLOCK_SIM] =
    {
        .p_cmd = (uint8_t *) "AT+CLCK=\"SC\",0,\"",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_QCTLCATM1_CLCK_RESP_WAIT_TIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },

    /** AT Command set to Cellular into Data Mode */
    [SF_CELLULAR_AT_CMD_INDEX_AT_ENTER_DATA_MODE] = // Comando alterado para alterar api
    {
        .p_cmd = (uint8_t *) "ATD*99***",//"AT\r\n",
        .p_success_resp = (uint8_t *) "CONNECT ",//"OK",
        .max_resp_length = (uint16_t) sizeof("\r\nCONNECT xxxxxxxxx\r\n"),//SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_QCTLCATM1_DATMODE_RESP_WAIT_TIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },
    /** AT Command set to exit Data Mode */
    [SF_CELLULAR_AT_CMD_INDEX_AT_EXIT_DATA_MODE] =
    {
        .p_cmd = (uint8_t *) "+++",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_WAITTIME_2000MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },
    /** AT Command set to set user name */
    [SF_CELLULAR_AT_CMD_INDEX_AT_USERNAME_SET] =
    {
        .p_cmd = (uint8_t *) "AT#USERID=\"",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },
    /** AT Command set to set user password */
    [SF_CELLULAR_AT_CMD_INDEX_AT_PASSWORD_SET] =
    {
        .p_cmd = (uint8_t *) "AT#PASSW=\"",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },
    /** AT Command set to set Authentication Type */
    [SF_CELLULAR_AT_CMD_INDEX_AT_AUTH_TYPE_SET] =
    {
        .p_cmd = (uint8_t *) "AT#GAUTH=",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },
    /** AT Command set to enable auto time zone update */
    [SF_CELLULAR_AT_CMD_INDEX_AT_AUTO_TIME_UPDATE_ENABLE_SET] =
    {
        .p_cmd = (uint8_t *) "AT+CTZU=1\r\n",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },
    /** AT Command set to disable auto time zone update */
    [SF_CELLULAR_AT_CMD_INDEX_AT_AUTO_TIME_UPDATE_DISABLE_SET] =
    {
        .p_cmd = (uint8_t *) "AT+CTZU=0\r\n",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },
    /** AT Command set to set default pdp context configuration */
    [SF_CELLULAR_AT_CMD_INDEX_AT_EMPTY_APN_SET] =
    {
        .p_cmd = (uint8_t *) "AT+CGDCONT=3,\"IP\",\"\",,0,0\r\n",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },
    /** AT Command set to get the IP address */
    [SF_CELLULAR_AT_CMD_INDEX_AT_GET_IP_ADDR] =
    {
        .p_cmd = (uint8_t *) "AT+CGPADDR=",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_128,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_5,
        .retry_delay = SF_CELLULAR_DELAY_200MS
    },
    /** AT Command set to get the IMSI ID */
    [SF_CELLULAR_AT_CMD_INDEX_AT_GET_IMSI] =
    {
       .p_cmd = (uint8_t *) "AT+CIMI\r\n",
       .p_success_resp = (uint8_t *) "OK",
       .max_resp_length = SF_CELLULAR_STR_LEN_64,
	   .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
       .retry = SF_CELLULAR_RETRY_VALUE_5,
       .retry_delay = SF_CELLULAR_DELAY_200MS
    },
    /** AT Command set to set SIM priority effect */
    [SF_CELLULAR_AT_CMD_INDEX_AT_SIM_EFFECT_SET] =
    {
       .p_cmd = (uint8_t *) "AT+QCFG=\"simeffect\",0\r\n",
       .p_success_resp = (uint8_t *) "OK",
       .max_resp_length = SF_CELLULAR_STR_LEN_64,
       .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
       .retry = SF_CELLULAR_RETRY_VALUE_5,
       .retry_delay = SF_CELLULAR_DELAY_200MS
    },

    /** AT Command to set band selection */
    [SF_CELLULAR_AT_CMD_INDEX_AT_NBIOT_BAND_SELECTION] =
    {
        .p_cmd = (uint8_t *) "AT+QCFG=\"band\",0,0,",
        .p_success_resp = (uint8_t *) "OK",
        .max_resp_length = SF_CELLULAR_STR_LEN_32,
        .resp_wait_time = SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS,
        .retry = SF_CELLULAR_RETRY_VALUE_0,
        .retry_delay = SF_CELLULAR_DELAY_100MS
    }
};

/** Cellular network sequence fallback code lookup */
//static const char * g_celr_fallback_mode_lookup[] =
//{
//     [SF_CELLULAR_QCTLCATM1_NWSCANSEQ_LTECATM1_LTECATNB1_GSM] = "020301",
//     [SF_CELLULAR_QCTLCATM1_NWSCANSEQ_LTECATM1_GSM_LTECATNB1] = "020103",
//     [SF_CELLULAR_QCTLCATM1_NWSCANSEQ_GSM_LTECATNB1_LTECATM1] = "010302",
//     [SF_CELLULAR_QCTLCATM1_NWSCANSEQ_GSM_LTECATM1_LTECATNB1] = "010203",
//     [SF_CELLULAR_QCTLCATM1_NWSCANSEQ_LTECATNB1_LTECATM1_GSM] = "030201",
//     [SF_CELLULAR_QCTLCATM1_NWSCANSEQ_LTECATNB1_GSM_LTECATM1] = "030102",
//};

/***********************************************************************************************************************
 * Implementation of Interface, Here Interfaces which can achieve the functionality using generic common interface APIs
 * call related SF_CELLULAR_COMMON_* APIs implemented in sf_cellular_common module of SSP. Interfaces which require module
 * specific handling are implemented in this Quectel CATM1 specific file with initial name SF_CELLULAR_QCTLCATM1_*
 **********************************************************************************************************************/
/*LDRA_INSPECTED 27 D This structure must be accessible in user code. It cannot be static. */
const sf_cellular_api_t g_sf_cellular_on_sf_cellular_qctlcatm1 =
{
    .open                   = SF_CELLULAR_QCTLCATM1_Open,
    .close                  = SF_CELLULAR_QCTLCATM1_Close,
    .provisioningSet        = SF_CELLULAR_QCTLCATM1_ProvisioningSet,
    .provisioningGet        = SF_CELLULAR_COMMON_ProvisioningGet,
    .infoGet                = SF_CELLULAR_QCTLCATM1_InfoGet,
    .statisticsGet          = SF_CELLULAR_COMMON_StatisticsGet,
    .transmit               = SF_CELLULAR_COMMON_Transmit,
    .versionGet             = SF_CELLULAR_QCTLCATM1_VersionGet,
    .networkConnect         = SF_CELLULAR_COMMON_NetworkConnect,
    .networkDisconnect      = SF_CELLULAR_COMMON_NetworkDisconnect,
    .networkStatusGet       = SF_CELLULAR_QCTLCATM1_NetworkStatusGet,
    .simPinSet              = SF_CELLULAR_COMMON_SimPinSet,
    .simLock                = SF_CELLULAR_COMMON_SimLock,
    .simUnlock              = SF_CELLULAR_COMMON_SimUnlock,
    .simIDGet               = SF_CELLULAR_COMMON_SimIDGet,
    .commandSend            = SF_CELLULAR_COMMON_CommandSend,
    .fotaCheck              = SF_CELLULAR_COMMON_FotaCheck,
    .fotaStart              = SF_CELLULAR_COMMON_FotaStart,
    .fotaStop               = SF_CELLULAR_COMMON_FotaStop,
    .reset                  = SF_CELLULAR_QCTLCATM1_Reset,
};

/*******************************************************************************************************************//**
 * @ingroup SF_Interface_Library
 * @defgroup SF_CELLULAR_QCTLCATM1_API Cellular Framework Example using Quectel CATM1 API
 * @brief SF_CELLULAR Framework API on Quectel CATM1
 *
 * @{
 **********************************************************************************************************************/

/*************************************************************************************************************//**
 * @brief   Initialize Cellular Quectel CATM1 Cellular driver.
 *
 * Implements sf_cellular_api_t::open
 *           Initializes driver and configures module with given parameters and saves this configuration.
 *
 * @param [out] p_ctrl                      Cellular control block
 * @param [in]  p_cfg                       Cellular configuration structure
 * @retval  SSP_SUCCESS                     Driver initialization successfully.
 * @retval  SSP_ERR_ALREADY_OPEN            Cellular QuecTel CATM1 Driver is already opened.
 * @retval  SSP_ERR_CELLULAR_CONFIG_FAILED  Cellular QuecTel CATM1 module Configuration failed
 * @retval  SSP_ERR_CELLULAR_INIT_FAILED    Cellular QuecTel CATM1 module initialization failed
 * @retval  SSP_ERR_ASSERTION               Argument NULL is passed
 * @retval  SSP_ERR_CELLULAR_FAILED         Driver initialization failed
 * @retval  SSP_ERR_IN_USE                  Device already in use
 ***************************************************************************************************************/
ssp_err_t SF_CELLULAR_QCTLCATM1_Open(sf_cellular_ctrl_t *p_ctrl, sf_cellular_cfg_t const * const p_cfg)
{
    ssp_err_t retval = SSP_ERR_CELLULAR_FAILED;
    UINT status;
    sf_cellular_instance_cfg_t *p_celr_instance;
    SSP_CRITICAL_SECTION_DEFINE;

#if SF_CELLULAR_QCTLCATM1_CFG_PARAM_CHECKING_ENABLE
    SSP_ASSERT(NULL != p_ctrl);
    SSP_ASSERT(NULL != p_cfg);
    SSP_ASSERT(NULL != p_cfg->p_extend);
    SSP_ASSERT(NULL != (sf_cellular_instance_cfg_t *)p_ctrl->p_driver_handle);
#endif

    p_celr_instance = (sf_cellular_instance_cfg_t *) p_ctrl->p_driver_handle;

    SSP_CRITICAL_SECTION_ENTER;
    /** Create Mutex for Synchronization */
    status = tx_mutex_create(p_celr_instance->p_cellular_mutex, (CHAR * ) "SF_CELLULAR mutex lock", TX_INHERIT);
    SSP_CRITICAL_SECTION_EXIT;
    if ((TX_SUCCESS == status) || (TX_MUTEX_ERROR == status))
    {
        /** Get Mutex Lock */
        status = tx_mutex_get (p_celr_instance->p_cellular_mutex, SF_CELLULAR_MUTEX_GET_TIMEOUT_TICKS);
        if (TX_SUCCESS == status)
        {
            retval = sf_cellular_qctlcatm1_module_open_and_config (p_ctrl, p_cfg);

            /** The return code is not checked here because tx_mutex_put cannot fail when called with a mutex owned by the current thread.
             * The mutex is owned by the current thread because this call follows a successful call to tx_mutex_get. */
            (void) tx_mutex_put (p_celr_instance->p_cellular_mutex);
        }
        else if (TX_NOT_AVAILABLE == status)
        {
            retval = SSP_ERR_IN_USE;

        }
    }

    return retval;
}


/*************************************************************************************************************//**
 * @brief   Stop Cellular QuecTel CATM1 driver functionality.
 *
 * Implements sf_cellular_api_t::close
 *          This function deactivates the PDP context and de-initializes the lower level interface
 *
 * @param [in] p_ctrl                Cellular control block
 * @retval  SSP_SUCCESS               Cellular Driver stop successfully.
 * @retval  SSP_ERR_NOT_OPEN          Device is not opened.
 * @retval  SSP_ERR_ASSERTION         Argument NULL is passed
 * @retval  SSP_ERR_CELLULAR_FAILED   Driver un-initialization failed
 * @retval  SSP_ERR_IN_USE            Device already in use
 * @retval  SSP_ERR_CELLULAR_INVALID_STATE     Module in Data mode can't send AT command
 ***************************************************************************************************************/
ssp_err_t SF_CELLULAR_QCTLCATM1_Close(sf_cellular_ctrl_t * const p_ctrl)
{
    ssp_err_t retval = SSP_ERR_NOT_OPEN;
    UINT status;
    sf_cellular_instance_cfg_t *p_celr_instance;

#if SF_CELLULAR_QCTLCATM1_CFG_PARAM_CHECKING_ENABLE
    SSP_ASSERT(NULL != p_ctrl);
    SSP_ASSERT(NULL != p_ctrl->p_driver_handle);
#endif
    p_celr_instance = (sf_cellular_instance_cfg_t *) p_ctrl->p_driver_handle;

    if (SF_CELLULAR_TRUE == p_celr_instance->is_opened)
    {
        /** Get Mutex */
        status = tx_mutex_get (p_celr_instance->p_cellular_mutex, SF_CELLULAR_MUTEX_GET_TIMEOUT_TICKS);
        if (TX_SUCCESS == status)
        {
            if (SF_CELLULAR_FALSE == p_celr_instance->is_data_mode_on)
            {
#if SF_CELLULAR_QCTLCATM1_CFG_ONCHIP_STACK_SUPPORT
                /** De-Activate the PDP context */
                sf_cellular_qctlcatm1_set_socket_context_state (p_celr_instance,
                        p_celr_instance->prov_info.context_id,
                        SF_CELLULAR_FALSE);
#endif
                /** close module */
                retval = sf_cellular_module_close (p_celr_instance);
                if (SSP_SUCCESS == retval)
                {
                    /**  Set module open flag and delete mutex */
                    p_celr_instance->is_opened = SF_CELLULAR_FALSE;
                    /** Delete is used in close API where all resources are released,
                     * hence no need to check the return code */
                    (void) tx_mutex_delete (p_celr_instance->p_cellular_mutex);
                }
                else
                {
                    SSP_ERROR_LOG((retval), (g_module_name), (&g_sf_cellular_qctlcatm1_version));
                    /** The return code is not checked here because tx_mutex_put cannot fail when called with a mutex owned by the current thread.
                     * The mutex is owned by the current thread because this call follows a successful call to tx_mutex_get. */
                    (void) tx_mutex_put (p_celr_instance->p_cellular_mutex);
                }
            }
            else
            {
                retval = SSP_ERR_CELLULAR_INVALID_STATE;
                /** The return code is not checked here because tx_mutex_put cannot fail when called with a mutex owned by the current thread.
                 * The mutex is owned by the current thread because this call follows a successful call to tx_mutex_get. */
                (void) tx_mutex_put (p_celr_instance->p_cellular_mutex);
            }
        }
        else
        {
            SSP_ERROR_LOG((retval), (g_module_name), (&g_sf_cellular_qctlcatm1_version));
        }
    }

    return retval;
}

/*************************************************************************************************************//**
 * @brief   Sets the provisioning information.
 *
 * Implements sf_cellular_api_t::provisioningSet
 *          Sets Cellular's provisioning information
 * @param [in]  p_ctrl                  Cellular control block
 * @param [in]  p_cellular_provisioning Cellular provisioning structure
 * @retval  SSP_SUCCESS                 Successfully set the provisioning information.
 * @retval  SSP_ERR_NOT_OPEN            Device not opened
 * @retval  SSP_ERR_ASSERTION           Argument NULL is passed
 * @retval  SSP_ERR_CELLULAR_FAILED     Provisioning configuration failed
 * @retval  SSP_ERR_IN_USE              Device already in use
 * @retval  SSP_ERR_CELLULAR_INVALID_STATE     Module in Data mode can't send AT command
 ***************************************************************************************************************/
ssp_err_t SF_CELLULAR_QCTLCATM1_ProvisioningSet(sf_cellular_ctrl_t * const p_ctrl,
        sf_cellular_provisioning_t const * const p_cellular_provisioning)
{
    sf_cellular_instance_cfg_t *p_celr_instance;
    ssp_err_t retval = SSP_ERR_NOT_OPEN;
    UINT status;

#if SF_CELLULAR_QCTLCATM1_CFG_PARAM_CHECKING_ENABLE
    SSP_ASSERT(NULL != p_ctrl);
    SSP_ASSERT(NULL != p_ctrl->p_driver_handle);
    SSP_ASSERT(NULL != p_cellular_provisioning);
#endif
    p_celr_instance = (sf_cellular_instance_cfg_t *) p_ctrl->p_driver_handle;

    if (SF_CELLULAR_TRUE == p_celr_instance->is_opened)
    {
        /** Get Mutex */
        status = tx_mutex_get (p_celr_instance->p_cellular_mutex, SF_CELLULAR_MUTEX_GET_TIMEOUT_TICKS);
        if (TX_SUCCESS == status)
        {
            if (SF_CELLULAR_FALSE == p_celr_instance->is_data_mode_on)
            {
                if (p_cellular_provisioning->airplane_mode == SF_CELLULAR_AIRPLANE_MODE_ON)
                {
                    /** Set AirPlane Mode on */
                    retval = sf_cellular_provision_airplane_mode (p_ctrl, p_cellular_provisioning);
                }
                else
                {
                    retval = sf_cellular_qctlcatm1_provision_set (p_ctrl, p_cellular_provisioning);
                }
            }
            else
            {
                retval = SSP_ERR_CELLULAR_INVALID_STATE;
                SSP_ERROR_LOG((retval), (g_module_name), (&g_sf_cellular_qctlcatm1_version));
            }
            /** The return code is not checked here because tx_mutex_put cannot fail when called with a mutex owned by the current thread.
             * The mutex is owned by the current thread because this call follows a successful call to tx_mutex_get. */
            (void) tx_mutex_put (p_celr_instance->p_cellular_mutex);
        }
        else if (TX_NOT_AVAILABLE == status)
        {
            retval = SSP_ERR_IN_USE;
        }
        else
        {
            retval = SSP_ERR_CELLULAR_FAILED;
        }
    }
    else
    {
        SSP_ERROR_LOG((retval), (g_module_name), (&g_sf_cellular_qctlcatm1_version));
    }

    return retval;
}

/*************************************************************************************************************//**
 * @brief   Get Network Status information.
 *
 * Implements sf_cellular_api_t::networkStatusGet
 * @param [in]  p_ctrl                         Cellular control block
 * @param [out] p_network_status               Cellular network structure
 *
 * @retval  SSP_SUCCESS                        Successfully read the Network status information
 * @retval  SSP_ERR_NOT_OPEN                   Cellular driver is not opened
 * @retval  SSP_ERR_CELLULAR_FAILED            Failed reading Network Status information.
 * @retval  SSP_ERR_ASSERTION                  Argument NULL is passed
 * @retval  SSP_ERR_IN_USE                     Device already in use
 * @retval  SSP_ERR_CELLULAR_INVALID_STATE     Module in Data mode can't send AT command
 ***************************************************************************************************************/
ssp_err_t SF_CELLULAR_QCTLCATM1_NetworkStatusGet(sf_cellular_ctrl_t * const p_ctrl,
        sf_cellular_network_status_t *p_network_status)
{
    sf_cellular_instance_cfg_t *p_celr_instance;
    ssp_err_t retval = SSP_ERR_NOT_OPEN;
    UINT status;

#if SF_CELLULAR_QCTLCATM1_CFG_PARAM_CHECKING_ENABLE
    SSP_ASSERT(NULL != p_ctrl);
    SSP_ASSERT(NULL != p_ctrl->p_driver_handle);
    SSP_ASSERT(NULL != p_network_status);
#endif
    p_celr_instance = (sf_cellular_instance_cfg_t *) p_ctrl->p_driver_handle;

    if (SF_CELLULAR_TRUE == p_celr_instance->is_opened)
    {
        /** Get Mutex */
        status = tx_mutex_get (p_celr_instance->p_cellular_mutex, SF_CELLULAR_MUTEX_GET_TIMEOUT_TICKS);
        if (TX_SUCCESS == status)
        {
            if (SF_CELLULAR_FALSE == p_celr_instance->is_data_mode_on)
            {
                retval = sf_cellular_qctlcatm1_get_network_status_info (p_celr_instance, p_network_status);
                if (SSP_SUCCESS == retval)
                {
                    retval = sf_cellular_get_network_reg_status (p_celr_instance, &p_network_status->reg_status);
                }
            }
            else
            {
                retval = SSP_ERR_CELLULAR_INVALID_STATE;
                SSP_ERROR_LOG((retval), (g_module_name), (&g_sf_cellular_qctlcatm1_version));
            }
            /** The return code is not checked here because tx_mutex_put cannot fail when called with a mutex owned by the current thread.
             * The mutex is owned by the current thread because this call follows a successful call to tx_mutex_get. */
            (void) tx_mutex_put (p_celr_instance->p_cellular_mutex);
        }
        else if (TX_NOT_AVAILABLE == status)
        {
            retval = SSP_ERR_IN_USE;
        }
        else
        {
            retval = SSP_ERR_CELLULAR_FAILED;
        }
    }
    else
    {
        SSP_ERROR_LOG((retval), (g_module_name), (&g_sf_cellular_qctlcatm1_version));
    }

    return retval;
}

/*************************************************************************************************************//**
 * @brief   Get Cellular module information.
 *
 * Implements sf_cellular_api_t::infoGet
 *          Get Cellular module information like chipset/driver information, RSSI, noise level, link quality
 * @param [in] p_ctrl                 Cellular control block
 * @param [in] p_cellular_info                 Cellular information structure
 * @retval  SSP_SUCCESS                        Successfully get the Cellular information
 * @retval  SSP_ERR_NOT_OPEN                   Driver not opened.
 * @retval  SSP_ERR_ASSERTION                  Argument NULL is passed
 * @retval  SSP_ERR_CELLULAR_FAILED            Failed reading Cellular information
 * @retval  SSP_ERR_IN_USE                     Device already in use
 * @retval  SSP_ERR_CELLULAR_INVALID_STATE     Module in Data mode can't send AT command
 ***************************************************************************************************************/
ssp_err_t SF_CELLULAR_QCTLCATM1_InfoGet(sf_cellular_ctrl_t * const p_ctrl, sf_cellular_info_t * const p_cellular_info)
{
    sf_cellular_instance_cfg_t *p_celr_instance;
    ssp_err_t retval = SSP_ERR_NOT_OPEN;
    UINT status;

#if SF_CELLULAR_QCTLCATM1_CFG_PARAM_CHECKING_ENABLE
    SSP_ASSERT(NULL != p_ctrl);
    SSP_ASSERT(NULL != p_cellular_info);
    SSP_ASSERT(NULL != p_ctrl->p_driver_handle);
#endif

    p_celr_instance = (sf_cellular_instance_cfg_t *) p_ctrl->p_driver_handle;

    if (SF_CELLULAR_TRUE == p_celr_instance->is_opened)
    {
        /** Get Mutex */
        status = tx_mutex_get (p_celr_instance->p_cellular_mutex, SF_CELLULAR_MUTEX_GET_TIMEOUT_TICKS);
        if (TX_SUCCESS == status)
        {
            if (SF_CELLULAR_FALSE == p_celr_instance->is_data_mode_on)
            {
                retval = sf_cellular_qctlcatm1_info_get (p_celr_instance, p_cellular_info);
            }
            else
            {
                retval = SSP_ERR_CELLULAR_INVALID_STATE;
                SSP_ERROR_LOG((retval), (g_module_name), (&g_sf_cellular_qctlcatm1_version));
            }

            /** The return code is not checked here because tx_mutex_put cannot fail when called with a mutex owned by the current thread.
             * The mutex is owned by the current thread because this call follows a successful call to tx_mutex_get. */
            (void) tx_mutex_put (p_celr_instance->p_cellular_mutex);
        }
        else if (TX_NOT_AVAILABLE == status)
        {
            retval = SSP_ERR_IN_USE;
        }
        else
        {
            retval = SSP_ERR_CELLULAR_FAILED;
        }
    }
    else
    {
        SSP_ERROR_LOG((retval), (g_module_name), (&g_sf_cellular_qctlcatm1_version));
    }

    return retval;
}

/*******************************************************************************************************************//**
 * @brief      Get driver version based on compile time macros.
 *
 * Implements sf_cellular_api_t::versionGet.
 * @param [out] p_version             Common version structure
 * @retval     SSP_SUCCESS               Success.
 * @retval     SSP_ERR_ASSERTION         The parameter p_version is NULL.
 **********************************************************************************************************************/
ssp_err_t SF_CELLULAR_QCTLCATM1_VersionGet(ssp_version_t * const p_version)
{
#if SF_CELLULAR_QCTLCATM1_CFG_PARAM_CHECKING_ENABLE
    SSP_ASSERT(p_version != NULL);
#endif
    *p_version = g_sf_cellular_qctlcatm1_version;

    return SSP_SUCCESS;
}

/*************************************************************************************************************//**
 * @brief   Reset the module.
 *
 * Implements sf_cellular_api_t::reset
 *          This function reset the module as per the reset type
 *
 * @param [in]  p_ctrl                Cellular control block
 * @param [in]  reset_type            Type of reset
 * @retval  SSP_SUCCESS               Cellular Driver stop successfully.
 * @retval  SSP_ERR_NOT_OPEN          Device is not opened.
 * @retval  SSP_ERR_ASSERTION         Argument NULL is passed
 * @retval  SSP_ERR_CELLULAR_FAILED   Driver un-initialization failed
 * @retval  SSP_ERR_IN_USE            Device already in use
 * @retval  SSP_ERR_CELLULAR_INVALID_STATE     Module in Data mode can't send AT command
 * @retval SSP_ERR_CELLULAR_INIT_FAILED        Failed due to invalid response received from modem
 ***************************************************************************************************************/
ssp_err_t SF_CELLULAR_QCTLCATM1_Reset(sf_cellular_ctrl_t * const p_ctrl, sf_cellular_reset_type_t reset_type)
{
    ssp_err_t retval = SSP_ERR_NOT_OPEN;
    UINT status;
    sf_cellular_instance_cfg_t *p_celr_instance;

#if SF_CELLULAR_QCTLCATM1_CFG_PARAM_CHECKING_ENABLE
    SSP_ASSERT(NULL != p_ctrl);
    SSP_ASSERT(NULL != p_ctrl->p_driver_handle);
#endif
    p_celr_instance = (sf_cellular_instance_cfg_t *) p_ctrl->p_driver_handle;

    if (SF_CELLULAR_TRUE == p_celr_instance->is_opened)
    {
        /** Get Mutex */
        status = tx_mutex_get (p_celr_instance->p_cellular_mutex, SF_CELLULAR_MUTEX_GET_TIMEOUT_TICKS);
        if (TX_SUCCESS == status)
        {
            /** Reset module */
            if ((SF_CELLULAR_FALSE == p_celr_instance->is_data_mode_on) || (SF_CELLULAR_RESET_TYPE_HARD == reset_type))
            {
                retval = sf_cellular_module_reset (p_celr_instance, reset_type);
                if (SSP_SUCCESS != retval)
                {
                    SSP_ERROR_LOG ((retval), (g_module_name), (&g_sf_cellular_qctlcatm1_version));
                }
                else
                {
                    retval = sf_cellular_is_module_init (p_celr_instance);
                    if (SSP_SUCCESS != retval)
                    {
                        SSP_ERROR_LOG ((retval), (g_module_name), (&g_sf_cellular_qctlcatm1_version));
                    }
                    else
                    {
                        /** Check whether SIM Pin is required */
                        retval = sf_cellular_check_pin_required (p_celr_instance, p_celr_instance->p_cfg->p_sim_pin,
                                                                 p_celr_instance->p_cfg->p_puk_pin);
                    }
                }
            }
            else
            {
                retval = SSP_ERR_CELLULAR_INVALID_STATE;
                SSP_ERROR_LOG((retval), (g_module_name), (&g_sf_cellular_qctlcatm1_version));
            }

            /** The return code is not checked here because tx_mutex_put cannot fail when called with a mutex owned by the current thread.
             * The mutex is owned by the current thread because this call follows a successful call to tx_mutex_get. */
            (void) tx_mutex_put (p_celr_instance->p_cellular_mutex);
        }
        else if (TX_NOT_AVAILABLE == status)
        {
            retval = SSP_ERR_IN_USE;
        }
        else
        {
            retval = SSP_ERR_CELLULAR_FAILED;
        }
    }

    return retval;
}

/**********************************************************************************************************************
 * Private function definitions
 **********************************************************************************************************************/

/*************************************************************************************************************//**
 * @brief   Initialize Cellular driver and set the configuration.
 *
 * @param [in]  p_ctrl                      Cellular control block
 * @param [in]  p_cfg                       Cellular configuration structure
 * @retval  SSP_SUCCESS                     Driver initialization successfully.
 * @retval  SSP_ERR_ALREADY_OPEN            Cellular driver is already opened.
 * @retval  SSP_ERR_CELLULAR_INIT_FAILED    Cellular module initialization failed
 * @retval  SSP_ERR_CELLULAR_FAILED         Driver initialization failed
 * @retval SSP_ERR_CELLULAR_CONFIG_FAILED   Failed Cellular configuration
 ***************************************************************************************************************/
static ssp_err_t sf_cellular_qctlcatm1_module_open_and_config(sf_cellular_ctrl_t *p_ctrl, sf_cellular_cfg_t const * const p_cfg)
{
    /** Cellular Driver configuration structure */
    sf_cellular_instance_cfg_t *p_celr_instance;
    ssp_err_t retval = SSP_SUCCESS;

    p_celr_instance = (sf_cellular_instance_cfg_t *) p_ctrl->p_driver_handle;

    retval = sf_cellular_qctlcatm1_module_open (p_ctrl, p_cfg);
    if (SSP_SUCCESS == retval)
    {
        /** Set Cellular configuration */
        retval = sf_cellular_qctlcatm1_config_set (p_ctrl, p_cfg);
        if (SSP_SUCCESS == retval)
        {
            p_celr_instance->is_opened = SF_CELLULAR_TRUE;
        }
        else
        {
            p_celr_instance->is_opened = SF_CELLULAR_FALSE;
        }
    }

    return retval;
}

/***************************************************************************************************************//**
 * @brief Set Cellular device interface configuration.
 *
 * @param [in] p_ctrl                       Pointer to cellular control block
 * @param [in] p_cfg                        Pointer to Cellular configuration
 * @retval SSP_SUCCESS                      Success
 * @retval SSP_ERR_CELLULAR_CONFIG_FAILED   Failed Cellular configuration
 * @retval SSP_ERR_CELLULAR_INIT_FAILED     Failed due to invalid response received from modem
 *******************************************************************************************************************/
ssp_err_t sf_cellular_qctlcatm1_config_set(sf_cellular_ctrl_t * p_ctrl, sf_cellular_cfg_t const * p_cfg)
{
    /** Local configuration variable */
    ssp_err_t result;
    ssp_err_t retval = SSP_ERR_CELLULAR_CONFIG_FAILED;
    sf_cellular_instance_cfg_t * p_celr_instance = p_ctrl->p_driver_handle;

    /** Set Preferred Operators List */
    result = sf_cellular_set_preferred_operator (p_celr_instance, p_cfg->pref_ops, p_cfg->num_pref_ops);
    if (SSP_SUCCESS == result)
    {
        /** Set Cellular operator select mode, in case of manual set operator details */
        result = sf_cellular_set_operator_selection_mode (p_celr_instance, p_celr_instance->p_cfg->op,
                                                          p_celr_instance->p_cfg->op_select_mode);
        if (SSP_SUCCESS == result)
        {
            sf_cellular_msec_delay (SF_CELLULAR_DELAY_50MS);

            /** Set cellular SIM priority effect. Due to the unavailability of SIM priority effect
             *  command in all the BG96 firmwares do not check the return value of this API  */
            //(void) sf_cellular_set_sim_priority_effect (p_celr_instance);

            /** set cellular network fallback sequence */
            //result = sf_cellular_set_fallback_sequence (p_celr_instance);
            //if (SSP_SUCCESS == result)
            //{
                /** Set TimeZone update mode policy */
                result = sf_cellular_set_timezone_update_policy (p_celr_instance, p_cfg->tz_upd_mode);
                if (SSP_SUCCESS == result)
                {
                    retval = SSP_SUCCESS;
                }
            //}
        }
    }

    return retval;
}
/*************************************************************************************************************//**
 * @brief   Set Provisioning configuration
 *
 * @param [in] p_ctrl                    Pointer to cellular control block
 * @param [in] p_cellular_provisioning   Pointer to cellular provisioning configuration
 *
 * @retval SSP_SUCCESS                             Success
 * @retval SSP_ERR_CELLULAR_FAILED                 Failed setting provisioning configuration
 * @retval SSP_ERR_CELLULAR_REGISTRATION_FAILED    Network Registration failed
 ********************************************************************************************************************/
static ssp_err_t sf_cellular_qctlcatm1_provision_set(sf_cellular_ctrl_t * p_ctrl,
        sf_cellular_provisioning_t const * p_cellular_provisioning)
{
    ssp_err_t retval = SSP_ERR_CELLULAR_FAILED;
    ssp_err_t result;
    sf_cellular_instance_cfg_t *p_celr_instance;
    sf_cellular_provisioning_t *p_local_prov;

    p_celr_instance = p_ctrl->p_driver_handle;

    p_local_prov = &(p_celr_instance->prov_info);

#if SF_CELLULAR_QCTLCATM1_CFG_ONCHIP_STACK_SUPPORT
    /** Initialize Socket Interface global variable */
    (void)sf_cellular_qctlcatm1_socket_init ((sf_cellular_instance_cfg_t *) p_ctrl->p_driver_handle);
#endif


    /** Set functionality mode */
    result = sf_cellular_set_phone_functionality (p_celr_instance, p_cellular_provisioning->airplane_mode);
    if (SSP_SUCCESS == result)
    {
#if SF_CELLULAR_QCTLCATM1_CFG_ONCHIP_STACK_SUPPORT
        /** Define PDP Socket Context */
        result = sf_cellular_qctlcatm1_define_pdp_context (p_celr_instance, p_cellular_provisioning);
#else
        /** Define PDP context */
        result = sf_cellular_define_pdp_context (p_celr_instance, p_cellular_provisioning->apn,
                                                 p_cellular_provisioning->context_id,
                                                 p_cellular_provisioning->pdp_type);
#endif
        if (SSP_SUCCESS == result)
        {
            /** Check for successful registration */
            result = sf_cellular_check_network_registration_status (p_celr_instance);
            if (SSP_SUCCESS == result)
            {
#if SF_CELLULAR_QCTLCATM1_CFG_ONCHIP_STACK_SUPPORT
                /** Activate the Socket PDP context */
                result = sf_cellular_qctlcatm1_set_socket_context_state (p_celr_instance,
                        p_cellular_provisioning->context_id,
                        SF_CELLULAR_TRUE);
                if (SSP_SUCCESS == result)
#endif
                {
                    retval = SSP_SUCCESS;
                }/* End of Socket PDP Context */
            } /* End of registration status check */
        } /* End of Define PDP context */
    } /* End of setting functionality mode */

    if (SSP_SUCCESS == retval)
    {
        /** Copy provisioning info to driver structure */
        (void) memcpy (p_local_prov->apn, p_cellular_provisioning->apn, sizeof(p_local_prov->apn));
        (void) memcpy (p_local_prov->username, p_cellular_provisioning->username, sizeof(p_local_prov->username));
        (void) memcpy (p_local_prov->password, p_cellular_provisioning->username, sizeof(p_local_prov->password));
        p_local_prov->auth_type = p_cellular_provisioning->auth_type;
        p_local_prov->airplane_mode = p_cellular_provisioning->airplane_mode;
        p_local_prov->context_id = p_cellular_provisioning->context_id;
        p_local_prov->pdp_type = p_cellular_provisioning->pdp_type;
    }

    return retval;
}

/*****************************************************************************************************************//**
 * @brief   Read Cellular information
 *
 * @param [in] p_celr_instance       Pointer to cellular instances configuration
 * @param [in] p_cellular_info       Pointer to cellular information
 * @retval SSP_SUCCESS              Success
 * @retval SSP_ERR_CELLULAR_FAILED  Reading information failed
 ********************************************************************************************************************/
static ssp_err_t sf_cellular_qctlcatm1_info_get(sf_cellular_instance_cfg_t * p_celr_instance, sf_cellular_info_t * const p_cellular_info)
{
    ssp_err_t result;
    ssp_err_t retval = SSP_ERR_CELLULAR_FAILED;

    /**
     * Command Used
     * AT#CGSN=1  - reads IMEI
     * AT+CSQ       reads RSSI and BER
     */

    memset (p_cellular_info, 0, sizeof(sf_cellular_info_t));

    /** Read firmware information */
    result = sf_cellular_get_firmware_version (p_celr_instance, &p_cellular_info->fw_version[0]);
    if (SSP_SUCCESS == result)
    {
        /** Read Manufacturer information */
        result = sf_cellular_get_mfg_name (p_celr_instance, &p_cellular_info->mfg_name[0]);
        if (SSP_SUCCESS == result)
        {
            /** Read IMEI */
            result = sf_cellular_get_imei (p_celr_instance, &p_cellular_info->imei[0]);
            if (SSP_SUCCESS == result)
            {
                /** Read module information */
                result = sf_cellular_get_signal_quality (p_celr_instance, &(p_cellular_info->rssi),
                                                         &(p_cellular_info->ber));
                if (SSP_SUCCESS == result)
                {
                    /** Read IP address */
                    result = sf_cellular_get_ip_addr (p_celr_instance, &p_cellular_info->ip_addr[0]);
                    if (SSP_SUCCESS == result)
                    {
                        /** Read Chipset info */
                        result = sf_cellular_get_chipset_info (p_celr_instance, &p_cellular_info->chipset[0]);
                        if (SSP_SUCCESS == result)
                        {
                            retval = SSP_SUCCESS;
                        }

                    }
                }
            }
        }
    }

    return retval;
}

/****************************************************************************************************************//**
 * @brief   Read Network Status information
 *
 * @param [in]  p_celr_instance            Pointer to cellular instances
 * @param [out] p_network_status_info      Pointer to network status information structure
 * @retval SSP_SUCCESS              Read information correctly
 * @retval SSP_ERR_CELLULAR_FAILED  Read failed
 *******************************************************************************************************************/
static ssp_err_t sf_cellular_qctlcatm1_get_network_status_info(sf_cellular_instance_cfg_t * p_celr_instance,
        sf_cellular_network_status_t * p_network_status_info)
{
    ssp_err_t result;
    ssp_err_t retval = SSP_ERR_CELLULAR_FAILED;

    memset (p_network_status_info, 0, sizeof(sf_cellular_network_status_t));

    /** Read operator name in alphanumeric format */
    result = sf_cellular_qctlcatm1_get_operator_info (p_celr_instance, p_network_status_info);
    if (SSP_SUCCESS == result)
    {
        /** Read International Mobile Subscriber Identity ID */
        result = sf_cellular_qctlcatm1_get_imsi (p_celr_instance, &p_network_status_info->imsi[0]);
        if (SSP_SUCCESS == result)
        {
            /** Read network status information */
            result = sf_cellular_qctlcatm1_get_network_status (p_celr_instance, p_network_status_info);
            if (SSP_SUCCESS == result)
            {
                retval = SSP_SUCCESS;
            }
        }
    }

    return retval;
}

/****************************************************************************************************************//**
 * @brief   Read operator information
 *
 * @param [in]  p_celr_instance            Pointer to cellular instances
 * @param [out] p_network_status_info      Pointer to network status information structure
 * @retval SSP_SUCCESS              Read information correctly
 * @retval SSP_ERR_CELLULAR_FAILED  Read failed
 *******************************************************************************************************************/
static ssp_err_t sf_cellular_qctlcatm1_get_operator_info(sf_cellular_instance_cfg_t * p_celr_instance,
        sf_cellular_network_status_t * p_network_status_info)
{
    uint8_t resp_buff[SF_CELLULAR_RFSTS_RSP_LEN];
    uint16_t bytes_read_write = 0U;
    ssp_err_t result = SSP_ERR_CELLULAR_FAILED;
    uint8_t retry_count = 0U;
    sf_cellular_at_cmd_set_t const *p_cmd_set = p_celr_instance->p_cfg->p_cmd_set;
    uint8_t expected_resp_present;

    do
    {
        bytes_read_write = (uint16_t) strlen ((const char *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_COPS_GET].p_cmd);

        /** Send command to read network status */
        sf_cellular_serial_write (p_celr_instance,
                               (uint8_t const *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_COPS_GET].p_cmd,
                               bytes_read_write);

        /** Expected bytes of successful response */
        bytes_read_write = p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_COPS_GET].max_resp_length;

        /** clear buffer */
        memset (resp_buff, 0, sizeof(resp_buff));

        /** Read response */
        result = sf_cellular_serial_read (p_celr_instance, resp_buff, &bytes_read_write, SF_CELLULAR_READ_TIMEOUT_MS);
        expected_resp_present = sf_cellular_is_str_present (
                (const char *) resp_buff,
                (const char *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_COPS_GET].p_success_resp);
        if ((SSP_SUCCESS == result) && (SF_CELLULAR_TRUE == expected_resp_present))
        {
            result = sf_cellular_qctlcatm1_cops_status_response_handling (resp_buff, p_network_status_info);
            if (SSP_SUCCESS == result)
            {
                break;
            }
        }
        /** Reset error value */
        result = SSP_ERR_CELLULAR_FAILED;

        /** Delay for next try */
        sf_cellular_msec_delay (p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_COPS_GET].retry_delay);
        ++retry_count;
    }
    while (retry_count < p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_COPS_GET].retry);

    return result;
}

/****************************************************************************************************************//**
 * @brief   Read network information
 *
 * @param [in]  p_celr_instance            Pointer to cellular instances
 * @param [out] p_network_status_info      Pointer to network status information structure
 * @retval SSP_SUCCESS              Read information correctly
 * @retval SSP_ERR_CELLULAR_FAILED  Read failed
 *******************************************************************************************************************/
static ssp_err_t sf_cellular_qctlcatm1_get_network_status(sf_cellular_instance_cfg_t * p_celr_instance,
        sf_cellular_network_status_t * p_network_status_info)
{
    uint8_t resp_buff[SF_CELLULAR_STR_LEN_128];
    uint16_t bytes_read_write = 0U;
    ssp_err_t result = SSP_ERR_CELLULAR_FAILED;
    uint8_t retry_count = 0U;
    sf_cellular_at_cmd_set_t const *p_cmd_set = p_celr_instance->p_cfg->p_cmd_set;
    uint8_t expected_resp_present;
    uint32_t resp_wait_ticks;

    do
    {
        bytes_read_write = (uint16_t) strlen ((const char *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NET_STATUS_GET].p_cmd);

        /** Send command to read network status */
        sf_cellular_serial_write (p_celr_instance,
                               (uint8_t const *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NET_STATUS_GET].p_cmd,
                               bytes_read_write);

        /** Expected bytes of successful response */
        bytes_read_write = p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NET_STATUS_GET].max_resp_length;

        /** clear buffer */
        memset (resp_buff, 0, sizeof(resp_buff));

        /** Get response wait time in ticks */
        resp_wait_ticks = sf_cellular_common_getticks_for_mseconds (
                p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NET_STATUS_GET].resp_wait_time);

        /** Read response */
        result = sf_cellular_serial_read (p_celr_instance, resp_buff, &bytes_read_write, resp_wait_ticks);
        expected_resp_present = sf_cellular_is_str_present (
                (const char *) resp_buff,
                (const char *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NET_STATUS_GET].p_success_resp);
        if ((SSP_SUCCESS == result) && (SF_CELLULAR_TRUE == expected_resp_present))
        {
            result = sf_cellular_qctlcatm1_network_status_response_handling (resp_buff, p_network_status_info);
            if (SSP_SUCCESS == result)
            {
                break;
            }
        }
        /** Reset error value */
        result = SSP_ERR_CELLULAR_FAILED;

        /** Delay for next try */
        sf_cellular_msec_delay (p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NET_STATUS_GET].retry_delay);
        ++retry_count;
    }
    while (retry_count < p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NET_STATUS_GET].retry);

    return result;
}

/*****************************************************************************************************************//**
 * @brief   Read IMSI ID
 *
 * @param [in]  p_celr_instance  Pointer to cellular instance
 * @param [out] p_imsi           IMSI ID
 * @retval SSP_SUCCESS                 Read IMSI successfully
 * @retval SSP_ERR_CELLULAR_FAILED     Reading IMSI failed
 *********************************************************************************************************************/
ssp_err_t sf_cellular_qctlcatm1_get_imsi(sf_cellular_instance_cfg_t * p_celr_instance, uint8_t * p_imsi)
{
    uint8_t resp_buff[SF_CELLULAR_STR_LEN_32] =
    { SF_CELLULAR_NULL_CHAR };
    uint16_t bytes_read_write = 0U;
    ssp_err_t result = SSP_ERR_CELLULAR_FAILED;
    uint8_t * p_index = NULL;
    uint8_t imsi_length;
    uint8_t retry_count = 0U;
    sf_cellular_at_cmd_set_t const * p_cmd_set = p_celr_instance->p_cfg->p_cmd_set;
    uint8_t expected_resp_present;
	uint32_t resp_wait_ticks;

    /**
     * AT Command used
     * AT+CIMI    - Read IMSI
     */

     /** Get response wait time in ticks */
     resp_wait_ticks = sf_cellular_common_getticks_for_mseconds (
	                   p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_GET_IMSI].resp_wait_time);
    do
    {
        bytes_read_write = (uint16_t) strlen ((const char *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_GET_IMSI].p_cmd);

        /** Send command to read IMEI number */
        sf_cellular_serial_write (p_celr_instance, (uint8_t const *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_GET_IMSI].p_cmd,
                               bytes_read_write);

        /** Expected bytes of successful response */
        bytes_read_write = p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_GET_IMSI].max_resp_length;

        /** Clear response buffer */
        memset (resp_buff, 0, sizeof(resp_buff));

        /** Read command response */
        memset (resp_buff, 0, sizeof(resp_buff));
        result = sf_cellular_serial_read (p_celr_instance, resp_buff, &bytes_read_write, resp_wait_ticks);
        if ((SSP_SUCCESS == result) || (SSP_ERR_TIMEOUT == result))
        {
            expected_resp_present = sf_cellular_is_str_present (
                    (const char *) resp_buff,
                    (const char *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_GET_IMSI].p_success_resp);
            if (SF_CELLULAR_TRUE == expected_resp_present)
            {
                /** Ignore first 2 bytes of resp_buff which contains newline ascii character */
                p_index = (uint8_t *) strstr ((const char *) &(resp_buff[2]), SF_CELLULAR_CRLF);
                if (p_index)
                {
                    *p_index = SF_CELLULAR_NULL_CHAR;
                    imsi_length = (uint8_t) (p_index - &(resp_buff[2]));
                    memcpy (p_imsi, &(resp_buff[2]), imsi_length);
                    result = SSP_SUCCESS;
                    break;
                }
            }
        }
        /** Reset result value */
        result = SSP_ERR_CELLULAR_FAILED;

        /** Delay for next try */
        sf_cellular_msec_delay (p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_GET_IMSI].retry_delay);
        ++retry_count;
    }
    while (retry_count < p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_GET_IMSI].retry);

    return result;
}

/******************************************************************************************************************//**
 * @brief Parse AT+COPS response and store information in passed structure
 *
 * @param [in]  p_buffer               Response buffer to parse
 * @param [out] p_network_status_info  Pointer to Network Status structure
 * @retval SSP_SUCCESS                      Parse information correctly
 * @retval SSP_ERR_CELLULAR_FAILED          Parse failed
 **********************************************************************************************************************/
static ssp_err_t sf_cellular_qctlcatm1_cops_status_response_handling(uint8_t * p_buffer,
        sf_cellular_network_status_t * p_network_status_info)
{
    uint8_t *p_str1 = NULL;
    uint8_t *p_token = NULL;
    uint8_t *p_saveptr1 = NULL;
    ssp_err_t result = SSP_ERR_CELLULAR_FAILED;

    p_str1 = (uint8_t *) strstr ((const char *) p_buffer, SF_CELLULAR_QCTLCATM1_COPS_STRING);
    if (NULL != p_str1)
    {
        /** Advance the Token pointer to point the network status */
        p_str1 += strlen (SF_CELLULAR_QCTLCATM1_COPS_STRING);

        /** process cops response and parse it's field */
        p_token = (uint8_t *) strtok_r ((char *) p_str1, SF_CELLULAR_CRLF, (char **) &p_saveptr1);
        if (NULL != p_token)
        {
            /** Read network status information */
            result = sf_cellular_qctlcatm1_parse_cops_status_response (p_token, p_network_status_info);
        }
    }

    return result;
}

/****************************************************************************************************************//**
 * @brief Parse Operator status response
 * @param [in]  p_token                 Token string to parse
 * @param [out] p_network_status_info   Pointer to Network status information structure
 *
 * @retval SSP_SUCCESS             Successfully read data
 * @retval SSP_ERR_CELLULAR_FAILED Failed to parse Network status response
 ********************************************************************************************************************/
static ssp_err_t sf_cellular_qctlcatm1_parse_cops_status_response(uint8_t * p_token,
        sf_cellular_network_status_t * p_network_status_info)
{
    uint8_t *p_str2 = NULL;
    uint8_t *p_subtoken = NULL;
    uint8_t *p_saveptr2 = NULL;
    uint16_t loc = 0U;
    ssp_err_t retval = SSP_SUCCESS;

    if ((p_token != NULL) && (p_network_status_info != NULL))
    {
        p_str2 = p_token;
        for (; loc <= SF_CELLULAR_SINR_LTE; loc++)
        {
            /** Increment loc if token is empty */
            if ((NULL != p_saveptr2) && (SF_CELLULAR_QCTLCATM1_COMMA_ASCII == (*p_saveptr2)))
            {
                ++p_saveptr2;
                loc++;
            }
            if (loc == 0U)
            {
                p_subtoken = (uint8_t *) strtok_r ((char *) p_str2, (const char *) ",", (char **) &p_saveptr2);
            }
            else
            {
                p_subtoken = (uint8_t *) strtok_r ((char *) NULL, (const char *) ",", (char **) &p_saveptr2);
            }

            if (p_subtoken == NULL)
            {
                break;
            }

            sf_cellular_qctlcatm1_cops_response_switch_handling (p_network_status_info, loc, p_subtoken);
        }
    }
    else
    {
        retval = SSP_ERR_CELLULAR_FAILED;
    }
    return retval;
}

/******************************************************************************************************************//**
 * @brief Parsing of Operator status response
 * @param [out] p_network_status_info       Pointer to network status information structure
 * @param [in]  loc                         Location index
 * @param [in]  p_subtoken                  Response token string
 * @retval void
 **********************************************************************************************************************/
static void sf_cellular_qctlcatm1_cops_response_switch_handling(sf_cellular_network_status_t *p_network_status_info,
        uint16_t loc, uint8_t *p_subtoken)
{
    if (SF_CELLULAR_QCTLCATM1_OP_NAME_INDEX == loc)
    {
        uint8_t *p_saveptr = NULL;
        uint8_t *p_subtoken1 = NULL;

        p_subtoken++; ///< Ignore first double quote '"'
        p_subtoken1 = (uint8_t *) strtok_r ((char *) p_subtoken, (const char *) "\"", (char **) &p_saveptr);
        if (NULL != p_subtoken1)
        {
            strncpy ((char *) p_network_status_info->op_name, (const char *) p_subtoken1,
                            sizeof (p_network_status_info->op_name)-1);
        }
    }
}

/******************************************************************************************************************//**
 * @brief Parse network status response and store information in passed structure
 *
 * @param [in]  p_buffer               Response buffer to parse
 * @param [out] p_network_status_info  Pointer to Network Status structure
 * @retval SSP_SUCCESS                      Parse information correctly
 * @retval SSP_ERR_CELLULAR_FAILED          Parse failed
 **********************************************************************************************************************/
static ssp_err_t sf_cellular_qctlcatm1_network_status_response_handling(uint8_t * p_buffer,
        sf_cellular_network_status_t * p_network_status_info)
{
    uint8_t *p_str1 = NULL;
    uint8_t *p_token = NULL;
    uint8_t *p_saveptr1 = NULL;
    ssp_err_t result = SSP_ERR_CELLULAR_FAILED;

    p_str1 = (uint8_t *) strstr ((const char *) p_buffer, SF_CELLULAR_QCTLCATM1_QNWINFO_STRING);
    if (NULL != p_str1)
    {
        /** Advance the Token pointer to point the network status */
        p_str1 += strlen (SF_CELLULAR_QCTLCATM1_QNWINFO_STRING);

        /** process cops response and parse it's field */
        p_token = (uint8_t *) strtok_r ((char *) p_str1, SF_CELLULAR_CRLF, (char **) &p_saveptr1);
        if (NULL != p_token)
        {
            /** Read network status information */
            result = sf_cellular_qctlcatm1_parse_network_status_response (p_token, p_network_status_info);
        }
    }

    return result;
}

/*****************************************************************************************************************//**
 * @brief Parse Network status response string
 * @param [in]  p_token                 Token string to parse
 * @param [out] p_network_status_info   Pointer to Network status information structure
 *
 * @retval SSP_SUCCESS             Successfully read data
 * @retval SSP_ERR_CELLULAR_FAILED Failed to parse Network status response
 ********************************************************************************************************************/
static ssp_err_t sf_cellular_qctlcatm1_parse_network_status_response(uint8_t * p_token,
        sf_cellular_network_status_t * p_network_status_info)
{
    uint8_t *p_str2 = NULL;
    uint8_t *p_subtoken = NULL;
    uint8_t *p_saveptr2 = NULL;
    uint16_t loc = 0U;
    ssp_err_t retval = SSP_SUCCESS;

    if ((NULL != p_token) && (NULL != p_network_status_info))
    {
        p_str2 = p_token;
        for (;; loc++)
        {
            /** Increment loc if token is empty */
            if ((NULL != p_saveptr2) && (SF_CELLULAR_QCTLCATM1_COMMA_ASCII == (*p_saveptr2)))
            {
                ++p_saveptr2;
                loc++;
            }
            if (0U == loc)
            {
                p_subtoken = (uint8_t *) strtok_r ((char *) p_str2, (const char *) ",", (char **) &p_saveptr2);
            }
            else
            {
                p_subtoken = (uint8_t *) strtok_r ((char *) NULL, (const char *) ",", (char **) &p_saveptr2);
            }

            if (NULL == p_subtoken)
            {
                break;
            }

            sf_cellular_qctlcatm1_network_response_switch_handling (p_network_status_info, loc, p_subtoken);
        }
    }
    else
    {
        retval = SSP_ERR_CELLULAR_FAILED;
    }
    return retval;
}

/******************************************************************************************************************//**
 * @brief Parsing of network status response
 * @param [out] p_network_status_info       Pointer to network status information structure
 * @param [in]  loc                         Location index
 * @param [in]  p_subtoken                  Response token string
 * @retval void
 **********************************************************************************************************************/
static void sf_cellular_qctlcatm1_network_response_switch_handling(sf_cellular_network_status_t *p_network_status_info,
        uint16_t loc, uint8_t *p_subtoken)
{
    uint8_t *p_saveptr = NULL;
    uint8_t *p_subtoken1 = NULL;
    uint8_t search_result;

    switch(loc)
    {
        case SF_CELLULAR_QCTLCATM1_ACCESS_TECH_INDEX:
            p_subtoken++; ///< Ignore first double quote '"'
            search_result = sf_cellular_is_str_present ((const char *) p_subtoken, (const char *) "\"");
            if (SF_CELLULAR_TRUE == search_result)
            {
                p_subtoken1 = (uint8_t *) strtok_r ((char *) p_subtoken, (const char *) "\"", (char **) &p_saveptr);
                if (NULL != p_subtoken1)
                {
                    strcpy ((char *) p_network_status_info->access_tech_name, (const char *) p_subtoken1);
                }
            }
            else
            {
                /** No network */
                strcpy ((char *) p_network_status_info->access_tech_name, SF_CELLULAR_ACCESS_TECH_UNKNOWN);
            }
            break;

        case SF_CELLULAR_QCTLCATM1_OP_CODE_INDEX:
            p_subtoken++; ///< Ignore first double quote '"'
            p_subtoken1 = (uint8_t *) strtok_r ((char *) p_subtoken, (const char *) "\"", (char **) &p_saveptr);
            if (NULL != p_subtoken1)
            {
                p_network_status_info->operator_code = (uint32_t)atol((const char *)p_subtoken1);
            }
            break;

        default:
            break;
    }
}

/*************************************************************************************************************//**
 * @brief   Initialize Cellular QuecTel CATM1 Cellular driver.
 *
 * @param [in]  p_ctrl                      Cellular control block
 * @param [in]  p_cfg                       Cellular configuration structure
 * @retval  SSP_SUCCESS                     Driver initialization successfully.
 * @retval  SSP_ERR_ALREADY_OPEN            Cellular QuecTel CATM1 Driver is already opened.
 * @retval  SSP_ERR_CELLULAR_INIT_FAILED    Cellular QuecTel CATM1 module initialization failed
 * @retval  SSP_ERR_CELLULAR_FAILED         Driver initialization failed
 ***************************************************************************************************************/
static ssp_err_t sf_cellular_qctlcatm1_module_open(sf_cellular_ctrl_t *p_ctrl, sf_cellular_cfg_t const * const p_cfg)
{
    /** Cellular Driver configuration structure */
    sf_cellular_instance_cfg_t *p_celr_instance;
    ssp_err_t retval = SSP_SUCCESS;

    p_celr_instance = (sf_cellular_instance_cfg_t *) p_ctrl->p_driver_handle;

    /** Initialize the driver if not initialized already */
    if (SF_CELLULAR_FALSE == p_celr_instance->init_done)
    {
        /** clear static memory content */
        (void) memset (&(((sf_cellular_instance_cfg_t *) p_ctrl->p_driver_handle)->celr_stats), 0,
                       sizeof(sf_cellular_stats_t));
        (void) memset (&(((sf_cellular_instance_cfg_t *) p_ctrl->p_driver_handle)->prov_info), 0,
                       sizeof(sf_cellular_provisioning_t));

        p_celr_instance->p_cfg = p_cfg;

        /** Configure low level communication instances */
        retval = sf_cellular_serial_init (p_celr_instance, p_cfg);
        if (SSP_SUCCESS == retval)
        {
            /** Hard reset cellular module */
            retval = sf_cellular_module_reset (p_celr_instance, SF_CELLULAR_RESET_TYPE_HARD);
            if (SSP_SUCCESS == retval)
            {
                /** Check if module is responding to the baud rate set in ISDE configuration */
                retval = sf_cellular_is_module_responding (p_celr_instance, SF_CELLULAR_DELAY_1000MS,
                                                           SF_CELLULAR_RETRY_VALUE_10);
                if (SSP_SUCCESS != retval)
                {
                    /** Module is not responding so detect the baud rate
                     *  at which modem is operating and update the modem
                     *  baud rate as per ISDE configuration*/
                    retval = sf_cellular_update_baud_rate (p_celr_instance);
                }

                if (SSP_SUCCESS == retval)
                {
                    /** Delay for Module to get registered */
                    sf_cellular_msec_delay (SF_CELLULAR_QCTLCATM1_INIT_DELAY);
                    /** Initialize Modem Driver */
                    retval = sf_cellular_qctlcatm1_driver_init (p_celr_instance, p_cfg->p_sim_pin, p_cfg->p_puk_pin);
                    if (SSP_SUCCESS == retval)
                    {
                        p_celr_instance->init_done = SF_CELLULAR_TRUE;
                    }
                    else
                    {
                        sf_cellular_serial_deinit (p_celr_instance);
                        /** Cellular Initialization failed */
                        retval = SSP_ERR_CELLULAR_INIT_FAILED;
                    }
                }
            }
        }
    }
    else
    {
        /** Driver is already initialized, check whther it is already opened */
        if (SF_CELLULAR_TRUE == p_celr_instance->is_opened)
        {
            /** Driver already opened */
            retval = SSP_ERR_ALREADY_OPEN;
        }
    }

    return retval;
}

/******************************************************************************************************************//**
 * @brief  Driver initialization routine
 *
 * @param [in] p_celr_instance      Pointer to cellular instance
 * @param [in] p_sim_pin            Sim PIN number
 * @param [in] p_puk_pin            Sim PUK number
 * @retval SSP_SUCCESS                     Driver initialization successful
 * @retval SSP_ERR_CELLULAR_INIT_FAILED    Driver initialization failed
 *********************************************************************************************************************/
static ssp_err_t sf_cellular_qctlcatm1_driver_init(sf_cellular_instance_cfg_t * p_celr_instance, uint8_t * const p_sim_pin,
        uint8_t * const p_puk_pin)
{
    ssp_err_t result;
    sf_cellular_command_parameters_info_t * p_cmd_param_info = {0U};
    uint8_t i = 0U;
    uint8_t cmd_num_index = 0U;

    /** Initialize module */
    result = sf_cellular_check_module_init (p_celr_instance);
    if (SSP_SUCCESS == result)
    {
        /** Check if user has specified callback to update ISDE configured AT command retry delay and retry count */
        if (p_celr_instance->p_cfg->p_cmd_param_callback)
        {
            /** Call AT command info callback */
            result = p_celr_instance->p_cfg->p_cmd_param_callback (&p_cmd_param_info, &cmd_num_index);
            if (SSP_SUCCESS == result)
            {
                /** Run a loop for the number of AT commands defined by user */
                for (i = 0U; i < cmd_num_index; i++)
                {
                    p_celr_instance->p_cfg->p_modifiable_cmd_set[p_cmd_param_info[i].cmd_index].retry =
                            p_cmd_param_info[i].retry_count;
                    p_celr_instance->p_cfg->p_modifiable_cmd_set[p_cmd_param_info[i].cmd_index].retry_delay =
                            p_cmd_param_info[i].retry_delay;
                }
            }
        }
        /** Check whether SIM Pin is required */
        result = sf_cellular_check_pin_required (p_celr_instance, p_sim_pin, p_puk_pin);
    }

    return result;
}

/*******************************************************************************************************************//**
 * @brief Set Cellular IOT Operation Mode
 *
 * @param [in]  p_celr_instance      Pointer to cellular instance
 * @retval SSP_SUCCESS                  Successfully set the IOT Operation mode
 * @retval SSP_ERR_CELLULAR_FAILED      Failed to set IOT Operation mode
 **********************************************************************************************************************/
//static ssp_err_t sf_cellular_set_iotopmode(sf_cellular_instance_cfg_t * const p_celr_instance)
//{
//    ssp_err_t result;
//    uint16_t bytes_read = 0U;
//    uint16_t bytes_write;
//    uint8_t resp_buff[SF_CELLULAR_STR_LEN_32] =
//    { SF_CELLULAR_NULL_CHAR };
//    uint8_t retry_count = 0U;
//    uint8_t res_string_present;
//    ssp_err_t retval = SSP_ERR_CELLULAR_FAILED;
//    sf_cellular_at_cmd_set_t const * p_cmd_set = p_celr_instance->p_cfg->p_cmd_set;
//    uint32_t resp_wait_ticks;
//
//    /**
//     * Command Used
//     * AT+QCFG="iotopmode",2,1
//     */
//
//    bytes_write = (uint16_t) strlen ((const char *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_IOTOPMODE_SET].p_cmd);
//    do
//    {
//
//        /** Send IOTOPMODE selection command and read the response */
//        sf_cellular_serial_write (p_celr_instance, (uint8_t const *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_IOTOPMODE_SET].p_cmd,
//                               bytes_write);
//
//        bytes_read = p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_IOTOPMODE_SET].max_resp_length;
//
//        memset (resp_buff, 0, sizeof(resp_buff));
//        resp_wait_ticks = sf_cellular_common_getticks_for_mseconds (
//                                    p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_IOTOPMODE_SET].resp_wait_time);
//
//        result = sf_cellular_serial_read (p_celr_instance, resp_buff, &bytes_read, resp_wait_ticks);
//        if ((result == SSP_SUCCESS) || (result == SSP_ERR_TIMEOUT))
//        {
//            res_string_present = sf_cellular_is_str_present (
//                    (const char *) resp_buff,
//                    (const char *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_IOTOPMODE_SET].p_success_resp);
//            if (SF_CELLULAR_TRUE == res_string_present)
//            {
//                retval = SSP_SUCCESS;
//                break;
//            }
//        }
//
//        /** Delay for next try */
//        sf_cellular_msec_delay (p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_IOTOPMODE_SET].retry_delay);
//        ++retry_count;
//    }
//    while (retry_count < p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_IOTOPMODE_SET].retry);
//
//    return retval;
//}
//
///*******************************************************************************************************************//**
// * @brief Set Cellular Network Scan Mode
// *
// * @param [in]  p_celr_instance      Pointer to cellular instance
// * @retval SSP_SUCCESS                  Successfully set the network Scan mode
// * @retval SSP_ERR_CELLULAR_FAILED      Failed to set network scan mode
// **********************************************************************************************************************/
//static ssp_err_t sf_cellular_set_nwscanmode(sf_cellular_instance_cfg_t * const p_celr_instance)
//{
//    ssp_err_t result;
//    uint16_t bytes_read = 0U;
//    uint16_t bytes_write;
//    uint8_t resp_buff[SF_CELLULAR_STR_LEN_32] =
//    { SF_CELLULAR_NULL_CHAR };
//    uint8_t retry_count = 0U;
//    uint8_t res_string_present;
//    ssp_err_t retval = SSP_ERR_CELLULAR_FAILED;
//    sf_cellular_at_cmd_set_t const * p_cmd_set = p_celr_instance->p_cfg->p_cmd_set;
//    uint32_t resp_wait_ticks;
//
//    /**
//     * Command Used
//     * AT+QCFG="nwscanmode",0,1
//     */
//
//    bytes_write = (uint16_t) strlen ((const char *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NWSCANMODE_SET].p_cmd);
//    do
//    {
//
//        /** Send Network scan mode selection command and read the response */
//        sf_cellular_serial_write (p_celr_instance, (uint8_t const *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NWSCANMODE_SET].p_cmd,
//                               bytes_write);
//
//        bytes_read = p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NWSCANMODE_SET].max_resp_length;
//
//        memset (resp_buff, 0, sizeof(resp_buff));
//        resp_wait_ticks = sf_cellular_common_getticks_for_mseconds (
//                                    p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NWSCANMODE_SET].resp_wait_time);
//
//        result = sf_cellular_serial_read (p_celr_instance, resp_buff, &bytes_read, resp_wait_ticks);
//        if ((result == SSP_SUCCESS) || (result == SSP_ERR_TIMEOUT))
//        {
//            res_string_present = sf_cellular_is_str_present (
//                    (const char *) resp_buff,
//                    (const char *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NWSCANMODE_SET].p_success_resp);
//            if (SF_CELLULAR_TRUE == res_string_present)
//            {
//                retval = SSP_SUCCESS;
//                break;
//            }
//        }
//
//        /** Delay for next try */
//        sf_cellular_msec_delay (p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NWSCANMODE_SET].retry_delay);
//        ++retry_count;
//    }
//    while (retry_count < p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NWSCANMODE_SET].retry);
//
//    return retval;
//}
//
//
///*******************************************************************************************************************//**
// * @brief Set Cellular NBIOT Bands used for registration on the network
// *
// * @param [in]  p_celr_instance      Pointer to cellular instance
// * @retval SSP_SUCCESS                  Successfully set the network Scan mode
// * @retval SSP_ERR_CELLULAR_FAILED      Failed to set network scan mode
// **********************************************************************************************************************/
//static ssp_err_t sf_cellular_set_nbiot_band_selection(sf_cellular_instance_cfg_t * const p_celr_instance)
//{
//    ssp_err_t result;
//    uint16_t bytes_read = 0U;
//    uint16_t bytes_write;
//    uint8_t resp_buff[SF_CELLULAR_STR_LEN_32] =
//        { SF_CELLULAR_NULL_CHAR };
//    uint8_t command[SF_CELLULAR_STR_LEN_64] =
//        { SF_CELLULAR_NULL_CHAR };
//    int8_t bands[32];
//    int8_t bands_temp[64];
//    uint8_t retry_count = 0U;
//    uint8_t res_string_present;
//    ssp_err_t retval = SSP_ERR_CELLULAR_FAILED;
//    sf_cellular_at_cmd_set_t const * p_cmd_set = p_celr_instance->p_cfg->p_cmd_set;
//    sf_cellular_extended_cfg_t * p_celr_ext_cfg = (sf_cellular_extended_cfg_t *)p_celr_instance->p_cfg->p_extend;
//    sf_cellular_qctlcatm1_extended_cfg_t * p_qctlcatm1_ext_cfg = NULL;
//    uint32_t resp_wait_ticks;
//    uint32_t band_selection_value = 0;
//
//    p_qctlcatm1_ext_cfg = (sf_cellular_qctlcatm1_extended_cfg_t *)p_celr_ext_cfg->p_module_extended_cfg;
//
//    /**
//     * Command Used
//     * AT+QCFG="band",0,0,XXXXXXXX,1
//     */
//
//    /** Prepare command to select NBIOT bands */
//    strcpy ((char *) command, (const char *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NBIOT_BAND_SELECTION].p_cmd);
//    strcpy((char*)bands_temp, (const char*)p_qctlcatm1_ext_cfg->nbiot_band_selection);
//
//    /** Iterate through string array and build band bitmap value */
//    char * o = strtok((char*)bands_temp, " ,\r\n");
//    for (uint32_t i = 0U; ((i < (13U)) && (NULL != o)); i++)
//    {
//        int32_t band_test = (int32_t)atoi(o);
//        band_selection_value |= band_lookup[band_test];
//        o = strtok(NULL, " ,\r\n");
//    }
//    sprintf((char*)bands, "%lx", band_selection_value);
//    strcat ((char *) command, (const char *) bands);
//    strcat ((char *) command, (const char *) ",1\r\n");
//
//    /** calculate the number of bytes for the serial write */
//    bytes_write = (uint16_t) strlen ((const char *) command);
//
//    do
//    {
//        /** Send Network scan mode selection command and read the response */
//        sf_cellular_serial_write (p_celr_instance, (uint8_t const *) command, bytes_write);
//
//        bytes_read = p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NBIOT_BAND_SELECTION].max_resp_length;
//
//        memset (resp_buff, 0, sizeof(resp_buff));
//
//        resp_wait_ticks = sf_cellular_common_getticks_for_mseconds (
//                                    p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NBIOT_BAND_SELECTION].resp_wait_time);
//
//        result = sf_cellular_serial_read (p_celr_instance, resp_buff, &bytes_read, resp_wait_ticks);
//        if ((result == SSP_SUCCESS) || (result == SSP_ERR_TIMEOUT))
//        {
//            res_string_present = sf_cellular_is_str_present (
//                    (const char *) resp_buff,
//                    (const char *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NBIOT_BAND_SELECTION].p_success_resp);
//            if (SF_CELLULAR_TRUE == res_string_present)
//            {
//                retval = SSP_SUCCESS;
//                break;
//            }
//        }
//
//        /** Delay for next try */
//        sf_cellular_msec_delay (p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NBIOT_BAND_SELECTION].retry_delay);
//
//        ++retry_count;
//    }
//    while (retry_count < p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NBIOT_BAND_SELECTION].retry);
//
//    return retval;
//}


///*******************************************************************************************************************//**
// * @brief Set Cellular Fallback Sequence
// *
// * @param [in]  p_celr_instance      Pointer to cellular instance
// * @retval SSP_SUCCESS                  Successfully set the fall back sequence
// * @retval SSP_ERR_CELLULAR_FAILED      Failed to set fall back sequence
// * @retval SSP_ERR_CELLULAR_INIT_FAILED Failed due to invalid response received from modem
// **********************************************************************************************************************/
//static ssp_err_t sf_cellular_set_fallback_sequence(sf_cellular_instance_cfg_t * const p_celr_instance)
//{
//    ssp_err_t result;
//    uint16_t bytes_read = 0U;
//    uint16_t bytes_write;
//    uint8_t command[SF_CELLULAR_STR_LEN_64] =
//        { SF_CELLULAR_NULL_CHAR };
//    uint8_t resp_buff[SF_CELLULAR_STR_LEN_32] =
//    { SF_CELLULAR_NULL_CHAR };
//    uint8_t retry_count = 0U;
//    uint8_t res_string_present;
//    ssp_err_t retval = SSP_ERR_CELLULAR_FAILED;
//    sf_cellular_at_cmd_set_t const * p_cmd_set = p_celr_instance->p_cfg->p_cmd_set;
//    sf_cellular_extended_cfg_t * p_celr_ext_cfg = (sf_cellular_extended_cfg_t *)p_celr_instance->p_cfg->p_extend;
//    sf_cellular_qctlcatm1_extended_cfg_t * p_qctlcatm1_ext_cfg = NULL;
//    uint32_t resp_wait_ticks;
//
//    p_qctlcatm1_ext_cfg = (sf_cellular_qctlcatm1_extended_cfg_t *)p_celr_ext_cfg->p_module_extended_cfg;
//
//    result = sf_cellular_set_nwscanmode(p_celr_instance);
//    if (SSP_SUCCESS == result)
//    {
//        result = sf_cellular_set_iotopmode(p_celr_instance);
//        if(SSP_SUCCESS == result)
//        {
//            result = sf_cellular_set_nbiot_band_selection(p_celr_instance);
//        }
//    }
//
//    /**
//     * Command Used
//     * AT+QCFG="nwscanseq",(scan sequence selected by user through ISDE)
//     */
//
//    if (SSP_SUCCESS == result)
//    {
//        memset (command, 0, sizeof(command));
//
//        /** Prepare command to select Cellular Network Fallback sequence */
//        strcpy ((char *) command, (const char *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NWSCANSEQ_SET].p_cmd);
//
//        strcat ((char *) command, (const char *) g_celr_fallback_mode_lookup[p_qctlcatm1_ext_cfg->nwscanseq]);
//
//        strcat ((char *) command, (const char *) ",1\r\n");
//
//        bytes_write = (uint16_t) strlen ((const char *) command);
//        do
//        {
//
//            sf_cellular_msec_delay (SF_CELLULAR_CONFIGURATION_DELAY);
//
//            /** Send Network fallback sequence selection command and read the response */
//            sf_cellular_serial_write (p_celr_instance, (uint8_t const *) command,
//                                   bytes_write);
//
//            bytes_read = p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NWSCANSEQ_SET].max_resp_length;
//
//            memset (resp_buff, 0, sizeof(resp_buff));
//            resp_wait_ticks = sf_cellular_common_getticks_for_mseconds (
//                                        p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NWSCANSEQ_SET].resp_wait_time);
//
//            result = sf_cellular_serial_read (p_celr_instance, resp_buff, &bytes_read, resp_wait_ticks);
//            if ((result == SSP_SUCCESS) || (result == SSP_ERR_TIMEOUT))
//            {
//                res_string_present = sf_cellular_is_str_present (
//                        (const char *) resp_buff,
//                        (const char *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NWSCANSEQ_SET].p_success_resp);
//                if (SF_CELLULAR_TRUE == res_string_present)
//                {
//                    retval = SSP_SUCCESS;
//                    break;
//                }
//            }
//
//            /** Delay for next try */
//            sf_cellular_msec_delay (p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NWSCANSEQ_SET].retry_delay);
//            ++retry_count;
//        }
//        while (retry_count < p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_NWSCANSEQ_SET].retry);
//    }
//
//    if (SSP_SUCCESS == retval)
//    {
//        /** As per the Quectel, we should power reset the
//         * module after setting the scan sequence mode */
//        retval = sf_cellular_module_reset (p_celr_instance, SF_CELLULAR_RESET_TYPE_HARD);
//        if (SSP_SUCCESS == retval)
//        {
//            /** Check for module ready after power reset */
//            retval = sf_cellular_is_module_init (p_celr_instance);
//            if (SSP_SUCCESS == retval)
//            {
//                /** Check whether SIM Pin is required */
//                retval = sf_cellular_check_pin_required (p_celr_instance, p_celr_instance->p_cfg->p_sim_pin,
//                                                         p_celr_instance->p_cfg->p_puk_pin);
//            }
//
//        }
//    }
//
//    return retval;
//}
//
///*******************************************************************************************************************//**
// * @brief Set Cellular SIM priority effect
// *
// * @param [in]  p_celr_instance         Pointer to cellular instance
// * @retval SSP_SUCCESS                  Successfully set SIM priority effect
// * @retval SSP_ERR_CELLULAR_FAILED      Failed to set SIM priority effect
// **********************************************************************************************************************/
//static ssp_err_t sf_cellular_set_sim_priority_effect(sf_cellular_instance_cfg_t * const p_celr_instance)
//{
//    uint16_t bytes_read_write = 0U;
//    uint8_t resp_buff[SF_CELLULAR_STR_LEN_32] =
//    { SF_CELLULAR_NULL_CHAR };
//    ssp_err_t result;
//    uint8_t expected_resp_present = SF_CELLULAR_FALSE;
//    uint8_t retry_count = 0U;
//    ssp_err_t retval = SSP_ERR_CELLULAR_FAILED;
//    sf_cellular_at_cmd_set_t const *p_cmd_set = p_celr_instance->p_cfg->p_cmd_set;
//    uint32_t resp_wait_ticks;
//
//    do
//    {
//        /** Set SIM priority effect */
//        bytes_read_write = (uint16_t) strlen (
//                (const char *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_SIM_EFFECT_SET].p_cmd);
//        (void) sf_cellular_serial_write (p_celr_instance,
//                                         (uint8_t const *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_SIM_EFFECT_SET].p_cmd,
//                                         bytes_read_write);
//
//        bytes_read_write = p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_SIM_EFFECT_SET].max_resp_length;
//
//        (void) memset (resp_buff, 0, sizeof(resp_buff));
//
//        /** Get response wait time in ticks */
//        resp_wait_ticks = sf_cellular_common_getticks_for_mseconds (
//                p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_SIM_EFFECT_SET].resp_wait_time);
//
//        result = sf_cellular_serial_read (p_celr_instance, resp_buff, &bytes_read_write, resp_wait_ticks);
//        if ((SSP_SUCCESS == result) || (SSP_ERR_TIMEOUT == result))
//        {
//            expected_resp_present = sf_cellular_is_str_present (
//                    (const char *) resp_buff,
//                    (const char *) p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_SIM_EFFECT_SET].p_success_resp);
//
//            if (SF_CELLULAR_TRUE == expected_resp_present)
//            {
//                retval = SSP_SUCCESS;
//                break;
//            }
//
//        }
//
//        /** Delay for next try */
//        sf_cellular_msec_delay (p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_SIM_EFFECT_SET].retry_delay);
//        ++retry_count;
//    }
//    while (retry_count < p_cmd_set[SF_CELLULAR_AT_CMD_INDEX_AT_SIM_EFFECT_SET].retry);
//
//    return retval;
//}

/*****************************************************************************************************************//**
 * @brief Check whether module is registered successfully
 *
 * @param [in] p_celr_instance                        Pointer to cellular instance
 * @retval SSP_SUCCESS                                If SIM registration successful
 * @retval SSP_ERR_CELLULAR_REGISTRATION_FAILED       If SIM registration failed
 *********************************************************************************************************************/
static ssp_err_t sf_cellular_check_network_registration_status(sf_cellular_instance_cfg_t * p_celr_instance)
{
    uint16_t bytes_read_write = 0U;
    uint8_t resp_buff[SF_CELLULAR_STR_LEN_32] =
    { SF_CELLULAR_NULL_CHAR };
    uint8_t retry_count = 0U;
    ssp_err_t retval = SSP_ERR_CELLULAR_REGISTRATION_FAILED;
    sf_cellular_at_cmd_set_t const * p_cmd_set = p_celr_instance->p_cfg->p_modifiable_cmd_set;
    uint32_t resp_wait_ticks;

    do
    {
		/** Try for AT+CREG command. As, AT+CREG is described as network registration status in GERAN and UTRAN */
		bytes_read_write = (uint16_t) strlen ((const char *) p_cmd_set[SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_CREG].p_cmd);

		/** Check for SIM Registration */
		sf_cellular_msec_delay (SF_CELLULAR_CONFIGURATION_DELAY);

		sf_cellular_serial_write (p_celr_instance,
								  (uint8_t const *) p_cmd_set[SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_CREG].p_cmd,
								  bytes_read_write);

		bytes_read_write = p_cmd_set[SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_CREG].max_resp_length;

		memset (resp_buff, 0, sizeof(resp_buff));

		/** Get response wait time in ticks */
		resp_wait_ticks = sf_cellular_common_getticks_for_mseconds (
				p_cmd_set[SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_CREG].resp_wait_time);

		retval = sf_cellular_read_network_registration_response (p_celr_instance, resp_buff, &bytes_read_write,
																 resp_wait_ticks);
		if (SSP_SUCCESS == retval)
		{
			break;
		}

		/** Delay for next try */
		sf_cellular_msec_delay (p_cmd_set[SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_CREG].retry_delay);


		/** Try for AT+CEREG command. As, AT+CEREG is described as LTE EPS network registration status. */
		bytes_read_write = (uint16_t) strlen ((const char *) p_cmd_set[SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_CEREG].p_cmd);

		/** Check for SIM Registration */
		sf_cellular_msec_delay (SF_CELLULAR_CONFIGURATION_DELAY);

		sf_cellular_serial_write (p_celr_instance,
								  (uint8_t const *) p_cmd_set[SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_CEREG].p_cmd,
								  bytes_read_write);

		bytes_read_write = p_cmd_set[SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_CEREG].max_resp_length;

		memset (resp_buff, 0, sizeof(resp_buff));

		/** Get response wait time in ticks */
		resp_wait_ticks = sf_cellular_common_getticks_for_mseconds (
				p_cmd_set[SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_CEREG].resp_wait_time);

		retval = sf_cellular_read_network_registration_response (p_celr_instance, resp_buff, &bytes_read_write,
																 resp_wait_ticks);
		if (SSP_SUCCESS == retval)
		{
			break;
		}

		/** Delay for next try */
		sf_cellular_msec_delay (p_cmd_set[SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_CEREG].retry_delay);
		++retry_count;
    }
    while (retry_count < p_cmd_set[SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_CREG].retry);

    return retval;
}

/*****************************************************************************************************************//**
 * @brief Read the network registration request
 *
 * @param [in]     p_celr_instance                    Pointer to cellular instance
 * @param[out]     p_resp_buff                        Pointer to response buffer
 * @param[in, out] p_bytes_read_write                 Number of bytes to read and Number of bytes read actually
 * @param[in]      resp_wait_ticks                    Timeout in ThreadX ticks
 * @retval SSP_SUCCESS                                Response is read successfully
 * @retval SSP_ERR_CELLULAR_REGISTRATION_FAILED       Registration failed
 *********************************************************************************************************************/
static ssp_err_t sf_cellular_read_network_registration_response(sf_cellular_instance_cfg_t * p_celr_instance,
        uint8_t * p_resp_buff, uint16_t * p_bytes_read_write, uint32_t resp_wait_ticks)
{
    ssp_err_t result;
    ssp_err_t retval = SSP_ERR_CELLULAR_REGISTRATION_FAILED;
    uint8_t home_reg;
    uint8_t roaming_reg;

    result = sf_cellular_serial_read (p_celr_instance, p_resp_buff, p_bytes_read_write, resp_wait_ticks);
    if ((SSP_SUCCESS == result) || (SSP_ERR_TIMEOUT == result))
    {
        home_reg = sf_cellular_is_str_present ((const char *) p_resp_buff, SF_CELLULAR_HOME_REG_RSP);
        roaming_reg = sf_cellular_is_str_present ((const char *) p_resp_buff, SF_CELLULAR_ROAMING_REG_RSP);
        if ((SF_CELLULAR_TRUE == home_reg) || (SF_CELLULAR_TRUE == roaming_reg))
        {
            retval = SSP_SUCCESS;
        }
    }
    return retval;
}

/*******************************************************************************************************************//**
 * @} (end addtogroup SF_CELLULAR_QCTLCATM1)
 **********************************************************************************************************************/
