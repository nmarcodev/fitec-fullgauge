/*
 * Eth_Wifi Thread
 *
 * Thread responsavel pelo gerenciamento da conectividade e comunicacao com o SITRAD.
 *
 * g_timer3 e EthWifiStatus_Semaphore sao responsaveis controlar a atualizacao das flags
 * de estado da rede, que ocorre a cada 1s. As flags sao enviadas para o MainManager Thread
 * pelas flags g_main_event_flags.
 *
 * As event flags EthWifi_Flags tratam os eventos relacionados a conexao e desconexao da rede.
 * Sao tratados pela maquina de estados na funcao NetStateMachine().
 *
 * Atraves do Message Framework, posta a mensagem SF_MESSAGE_EVENT_CLASS_DATE_TIME_MESSAGE para a thread RTC
 * quando recebe do SITRAD comando para ajustar o horario. Envia SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_IN para a thread Serial
 * quando recebe comando para enviar dados pela rede 485. Recebe da thread Serial a mensagem
 * SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_OUT e envia os dados recebidos ao SITRAD.
 *
 * Comunica com a thread Datalogger pela queue Datalogger_Flash, enviando o estado de conexao com o SITRAD
 * e os avisos de inicio de teste de fabrica, e inicio e fim de dowlonad de firmware, quando o datalogger
 * deve ser inativado.
 * Envia tambem as requisicoes de paginas de datalog pedidas pelo SITRAD, que sao respondidas atraves da queue EthWifi_Flash.
 *
 */

#include <Includes/common_util.h>
#include "Eth_Wifi_Thread.h"
#include "Includes/Eth_thread_entry.h"
#include "Includes/UdpTcpTypes.h"
#include "Includes/MainManagerThreadEntry.h"
#include "Includes/internal_flash.h"
#include "Includes/Eth_Wifi_Thread_Entry.h"
#include "Includes/DataLoggerFlash.h"
#include "Includes/ApplicationUdp.h"
#include "Includes/ApplicationTcp.h"
#include "Includes/ApplicationTcpPayload.h"
#include "Includes/ApplicationIp.h"
#include "Includes/ApplicationMeFramw.h"
#include "Includes/ApplicationWiFi.h"
#include "Includes/ApplicationCommonModule.h"
#include "Includes/ApplicationLedsStatus.h"
#include "Includes/ApplicationAP.h"
#include "Includes/messages.h"
#include "Includes/UpdateOTA.h"
#include "Includes/Define_IOs.h"
#include "Includes/ConfDefault.h"
#include "Includes/ApplicationTcp485.h"
#include "Includes/ApplicationTimerMain.h"
#include "Includes/Thread_Monitor_entry.h"

#if RECORD_DATALOGGER_EVENTS
#include "Includes/Thread_Datalogger_entry.h"
#endif
//
//
///*********** Declaracao das funcoes **********/
//void EthWifi_UDP_Notify(NX_UDP_SOCKET *socket_ptr);
//void EthWifi_TCP_DisconnectRequest(NX_TCP_SOCKET *socket_ptr);
//void EthWifi_TCP_DisconnectRequestConf(NX_TCP_SOCKET *socket_ptr);
//void EthWifi_TCP_ReceiveNotify(NX_TCP_SOCKET *socket_ptr);
//void EthWifi_TCP_ReceiveNotifyConf(NX_TCP_SOCKET *socket_ptr);
//void EthWifi_TCP_ConnectRequest(NX_TCP_SOCKET *socket_ptr, UINT port);
//void EthWifi_TCP_ConnectRequestConf(NX_TCP_SOCKET *socket_ptr, UINT port);
//
//void EthWifi_Message_Framework_NotifyCallback(TX_QUEUE *EthWifi_thread_message_queue);

//void TimeoutTCP_Open(void);
//void StartTimeoutTcp(timer_size_t TimeOut);
//void RestartTimeoutTcp(timer_size_t TimeOut);
//void StopTimeoutTcp(void);

//void RestartIp(_ETH_WIFI_CONTROL *EthWifiControl, _STATUS_FLAGS *StatusFlagsX);

//void Timer3_Open(void);
//void StopTimerMain(void);
//void StartTimerMain(void);

/////********* Menssage Framework *************/
//void RtcPost(_ETH_WIFI_CONTROL *EthWifiData);
//void SerialPost(_ETH_WIFI_CONTROL *EthWifiData);


/* Eth_Wifi Thread entry function */
void Eth_Wifi_Thread_entry(void)
{
//    static _ETH_WIFI_CONTROL EthWifiControl;
//    static sf_wifi_provisioning_t  SfWifi;
//    static _STATUS_FLAGS StatusFlags;
//
//    static uint8_t  BufferRXTCP[SIZE_FRAG_PAYLOAD_TCP];
//    static NX_PACKET PacketReceiveTCPS;
//    static NX_PACKET PacketReceiveTCPConf;
//    static NX_PACKET PacketUdpSendS;
//    static NX_UDP_SOCKET UdpSocketStatic;
//
//    EthWifiControl.PacketReceiveTCP     = &PacketReceiveTCPS;
//    EthWifiControl.PacketReceiveTCPConf = &PacketReceiveTCPConf;
//
//    EthWifiControl.BufferTcpRX          = &BufferRXTCP[0];
//    EthWifiControl.SfProvisionWifi      = &SfWifi;
//
//    EthWifiControl.PacketUdpSendRx      = &PacketUdpSendS;
//    EthWifiControl.UdpSocket            = &UdpSocketStatic;
//
//    tx_thread_sleep(TIME_1S);
//
//    //EthWifiControl.FlagFAC = GetEthWifiFAC(); //Verifica se o modulo esta em fac
//    //EthWifiInitConf(&EthWifiControl);
//    //EthWifiOpen(&EthWifiControl, &StatusFlags);
//
//    //Timer3_Open();
//    //TimeoutTCP_Open();
//
//#if RECORD_DATALOGGER_EVENTS
//    DataLoggerRegisterEvent(DATALOGGER_EVENT_NET_INIT, 0xF000, 0);
//#endif

//    while (1)
//    {
//        NetStateMachine(&EthWifiControl, &StatusFlags);
//
//        StatusSitrad(&StatusFlags);
//
//        if(TX_SUCCESS == tx_event_flags_set (&g_main_event_flags, NetStatusModule(&EthWifiControl, &StatusFlags), TX_OR))
//        {
//            ThreadMonitor_SendKeepAlive(T_MONITOR_ETH_WIFI_THREAD);
//        }
//    }
}

////*************************************************************************************************
////* Configures the Timer03 and created semaphore for its uses
////*************************************************************************************************
//void Timer3_Open(void)
//{
//    g_timer3.p_api->open(g_timer3.p_ctrl, g_timer3.p_cfg);
//
//    g_timer3.p_api->periodSet( g_timer3.p_ctrl, 1, TIMER_UNIT_PERIOD_SEC );
//    g_timer3.p_api->start(g_timer3.p_ctrl);
//}
//
////*************************************************************************************************
////* Configures the Timer04 and created semaphore for its uses
////*************************************************************************************************
//void TimeoutTCP_Open(void)
//{
//    g_timer4.p_api->open(g_timer4.p_ctrl, g_timer4.p_cfg);
//}
//
//void StartTimeoutTcp(timer_size_t TimeOut)
//{
//    g_timer4.p_api->periodSet( g_timer4.p_ctrl, TimeOut, TIMER_UNIT_PERIOD_SEC );
//    g_timer4.p_api->start(g_timer4.p_ctrl);
//}
//
//void StartTimerMain(void)
//{
//    g_timer6Main.p_api->open(g_timer6Main.p_ctrl, g_timer6Main.p_cfg);
//    g_timer6Main.p_api->periodSet( g_timer6Main.p_ctrl, 1, TIMER_UNIT_PERIOD_SEC );
//    g_timer6Main.p_api->start(g_timer6Main.p_ctrl);
//}
//
//void StopTimerMain(void)
//{
//    g_timer4.p_api->stop(g_timer6Main.p_ctrl);
//}
//
//void RestartTimeoutTcp(timer_size_t TimeOut)
//{
//    g_timer4.p_api->periodSet( g_timer4.p_ctrl, TimeOut, TIMER_UNIT_PERIOD_SEC );
//    g_timer4.p_api->reset(g_timer4.p_ctrl);
//}
//
//void StopTimeoutTcp(void)
//{
//    g_timer4.p_api->stop(g_timer4.p_ctrl);
//}
//
////Callback que sinaliza que o timer3 estourou
//void UserTimer3Callback (timer_callback_args_t *p_args)
//{
//    if (p_args->event == TIMER_EVENT_EXPIRED)
//    {
//        //Sinaliza que o evento ocorreu
//        tx_semaphore_put(&EthWifiStatus_Semaphore);
//
//        g_timer3.p_api->periodSet( g_timer3.p_ctrl, 1, TIMER_UNIT_PERIOD_SEC );
//        g_timer3.p_api->start(g_timer3.p_ctrl);
//    }
//}
//
////Callback que sinaliza que o TimeOutTCPCallback (timer4)
//void TimeOutTCPCallback (timer_callback_args_t *p_args)
//{
//    if (p_args->event == TIMER_EVENT_EXPIRED)
//    {
//        tx_event_flags_set (&EthWifi_Flags, EW_FLAG_TIMEOUT_TCP, TX_OR);
//    }
//}
//
//void TimerMainCallback(timer_callback_args_t *p_args)
//{
//    if (p_args->event == TIMER_EVENT_EXPIRED)
//    {
//        tx_event_flags_set (&EthWifi_Flags, EW_FLAG_TIMEOUT_TIMER_MAIN, TX_OR);
//    }
//}

//void EthWifi_UDP_Notify(NX_UDP_SOCKET *socket_ptr)
//{
//    UdpSendOnDemand(socket_ptr);
//}

//CallBack de recebimento de um pedido de conexao TCP porta comunicacao
//void EthWifi_TCP_ConnectRequest(NX_TCP_SOCKET *socket_ptr, UINT port)
//{
//    tx_event_flags_set (&EthWifi_Flags, EW_FLAG_TCP_CONNECT, TX_OR);
//
//    SSP_PARAMETER_NOT_USED(socket_ptr);
//    SSP_PARAMETER_NOT_USED(port);
//}

//CallBack de recebimento de um pedido de conexao TCP porta Conf
//void EthWifi_TCP_ConnectRequestConf(NX_TCP_SOCKET *socket_ptr, UINT port)
//{
//    tx_event_flags_set (&EthWifi_Flags, EW_FLAG_TCP_CONNECT_CONF, TX_OR);
//
//    SSP_PARAMETER_NOT_USED(socket_ptr);
//    SSP_PARAMETER_NOT_USED(port);
//}

//CallBack de recebimento de um pacote TCP porta comunicacao
//void EthWifi_TCP_ReceiveNotify(NX_TCP_SOCKET *socket_ptr)
//{
//    tx_event_flags_set (&EthWifi_Flags, EW_FLAG_TCP_RECEIVED, TX_OR);
//    tx_event_flags_set (&EthWifi_Flags, EW_FLAG_DETECTED_ACTIVITY, TX_OR);
//    SSP_PARAMETER_NOT_USED(socket_ptr);
//}

//CallBack de recebimento de um pacote TCP porta Configuracao
//void EthWifi_TCP_ReceiveNotifyConf(NX_TCP_SOCKET *socket_ptr)
//{
//    tx_event_flags_set (&EthWifi_Flags, EW_FLAG_TCP_RECEIVED_CONF, TX_OR);
//    SSP_PARAMETER_NOT_USED(socket_ptr);
//}

//CallBack de pedido de desconexao TCP
//void EthWifi_TCP_DisconnectRequest(NX_TCP_SOCKET *socket_ptr)
//{
//    tx_event_flags_set (&EthWifi_Flags, EW_FLAG_TCP_DISCONNECT, TX_OR);
//    SSP_PARAMETER_NOT_USED(socket_ptr);
//}

//CallBack de pedido de desconexao TCP
//void EthWifi_TCP_DisconnectRequestConf(NX_TCP_SOCKET *socket_ptr)
//{
//    tx_event_flags_set (&EthWifi_Flags, EW_FLAG_TCP_DISCONNECT_CONF, TX_OR);
//    SSP_PARAMETER_NOT_USED(socket_ptr);
//}
//
////Callback de recebimento de Message Framework
//void EthWifi_Message_Framework_NotifyCallback(TX_QUEUE *EthWifi_thread_message_queue)
//{
//    tx_event_flags_set (&EthWifi_Flags, EW_FLAG_MFRAMEWORK, TX_OR);
//    SSP_PARAMETER_NOT_USED(EthWifi_thread_message_queue);
//}
