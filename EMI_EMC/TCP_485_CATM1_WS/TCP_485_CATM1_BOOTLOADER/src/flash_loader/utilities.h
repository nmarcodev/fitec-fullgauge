/*
 * utilities.h
 *
 *  Created on: Dec 19, 2016
 *      Author: LAfonso01
 */

#ifndef FLASH_LOADER_UTILITIES_H_
#define FLASH_LOADER_UTILITIES_H_

uint16_t fl_check_application(void);
uint16_t fl_check_ext_application(void);

#endif /* FLASH_LOADER_UTILITIES_H_ */
