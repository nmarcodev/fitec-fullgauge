///* HAL-only entry function */
//#include "hal_data.h"
//void hal_entry(void)
//{
//    /* TODO: add your own code here */
//}

///* HAL-only entry function */
//#include "hal_data.h"
//void hal_entry(void)
//{
//    /* TODO: add your own code here */
//}

/* HAL-only entry function */
#include "hal_data.h"
#include "TypeDefs.h"
#include "./flash_loader/bootloader.h"

// definição complementar ao ID CODE para funcionar com o gravador FLASHER no modo stand alone

#define BSP_CFG_ID_CODE_EXT_1 (0xFFFFFFFF)
#define BSP_CFG_ID_CODE_EXT_2 (0xFFFFFFFF)
#define BSP_CFG_ID_CODE_EXT_3 (0xFFFFFFFF)
#define BSP_CFG_ID_CODE_EXT_4 (0xFFFFFFFF)

#define BSP_SECTION_ID_CODE_EXT_1 ".id_code_ext_1"
#define BSP_SECTION_ID_CODE_EXT_2 ".id_code_ext_2"
#define BSP_SECTION_ID_CODE_EXT_3 ".id_code_ext_3"
#define BSP_SECTION_ID_CODE_EXT_4 ".id_code_ext_4"

/** ID code complement definitions defined here. */
/*LDRA_INSPECTED 57 D - This global is being initialized at it's declaration below. */
BSP_DONT_REMOVE static const uint32_t g_bsp_id_codes_ext[] BSP_PLACE_IN_SECTION_V2(BSP_SECTION_ID_CODE_EXT_1) =
{
     BSP_CFG_ID_CODE_EXT_1,
     BSP_CFG_ID_CODE_EXT_2,
     BSP_CFG_ID_CODE_EXT_3,
     BSP_CFG_ID_CODE_EXT_4
};

extern void R_BSP_WarmStart (bsp_warm_start_event_t event);

void hal_entry(void)
{
    _ERROR_CODE ErrorCode;

    jumptoApplication(&ErrorCode);
    bootloader(ErrorCode);
}


void R_BSP_WarmStart (bsp_warm_start_event_t event)
{
    if (BSP_WARM_START_PRE_C == event)
    {
        /* C runtime environment has not been setup so you cannot use globals. System clocks and pins are not setup. */
    }
    else if (BSP_WARM_START_POST_C == event)
    {
        //jumptoApplication();
    //    JumpFlag = true;
    }
    else
    {
        /* Do nothing */
    }
}
