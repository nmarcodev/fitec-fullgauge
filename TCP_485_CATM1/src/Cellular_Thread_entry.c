/*
 * Cellular Thread
 *
 * Thread responsavel pelo gerenciamento da conectividade e comunicacao com o SITRAD.
 *
 * g_timer3 e CellularStatus_Semaphore sao responsaveis controlar a atualizacao das flags
 * de estado da rede, que ocorre a cada 1s. As flags sao enviadas para o MainManager Thread
 * pelas flags g_main_event_flags.
 *
 * As event flags Cellular_Flags tratam os eventos relacionados a conexao e desconexao da rede.
 * Sao tratados pela maquina de estados na funcao NetStateMachine().
 *
 * Atraves do Message Framework, posta a mensagem SF_MESSAGE_EVENT_CLASS_DATE_TIME_MESSAGE para a thread RTC
 * quando recebe do SITRAD comando para ajustar o horario. Envia SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_IN para a thread Serial
 * quando recebe comando para enviar dados pela rede 485. Recebe da thread Serial a mensagem
 * SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_OUT e envia os dados recebidos ao SITRAD.
 *
 * Comunica com a thread Datalogger pela queue Datalogger_Flash, enviando o estado de conexao com o SITRAD
 * e os avisos de inicio de teste de fabrica, e inicio e fim de dowlonad de firmware, quando o datalogger
 * deve ser inativado.
 * Envia tambem as requisicoes de paginas de datalog pedidas pelo SITRAD, que sao respondidas atraves da queue Cellular_Flash.
 *
 */

#include "Includes/common_util.h"
#include "Cellular_Thread.h"
#include "Includes/Cellular_Thread_Entry.h"
#include "Includes/UdpTcpTypes.h"
#include "Includes/MainManagerThreadEntry.h"
#include "Includes/internal_flash.h"
#include "Includes/DataLoggerFlash.h"
#include "Includes/ApplicationUdp.h"
#include "Includes/ApplicationTcp.h"
#include "Includes/ApplicationTcpPayload.h"
#include "Includes/ApplicationIp.h"
#include "Includes/ApplicationMeFramw.h"
#include "Includes/ApplicationCellular.h"
#include "Includes/ApplicationCommonModule.h"
#include "Includes/ApplicationLedsStatus.h"
#include "Includes/messages.h"
#include "Includes/Define_IOs.h"
#include "Includes/ConfDefault.h"
#include "Includes/ApplicationTcp485.h"
#include "Includes/ApplicationTimerMain.h"
#include "Includes/Thread_Monitor_entry.h"

#if RECORD_DATALOGGER_EVENTS
#include "Includes/Thread_Datalogger_entry.h"
#endif


/*********** Declaracao das funcoes **********/
void Cellular_UDP_Notify(NX_UDP_SOCKET *socket_ptr);
void Cellular_TCP_DisconnectRequest(NX_TCP_SOCKET *socket_ptr);
void Cellular_TCP_ReceiveNotify(NX_TCP_SOCKET *socket_ptr);
void Cellular_TCP_ConnectRequest(NX_TCP_SOCKET *socket_ptr, UINT port);

void Cellular_Message_Framework_NotifyCallback(TX_QUEUE *Cellular_thread_message_queue);

void TimeoutTCP_Open(void);
void StartTimeoutTcp(timer_size_t TimeOut);
void RestartTimeoutTcp(timer_size_t TimeOut);
void StopTimeoutTcp(void);

void RestartIp(_COMM_CONTROL *CommControl, _STATUS_FLAGS *StatusFlagsX);

void Timer3_Open(void);
void StopTimerMain(void);
void StartTimerMain(void);

/********* Menssage Framework *************/
void RtcPost(_COMM_CONTROL *CommData);
void SerialPost(_COMM_CONTROL *CommData);


/* Cellular Thread entry function */
void Cellular_Thread_entry(void)
{
    static sf_cellular_provisioning_t  SfCellular;
    static _STATUS_FLAGS StatusFlags;

    static uint8_t   BufferRXTCP[SIZE_FRAG_PAYLOAD_TCP]; //TODO: Tamanho 4096, checar necessidade de redução
    static NX_PACKET PacketReceiveTCPS;
    static NX_PACKET PacketUdpSendS;
    static NX_UDP_SOCKET UdpSocketStatic;

    CommControl.PacketReceiveTCP     = &PacketReceiveTCPS;

    CommControl.BufferTcpRX          = &BufferRXTCP[0];
    CommControl.SfProvisionCellular  = &SfCellular;

    CommControl.PacketUdpSendRx      = &PacketUdpSendS;
    CommControl.UdpSocket            = &UdpSocketStatic;

    EXS82_init();

    CommControl.FlagFAC = GetCommFAC(); //Verifica se o modulo esta em fac
    CommInitConf(&CommControl);
    CommOpen(&CommControl, &StatusFlags);

    Timer3_Open();
    TimeoutTCP_Open();

#if RECORD_DATALOGGER_EVENTS
    DataLoggerRegisterEvent(DATALOGGER_EVENT_NET_INIT, 0xF000, 0);
#endif

    while (1)
    {
        NetStateMachine(&CommControl, &StatusFlags);

        StatusSitrad(&StatusFlags);

        if(TX_SUCCESS == tx_event_flags_set (&g_main_event_flags, NetStatusModule(&CommControl, &StatusFlags), TX_OR))
        {
            ThreadMonitor_SendKeepAlive(T_MONITOR_CELLULAR_THREAD);
        }
    }
}

//*************************************************************************************************
//* Configures the Timer03 and created semaphore for its uses
//*************************************************************************************************
void Timer3_Open(void)
{
    g_timer3.p_api->open(g_timer3.p_ctrl, g_timer3.p_cfg);

    g_timer3.p_api->periodSet( g_timer3.p_ctrl, 1, TIMER_UNIT_PERIOD_SEC );
    g_timer3.p_api->start(g_timer3.p_ctrl);
}

//*************************************************************************************************
//* Configures the Timer04 and created semaphore for its uses
//*************************************************************************************************
void TimeoutTCP_Open(void)
{
    g_timer4.p_api->open(g_timer4.p_ctrl, g_timer4.p_cfg);
}

//TODO: Aparentemente não utilizado.
void StartTimeoutTcp(timer_size_t TimeOut)
{
    g_timer4.p_api->periodSet( g_timer4.p_ctrl, TimeOut, TIMER_UNIT_PERIOD_SEC );
    g_timer4.p_api->start(g_timer4.p_ctrl);
}

void StartTimerMain(void)
{
#ifdef DEBUG_CELLULAR
	printf("Start Timer Main\r\n");
#endif
    g_timer6Main.p_api->open(g_timer6Main.p_ctrl, g_timer6Main.p_cfg);
    g_timer6Main.p_api->periodSet( g_timer6Main.p_ctrl, 1, TIMER_UNIT_PERIOD_SEC );
    g_timer6Main.p_api->start(g_timer6Main.p_ctrl);
}

void StopTimerMain(void)
{
    g_timer4.p_api->stop(g_timer6Main.p_ctrl);
}

//TODO: Aparentemente não utilizado.
void RestartTimeoutTcp(timer_size_t TimeOut)
{
    g_timer4.p_api->periodSet( g_timer4.p_ctrl, TimeOut, TIMER_UNIT_PERIOD_SEC );
    g_timer4.p_api->reset(g_timer4.p_ctrl);
}

void StopTimeoutTcp(void)
{
    g_timer4.p_api->stop(g_timer4.p_ctrl);
}

//Callback que sinaliza que o timer3 estourou
void UserTimer3Callback (timer_callback_args_t *p_args)
{
    if (p_args->event == TIMER_EVENT_EXPIRED)
    {
        //Sinaliza que o evento ocorreu
        tx_semaphore_put(&CellularStatus_Semaphore);

        g_timer3.p_api->periodSet( g_timer3.p_ctrl, 1, TIMER_UNIT_PERIOD_SEC );
        g_timer3.p_api->start(g_timer3.p_ctrl);
    }
}

//Callback que sinaliza que o TimeOutTCPCallback (timer4)
void TimeOutTCPCallback (timer_callback_args_t *p_args)
{
    if (p_args->event == TIMER_EVENT_EXPIRED)
    {
        tx_event_flags_set (&Cellular_Flags, CELL_FLAG_TIMEOUT_TCP, TX_OR);
    }
}

void TimerMainCallback(timer_callback_args_t *p_args)
{
    if (p_args->event == TIMER_EVENT_EXPIRED)
    {
        tx_event_flags_set (&Cellular_Flags, CELL_FLAG_TIMEOUT_TIMER_MAIN, TX_OR);
    }
}

void Cellular_UDP_Notify(NX_UDP_SOCKET *socket_ptr)
{
#ifdef DEBUG_CELLULAR
	printf("CELLULAR EVENT: Cellular_UDP_Notify");
#endif
    UdpSendOnDemand(socket_ptr);
}

//CallBack de recebimento de um pedido de conexao TCP porta comunicacao
void Cellular_TCP_ConnectRequest(NX_TCP_SOCKET *socket_ptr, UINT port)
{
#ifdef DEBUG_CELLULAR
    printf("CELLULAR EVENT: Cellular_TCP_ConnectRequest");
#endif
    tx_event_flags_set (&Cellular_Flags, CELL_FLAG_TCP_CONNECT, TX_OR);
    SSP_PARAMETER_NOT_USED(socket_ptr);
    SSP_PARAMETER_NOT_USED(port);
}


//CallBack de recebimento de um pacote TCP porta comunicacao
void Cellular_TCP_ReceiveNotify(NX_TCP_SOCKET *socket_ptr)
{
#ifdef DEBUG_CELLULAR
	printf("CELLULAR EVENT: Cellular_TCP_ReceiveNotify");
#endif
    tx_event_flags_set (&Cellular_Flags, CELL_FLAG_TCP_RECEIVED, TX_OR);
    tx_event_flags_set (&Cellular_Flags, CELL_FLAG_DETECTED_ACTIVITY, TX_OR);
    SSP_PARAMETER_NOT_USED(socket_ptr);
}

//CallBack de pedido de desconexao TCP
void Cellular_TCP_DisconnectRequest(NX_TCP_SOCKET *socket_ptr)
{
#ifdef DEBUG_CELLULAR
	printf("CELLULAR EVENT: Cellular_TCP_DisconnectRequest");
#endif
    tx_event_flags_set (&Cellular_Flags, CELL_FLAG_TCP_DISCONNECT, TX_OR);
    SSP_PARAMETER_NOT_USED(socket_ptr);
}


//Callback de recebimento de Message Framework
void Cellular_Message_Framework_NotifyCallback(TX_QUEUE *Cellular_thread_message_queue)
{
#ifdef DEBUG_CELLULAR
	printf("CELLULAR EVENT: Cellular_Message_Framework_NotifyCallback");
#endif
    tx_event_flags_set (&Cellular_Flags, CELL_FLAG_MFRAMEWORK, TX_OR);
    SSP_PARAMETER_NOT_USED(Cellular_thread_message_queue);
}




