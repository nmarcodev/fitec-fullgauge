/*
 * ApplicationTcpPayload.h
 *
 *  Created on: 6 de jun de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_APPLICATIONTCPPAYLOAD_H_
#define INCLUDES_APPLICATIONTCPPAYLOAD_H_

#include "Includes/Cellular_Thread_Entry.h"

typedef enum
{

    FAC_TEST_START          = 0,

    FAC_TEST_SUPER_CAP_ON,  // 1
    FAC_TEST_SUPER_CAP_OFF, // 2
    FAC_TEST_QSPI,          // 3

    FAC_TEST_LEDS,          // 4
    FAC_TEST_485,           // 5
    FAC_TEST_CELLULAR,      // 6
    FAC_TEST_MAC,           // 7
    FAC_TEST_KEY,           // 8

    FAC_TEST_END,

    FAC_TEST_DO_FAC         = 0x66, //Esse id coloca o modulo em modo FAC.
    FAC_TEST_WAIT_STEP,     // deve ser o penultimo
    FAC_TEST_NONE           = 0xFF,

}_FACTORY_TEST_STEP;

extern _TCP_RETURN  AppPacketTcp(_COMM_CONTROL *CommData, uint16_t PortSitrad);
extern uint8_t      TcpInstrument(_COMM_CONTROL *CommControl);

#endif /* INCLUDES_APPLICATIONTCPPAYLOAD_H_ */
