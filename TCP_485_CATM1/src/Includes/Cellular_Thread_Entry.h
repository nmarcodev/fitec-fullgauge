/*
 * Eth_Wifi_Thread_Entry.h
 *
 */

#ifndef INCLUDES_CELLULAR_THREAD_ENTRY_H_
#define INCLUDES_CELLULAR_THREAD_ENTRY_H_

#include "common_data.h"
#include "Includes/utility.h"
#include "Includes/messages.h"
#include "Includes/TypeDefs.h"
#include "nxd_dhcp_client.h"
#include "Includes/DateTime.h"

#define APN_TIM    "timbrasil.br"
#define APN_VIVO   "zap.vivo.com.br"
#define APN_CLARO  "java.claro.com.br"
#define APN_OI     "gprs.oi.com.br"

#define PPP_LINK_UP             (1<<0)
#define PPP_LINK_DOWN           (1<<1)
#define PPP_LINK_TIMEOUT        (60 * TX_TIMER_TICKS_PER_SECOND)

typedef enum
{
    COMM_SM_IDLE,
    COMM_SM_WAIT_INTERFACE,
    COMM_SM_WAIT_QUEUE,
	COMM_SM_WAIT_RESET,
	COMM_SM_EVENT_FLAG,
	COMM_SM_DHCP,
	COMM_SM_DNS,
	COMM_SM_STATUS_IP,
	COMM_SM_RESTART_IP,
    COMM_SM_VIEW_IP,
	COMM_SM_TESTE_CHANGE,

	COMM_SM_INIT_CELL,
    COMM_SM_READ_FLASH,
    COMM_SM_READ_FLASH_NET_DEFAULT,
    COMM_SM_SCAN,
    COMM_SM_WAIT_PPP,
    COMM_SM_PROVISION_FAIL,

    COMM_SM_DONE,

    COMM_SM_RESTART_DEVICE,

	COMM_SM_RESTORE_FACTORY_DEVICE,
	COMM_SM_UPDATE_SERVER

}_COMM_STATES_MACHINE;

typedef enum
{
	CELL_FLAGS_NONE               = 0,
   // eventos
	CELL_FLAG_DETECTED_ACTIVITY   = 0x00000001,
	CELL_FLAG_TCP_RECEIVED        = 0x00000002,
	CELL_FLAG_TCP_DELETE          = 0x00000008,

	CELL_FLAG_TCP_CONNECT         = 0x00000010,
	CELL_FLAG_TCP_DISCONNECT      = 0x00000020,

	CELL_FLAG_RTC_SEND            = 0x00000100,
	CELL_FLAG_FREE7               = 0x00000200,
	CELL_FLAG_FREE8               = 0x00000400,
	CELL_FLAG_QUEUE_CONSOLE       = 0x00000800,

	CELL_FLAG_MFRAMEWORK          = 0x00001000,
	CELL_FLAG_TIMEOUT_TCP         = 0x00002000,
	CELL_FLAG_GET_STATUS          = 0x00004000,
	CELL_FLAG_TIMEOUT_TIMER_MAIN  = 0x00008000,

	CELL_FLAG_CELL_CONNECTED      = 0x00010000,
	CELL_FLAG_CELL_DISCONNECTED   = 0x00020000,
	CELL_FLAG_STATUS_CELL         = 0x000F0000,

	//TODO: Realocar essas flags
    SM_RESTART_DEVICE			  = 0x03,//0x00011300,
	SM_RESTORE_FACTORY_DEVICE     = 0x05,//0x00014000,
	SM_CLEAN_DATALLOGER		      = 0x07,//0x00015000,
	SM_UPDATE_DATE_TIME		      = 0x09,//0x00016000,

	CELL_FLAG_UDPTCP_ALL          = 0x000FFFFF,

}CELL_TCP_UDP_FLAGS;

typedef enum
{
   CELL_TCP_NONE,
   // eventos
   CELL_TCP_SEND,
   CELL_TCP_REQUESTED,
   CELL_TCP_REQUESTED_485,
   CELL_TCP_RELISTEN,

}_TCP_RETURN;

typedef struct
{
    unsigned  PasWrd:1, E_Ip:1, E_2:1, E_3:1, E_4:1, E_5:1, E_6:1, E_7:1, E_8:1;
}_F_Error_;

typedef struct
{
    unsigned    FlagStartUdpTcp:1,
                FlagSitradConnected:1,
                StateSitrad:1,
                FlagNetConnected:1,
                FlagNetConnecting:1,
                FlagNumAttempts:1,
                FlagFree01:1,
                FlagFree02:1,
                FlagFree03:1,

                FlagErrorPassWrd:1,
                FlagErrorIp:1,
                FlagErrorInvalidStaticIp:1,
                FlagErrorFree03:1,
                FlagErrorFree04:1,
                FlagErrorFree05:1,
                FlagErrorFree06:1,
                FlagErrorFree07:1,
				FlagRestartModule:1,
				FlagEreaseDatalogger:1,
				FlagUpdateDateTime:1,
				FlagRestoreFactory:1;
}_STATUS_FLAGS;

typedef union
{
    uint8_t FlgError;
    _F_Error_ FlgE;
}_F_Error;


typedef struct
{
	_BUFFER_USB                         BufferUSB;
	UCHAR BufferInfoConf[900];
	UCHAR Buffer_Rx[900];
	UCHAR Buffer_Tx[900];
	int PayloadLen, CurCmd, QtdBytesToEndPage, SendPkts;
	bool FlagAttFW;
	uint16_t FragFile, CompFile;

	uint8_t identifierValidation[20];

	//char ip_addr[30];


    _BUFFER_TCP                         BufferTcp;

    sf_cellular_provisioning_t          * SfProvisionCellular;


    _CONF_NET_BASIC                     ConfNetBasic;    // Contem TipoInterface, DHCP ou Estatico.
    _NET_CELLULAR_CONF                  NetCellularConf; // Configuracoes Cellular

    bool                                FlagConnect;  ///>Utilzado para verificar se ja conectou a rede- status
    bool                                FlagConnectSitrad;  ///>Utilzado para verificar se o Sitrad está conectado
    bool								FlagConnectGPRS;
    bool                                FlagAtCommand;

    uint8_t                             DateTimeValues[DATE_TIME_SIZE]; ///> Utilizado para atualizar o RTC

    _IPSTRUCT                           IpStruct;
    _NET_STATIC_MODE                    IpStaticMode;

    NX_PACKET                           * PacketUdp;
    NX_PACKET                           * PacketUdpSendRx;
    NX_PACKET                           * PacketReceiveTCP;

    NX_TCP_SOCKET                       TcpSocket;
    NX_UDP_SOCKET                       * UdpSocket;

    NX_IP                               * NxIP;
    NX_PPP                              * NxPPP;
    NX_DHCP                             * NxDHCP;

//    uint32_t                            IpHttpRemote;

    uint32_t                            ActualFlags;
    _PACKET_UDP                         PacketUdpSend;

    _BUFFER_TCP                         PacketTcpSend;

    uint8_t                             *BufferTcpRX;           ///Utilzado na recepcao TPCPayload

    _COMM_STATES_MACHINE                 CommStatesMachine;  ///> Contem o estado atual da maquina de estados.

    sf_message_header_t                 *Header;
    _MESSAGE_FRAMEWORK_CFG              MessageCfg; //sk

    //mensagem recebida via framework
    _DATA_IP_PAYLOAD_MESSAGE_PAYLOAD    *DataIpPayload;             ///> ???
    _DATA_TX_PAYLOAD_MESSAGE_PAYLOAD    DataTxPayloadMessagePayload;///> ???

    _DATE_TIME_MESSAGE_PAYLOAD          *DateTimeMessagePayload;    ///> Messagem Payload - Atualiza o RTC Envio
    _SERIAL_DATA_IN_PAYLOAD             *SerialDataInPayload;       ///> Messagem Payload - Serial 485 Recebimento
    _SERIAL_DATA_OUT_PAYLOAD            SerialDataOutPayload;       ///> Messagem Payload - Serial 485 Envio

    _FLASHEXTERNAL_PAYLOAD              *FlashExternalPaylod;

    uint8_t                             CellularRssid;             ///> Contem a intensidade de sinal da rede cellular

    uint8_t                             TimerBigFrameTCP;           ///> TimerBigFrameTCP
    uint8_t                             TimerRestartModule;         ///> TimerResetConv ... Contagem para resetar o modulo conversor.
    uint8_t                             TimerUpdateFWCheckCRC;      ///> TimerOTACheckCRC ... Contagem para verificar o CRC da flash.
    uint8_t                             TimerTimeOutUpdateFW;       ///> TimeOut para atualizar o firmware.
    uint8_t                             TimerRestoreFactory;        ///> Timer responsavel por determinar a hora de restaurar as confs de fabrica.
    uint8_t                             TimerConnectSitrad;         ///> Timer para timeout sitrad
    uint8_t                             TimerInactivity;            ///> Timer para monitoramento de inatividade

    bool                                ValidAccess;                ///> Utilizado em TCP, valida o acesso aos dados de reposta
//    bool                                ValidAccessUSB;
    bool                                BigFrameTCP;                ///> Utilizado em TCPPayload
    bool                                FlagUpdateFWFinished;       ///> Utilziado para sinalizar que uma atualização esta  finalizando.
    bool                                FlagUpdateFWRun;            ///> Utilizado para sinalizar que a atualizacao esta em curso.
    bool                                FlagFAC;                    ///> Utilizado para determinar se está em fac ou nao.
}_COMM_CONTROL;

extern TX_QUEUE Cellular_Flash;
extern TX_QUEUE g_TestFac_Serial;

extern void Cellular_UDP_Notify(NX_UDP_SOCKET *socket_ptr);
extern void Cellular_TCP_DisconnectRequest(NX_TCP_SOCKET *socket_ptr);
extern void Cellular_TCP_ReceiveNotify(NX_TCP_SOCKET *socket_ptr);
extern void Cellular_TCP_ConnectRequest(NX_TCP_SOCKET *socket_ptr, UINT port);
extern void Cellular_Message_Framework_NotifyCallback(TX_QUEUE *Cellular_thread_message_queue);
extern TX_SEMAPHORE CellularStatus_Semaphore;
extern TX_EVENT_FLAGS_GROUP Cellular_Flags;
extern TX_EVENT_FLAGS_GROUP UpFirmareEventFlags;
extern void RtcPost(_COMM_CONTROL *CommData);
extern void RestartTimeoutTcp(timer_t TimeOut);
extern void StartTimeoutTcp(timer_t TimeOut);
extern void StopTimeoutTcp(void);
extern void StopTimerMain(void);
extern void StartTimerMain(void);


#endif /* INCLUDES_CELLULAR_THREAD_ENTRY_H_ */
