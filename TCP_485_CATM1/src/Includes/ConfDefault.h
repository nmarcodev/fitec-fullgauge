/*
 * ConfDefault.h
 *
 *  Created on: 29 de out de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_CONFDEFAULT_H_
#define INCLUDES_CONFDEFAULT_H_

#include "Includes/Cellular_Thread_Entry.h"
#include "Includes/internal_flash.h"
#include "Includes/TypeDefs.h"

extern void CommInitConf(_COMM_CONTROL *CommData);
extern void SetCommFAC(bool bFlagFac);
extern bool GetCommFAC(void);

#endif /* INCLUDES_CONFDEFAULT_H_ */
