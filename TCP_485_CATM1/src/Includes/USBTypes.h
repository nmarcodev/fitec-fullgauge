/*
 * USBTypes.h
 *
 *  Created on: 9 de jul de 2021
 *      Author: rhenanbezerra
 */

#ifndef INCLUDES_USBTYPES_H_
#define INCLUDES_USBTYPES_H_

#include "Includes/TypeDefs.h"
#include "common_data.h" // include obrigatório

//--------------------- DEFINES ---------------------//
#define MODEL_CONV_SIZE         20
#define NAME_CONV_SIZE          30
#define TIME_OUT_485_SIZE       2
#define PASSWORD_SIZE           8
#define LISTS_INSTRUMENTS_SIZE  247



typedef struct
{
    uint8_t ConfDefault;//1 bytes
    uint8_t ModelConv[MODEL_CONV_SIZE]; //20bytes
    uint8_t NameConv[NAME_CONV_SIZE]; //30bytes
    uint8_t EnablePassword;//1 bytes
    uint8_t Version[VERSION_SIZE];  //20bytes
    uint8_t BaudRate485[BAUD_RATE_485_SIZE];//4bytes
    uint8_t TimeOutRs485[TIME_OUT_485_SIZE];//2bytes
    uint8_t Password[PASSWORD_SIZE];//8bytes
    uint8_t StatusSitrad;

    uint8_t BaseStation[BASE_STATION_SIZE];
    uint8_t StatusNetCell;
    uint8_t VersionEXS82[VERSION_EXS82_SIZE];      //20bytes
    uint8_t ApnMode;
    uint8_t ApnActual[APN_ACTUAL_SIZE];         //100bytes
	uint8_t ApnDefault[APN_DEFAULT_SIZE];       //100bytes
	uint8_t ApnAlternative[APN_ALTERNATIVE_SIZE];    //100bytes
	uint8_t ApnManual[APN_MANUAL_SIZE];         //100bytes
	uint8_t ApnUser[APN_USER_SIZE];           //30bytes
	uint8_t ApnPassword[APN_PASSWORD_SIZE];            //30bytes
	uint8_t IdSitrad[ID_SITRAD_SIZE];          //10bytes
	uint8_t Imei[IMEI_SIZE];               //20bytes

}_USB_CONF;

#endif /* INCLUDES_USBTYPES_H_ */
