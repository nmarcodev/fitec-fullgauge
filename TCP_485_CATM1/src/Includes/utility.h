/*
 * utility.h
 *
 *  Created on: July 27, 2017
 *      Author: Xianghui Wang
 */

#ifndef INCLUDES_UTILITY_H_
#define INCLUDES_UTILITY_H_

#include "Includes/Cellular_app_thread_entry.h"
#include "Includes/console_config.h"

#define NUM_MAX_NETS_MEM                1
#define NUM_MAX_TIMES_DEFAULT           2
#define NUM_ATTEMPTS                    2

#define BASE_STATION_SIZE       8
#define VERSION_EXS82_SIZE      20
#define APN_ACTUAL_SIZE         100
#define APN_DEFAULT_SIZE        100
#define APN_ALTERNATIVE_SIZE    100
#define APN_MANUAL_SIZE         100
#define APN_USER_SIZE           30
#define APN_PASSWORD_SIZE       30
#define ID_SITRAD_SIZE          10
#define IMEI_SIZE               20


/** Module Operation Mode */
//typedef enum
//{
//    MODE_AP         = 0,        ///< Operando em modo AP, onde eh provedor de Acesso e de DHCP.
//    MODE_CLIENT     = 1,        ///< Operando em modo Cliente. Neste modo, o equipamento eh cliente.
//}_OP_MODE_WIFI;

typedef struct
{
	uint8_t ValCod;//1 -> A5
    uint8_t BaseStation[BASE_STATION_SIZE];
    uint8_t StatusNetCell;
    uint8_t VersionEXS82[VERSION_EXS82_SIZE];
    uint8_t ApnMode;
    uint8_t ApnActual[APN_ACTUAL_SIZE];
	uint8_t ApnDefault[APN_DEFAULT_SIZE];
	uint8_t ApnAlternative[APN_ALTERNATIVE_SIZE];
	uint8_t ApnManual[APN_MANUAL_SIZE];
	uint8_t ApnUser[APN_USER_SIZE];
	uint8_t ApnPassword[APN_PASSWORD_SIZE];
	uint8_t IdSitrad[ID_SITRAD_SIZE];
	uint8_t Imei[IMEI_SIZE];
}_NET_CELLULAR_CONF; // total 521 bytes


typedef struct NetifStaticMode
{
    uint32_t AddIP;
    uint32_t AddMask;
    uint32_t AddGw;
    uint32_t AddDNS;    ///> DNS Primario
    uint32_t AddDNS2;   ///> DNS Secundario
}_NET_STATIC_MODE;

/** Network interface types */
typedef enum
{
	CELLULAR        = 0,    ///< Cellular
//    WIFI            = 0,    ///< Wi-Fi
//    ETHERNET        = 1,    ///< Ethernet
}_INTERFACE_TYPE;

/** IP Address Mode */
typedef enum
{
    MODE_DHCP           = 0,        ///< Obtain address via DHCP
    MODE_STATIC         = 1,        ///< Static IP address
}_ADDR_MODE;


typedef struct
{
    uint8_t             CodMagic;           ///> Numero coringa para validar configuracao //1 Bytes
    _INTERFACE_TYPE     InterfaceType;      ///> Apenas cellular //1 Bytes
    uint8_t             FlagDNS;            ///> Flag que verifica se tem DNS (0-Nao, 1-Sim)// 1Byte
    _ADDR_MODE          AddrMode;           ///> Configura do modo (0-Dhcp, 1-Static)// 1Byte
    uint8_t             HiddenNet;          ///> Flag da rede -> caso oculta
    _INTERFACE_TYPE     OldInterfaceType;   ///> Apenas cellular //1 Bytes
     char             PasswordDevice[PASSWORD_SIZE];
}_CONF_NET_BASIC;

void print_to_console(char* msg);   /* print user information to console */

#define APP_ERR_TRAP(a)     if(a) {__asm("BKPT #0\n");} /* trap the error location */
#define BUFFER_SIZE (64)    /* assume SSID and password are 64 character or less */
#define MAX_RETRY_CNT       (3)
#endif /* INCLUDES_UTILITY_H_ */
