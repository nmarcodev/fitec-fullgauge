/*
 * ApplicationUSB.h
 *
 *  Created on: 13 de out de 2021
 *      Author: rhenanbezerra
 */

#ifndef INCLUDES_APPLICATIONUSB_H_
#define INCLUDES_APPLICATIONUSB_H_

#include "Includes/Cellular_Thread_Entry.h"
#include "Includes/TypeDefs.h"
#include "Includes/utility.h"
#include "messages.h"


#define HEADER_TX_USB 0x12

//Lista de comandos especiais

#define CMD_LOGIN_CONVERSOR			 	0x01
#define CMD_REINICIA_CONVERSOR			0x02
#define CMD_CONF_FABRICA 				0x03
#define CMD_ATUALIZA_CONF               0x04
#define CMD_CMD_REQ_CONF 				0x05
#define CMD_DESC_CONVERSOR 				0x06
#define CMD_LIMPAR_DATALOGGER 			0x07
#define CMD_LEITURA_LOG 				0x08
#define CMD_AJUSTE_DATA_HORA 			0x09
#define	CMD_STATUS_CONVERSOR 			0x0A
#define CMD_ATUALIZACAO_FW 				0x0B
#define CMD_TESTE_FABRICA 				0x0C

#define DEBUG_USB (1)

typedef enum
{
   USB_NONE,
   // eventos
   USB_SEND,
   USB_BUFFERING,
   USB_REQUESTED,
   USB_REQUESTED_CONFIG,
   USB_REQUESTED_485,
   USB_REQUESTED_CONF,
   USB_RELISTEN,

}_USB_RETURN;

typedef enum
{
    SM_IDLE,
    SM_INIT_ETH,
    SM_WAIT_INTERFACE,
    SM_WAIT_QUEUE,
    SM_WAIT_RESET,
    SM_EVENT_FLAG,
    SM_DHCP,
    SM_STATIC_IP,
    SM_STATUS_IP,
    SM_RESTART_IP,
    SM_VIEW_IP,
    SM_TESTE_CHANGE,

    SM_CHANGE_MODE,
    SM_WAIT_LINK_ETH,

    SM_INIT_WIFI,
    SM_READ_FLASH,
    SM_READ_FLASH_NET_DEFAULT,
    SM_SCAN,
    SM_PROVISION,
    SM_PROVISION_FAIL,

    SM_INIT_AP,

    SM_DONE,

}_USB_STATES_MACHINE;


typedef enum
{

	CNV_TXPACKT		 	  = 0x21,
	CNV_INIT_TXBUFF 	  = 0x22,
	CNV_BDY_TXBUFF  	  = 0x23,
	CNV_END_TXBUFF 	      = 0x25,
	CNV_RXBUFF	 	      = 0x26,
	CNV_FLUSH_USBBUFF     = 0x27,
	CNV_RD_RXSIZE         = 0x29,

//----------------------------------
	// respostas conv
	CNV_TXPACKT_OK	 	  = 0xA1,
	CNV_INIT_TXBUFF_OK 	  = 0xA2,
	CNV_BDY_TXBUFF_OK  	  = 0xA3,
	CNV_END_TXBUFF_OK 	  = 0xA5,
	CNV_RXBUFF_BDY	 	  = 0xA6,
	CNV_FLUSH_USBBUFF_OK  = 0xA7,
	CNV_RXPACKT_OK 		  = 0X26,
	CNV_CRC_ERROR		  = 0x8B,
	CNV_RD_RXSIZE_BDY     = 0xA9,
}CONV_CMD;

extern _USB_RETURN SendPacketUSB(_COMM_CONTROL *CommData, uint16_t len);

extern void  usb_activate(void *hid_instance);
extern void  usb_deactivate(void *hid_instance);

#endif /* INCLUDES_APPLICATIONUSB_H_ */
