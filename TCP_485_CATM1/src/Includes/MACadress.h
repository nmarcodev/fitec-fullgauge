#ifndef INCLUDES_MACADRESS_H_
#define INCLUDES_MACADRESS_H_

#include "Includes/TypeDefs.h"

extern void SetMACValueCell(uint8_t *MacAddressCell);
extern _FG_ERR GetMACValueCell(uint8_t *MacAddressCell);

#endif /* INCLUDES_MACADRESS_H_ */
