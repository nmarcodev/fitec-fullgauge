#ifndef __RX_8571LC_H
#define __RX_8571LC_H


#define RX_8571LC_INITIAL_REGISTER      0x00
#define RX_8571LC_RAM_REGISTER          0x07
#define RX_8571LC_FLAG_REGISTER         0x0E
#define RX_8571LC_CONTROL_REGISTER      0x0F

#define RX_8571LC_DATETIME_SIZE         0x07
#define RX_8571LC_INIT_SIZE             0x08
#define RX_8571LC_ALL_REGISTERS_SIZE    0x10

#define RX_8571LC_VLF_BIT               0x02
#define RX_8571LC_STOP_BIT              0x02


_FG_ERR RX8571LC_GetExternalDateTime(uint8_t* DateTimeList);
_FG_ERR RX8571LC_SetExternalDateTime(uint8_t* const DateTimeList);
_FG_ERR RX8571LC_VerifyEclo(void);
_FG_ERR RX8571LC_Init(void);

typedef struct
{
    uint8_t Second;
    uint8_t Minute;
    uint8_t Hour;
    uint8_t Week;
    uint8_t Day;
    uint8_t Month;
    uint8_t Year;
    uint8_t RAM;
    uint8_t MinAlarm;
    uint8_t HourAlarm;
    uint8_t WeekDayAlarm;
    uint8_t TimerCounter0;
    uint8_t TimerCounter1;
    uint8_t ExtensionRegister;       // bit7 FSEL1   bit6 FSEL0  bit5 USEL   bit4 TE     bit3 WADA   bit2 TSEL2  bit1 TSEL1  bit0 TSEL0
    uint8_t FlagRegister;            // bit7 TES1    bit6 TEST2  bit5 UF     bit4 TF     bit3 AF     bit2 TEST3  bit1 VLF    bit0 NA
    uint8_t ControlRegister;         // bit7 NA      bit6 NA     bit5 UIE    bit4 TIE    bit3 AIE    bit2 TSTP   bit1 STOP   bit0 NA
}_EXTERNAL_RTC_REGISTER_TABLE;

typedef struct
{
    uint8_t FirstRegister;
    _EXTERNAL_RTC_REGISTER_TABLE RegisterTable;
}__EXTERNAL_RTC_RW_TABLE;


extern void InitI2C_RTC(void);
extern void CloseI2C_RTC(void);

#endif
