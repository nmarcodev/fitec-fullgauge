/*
 * MainManagerThreadEntry.h
 *
 *  Created on: 27 de mai de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_MAINMANAGERTHREADENTRY_H_
#define INCLUDES_MAINMANAGERTHREADENTRY_H_

#include  "Includes/ApplicationLedsStatus.h"

typedef enum
{
    MM_FLAGS_NONE               = 0,
    // eventos
    MM_CELL_TYPE                = 0x00000010,
    MM_CELL_WAIT                = 0x00000020,
    MM_CELL_CONNECTED           = 0x00000040,
    MM_CELL_NOT_CONNECTED       = 0x00000080,

    MM_TIMER_IO                 = 0x00000100,
    MM_ERRO_PWD                 = 0x00000200,
    MM_ERRO_IP                  = 0x00000400,
    MM_ERRO_STATIC_IP           = 0x00000800,

    MM_CONNECTED                = 0x00001000,
    MM_DISCONNECTED             = 0x00002000,
    MM_CONNECTED_SITRAD         = 0x00004000,
    MM_DISCONNECTED_SITRAD      = 0x00008000,

    MM_READY_TO_CONNECT         = 0x00010000,
    MM_CONNECT_AND_READY        = 0x00020000,
    MM_NOT_READY                = 0x00040000,
    MM_NET_CONNECTING           = 0x00080000,

    MM_S_CELL_EXCELLENT         = 0x00100000, //VERDE
    MM_S_CELL_GOOD              = 0x00200000, //AMARELO
    MM_S_CELL_POOR              = 0x00400000, //VINHO_ROSE
    MM_S_CELL_VERY_POOR         = 0x00800000, //VERMELHO

    MM_STATUS_CELL              = 0x01000000,
    MM_S_CELL_NO_SIGNAL         = 0x02000000,

    MM_STATUS_UPDATE_FW         = 0x10000000,
    MM_RESTORE_FACTORY          = 0x20000000,
    MM_FAC_MODE                 = 0x40000000,
    MM_FAC_TEST_LEDs            = 0x80000000,

    MM_ALL                      = 0xFFFFFFFF,
    MM_SIGNAL_CELL              = 0x00000000,

}_MAIN_MANAGER_FLAGS;

typedef enum
{
    STATE_IDLE,

    STATE_CELL_STARTING,
    STATE_CELL_WAIT_LINK,
    STATE_CELL_CONNECTING,
    STATE_CELL_CONNECTED,
    STATE_CELL_DISCONNECTED,

}_MAIN_MANAGER_STATES;

//----------------------------------------------------//

extern TX_EVENT_FLAGS_GROUP g_main_event_flags;

extern void ThreadMainManagerSetFlag(_MAIN_MANAGER_FLAGS EventFlag);
extern void ThreadMainManagerSetLedColor(uint8_t LedColorTest);
extern bool ThreadMainManagerGetStatusButton(void);

#endif /* INCLUDES_MAINMANAGERTHREADENTRY_H_ */
