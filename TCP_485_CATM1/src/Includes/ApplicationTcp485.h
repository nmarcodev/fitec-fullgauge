/*
 * ApplicationTcp485.h
 *
 *  Created on: 4 de nov de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_APPLICATIONTCP485_H_
#define INCLUDES_APPLICATIONTCP485_H_

#include "common_data.h"
#include "Includes/Cellular_Thread_Entry.h"
#include "Includes/MainManagerThreadEntry.h"
#include "Includes/ApplicationIp.h"
#include "Includes/ApplicationTcpPayload.h"
#include "Includes/ApplicationMeFramw.h"
#include "Includes/ApplicationCellular.h"
#include "Includes/ApplicationCommonModule.h"
#include "Cellular_Thread.h"
#include "Thread_Datalogger.h"
#include "Includes/utility.h"
#include "Includes/MainManagerThreadEntry.h"
#include "Includes/internal_flash.h"
#include "Includes/ApplicationUdp.h"
#include "Includes/ApplicationTcp.h"
#include "Includes/ApplicationLedsStatus.h"
#include "Includes/Define_IOs.h"
#include "Includes/ApplicationTimerMain.h"
#include "Includes/ApplicationUSB.h"

extern void ReadFlashNetDefault(_COMM_CONTROL *CommControl);

extern void InitDhcp(_COMM_CONTROL *CommControl, _STATUS_FLAGS *StatusFlagsX);
extern void DnsIp(_COMM_CONTROL *CommControl, _STATUS_FLAGS *StatusFlagsX);

extern void IdleInit(_COMM_CONTROL *CommControl, _STATUS_FLAGS *StatusFlagsX);
extern void InitCellular(_COMM_CONTROL *CommControl, _STATUS_FLAGS *StatusFlagsX);
extern void NetStateMachine(_COMM_CONTROL *CommControl, _STATUS_FLAGS *StatusFlagsX);
extern void WaitPPP(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX);
extern void CellularProvisionFail(_COMM_CONTROL *CommControl, _STATUS_FLAGS *StatusFlagsX, uint8_t *NumAttmptsPwdX);
extern _MAIN_MANAGER_FLAGS NetStatusModule(_COMM_CONTROL *CommControl, _STATUS_FLAGS *StatusFlagsX);
extern void StatusSitrad(_STATUS_FLAGS *StatusFlagsX);
extern bool GetStatusSitrad(void);

#endif /* INCLUDES_APPLICATIONTCP485_H_ */
