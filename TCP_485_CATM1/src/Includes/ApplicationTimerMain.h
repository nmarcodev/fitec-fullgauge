/*
 * ApplicationTimerMain.h
 *
 *  Created on: 18 de nov de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_APPLICATIONTIMERMAIN_H_
#define INCLUDES_APPLICATIONTIMERMAIN_H_

#include "Includes/Cellular_Thread_Entry.h"
#include "common_data.h" // include obrigatório
#include "Cellular_Thread.h"
#include "Includes/Define_IOs.h"
#include "Includes/ApplicationCommonModule.h"
#include "Includes/FlashQSPI.h"

extern void EventTimerMain(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX);

#endif /* INCLUDES_APPLICATIONTIMERMAIN_H_ */
