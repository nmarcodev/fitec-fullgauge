/*
 * ApplicationTcp.h
 *
 *  Created on: 6 de jun de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_APPLICATIONTCP_H_
#define INCLUDES_APPLICATIONTCP_H_

extern void TcpStart(_COMM_CONTROL *CommData);
extern uint8_t TcpAcceptConnect(_COMM_CONTROL *CommData);
extern _TCP_RETURN TcpEvent(_COMM_CONTROL *CommData);
extern void TcpSend(_COMM_CONTROL *CommData);
extern void TcpFinish(_COMM_CONTROL *CommData);
extern void TcpDisconnect(_COMM_CONTROL *CommData);
extern void TcpRefugeeConnection(_COMM_CONTROL *CommData);

#endif /* INCLUDES_APPLICATIONTCP_H_ */
