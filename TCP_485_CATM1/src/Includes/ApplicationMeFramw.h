/*
 * ApplicationMeFramw.h
 *
 *  Created on: 7 de jun de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_APPLICATIONMEFRAMW_H_
#define INCLUDES_APPLICATIONMEFRAMW_H_

#include "Includes/Cellular_Thread_Entry.h"

extern void RtcPost(_COMM_CONTROL *CommData);
extern void SerialPost(_COMM_CONTROL *CommData);
extern bool PendSerial485(_COMM_CONTROL *CommData);

#endif /* INCLUDES_APPLICATIONMEFRAMW_H_ */
