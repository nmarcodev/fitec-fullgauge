/*
 * ApplicationIp.h
 *
 *  Created on: 7 de jun de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_APPLICATIONIP_H_
#define INCLUDES_APPLICATIONIP_H_

extern void CommOpen(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX);
extern uint8_t CellularStartDhcpClient (_COMM_CONTROL *CommData);
extern uint8_t CellularStartStaticIP(NX_IP * NxIP, ULONG Address, ULONG Mask, ULONG Gtw, ULONG Dns);
extern uint8_t CellularGetStatusIp(_COMM_CONTROL *CommData);
extern UINT NetStop (_COMM_CONTROL *CommData);
extern void CellularCheckConnectivity(_COMM_CONTROL *CommControl, _STATUS_FLAGS *StatusFlagsX);

#endif /* INCLUDES_APPLICATIONIP_H_ */
