/*
 * ApplicationUdp.h
 *
 *  Created on: 16 de mai de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_APPLICATIONUDP_H_
#define INCLUDES_APPLICATIONUDP_H_

#include "Includes/Cellular_Thread_Entry.h"

extern void UdpStart(_COMM_CONTROL *CommData);
extern void UdpUnbind(_COMM_CONTROL *CommData);
extern void UdpRelease(_COMM_CONTROL *CommData);
extern void UdpFinish(_COMM_CONTROL *CommData);

extern void ReadPacketUdpCuston(_UDP_CONF *ConfSitradUdp);
extern void XatPacketUdp(_PACKET_UDP *PacketUdp, _IPSTRUCT *PonterIpStruct);

extern void GetPacketUdpCuston(_UDP_CONF *ConfSitradUdp);
extern void SetPacketUdpCuston(_UDP_CONF *ConfSitradUdp);

extern void GetUdpIp(_IPSTRUCT *ConfSitradIPUdp);
extern void SetUdpIp(_IPSTRUCT *ConfSitradIPUdp);

extern void UdpSendOnDemand(NX_UDP_SOCKET *UdpX);

#endif /* INCLUDES_APPLICATIONUDP_H_ */
