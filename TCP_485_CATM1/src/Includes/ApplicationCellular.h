#ifndef INCLUDES_APPLICATIONCELLULAR_H_
#define INCLUDES_APPLICATIONCELLULAR_H_

#include "Includes/TypeDefs.h"
#include "Includes/Cellular_Thread_Entry.h"
#include "nx_web_http_client.h"
#include "nx_secure_tls.h"

#define DEBUG_CELLULAR (1)

#define MAX_NUM_NET_SCAN    BUFFER_SIZE_RX_WINDS
#define MAX_NUM_MAC_LIST    5

#define CELLULAR_MODULE_RESET_PIN       (IOPORT_PORT_04_PIN_08)
#define CELLULAR_MODULE_POWER_PIN       (IOPORT_PORT_03_PIN_07)

#define BUFFER_SIZE             (64)
#define TIM
#define NETWORK_SCAN_MODE_CMD   ("AT+QCFG=\"nwscanmode\",1,1\r\n")

#define NETWORK_SCAN_SEQ_CMD    ("AT^SXRAT=9,0\r\n")//("AT+QCFG=\"nwscanseq\",010203,1\r\n")
#define NETWORK_ROAM_SERV_CMD   ("at+creg=0\r\n")//("AT+QCFG=\"roamservice\",2,1\r\n")
#define CMD_RESPONSE            ("\r\nOK\r\n")
#define CMD_TIMEOUT             (300)

#define PWRKEY_PIN              (IOPORT_PORT_01_PIN_02)
#define PPP_LINK_TIMEOUT        (60 * TX_TIMER_TICKS_PER_SECOND)
#define DNS_SERVER_1_ADDRESS    (IP_ADDRESS(8,8,8,8)) /* Google Public DNS servers as a backup */
#define DNS_SERVER_2_ADDRESS    (IP_ADDRESS(8,8,4,4)) /* Google Public DNS servers as a backup */

#define ID_SITRAD_SERVER       "hlg-introducer.sitrad.com"
#define NETWORK_TIMEOUT        (NX_WAIT_FOREVER)

extern _FG_ERR ScanConfig(_COMM_CONTROL *CommData);
extern uint8_t ModuleProvision(_COMM_CONTROL *CommData);
extern uint8_t FlashReadNets(_COMM_CONTROL *WifiData);
extern void GetStatusCellular(_COMM_CONTROL *CommData);
extern bool MacAddressList(uint8_t *MacAdd, bool AddTRemoveF);
extern void EXS82_init(void);
extern uint8_t GetIDSitrad(_COMM_CONTROL *CommData);
extern uint8_t UpdateIPServer(_COMM_CONTROL *CommData);

UINT tls_setup_callback(NX_WEB_HTTP_CLIENT *p_client_ptr, NX_SECURE_TLS_SESSION *p_tls_session);

_COMM_CONTROL CommControl;

extern char IP_Adress[30];

#endif /* INCLUDES_APPLICATIONCELLULAR_H_ */
