/*
 * Eeprom25AA02E48.h
 *
 *  Created on: 5 de jun de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_EEPROM25AA02E48_H_
#define INCLUDES_EEPROM25AA02E48_H_

#include "Includes/TypeDefs.h"

#define CS_EEPROM      IOPORT_PORT_01_PIN_07

///> Microchip EEPROM GET MAC_ADDRESS 25AA02E48
#define EEPROM_READ         0x03 ///> Read data from memory array beginning at selected address
#define EEPROM_WRITE        0x02 ///> Write data to memory array beginning at selected address
#define EEPROM_WRDI         0x04 ///> Reset the write enable latch (disable write operations)
#define EEPROM_WREN         0x06 ///> Set the write enable latch (enable write operations)
#define EEPROM_RDSR         0x05 ///> Read STATUS register
#define EEPROM_WRSR         0x01 ///> Write STATUS register

#define EEPROM_ADDR_STA     0x00 ///> Inicio da posição de memoria
#define EEPROM_ADDR_LAS     0xBD ///> Ultima posicao da memoria para salvar sectores

#define EEPROM_ADDR_OVERF   0xBE ///> Posicao para salvar status de sobrescrita do datalogger (Overrun - Virada da memoria/flash Datalogger)

#define EEPROM_ADDR_FLGS    0xBF ///> Posicao para salvar os flags da eeprom (Teste de Fabrica)

#define EEPROM_ADDR_MAC     0xFA ///> Posicao de memoria da eeprom onde inicia o MAC_ADDRESS

#define EEPROM_SIZE         0XC0 ///> Tamanho da memoria
#define EEPROM_SIZE_PAGE    0x10 ///> Tamanho da pagina da memoria

extern bool MACValue(uint8_t *EepromMacAddress, uint8_t Select);
extern void SetMACValue(uint8_t *EepromMacAddress);
extern _FG_ERR GetMACValue(uint8_t *EepromMacAddress);

#endif /* INCLUDES_EEPROM25AA02E48_H_ */
