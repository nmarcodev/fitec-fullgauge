/***********************************************************************************************************************
 * Copyright [2015-2017] Renesas Electronics Corporation and/or its licensors. All Rights Reserved.
 *
 * This file is part of Renesas SynergyTM Software Package (SSP)
 *
 * The contents of this file (the "contents") are proprietary and confidential to Renesas Electronics Corporation
 * and/or its licensors ("Renesas") and subject to statutory and contractual protections.
 *
 * This file is subject to a Renesas SSP license agreement. Unless otherwise agreed in an SSP license agreement with
 * Renesas: 1) you may not use, copy, modify, distribute, display, or perform the contents; 2) you may not use any name
 * or mark of Renesas for advertising or publicity purposes or in connection with your use of the contents; 3) RENESAS
 * MAKES NO WARRANTY OR REPRESENTATIONS ABOUT THE SUITABILITY OF THE CONTENTS FOR ANY PURPOSE; THE CONTENTS ARE PROVIDED
 * "AS IS" WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT; AND 4) RENESAS SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, OR
 * CONSEQUENTIAL DAMAGES, INCLUDING DAMAGES RESULTING FROM LOSS OF USE, DATA, OR PROJECTS, WHETHER IN AN ACTION OF
 * CONTRACT OR TORT, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THE CONTENTS. Third-party contents
 * included in this file may be subject to different terms.
 **********************************************************************************************************************/
/***********************************************************************************************************************
 * File Name    : sf_cellular_api.h
 * Description  : This layer provides APIs to manage Cellular device driver.
 ***********************************************************************************************************************/

#ifndef SF_CELLULAR_API_H
#define SF_CELLULAR_API_H

/*******************************************************************************************************************//**
 * @ingroup SF_Interface_Library
 * @defgroup SF_CELLULAR_API SF CELLULAR Framework Interface
 * @brief RTOS-integrated SF CELUULAR Framework Interface.
 *
 * @section SF_CELLULAR_API_SUMMARY Summary
 *
 * This SSP Interface provides access to the ThreadX-aware SF CELLULAR Framework.
 *
 * @{
 **********************************************************************************************************************/

/***********************************************************************************************************************
 * Includes
 **********************************************************************************************************************/
#include "bsp_api.h"

/* Common macro for SSP header files. There is also a corresponding SSP_FOOTER macro at the end of this file. */
SSP_HEADER

/**********************************************************************************************************************
 * Macro definitions
 **********************************************************************************************************************/

/** Major Version of the API defined in this file */
#define SF_CELLULAR_API_VERSION_MAJOR (1U)
/** Minor Version of the API defined in this file */
#define SF_CELLULAR_API_VERSION_MINOR (6U)

#define SF_CELLULAR_ACESS_TECH_NAME_LEN           (64U)   ///< Maximum length for Access Technology name
#define SF_CELLULAR_MAX_OPERATOR_NAME_LEN         (32U)   ///< Maximum length for Operator name
#define SF_CELLULAR_MAX_PREFFERED_OPERATOR_COUNT  (5U)    ///< Maximum number of preferred operator
#define SF_CELLULAR_IMEI_LEN                      (16U)   ///< Maximum length of IMEI number
#define SF_CELLULAR_FWVERSION_LEN                 (32U)   ///< Maximum length of Firmware Version
#define SF_CELLULAR_MAX_STRING_LEN                (32U)   ///< Maximum string length
#define SF_CELLULAR_CHIPSET_LEN                   (16U)   ///< Maximum length of Chipset info
#define SF_CELLULAR_MFG_NAME_LEN                  (16U)   ///< Maximum length of manufacturer
#define SF_CELLULAR_CID_LEN                       (16U)   ///< Maximum length of CID
#define SF_CELLULAR_IMSI_LEN                      (24U)   ///< Maximum length of IMSI
#define SF_CELLULAR_IP_ADDR_LEN                   (128U)  ///< Maximum lenfth of IP address

/** Logical Value TRUE for Cellular */
#define SF_CELLULAR_TRUE        (1U)
/** Logical Value FALSE for Cellular */
#define SF_CELLULAR_FALSE       (0U)

/**********************************************************************************************************************
 * Typedef definitions
 **********************************************************************************************************************/
/** Operator selection mode */
typedef enum e_sf_cellular_op_select_mode
{
    SF_CELLULAR_OP_SELECT_MODE_AUTO = 0,                ///< Automatic Operator selection
    SF_CELLULAR_OP_SELECT_MODE_MANUAL = 1,              ///< Manual Operator selection
    SF_CELLULAR_OP_SELECT_MODE_DEREGISTER = 2,          ///< De-register from the network
    SF_CELLULAR_OP_SELECT_MODE_MANUAL_FALLBACK = 4,     ///< Manual with fallback to automatic
} sf_cellular_op_select_mode_t;

/** Cellular Network Registration Status */
typedef enum e_sf_cellular_network_reg_status
{
    SF_CELLULAR_NETWORK_REG_STATUS_NOT_REGISTERED_NO_SEARCH = 0,    ///< not registered, MT is not currently searching a new operator to register to
    SF_CELLULAR_NETWORK_REG_STATUS_REGISTERED_HOME_NETWORK = 1,     ///< registered, home network
    SF_CELLULAR_NETWORK_REG_STATUS_NOT_REGISTERED_SEARCHING = 2,    ///< not registered, but MT is currently searching a new operator to register to
    SF_CELLULAR_NETWORK_REG_STATUS_REGISTRATION_DENIED = 3,         ///< registration denied
    SF_CELLULAR_NETWORK_REG_STATUS_UNKNOWN = 4,                     ///< registration status unknown
    SF_CELLULAR_NETWORK_REG_STATUS_REGISTERED_ROAMING = 5,          ///< Registered, roaming
} sf_cellular_network_reg_status_t;

/** Timezone update mode */
typedef enum e_sf_cellular_timezone_update_mode
{
    SF_CELLULAR_TIMEZONE_UPDATE_AUTO_DISABLE,   ///< Disable automatic time zone update
    SF_CELLULAR_TIMEZONE_UPDATE_AUTO_ENABLE     ///< Enable automatic time zone update
} sf_cellular_timezone_update_mode_t;

/** Cellular Framework event codes */
typedef enum e_sf_cellular_event
{
    SF_CELLULAR_EVENT_RX,           ///< Packet received event
    SF_CELLULAR_EVENT_PROVISIONSET  ///< Provisioning Set event
} sf_cellular_event_t;

/** Cellular Module reset type */
typedef enum e_sf_cellular_reset_type
{
    SF_CELLULAR_RESET_TYPE_SOFT,   ///< Soft reset module using AT command
    SF_CELLULAR_RESET_TYPE_HARD,   ///< Hard reset module by toggling Reset Pin
} sf_cellular_reset_type_t;

/** Airplane mode */
typedef enum e_sf_cellular_airplane_mode
{
    SF_CELLULAR_AIRPLANE_MODE_OFF,  ///< Airplane mode disabled
    SF_CELLULAR_AIRPLANE_MODE_ON    ///< Airplane mode enabled
} sf_cellular_airplane_mode_t;

/** PDP type */
typedef enum e_sf_cellular_pdp_type
{
    SF_CELLULAR_PDP_TYPE_IP,          ///< Internet protocol
    SF_CELLULAR_PDP_TYPE_PPP,         ///< Point to point protocol
    SF_CELLULAR_PDP_TYPE_IPV6,        ///< Internet protocol, version 6
    SF_CELLULAR_PDP_TYPE_IPV4V6       ///< Virtual introduced to handle dual stack UE capability
} sf_cellular_pdp_type_t;

/** Cellular operator name format */
typedef enum e_sf_cellular_op_name_format
{
    SF_CELLULAR_OP_NAME_FORMAT_LONG,     ///< Long alphanumeric
    SF_CELLULAR_OP_NAME_FORMAT_SHORT,    ///< Short alphanumeric
    SF_CELLULAR_OP_NAME_FORMAT_NUMERIC   ///< Numeric
} sf_cellular_op_name_format_t;

/** Cellular authentication type */
typedef enum e_sf_cellular_auth_type
{
    SF_CELLULAR_AUTH_TYPE_NONE,   ///< No authentication
    SF_CELLULAR_AUTH_TYPE_PAP,    ///< PAP
    SF_CELLULAR_AUTH_TYPE_CHAP,   ///< CHAP
} sf_cellular_auth_type_t;

/** Enumeration for AT command index */
typedef enum e_sf_cellular_at_cmd_index
{
    SF_CELLULAR_AT_CMD_INDEX_AT = 0,                            ///< Index for Command AT
    SF_CELLULAR_AT_CMD_INDEX_ATZ0,                              ///< Index for Command ATZ0
    SF_CELLULAR_AT_CMD_INDEX_AT_CREG_SET_0,                     ///< Index for Command to set AT+CREG
    SF_CELLULAR_AT_CMD_INDEX_AT_CMEE_SET_0,                     ///< Index for Command to set AT+CMEE
    SF_CELLULAR_AT_CMD_INDEX_AT_ECHO,                           ///< Index for Command ATE
    SF_CELLULAR_AT_CMD_INDEX_AT_SAVE,                           ///< Index for Command AT&W
    SF_CELLULAR_AT_CMD_INDEX_AT_ENTER_CPIN,                     ///< Index for Command to unlock SIM
    SF_CELLULAR_AT_CMD_INDEX_AT_CPIN_SET,                       ///< Index for Command to set SIM PIN
    SF_CELLULAR_AT_CMD_INDEX_AT_CGDCONT_SET,                    ///< Index for Command to set AT+CGDCOND
    SF_CELLULAR_AT_CMD_INDEX_AT_CPOL_SET,                       ///< Index for Command to set AT+CPOL
    SF_CELLULAR_AT_CMD_INDEX_AT_COPS_GET,                       ///< Index for Command to get current operator details
    SF_CELLULAR_AT_CMD_INDEX_AT_AIRPLANE_OFF,                   ///< Index for Command to set Airplane mode OFF
    SF_CELLULAR_AT_CMD_INDEX_AT_AIRPLANE_ON,                    ///< Index for Command to set Airplane mode ON
    SF_CELLULAR_AT_CMD_INDEX_AT_CONTEXT_ACTIVE,                 ///< Index for Command to activate context
    SF_CELLULAR_AT_CMD_INDEX_AT_CONTEXT_DEACTIVE,               ///< Index for Command to deactivate context
    SF_CELLULAR_AT_CMD_INDEX_AT_CGDATA_ACTIVE,                  ///< Index for Command to activate Data mode
    SF_CELLULAR_AT_CMD_INDEX_AT_CGDATA_DEACTIVE,                ///< Index for Command to deactivate Data mode
    SF_CELLULAR_AT_CMD_INDEX_AT_CSQ_GET,                        ///< Index for Command to get signal quality
    SF_CELLULAR_AT_CMD_INDEX_AT_VER_GET,                        ///< Index for Command to get Modem stack Version
    SF_CELLULAR_AT_CMD_INDEX_AT_CHIPSET_GET,                    ///< Index for Command to get chipset details
    SF_CELLULAR_AT_CMD_INDEX_AT_IMEI_GET,                       ///< Index for Command to get IMEI number
    SF_CELLULAR_AT_CMD_INDEX_AT_MANF_NAME_GET,                  ///< Index for Command to get manufacturer name
    SF_CELLULAR_AT_CMD_INDEX_AT_SIMID_GET,                      ///< Index for Command to get SIM Card ID
    SF_CELLULAR_AT_CMD_INDEX_AT_NET_TYPE_STATUS_GET,            ///< Index for Command to get network type information
    SF_CELLULAR_AT_CMD_INDEX_AT_NET_STATUS_GET,                 ///< Index for Command to get network status information
    SF_CELLULAR_AT_CMD_INDEX_AT_LOCK_SIM,                       ///< Index for Command to lock SIM card
    SF_CELLULAR_AT_CMD_INDEX_AT_UNLOCK_SIM,                     ///< Index for Command to unlock SIM card
    SF_CELLULAR_AT_CMD_INDEX_AT_ENTER_DATA_MODE,                ///< Index for Command to enter data mode
    SF_CELLULAR_AT_CMD_INDEX_AT_EXIT_DATA_MODE,                 ///< Index for Command to exit data mode
    SF_CELLULAR_AT_CMD_INDEX_AT_USERNAME_SET,                   ///< Index for Command to set username
    SF_CELLULAR_AT_CMD_INDEX_AT_PASSWORD_SET,                   ///< Index for Command to set password
    SF_CELLULAR_AT_CMD_INDEX_AT_AUTH_TYPE_SET,                  ///< Index for Command to set authorisation type
    SF_CELLULAR_AT_CMD_INDEX_AT_AUTO_TIME_UPDATE_ENABLE_SET,    ///< Index for Command to enable auto time update
    SF_CELLULAR_AT_CMD_INDEX_AT_AUTO_TIME_UPDATE_DISABLE_SET,   ///< Index for Command to disable auto time update
    SF_CELLULAR_AT_CMD_INDEX_AT_EMPTY_APN_SET,                  ///< Index for Command to set empty APN
    SF_CELLULAR_AT_CMD_INDEX_AT_GET_IP_ADDR,                    ///< Index for Command to get IP address
    SF_CELLULAR_AT_CMD_INDEX_AT_NWSCANSEQ_SET,                  ///< Index for Command to set network fallback sequence
    SF_CELLULAR_AT_CMD_INDEX_AT_NWSCANMODE_SET,                 ///< Index for Command to set network scan mode
    SF_CELLULAR_AT_CMD_INDEX_AT_IOTOPMODE_SET,                  ///< Index for Command to Configure Network Category to be Searched under LTE
    SF_CELLULAR_AT_CMD_INDEX_AT_SWITCH_BACK_TO_DATA_MODE,       ///< Index for Command to switch back to Data mode
    SF_CELLULAR_AT_CMD_INDEX_AT_GET_IMSI,                       ///< Index for Command to get IMSI ID
    SF_CELLULAR_AT_CMD_INDEX_AT_IPR_SET,                        ///< Index for Command to set baud rate of modem
    SF_CELLULAR_AT_CMD_INDEX_AT_BAUD_CHECK,                     ///< Index for Command to check whether modem is responding after baud update
    SF_CELLULAR_AT_CMD_INDEX_AT_SIM_EFFECT_SET,                 ///< Index for Command to set SIM priority effect
    SF_CELLULAR_AT_CMD_INDEX_AT_NBIOT_BAND_SELECTION,

} sf_cellular_at_cmd_index_t;

/** Enumeration for configurable AT command index */
typedef enum e_sf_cellular_config_at_cmd_index
{
    SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_CREG,                     ///< Index for Command AT+CREG
    SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_CEREG,                    ///< Index for Command AT+CEREG
    SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_COPS_AUTO_SET,            ///< Index for Command to set AUTO AT+COPS
    SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_COPS_MANUAL_SET,          ///< Index for Command to set Manual AT+COPS
    SF_CELLULAR_CONFIG_AT_CMD_INDEX_AT_CPIN_STATUS_GET,          ///< Index for Command to get status of SIM lock
}sf_cellular_config_at_cmd_index_t;

/** Enumeration for SIM lock status */ 
typedef enum e_sf_cellular_sim_status
{
    SF_CELLULAR_SIM_STATUS_READY,               ///< SIM in Cellular modem is not pending for any password
    SF_CELLULAR_SIM_STATUS_PIN_REQUIRED,        ///< SIM in Cellular modem is lock and waiting for (U)SIM PIN to be given
    SF_CELLULAR_SIM_STATUS_PIN_PUK_REQUIRED,    ///< SIM in Cellular modem is lock and waiting for (U)SIM PIN and PUK to be given
} sf_cellular_sim_status_t;

/** Command Buffer type specifies the type of data in debug log buffer */
typedef enum e_sf_cellular_log_buffer_type
{
    SF_CELLULAR_LOG_BUFFER_TYPE_COMMAND,                          ///< Data in logging buffer is an AT command
    SF_CELLULAR_LOG_BUFFER_TYPE_RESPONSE_RECEIVED,                ///< Data in logging buffer is complete response received from modem
    SF_CELLULAR_LOG_BUFFER_TYPE_RESPONSE_RECEIVED_WITH_TIMEOUT,   ///< Data in logging buffer is response received from modem with timeout
} sf_cellular_log_buffer_type_t;

/** Cellular Provisioning information structure */
typedef struct st_sf_cellular_provisioning
{
    uint8_t apn[SF_CELLULAR_MAX_STRING_LEN];        ///< Access Point Name
    sf_cellular_auth_type_t auth_type;                              ///< Authentication type: PAP/CHAP
    uint8_t username[SF_CELLULAR_MAX_STRING_LEN];   ///< User name used for authentication
    uint8_t password[SF_CELLULAR_MAX_STRING_LEN];   ///< Password used for authentication
    sf_cellular_airplane_mode_t airplane_mode;                          ///< Airplane mode
    uint8_t context_id;                             ///< Context ID to be used for connection
    sf_cellular_pdp_type_t pdp_type;                               ///< PDP Type for Context
} sf_cellular_provisioning_t;

/** Modem Command/Response structure used to send Custom AT command and
 *  to receive response for the same
 */
typedef struct st_sf_cellular_cmd_resp
{
    /** AT command/Response buffer. In case of AT command it is input buffer
     * in which user should pass custom command to be sent. In case of Response
     * it is output buffer in which framework will fill the response
     */
    uint8_t *p_buff;

    /** AT command/Response buffer length. In case of Response this is both
     * in and out parameter. Input is the length of buffer pointed by p_buff and
     * output is number of bytes of response copied by framework. In case of command
     * it is length of command in p_buff
     */
    uint32_t buff_len;
} sf_cellular_cmd_resp_t;

/** Cellular Framework control structure */
typedef struct st_sf_cellular_ctrl
{
    void * p_driver_handle;             ///< Stores information required by underlying Cellular device driver.
} sf_cellular_ctrl_t;

/** The statistic and error counters for this instance */
typedef struct st_sf_cellular_stats
{
    uint32_t rx_bytes;       ///< Bytes received successfully
    uint32_t tx_bytes;       ///< Bytes transmitted successfully
    uint32_t rx_err;         ///< Bytes receive errors
    uint32_t tx_err;         ///< Bytes transmit errors
} sf_cellular_stats_t;

/** Cellular Driver Information */
typedef struct st_sf_cellular_info
{
    uint8_t mfg_name[SF_CELLULAR_MFG_NAME_LEN];        ///< Manufacturer name
    uint8_t chipset[SF_CELLULAR_CHIPSET_LEN];          ///< Pointer to string showing Cellular chipset/driver information.
    uint8_t fw_version[SF_CELLULAR_FWVERSION_LEN];     ///< Cellular firmware version
    uint8_t imei[SF_CELLULAR_IMEI_LEN];                ///< IMEI number
    uint16_t rssi;                                     ///< Received signal strength indication
    uint16_t ber;                                      ///< Bit rate error
    uint8_t ip_addr[SF_CELLULAR_IP_ADDR_LEN];          ///< IP address
} sf_cellular_info_t;

/** Network Status information */
typedef struct st_sf_cellular_network_status
{
    uint16_t country_code;                              ///< Country code
    uint32_t operator_code;                             ///< Operator code
    int16_t rssi;                                      ///< RSSI
    uint8_t cid[SF_CELLULAR_CID_LEN];                  ///< Cell ID
    uint8_t imsi[SF_CELLULAR_IMSI_LEN];                ///< IMSI
    uint8_t op_name[SF_CELLULAR_MAX_OPERATOR_NAME_LEN];                ///< Operator name
    uint8_t access_tech_name[SF_CELLULAR_ACESS_TECH_NAME_LEN];         ///< Access Technology name
    uint8_t service_domain;                            ///< Service Domain
    uint8_t active_band;                               ///< Active Band
    sf_cellular_network_reg_status_t reg_status;       ///< Cellular network registration status
} sf_cellular_network_status_t;

/** Callback Structure to configure AT command parameters from user */
typedef struct st_sf_cellular_command_parameters_info
{
    uint8_t retry_count;                           ///< Count for which AT command will be retried
    uint16_t retry_delay;                          ///< Delay between AT command retry
    sf_cellular_config_at_cmd_index_t cmd_index;   ///< configurable AT command index
} sf_cellular_command_parameters_info_t;

/** Callback structure for Cellular driver to get the data receive notification */
typedef struct st_sf_cellular_callback_args
{
    sf_cellular_event_t event;     ///< Event Code
    uint8_t * p_data;    ///< Pointer to received data
    uint32_t length;    ///< Receive Data Length
    void const * p_context; ///< Context Provided to user during callback
} sf_cellular_callback_args_t;

/** Preferred operator selection structure */
typedef struct st_sf_cellular_op
{
    sf_cellular_op_name_format_t op_name_format;                                 ///< Cellular operator name format
    uint8_t op_name[SF_CELLULAR_MAX_OPERATOR_NAME_LEN];     ///< Cellular operator name
} sf_cellular_op_t;

/** Structure defining AT commands parameters */
typedef struct st_sf_cellular_at_cmd_set
{
    uint8_t * p_cmd;                ///< AT Command
    uint8_t * p_success_resp;       ///< Success response string
    uint16_t max_resp_length;       ///< Maximum length of expected response
    uint32_t resp_wait_time;        ///< AT command response wait time in milliseconds
    uint8_t retry;                  ///< Retry count
    uint16_t retry_delay;           ///< Delay between AT command retry
} sf_cellular_at_cmd_set_t;

/** Callback Structure to read SIM Pin and PUK from user */
typedef struct st_sf_cellular_sim_pin_info
{
    sf_cellular_sim_status_t   sim_status;  ///< SIM Pin type expected from user to enter, in case of PUK type, user has to
                                            ///< fill SIM Pin and PUK both pins
    uint8_t * p_sim_pin;                    ///< SIM Pin is used to unlock the SIM if the SIM is locked for SIM PIN
    uint8_t * p_sim_puk;                    ///< SIM PUK is used to unlock the SIM if the SIM is locked for SIM PUK
} sf_cellular_sim_pin_info_t;

/** Define the Cellular configuration parameters */
typedef struct st_sf_cellular_cfg
{
    sf_cellular_op_select_mode_t op_select_mode;                 ///< Cellular Operator selection mode
    sf_cellular_op_t op;                            ///< Cellular operator. Valid when operator selection mode is manual
    uint16_t num_pref_ops;                   ///< Number of preferred operators in the pref_ops array
    sf_cellular_op_t pref_ops[SF_CELLULAR_MAX_PREFFERED_OPERATOR_COUNT]; ///< Array of structures describing preferred operators
    sf_cellular_timezone_update_mode_t tz_upd_mode;                    ///< TimeZone update mode policy.
    uint8_t * p_sim_pin;                      ///< SIM Pin
    uint8_t * p_puk_pin;                      ///< PUK Pin
    /** Pointer to provisioning callback function, used in NSAL */
    ssp_err_t (*p_prov_callback)(sf_cellular_callback_args_t * p_args);
    /** This is the receive callback function used by NetX which will take a data packet
     * from the Cellular module and hand it over to NetX for further processing. */
    void (*p_recv_callback)(sf_cellular_callback_args_t * p_args);
    /** Pointer to callback function to configure the SIM properties at runtime */
    ssp_err_t (*p_read_sim_pin_info_callback)(sf_cellular_sim_pin_info_t * p_args);
    /** Pointer to callback function to configure AT command parameters like retry delay and retry count at runtime */
    ssp_err_t (*p_cmd_param_callback)(sf_cellular_command_parameters_info_t ** p_args, uint8_t * p_at_cmd_num);
    void const * p_context;                      ///< User defined context passed into callback function
    void const * p_extend;                       ///< Instance specific configuration
    sf_cellular_at_cmd_set_t const * p_cmd_set;                ///< Instance specific command set
    sf_cellular_at_cmd_set_t * p_modifiable_cmd_set;           ///< Instance specific modifiable command set
} sf_cellular_cfg_t;

/** Cellular Framework API structure. */
typedef struct st_sf_cellular_api
{
    /**
     * @brief Initializes and enables the Cellular module.
     *
     * @param[in, out]  p_ctrl Pointer to user-provided storage for the control block.
     * @param[in]       p_cfg  Pointer to Cellular configuration structure.
     */
    ssp_err_t (*open)(sf_cellular_ctrl_t * p_ctrl, sf_cellular_cfg_t const * const p_cfg);

    /**
     * @brief Disables the Cellular module.
     *
     * @param[in]  p_ctrl Pointer to the control block for the Cellular module. .
     */
    ssp_err_t (*close)(sf_cellular_ctrl_t * const p_ctrl);

    /**
     * @brief Pointer to function to Get the Cellular module provisioning information.
     *
     * @param[in]  p_ctrl                   Pointer to the control block for the Cellular module.
     * @param[out] p_cellular_provisioning  Pointer to Cellular provisioning structure.
     */
    ssp_err_t (*provisioningGet)(sf_cellular_ctrl_t * const p_ctrl,
            sf_cellular_provisioning_t * const p_cellular_provisioning);

    /**
     * @brief Pointer to function to Set the Cellular module's provisioning information
     *
     * @param[in]  p_ctrl                   Pointer to the control block for the Cellular module.
     * @param[in]  p_cellular_provisioning  Pointer to Cellular provisioning structure.
     */
    ssp_err_t (*provisioningSet)(sf_cellular_ctrl_t * const p_ctrl,
            sf_cellular_provisioning_t const * const p_cellular_provisioning);

    /**
     * @brief Reads the Cellular module's information
     *
     * @param[in]  p_ctrl             Pointer to the control block for the Cellular module.
     * @param[out] p_cellular_info    Pointer to Cellular info structure.
     */
    ssp_err_t (*infoGet)(sf_cellular_ctrl_t * const p_ctrl, sf_cellular_info_t * const p_cellular_info);

    /**
     * @brief Returns statistics information of Cellular module.
     *
     * @param[in]   p_ctrl                   Pointer to the control block for the Cellular module.
     * @param[out]  p_cellular_device_stats  Pointer to Cellular statistics information structure.
     */
    ssp_err_t (*statisticsGet)(sf_cellular_ctrl_t * const p_ctrl, sf_cellular_stats_t * const p_cellular_device_stats);

    /**
     * @brief Passes packet buffer to PPP stack for transmission.
     *
     * @param[in]  p_ctrl   Pointer to the control block for the Cellular module.
     * @param[in]  p_buf    Pointer to packet buffer to transmit
     * @param[in]  length   Length of packet buffer
     */
    ssp_err_t (*transmit)(sf_cellular_ctrl_t * const p_ctrl, uint8_t * const p_buf, uint32_t length);

    /**
     * @brief Gets version and stores it in provided pointer p_version.
     *
     * @param[out]  p_version pointer to memory location to return version number
     */
    ssp_err_t (*versionGet)(ssp_version_t * const p_version);

    /**
     * @brief Initiates the Data connection
     *
     * @param[in]  p_ctrl                   Pointer to the control block for the Cellular module.
     */
    ssp_err_t (*networkConnect)(sf_cellular_ctrl_t * const p_ctrl);

    /**
     * @brief Terminates the Data connection.
     *
     * @param[in]  p_ctrl                   Pointer to the control block for the Cellular module.
     */
    ssp_err_t (*networkDisconnect)(sf_cellular_ctrl_t * const p_ctrl);

    /**
     * @brief Get Network Status information.
     *
     * @param[in]  p_ctrl                   Pointer to the control block for the Cellular module.
     * @param[out] p_network_status         Pointer to Network Status structure
     */
    ssp_err_t (*networkStatusGet)(sf_cellular_ctrl_t * const p_ctrl, sf_cellular_network_status_t * p_network_status);

    /**
     * @brief Set SIM Pin
     *
     * @param[in]  p_ctrl                   Pointer to the control block for the Cellular module.
     * @param[in]  p_old_pin                Pointer to char array containing current 4 digit pin.
     * @param[in]  p_new_pin                Pointer to char array containing new 4 digit pin.
     */
    ssp_err_t (*simPinSet)(sf_cellular_ctrl_t * const p_ctrl, uint8_t * const p_old_pin, uint8_t * const p_new_pin);

    /**
     * @brief Locks SIM
     *
     * @param[in]  p_ctrl                   Pointer to the control block for the Cellular module.
     * @param[in]  p_pin                    PIN number to lock the SIM
     */
    ssp_err_t (*simLock)(sf_cellular_ctrl_t * const p_ctrl, uint8_t * const p_pin);

    /**
     * @brief Unlocks SIM
     *
     * @param[in]  p_ctrl                   Pointer to the control block for the Cellular module.
     * @param[in]  p_pin                    PIN number to unlock the SIM
     */
    ssp_err_t (*simUnlock)(sf_cellular_ctrl_t * const p_ctrl, uint8_t * const p_pin);

    /**
     * @brief Gets the SIM ID
     *
     * @param[in]  p_ctrl       Pointer to the control block for the Cellular module.
     * @param[out] p_sim_id     SIM ID
     */
    ssp_err_t (*simIDGet)(sf_cellular_ctrl_t * const p_ctrl, uint8_t * p_sim_id);

    /**
     * @brief Send AT command directly to Cellular Modem
     *
     * This API will send AT command provided by user to the Cellular Modem and will collect the
     * response from the Modem and will send it back to the user. If Modem is in Data Mode when this API
     * is called then Framework will first switch Modem to Command Mode, then send the AT command and
     * collect the response and then switches the Modem back to Data Mode.
     *
     * @param[in]        p_ctrl                    Pointer to the control block for the Cellular module.
     * @param[in]        p_input_at_command     Pointer to structure which contains Modem command to send
     * @param[in, out]   p_output               Pointer to buffer in which response will be sent to user, Also
     *                                            user will pass the size of the buffer which is pointed by p_output
     * @param[in]        timeout                Timeout for which framework will wait for response
     */
    ssp_err_t (*commandSend)(sf_cellular_ctrl_t * const p_ctrl, sf_cellular_cmd_resp_t * const p_input_at_command,
            sf_cellular_cmd_resp_t * const p_output, uint32_t const timeout);

    /**
     * @brief Checks for Available Firmware upgrade
     *
     * @param[in]  p_ctrl                   Pointer to the control block for the Cellular module.
     * @param[in]  p_fotacheck              Pointer to fota check specific data structure
     */
    ssp_err_t (*fotaCheck)(sf_cellular_ctrl_t * const p_ctrl, void * p_fotacheck);

    /**
     * @brief Starts the Firmware upgrade
     *
     * @param[in]  p_ctrl                   Pointer to the control block for the Cellular module.
     * @param[in]  p_fotastart              Pointer to fota start specific data structure
     */
    ssp_err_t (*fotaStart)(sf_cellular_ctrl_t * const p_ctrl, void * p_fotastart);

    /**
     * @brief Stops the Firmware upgrade
     *
     * @param[in]  p_ctrl                   Pointer to the control block for the Cellular module.
     * @param[in]  p_fotastop               Pointer to fota stop specific data structure
     */
    ssp_err_t (*fotaStop)(sf_cellular_ctrl_t * const p_ctrl, void * p_fotastop);

    /**
     * @brief Reset cellular module. This reset() API will only work when module is opened.
     *
     * @param[in]  p_ctrl                   Pointer to the control block for the Cellular module.
     * @param[in]  reset_type               Reset type
     */
    ssp_err_t (*reset)(sf_cellular_ctrl_t * const p_ctrl, sf_cellular_reset_type_t reset_type);

} sf_cellular_api_t;

/** This structure encompasses everything that is needed to use an instance of this interface. */
typedef struct st_sf_cellular_instance
{
    sf_cellular_ctrl_t * p_ctrl;       ///< Pointer to the control structure for this instance
    sf_cellular_cfg_t const * p_cfg;        ///< Pointer to the configuration structure for this instance
    sf_cellular_api_t const * p_api;        ///< Pointer to the API structure for this instance
} sf_cellular_instance_t;
/*******************************************************************************************************************//**
 * @} (end defgroup SF_CELLULAR_API)
 **********************************************************************************************************************/

/* Common macro for SSP header files. There is also a corresponding SSP_HEADER macro at the top of this file. */
SSP_FOOTER
#endif /* SF_CELLULAR_API_H */
