/***********************************************************************************************************************
 * Copyright [2015-2017] Renesas Electronics Corporation and/or its licensors. All Rights Reserved.
 * 
 * This file is part of Renesas SynergyTM Software Package (SSP)
 *
 * The contents of this file (the "contents") are proprietary and confidential to Renesas Electronics Corporation
 * and/or its licensors ("Renesas") and subject to statutory and contractual protections.
 *
 * This file is subject to a Renesas SSP license agreement. Unless otherwise agreed in an SSP license agreement with
 * Renesas: 1) you may not use, copy, modify, distribute, display, or perform the contents; 2) you may not use any name
 * or mark of Renesas for advertising or publicity purposes or in connection with your use of the contents; 3) RENESAS
 * MAKES NO WARRANTY OR REPRESENTATIONS ABOUT THE SUITABILITY OF THE CONTENTS FOR ANY PURPOSE; THE CONTENTS ARE PROVIDED
 * "AS IS" WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT; AND 4) RENESAS SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, OR
 * CONSEQUENTIAL DAMAGES, INCLUDING DAMAGES RESULTING FROM LOSS OF USE, DATA, OR PROJECTS, WHETHER IN AN ACTION OF
 * CONTRACT OR TORT, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THE CONTENTS. Third-party contents
 * included in this file may be subject to different terms.
 **********************************************************************************************************************/

/**********************************************************************************************************************
 * File Name    : sf_cellular_qctlcatm1.h
 * Description  : RTOS-integrated Cellular Framework example. Implementation is Quectel CATM1 Cellular Driver interface.
 **********************************************************************************************************************/

#ifndef SF_CELLULAR_QCTLCATM1_H
#define SF_CELLULAR_QCTLCATM1_H

/*******************************************************************************************************************//**
 * @ingroup SF_Library
 * @defgroup SF_CELLULAR_QCTLCATM1 Cellular Framework Example using Quectel CATM1
 * @brief RTOS-integrated Cellular Framework example. Implementation of Cellular Quectel CATM1 Driver.
 * It implements the following interfaces:
 *   - @ref SF_CELLULAR_API
 * @{
 **********************************************************************************************************************/

/***********************************************************************************************************************
 * Includes
 **********************************************************************************************************************/
/** Cellular Interface. */
#include "sf_cellular_qctlcatm1_cfg.h"
#include "sf_cellular_api.h"

/* Common macro for SSP header files. There is also a corresponding SSP_FOOTER macro at the end of this file. */
SSP_HEADER

/**********************************************************************************************************************
 * Macro definitions
 **********************************************************************************************************************/
/** Major Version of code that implements the API defined in this file */
#define SF_CELLULAR_QCTLCATM1_CODE_VER_MAJOR (1U)
/** Minor Version of code that implements the API defined in this file */
#define SF_CELLULAR_QCTLCATM1_CODE_VER_MINOR (0U)

/**********************************************************************************************************************
 * Enumeration definitions
 **********************************************************************************************************************/

/** Cellular Fallback sequence type */
typedef enum e_sf_cellular_qctlcatm1_network_scan_seq
{
    SF_CELLULAR_QCTLCATM1_NWSCANSEQ_LTECATM1_LTECATNB1_GSM = 0,  ///< Network scan sequence Default (LTE Cat.M1->LTE Cat.NB1->GSM)
    SF_CELLULAR_QCTLCATM1_NWSCANSEQ_LTECATM1_GSM_LTECATNB1 = 1,  ///< Network scan sequence LTE Cat.M1->GSM->LTE Cat.NB1
    SF_CELLULAR_QCTLCATM1_NWSCANSEQ_GSM_LTECATNB1_LTECATM1 = 3,  ///< Network scan sequence GSM->LTE Cat.NB1->LTE Cat.M1
    SF_CELLULAR_QCTLCATM1_NWSCANSEQ_GSM_LTECATM1_LTECATNB1 = 2,  ///< Network scan sequence GSM->LTE Cat.M1->LTE Cat.NB1
    SF_CELLULAR_QCTLCATM1_NWSCANSEQ_LTECATNB1_LTECATM1_GSM = 4,  ///< Network scan sequence LTE Cat.NB1->LTE Cat.M1->GSM
    SF_CELLULAR_QCTLCATM1_NWSCANSEQ_LTECATNB1_GSM_LTECATM1 = 5,  ///< Network scan sequence LTE Cat.NB1->GSM->LTE Cat.M1
} sf_cellular_qctlcatm1_network_scan_seq_t;

/**********************************************************************************************************************
 * Structure definitions
 **********************************************************************************************************************/
/** Extended configuration for Quectel CATM1 BG96 */
typedef struct st_sf_cellular_qctlcatm1_module_extended_cfg
{
    sf_cellular_qctlcatm1_network_scan_seq_t nwscanseq; ///< Network fall back sequence selection
    uint8_t * nbiot_band_selection; ///< NBIOT band selections
} sf_cellular_qctlcatm1_extended_cfg_t;

/**********************************************************************************************************************
 * Exported global variables
 **********************************************************************************************************************/
/** @cond INC_HEADER_DEFS_SEC */
/** Filled in Interface API structure for this Instance. */
extern const sf_cellular_api_t g_sf_cellular_on_sf_cellular_qctlcatm1;
/** Instance specific command set array, This array contains the module specific commands,
 * its expected success string, response wait timeout for command */
extern const sf_cellular_at_cmd_set_t g_sf_cellular_qctlcatm1_cmd_set[];
/** Instance specific command set array, This array contains the module specific commands
 * whose parameters can be modified at run time */
extern sf_cellular_at_cmd_set_t g_sf_cellular_qctlcatm1_modifiable_cmd_set[];
/** @endcond */
/* Common macro for SSP header files. There is also a corresponding SSP_HEADER macro at the top of this file. */

/*******************************************************************************************************************//**
 * @} (end defgroup SF_CELLULAR_QCTLCATM1)
 **********************************************************************************************************************/

/* Common macro for SSP header files. There is also a corresponding SSP_HEADER macro at the top of this file. */
SSP_FOOTER
#endif /* SF_CELLULAR_QCTLCATM1_H */
