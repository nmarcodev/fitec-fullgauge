/***********************************************************************************************************************
 * Copyright [2015-2017] Renesas Electronics Corporation and/or its licensors. All Rights Reserved.
 *
 * This file is part of Renesas SynergyTM Software Package (SSP)
 *
 * The contents of this file (the "contents") are proprietary and confidential to Renesas Electronics Corporation
 * and/or its licensors ("Renesas") and subject to statutory and contractual protections.
 *
 * This file is subject to a Renesas SSP license agreement. Unless otherwise agreed in an SSP license agreement with
 * Renesas: 1) you may not use, copy, modify, distribute, display, or perform the contents; 2) you may not use any name
 * or mark of Renesas for advertising or publicity purposes or in connection with your use of the contents; 3) RENESAS
 * MAKES NO WARRANTY OR REPRESENTATIONS ABOUT THE SUITABILITY OF THE CONTENTS FOR ANY PURPOSE; THE CONTENTS ARE PROVIDED
 * "AS IS" WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT; AND 4) RENESAS SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, OR
 * CONSEQUENTIAL DAMAGES, INCLUDING DAMAGES RESULTING FROM LOSS OF USE, DATA, OR PROJECTS, WHETHER IN AN ACTION OF
 * CONTRACT OR TORT, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THE CONTENTS. Third-party contents
 * included in this file may be subject to different terms.
 **********************************************************************************************************************//***********************************************************************************************************************
 * File Name    : sf_cellular_common_api.h
 * Description  : This module contains Structure, enumeration definition for Cellular framework.
 ***********************************************************************************************************************/
/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 ***********************************************************************************************************************/

#ifndef SF_CELLULAR_COMMON_H
#define SF_CELLULAR_COMMON_H
#include "tx_api.h"
#include "stdlib.h"
#include "r_ioport_api.h"
#include "sf_cellular_api.h"
#include "sf_comms_api.h"
/* Common macro for SSP header files. There is also a corresponding SSP_FOOTER macro at the end of this file. */
SSP_HEADER

/**********************************************************************************************************************
 * Macro definitions
 **********************************************************************************************************************/
#define SF_CELLULAR_COMMON_CODE_VER_MAJOR (1U)  ///< Macro to use code version for SF Cellular Common Code
#define SF_CELLULAR_COMMON_CODE_VER_MINOR (0U)  ///< Macro to use code version for SF Cellular Common Code

/** Macros to define string length */
#define SF_CELLULAR_STR_LEN_8        (8U)		///< Length 8
#define SF_CELLULAR_STR_LEN_16       (16U)		///< Length 16
#define SF_CELLULAR_STR_LEN_32       (32U)		///< Length 32 
#define SF_CELLULAR_STR_LEN_64       (64U)		///< Length 64
#define SF_CELLULAR_STR_LEN_128      (128U)		///< Length 128
#define SF_CELLULAR_STR_LEN_256      (256U)     ///< Length 256
#define SF_CELLULAR_STR_LEN_512      (512U)     ///< Length 512
#define SF_CELLULAR_RFSTS_RSP_LEN    (256U)		///< RFSTS Response length

/** SF_CELLULAR delay in milliseconds between AT command retry */
#define SF_CELLULAR_DELAY_0      (0U)       ///< No delay
#define SF_CELLULAR_DELAY_20MS   (20U)      ///< Delay of 20 milliseconds
#define SF_CELLULAR_DELAY_50MS   (50U)      ///< Delay of 50 milliseconds
#define SF_CELLULAR_DELAY_100MS  (100U)     ///< Delay of 100 milliseconds
#define SF_CELLULAR_DELAY_200MS  (200U)     ///< Delay of 200 milliseconds
#define SF_CELLULAR_DELAY_300MS  (300U)     ///< Delay of 300 milliseconds
#define SF_CELLULAR_DELAY_500MS  (500U)     ///< Delay of 500 milliseconds
#define SF_CELLULAR_DELAY_1000MS (1000U)    ///< Delay of 1000 milliseconds
#define SF_CELLULAR_DELAY_2000MS (2000U)    ///< Delay of 2000 milliseconds

/** SF_CELLULAR retry count for AT command set */
#define SF_CELLULAR_RETRY_VALUE_0    (0U)       ///< No Retry
#define SF_CELLULAR_RETRY_VALUE_1    (1U)       ///< Retry Once
#define SF_CELLULAR_RETRY_VALUE_3    (3U)       ///< Retry 3 times
#define SF_CELLULAR_RETRY_VALUE_5    (5U)       ///< Retry 5 times
#define SF_CELLULAR_RETRY_VALUE_10   (10U)      ///< Retry 10 times

#define SF_CELLULAR_MODULE_RESET_DELAY_MS       (500U)      ///< Delay in MilliSecond for processing module reset
#define SF_CELLULAR_READ_TIMEOUT_MS             (3000U)     ///< Read Timeout in MilliSecond

#define SF_CELLULAR_IMEI_LENGTH                (15U)         ///< Maximum Length of IMEI code
#define SF_CELLULAR_CONFIGURATION_DELAY        (200U)        ///< Delay in Millisecond for configuration command

/** Basic AT Command */
#define SF_CELLULAR_SUCCESS_RSP           ((const char *)"\r\nOK\r\n")      ///< Success Response string
#define SF_CELLULAR_ERROR_RSP             ((const char *)"\r\nERROR\r\n")   ///< Error response string
/** Cellular PDP type */
#define SF_CELLULAR_PDP_TYPE_IP_STR       ((const char *)"IP")              ///< String value for SF_CELLULAR_PDP_TYPE_IP
#define SF_CELLULAR_PDP_TYPE_PPP_STR      ((const char *)"PPP")             ///< String value for SF_CELLULAR_PDP_TYPE_PPP
#define SF_CELLULAR_PDP_TYPE_IPV6_STR     ((const char *)"IPV6")            ///< String value for SF_CELLULAR_PDP_TYPE_IPV4
#define SF_CELLULAR_PDP_TYPE_IPV4V6_STR   ((const char *)"IPV4V6")          ///< String value for SF_CELLULAR_PDP_TYPE_IPV4V6
/** Cellular response string */
#define SF_CELLULAR_CPIN_READY_STR            ((const char *)"READY")       ///< String value for SIM Ready response status
#define SF_CELLULAR_CPIN_PUK_STR              ((const char *)"SIM PUK")     ///< String value for SIM PUK response status
#define SF_CELLULAR_CPIN_SIM_PIN_STR          ((const char *)"SIM PIN")     ///< String value for SIM PIN response status
#define SF_CELLULAR_CRLF                      ((const char *)"\r\n")        ///< Carriage return line feed
#define SF_CELLULAR_NO_CARRIER_STR            ((const char *)"NO CARRIER")  ///< String value for No Carrier

#define SF_CELLULAR_SERIAL_READ_TIMEOUT_TICKS    (200U)       ///< Serial Read Timeout in Thread Ticks
#define SF_CELLULAR_PDP_CONTEXT_MIN_VAL          (1U)         ///< Minimum context ID
#define SF_CELLULAR_PDP_CONTEXT_MAX_VAL          (6U)         ///< Maximum context ID
#define SF_CELLULAR_MILLISECONDS_PER_SECOND      (1000U)      ///< Milliseconds per seconds

#define SF_CELLULAR_AT_CMD_RESP_WAITTIME_300MS   (300U)       ///< AT command response wait timeout of 300 millisecond
#define SF_CELLULAR_AT_CMD_RESP_WAITTIME_2000MS  (2000U)      ///< AT command response wait timeout of 2000 millisecond
#define SF_CELLULAR_AT_CMD_RESP_WAITTIME_5000MS  (5000U)      ///< AT command response wait timeout of 5000 millisecond
#define SF_CELLULAR_AT_CMD_RESP_DEF_WAITTIME_MS  (SF_CELLULAR_AT_CMD_RESP_WAITTIME_300MS)  ///< Default at command response wait timeout in milliseconds
#define SF_CELLULAR_MUTEX_GET_TIMEOUT_TICKS      (200U)       ///< Default timeout for getting mutex

/**********************************************************************************************************************
 * Enumeration definitions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 * External global variables
 **********************************************************************************************************************/
/***********************************************************************************************************************
 * Global variables
 **********************************************************************************************************************/
/** Circular queue configuration */
typedef struct st_sf_cellular_circular_queue_cfg
{
    uint32_t * p_circular_queue_buffer;            ///< Circular Queue buffer
    ULONG queue_size;                              ///< Circular Queue Size
    uint8_t ok_check_index;                        ///< Variable to store index for data checking success string response
    uint8_t error_check_index;                     ///< Variable to store index for data checking error string response
    TX_QUEUE * p_circular_queue;                   ///< Circular Queue
} sf_cellular_circular_queue_cfg_t;

/** SF Communication framework configuration */
typedef struct st_sf_cellular_comms_extend_cfg
{
    sf_comms_instance_t const * p_sf_comms_instance;            ///< Lower level HAL driver instance
    TX_THREAD * p_sf_comms_rx_thread;           ///< Received data ThreadX ID
    uint8_t * p_sf_comms_rx_thread_stack;     ///< Receive data thread stack memory pointer
    ULONG sf_comms_rx_thread_stack_size;  ///< Receive data thread stack size
    UINT rx_thread_priority;             ///< Receive data thread priority
    uint8_t do_run;                         ///< Thread running status
} sf_cellular_comms_extend_cfg_t;

/** SF Cellular framework extended configuration */
typedef struct st_sf_cellular_extended_cfg
{
    sf_cellular_circular_queue_cfg_t * p_circular_queue_cfg;  ///< Circular Queue configuration
    sf_cellular_comms_extend_cfg_t * p_sf_comms_cfg;          ///< Lower level HAL interface configuration
    ioport_port_pin_t pin_reset;                              ///< Port pin used for resetting cellular module
    ioport_level_t reset_level;                               ///< Module reset level
    void * p_module_extended_cfg;                             ///< Instance specific module configuration
} sf_cellular_extended_cfg_t;

/** SF Cellular framework instance configuration */
typedef struct st_sf_cellular_instance_cfg
{
    uint8_t init_done;                          ///< Status flag storing driver initialization status
    uint8_t is_opened;                          ///< Status flag storing framework open status
    uint8_t is_data_mode_on;                    ///< Status flag storing data mode status
    sf_cellular_cfg_t const * p_cfg;            ///< Instance configuration
    sf_cellular_provisioning_t prov_info;       ///< Provisioning information
    sf_cellular_stats_t celr_stats;             ///< Cellular Statistics information
    TX_MUTEX * p_cellular_mutex;                ///< Mutex for Framework API synchronization
    uint8_t * p_socket_status_buffer;           ///< Buffer to store the socket status information
    uint16_t socket_status_buffer_length;       ///< Size of Socket status buffer
} sf_cellular_instance_cfg_t;

/************************************************************************************************************
 * Function Declaration
 ***********************************************************************************************************/
char * strtok_r(char * var1, const char * var2, char ** var3);
ssp_err_t SF_CELLULAR_COMMON_Open(sf_cellular_ctrl_t * p_ctrl, sf_cellular_cfg_t const * const p_cfg);
ssp_err_t SF_CELLULAR_COMMON_Close(sf_cellular_ctrl_t * const p_ctrl);
ssp_err_t SF_CELLULAR_COMMON_InfoGet(sf_cellular_ctrl_t * const p_ctrl, sf_cellular_info_t * const p_cellular_info);
ssp_err_t SF_CELLULAR_COMMON_StatisticsGet(sf_cellular_ctrl_t * const p_ctrl,
        sf_cellular_stats_t * const p_cellular_device_stats);
ssp_err_t SF_CELLULAR_COMMON_Transmit(sf_cellular_ctrl_t * const p_ctrl, uint8_t * const p_buf, uint32_t length);
ssp_err_t SF_CELLULAR_COMMON_ProvisioningGet(sf_cellular_ctrl_t * const p_ctrl,
        sf_cellular_provisioning_t * const p_cellular_provisioning);
ssp_err_t SF_CELLULAR_COMMON_ProvisioningSet(sf_cellular_ctrl_t * const p_ctrl,
        sf_cellular_provisioning_t const * const p_cellular_provisioning);
ssp_err_t SF_CELLULAR_COMMON_NetworkConnect(sf_cellular_ctrl_t * const p_ctrl);
ssp_err_t SF_CELLULAR_COMMON_NetworkConnectWithCGDATA(sf_cellular_ctrl_t * const p_ctrl);
ssp_err_t SF_CELLULAR_COMMON_NetworkDisconnect(sf_cellular_ctrl_t * const p_ctrl);
ssp_err_t SF_CELLULAR_COMMON_NetworkStatusGet(sf_cellular_ctrl_t * const p_ctrl,
        sf_cellular_network_status_t * p_network_status);
ssp_err_t SF_CELLULAR_COMMON_SimPinSet(sf_cellular_ctrl_t * const p_ctrl, uint8_t * const p_old_pin,
        uint8_t * const p_new_pin);
ssp_err_t SF_CELLULAR_COMMON_SimLock(sf_cellular_ctrl_t * const p_ctrl, uint8_t * const p_pin);
ssp_err_t SF_CELLULAR_COMMON_SimUnlock(sf_cellular_ctrl_t * const p_ctrl, uint8_t * const p_pin);
ssp_err_t SF_CELLULAR_COMMON_SimIDGet(sf_cellular_ctrl_t * const p_ctrl, uint8_t *p_sim_id);
ssp_err_t SF_CELLULAR_COMMON_CommandSend(sf_cellular_ctrl_t * const p_ctrl,
        sf_cellular_cmd_resp_t * const p_input_at_command, sf_cellular_cmd_resp_t * const p_output,
        uint32_t const timeout);
ssp_err_t SF_CELLULAR_COMMON_FotaCheck(sf_cellular_ctrl_t * const p_ctrl, void *p_fotacheck);
ssp_err_t SF_CELLULAR_COMMON_FotaStart(sf_cellular_ctrl_t * const p_ctrl, void *p_fotastart);
ssp_err_t SF_CELLULAR_COMMON_FotaStop(sf_cellular_ctrl_t * const p_ctrl, void *p_fotastop);

/** Inline function to convert milliseconds to Ticks */
static inline uint32_t sf_cellular_common_getticks_for_mseconds(uint32_t milliseconds)
{
    uint32_t ticks;
    ticks = (milliseconds * (uint32_t) TX_TIMER_TICKS_PER_SECOND) / SF_CELLULAR_MILLISECONDS_PER_SECOND;
    return ticks;
}

/* Common macro for SSP header files. There is also a corresponding SSP_HEADER macro at the top of this file. */
SSP_FOOTER

#endif /* SF_CELLULAR_COMMON_H */
