/***********************************************************************************************************************
 * Copyright [2015-2017] Renesas Electronics Corporation and/or its licensors. All Rights Reserved.
 *
 * This file is part of Renesas SynergyTM Software Package (SSP)
 *
 * The contents of this file (the "contents") are proprietary and confidential to Renesas Electronics Corporation
 * and/or its licensors ("Renesas") and subject to statutory and contractual protections.
 *
 * This file is subject to a Renesas SSP license agreement. Unless otherwise agreed in an SSP license agreement with
 * Renesas: 1) you may not use, copy, modify, distribute, display, or perform the contents; 2) you may not use any name
 * or mark of Renesas for advertising or publicity purposes or in connection with your use of the contents; 3) RENESAS
 * MAKES NO WARRANTY OR REPRESENTATIONS ABOUT THE SUITABILITY OF THE CONTENTS FOR ANY PURPOSE; THE CONTENTS ARE PROVIDED
 * "AS IS" WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT; AND 4) RENESAS SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, OR
 * CONSEQUENTIAL DAMAGES, INCLUDING DAMAGES RESULTING FROM LOSS OF USE, DATA, OR PROJECTS, WHETHER IN AN ACTION OF
 * CONTRACT OR TORT, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THE CONTENTS. Third-party contents
 * included in this file may be subject to different terms.
 **********************************************************************************************************************/
/***********************************************************************************************************************
 * File Name    : sf_cellular_nsal_api.h
 * Description  : This file provides configuration used by Cellular NSAL.
 ***********************************************************************************************************************/

#ifndef SF_CELLULAR_NSAL_API_H
#define SF_CELLULAR_NSAL_API_H

/*******************************************************************************************************************//**
 * @ingroup SF_Interface_Library
 * @defgroup SF_CELLULAR_NSAL_API SF CELLULAR NSAL Framework Interface
 * @brief RTOS-integrated SF CELLULAR NSAL Framework Interface.
 *
 * @section SF_CELLULAR_NSAL_API_SUMMARY Summary
 *
 * This SSP Interface provides access to the ThreadX-aware SF CELLULAR NSAL Framework.
 *
 * @{
 **********************************************************************************************************************/

/***********************************************************************************************************************
 * Includes
 **********************************************************************************************************************/
#include <nx_api.h>
#include <nx_ppp.h>
#include "sf_cellular_api.h"

/* Common macro for SSP header files. There is also a corresponding SSP_FOOTER macro at the end of this file. */
SSP_HEADER

/**********************************************************************************************************************
 * Typedef definitions
 **********************************************************************************************************************/
/** Define the NSAL configuration parameters */
typedef struct st_sf_cellular_nsal_cfg
{
    uint8_t * p_ppp_stack;            ///< PPP Stack
    uint32_t ppp_stack_size;         ///< PPP Stack size
    UINT priority;                  ///< PPP Thread Priority
    NX_PPP * p_ppp;                  ///< NetX PPP Interface
    NX_IP * p_ip;                   ///< NetX IP Interface

    NX_PACKET_POOL * p_ppp_packet_pool;      ///< NetX Packet pool for internal

    void (*p_ppp_invalid_packet_cb)(NX_PACKET * p_packet_ptr); ///< Callback handler for Invalid packet
    void (*p_ppp_send_byte)(UCHAR byte);                       ///< PPP Send byte callback function
    /** Link Notification callback function */
    void (*p_link_down_cb)(NX_PPP * p_ppp_ptr);                ///< PPP Link down notification callback
    void (*p_link_up_cb)(NX_PPP * p_ppp_ptr);                  ///< PPP Link up notification callback

    sf_cellular_auth_type_t auth_type;              ///< Authentication Type

    /** CHAP Callback Function */
    UINT (*p_chap_get_challenge_cb)(CHAR * p_rand_value, CHAR * p_id, CHAR * p_name); ///< Get challenge notification callback

    UINT (*p_chap_get_responder_cb)(CHAR * p_system, CHAR * p_name, CHAR * p_secret); ///< Get Responder notification callback

    UINT (*p_chap_get_verify_cb)(CHAR * p_system, CHAR * p_name, CHAR * p_secret);   ///< Get Chap verification callback

    /** PAP Callback Function */
    UINT (*p_pap_generate_login)(CHAR * p_name, CHAR * p_password);      ///< PAP Authentication generate login callback

    UINT (*p_pap_verify_login)(CHAR * p_name, CHAR * p_password);          ///< PAP authentication verification callback

    uint32_t local_ip;               ///< Local IP Address
    uint32_t peer_ip;                ///< Peer IP Address

    void const * p_extend;               ///< Instance specific configuration
} sf_cellular_nsal_cfg_t;

/*******************************************************************************************************************//**
 * Function Prototypes
 **********************************************************************************************************************/
ssp_err_t sf_cellular_deinit(NX_IP_DRIVER * driver_req_ptr, sf_cellular_instance_t const * p_cellular_instance,
        sf_cellular_nsal_cfg_t * p_cellular_nsal_cfg);

/*******************************************************************************************************************//**
 * @} (end defgroup SF_CELLULAR_NSAL_API)
 **********************************************************************************************************************/

/* Common macro for SSP header files. There is also a corresponding SSP_HEADER macro at the top of this file. */
SSP_FOOTER

#endif /* SF_CELLULAR_NSAL_API_H */
