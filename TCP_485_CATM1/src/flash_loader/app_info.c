/*
 * app_info.c
 *
 *  Created on: Dec 21, 2016
 *      Author: LAfonso01
 */
#include "common_data.h"
#include "app_info.h"
#include "Includes/TypeDefs.h"
//#include "Includes/File.h"

//const sf_firmware_image_file_header_t g_sf_firmware_image_internal0_image_file_header BSP_PLACE_IN_SECTION(".sf_firmware_image_file_header");

//fl_image_header_t fileInfo;

fl_image_header_t __attribute__((section (".app_info"))) AppInfo =
{
 // codigo de firmware valido
 0xAAAA,
 // versao do firmware
 FIRMWARE_VERSION,
 // release do firmware
 FIRMWARE_RELEASE,
 //MCU Model S1 = 1, S3 = 3, 4= S5D5 500k, S5D5 1M= 5, 6= S5D9, S7 = 7
 MCU_MODEL,
 //modelo de produto
 PRODUCT_MODEL,

 // versão da tabela de parâmetros
 0,
 0,

 // livre
 0,
 0,
 0,
 0,
 0,
 0,
 /* CRC-16 CCITT of image as in MCU flash */
 0xFFFF,
};
