/*
 * ApplicationTimerMain.c
 *
 *  Created on: 18 de nov de 2019
 *      Author: leonardo.ceolin
 *
 * Faz o controle de operacoes controladas por timer.
 * Operacoes como reset do equipamento, finalizacao de atualizacao remota por timeout,
 * timeout de conexao com SITRAD, reconfiguracao do equipamento para padroes de fabrica.
 */

#include "Includes/ApplicationTimerMain.h"
#include "Includes/MainManagerThreadEntry.h"
#include "Includes/Thread_Monitor_entry.h"
#include "USB_HID_Device_Thread.h"

#if RECORD_DATALOGGER_EVENTS
#include "Includes/Thread_Datalogger_entry.h"
#endif

void EventTimerMain(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX);
void EndUpdateFW(void);

void EventTimerMain(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX)
{
    if(CommData->TimerBigFrameTCP)
    {
        CommData->TimerBigFrameTCP--;
        if(!CommData->TimerBigFrameTCP)
        {
            CommData->BigFrameTCP = 0;
            g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_HIGH);
        }
    }

    if(CommData->TimerUpdateFWCheckCRC)
    {
        CommData->TimerUpdateFWCheckCRC--;
        if(!CommData->TimerUpdateFWCheckCRC)
        {
            if(!CheckCRCFirmwareQSPI())
            {
                RestartModule();
            }
            else
            {
                CommData->TimerTimeOutUpdateFW = 2;
            }
        }
    }

    if(CommData->TimerTimeOutUpdateFW)
    {
        CommData->TimerTimeOutUpdateFW--;
        if(!CommData->TimerTimeOutUpdateFW)
        {
            CommData->FlagUpdateFWRun = false;
            CommData->CommStatesMachine = COMM_SM_RESTART_IP;
            g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_HIGH);
            EndUpdateFW();
        }

        if(CommData->FlagConnectSitrad)
        {
            tx_event_flags_set (&Cellular_Flags, CELL_FLAG_TCP_DELETE, TX_OR);//seta a flag para desconectar a tcp porta sitrad.
        }
    }

    if(CommData->TimerRestartModule)
    {
        CommData->TimerRestartModule--;
        if(!CommData->TimerRestartModule)
        {
            RestartModule();
        }
    }

    if(CommData->TimerRestoreFactory)
    {
        CommData->TimerRestoreFactory--;
        if(!CommData->TimerRestoreFactory)
        {
            ThreadMainManagerSetFlag(MM_RESTORE_FACTORY);
        }
    }

    if(CommData->TimerConnectSitrad)
    {
        CommData->TimerConnectSitrad--;
        if(!CommData->TimerConnectSitrad)
        {
            StatusFlagsX->FlagSitradConnected = false;
            tx_event_flags_set (&Cellular_Flags, CELL_FLAG_TCP_DISCONNECT, TX_OR);

        }
    }

    if(CommData->TimerInactivity)
    {
        CommData->TimerInactivity--;
        if(!CommData->TimerInactivity)
        {
            if(CommData->ConfNetBasic.InterfaceType == CELLULAR)
            {
                tx_event_flags_set (&Cellular_Flags, CELL_FLAG_TCP_DISCONNECT, TX_OR);
            }
        }
    }
}

void EndUpdateFW(void)
{

#if RECORD_DATALOGGER_EVENTS
    DataLoggerRegisterEvent(DATALOGGER_EVENT_FOTA_STATUS_CHANGE, 0, false);
#endif

    _DATA_LOGGER_DATA DataLoggerData;
    DataLoggerData.OpCod = END_UPDATE_FW; //Avisa a thread datalogger que a atualização terminou.
    tx_queue_send(&Datalogger_Flash, &DataLoggerData, TX_WAIT_FOREVER); //envia para a thread Datalogger

    //ThreadMonitor_SendKeepAlive(T_MONITOR_START_MONITORING);
}
