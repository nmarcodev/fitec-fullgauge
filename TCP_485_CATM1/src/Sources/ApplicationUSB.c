/*
 * ApplicationUSB.c
 *
 *  Created on: 27 de mai de 2021
 *      Author: rhenanbezerra
 */

#include "usb_hid_device_thread.h"
#include "Includes/ApplicationUSB.h"
#include "Includes/ApplicationCommonModule.h"
#include "Includes/TypeDefs.h"
#include "ux_api.h"
#include "Includes/FlashQSPI.h"
#include "common_data.h"
#include "Includes/Define_IOs.h"
#include "Includes/ExternalFlash.h"
#include "stdio.h"
#include "Includes/CRCModbus.h"
#include "Includes/Thread_Monitor_entry.h"
#include "Includes/Thread_Datalogger_entry.h"
#include "Includes/MainManagerThreadEntry.h"
#include "Includes/RX_8571LC.h"
#include "Includes/Serial_thread_entry.h"
#include "Includes/USBTypes.h"
#include "Includes/ApplicationFlash.h"
#include "Includes/common_util.h"
#include "Includes/ApplicationCellular.h"
#include "Includes/ApplicationTcpPayload.h"
#include "Includes/UdpTcpTypes.h"
#include "cellular_thread.h"

#define HANDLE_FLASH_ERRORS (0)
#define HID_FLAG ((ULONG)0x0001)
#if FIRMWARE_VERSION == 99
#else
#error no firmware version defined!
#endif

_USB_RETURN AppPacketUSB(_COMM_CONTROL *CommData,  _STATUS_FLAGS *StatusFlagsX);

uint16_t AESEnc(uint8_t * PMessage, uint8_t *EncMessage, uint16_t InputOctets);
uint16_t AESDec(uint8_t * PMessage, uint8_t *DecMessage, uint16_t InputOctets);
uint16_t ProcessPayloadUSB(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX);
uint16_t GetConfSitradUSB(uint8_t id, uint8_t * GetConfData, _COMM_CONTROL *CommData, bool FlagReadConf);
uint16_t RequesitaInfoConfUSB(_COMM_CONTROL *CommData, uint16_t RandomNumber);
uint16_t DownloadFirmware(_COMM_CONTROL *CommData, uint16_t RandomNumber);
uint16_t AtualizaInfoConfUSB(_COMM_CONTROL *CommData, uint16_t RandomNumber);
uint16_t UpdadeDateHour(_COMM_CONTROL *CommData, uint16_t RandomNumber);
uint16_t ReadPgDatalogger(_COMM_CONTROL *CommData, uint16_t RandomNumber);
uint16_t GetStatus(_COMM_CONTROL *CommData, uint16_t RandomNumber);
uint16_t AnswerDefault(_COMM_CONTROL *CommData, uint16_t RandomNumber);
uint16_t FactoryTest(_COMM_CONTROL *CommData, uint16_t RandomNumber);
uint16_t LoginDataResponse(_COMM_CONTROL *CommData, uint16_t RandomNumber);

uint8_t  TestStatusRssid(uint8_t Rssid_In);

bool     TestQSPI(void);
bool     TestSerial485(uint8_t AddressInst485);

void StatusUSB(_STATUS_FLAGS *StatusFlagsX);
void     SetConfSitradUSB(uint8_t * SetConfData, _COMM_CONTROL *CommData, uint16_t SizeRequest);
void FrameBufferResponse(_COMM_CONTROL *CommData,uint8_t FrameBuffer);
void ResetRxBuffer(void);
void LoadRxBuffer(uint8_t *BufferInput, _COMM_CONTROL *CommData, uint16_t Len);
void ProcessRxBuffer( _COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX);
void ClearRxBuffer(_COMM_CONTROL *CommData);
void ClearTxBuffer(_COMM_CONTROL *CommData);
void saveFW(_COMM_CONTROL *CommData);

extern _COMM_CONTROL CommControl;
TX_THREAD   USB_Thread;

int cont = 2;
int j = 0;
uint32_t    Rx_Qtde;
uint32_t    Rx_Lim;
uint32_t    Rx_Cnt = 0;
uint16_t    SizeRxFrame, pos;
uint8_t 	padding;
int ExpFragFile = 1;
const uint8_t aes_key2[]   = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F};
const uint8_t IVc2[]   = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
uint32_t   AddressFirmware = 0;

UINT ux_hid_device_callback(UX_SLAVE_CLASS_HID* hid, UX_SLAVE_CLASS_HID_EVENT* hid_event)
{
    /* Does nothing. */
    _STATUS_FLAGS StatusFlagsX;
    SSP_PARAMETER_NOT_USED(hid);
    SSP_PARAMETER_NOT_USED(StatusFlagsX);

    if(hid_event->ux_device_class_hid_event_buffer[0] == 0x26 && CommControl.SendPkts>=0){
    	ProcessRxBuffer(&CommControl, &StatusFlagsX);
    	return UX_SUCCESS;
    }

    memcpy(&CommControl.BufferUSB.HeaderPayload[0],&hid_event->ux_device_class_hid_event_buffer[0], SIZE_HEADER_USB + SIZE_PAYLOAD_USB);
    memcpy(&CommControl.BufferUSB.HP.Header[0],&hid_event->ux_device_class_hid_event_buffer[0], SIZE_HEADER_USB);
    memcpy(&CommControl.BufferUSB.HP.Payload[0],&hid_event->ux_device_class_hid_event_buffer[3], SIZE_PAYLOAD_USB);

    return UX_SUCCESS;
}

/**
 * Trata um pacote USB recebido.
 */

_USB_RETURN AppPacketUSB(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX)
{
	_BUFFER_USB     *BufferUSB;
	uint8_t			CurCmd;
	ConvBytesToInt  CmdSize;
	uint8_t         BufferRxUSBSitradOut[SIZE_PAYLOAD_USB]; ///> Buffer de envio
	uint16_t        PayloadLength, RNumber;
	SizeRxFrame = 0;

	BufferUSB = &CommData->BufferUSB;
	CommData->CurCmd = CurCmd = BufferUSB->HP.Header[0];
	CmdSize.Bytes[0] = BufferUSB->HP.Header[2];
	CmdSize.Bytes[1] = BufferUSB->HP.Header[1];
	PayloadLength    = CmdSize.Word16;

	// Certifica que este pacote nao sera processado novamente.
	BufferUSB->HP.Header[0] = BufferUSB->HP.Header[1] = BufferUSB->HP.Header[2] = 0;

	 // Comando invalido
	if(CurCmd < 0x21 || CurCmd > 0x29)
	{
		return USB_RELISTEN;
	}

	// Tamanho invalido
	if(!PayloadLength)
	{
		return USB_RELISTEN;
	}

	// Todos os pacotes sao criptografados com uma chave AES
	memset(&BufferRxUSBSitradOut[0], 0, sizeof(BufferRxUSBSitradOut));

	switch(CurCmd)
	{
	   // Quando toda a informação chega em um unico pacote
	   case CNV_TXPACKT:
		   if (!Rx_Lim )
		   {
			   CommData->FragFile=0;
			   CommData->PayloadLen = PayloadLength;

			   LoadRxBuffer(&BufferUSB->HP.Payload[0], CommData,PayloadLength);
			   SizeRxFrame = AESDec(&CommData->BufferInfoConf[0], &CommData->Buffer_Rx[0], (uint16_t)CommData->PayloadLen);

			   if(!SizeRxFrame)
			   {
#ifdef DEBUG_USB
				   printf("\n ERRO CRIPT \n");
#endif
				   return USB_RELISTEN;
			   }
#ifdef DEBUG_USB
//			   printf("\n----------0x21-------------\n");
//			   for(int i = 0; i<= CommData->PayloadLen; i++)
//			   {
//				   printf(" %x",CommData->Buffer_Rx[i]);
//			   }
//			   printf("\n-----------------------\n");
#endif
			   ProcessRxBuffer(CommData, StatusFlagsX);
			   if(CommData->FlagUpdateFWRun)
				   FrameBufferResponse(CommData, CNV_TXPACKT_OK);

			   ResetRxBuffer();
			   return USB_SEND;
		   }
	   break;

	   // Quando a informação chega em vários pacotes
	   //CNV_INIT_TXBUFF: Pacote com início do payload
	   //CNV_BDY_TXBUFF: Pacote com as informações intermediarias do payload
	   //CNV_END_TXBUFF: Pacote com o final do payload
	   case CNV_INIT_TXBUFF:
		   if (!Rx_Lim)
		   {
#ifdef DEBUG_USB
			   printf("\n INT BUFFER\n");
#endif
			   CommData->PayloadLen = PayloadLength;
			   RNumber = (uint16_t)rand();

			   if(CommData->FlagUpdateFWRun)
			   {
				   LoadRxBuffer(&BufferUSB->HP.Payload[0], CommData, (uint16_t)(CommData->PayloadLen)); // ignorar o ultimo byte
				   SizeRxFrame = AESDec(&CommData->BufferInfoConf[0], &CommData->Buffer_Rx[0], (uint16_t)(CommData->PayloadLen-2));

					if(!SizeRxFrame)
					{
#ifdef DEBUG_USB
						printf("\n ERRO CRIPT \n");
#endif
						return USB_RELISTEN;
					}
#ifdef DEBUG_USB
//					   printf("\n----------0x22-------------\n");
//					   for(int i = 0; i<= CommData->PayloadLen; i++)
//					   {
//						   printf(" %x",CommData->Buffer_Rx[i]);
//					   }
//					   printf("\n-----------------------\n");
#endif
				   DownloadFirmware(CommData, (uint16_t)RNumber);
				   FrameBufferResponse(CommData, CNV_BDY_TXBUFF_OK);
				   ResetRxBuffer();
				   return USB_SEND;
			   }
			   else
			   {
				   LoadRxBuffer(&BufferUSB->HP.Payload[0], CommData,PayloadLength); // ignorar o ultimo byte
				   FrameBufferResponse(CommData, CNV_INIT_TXBUFF);
			   }

		       return USB_BUFFERING;
		   }
	   break;

	   case CNV_BDY_TXBUFF:

		   if (!Rx_Lim)
		   {
			   if(CommData->FlagUpdateFWRun)CommData->PayloadLen = PayloadLength;
			   else CommData->PayloadLen+= PayloadLength;

#ifdef DEBUG_USB
			   if(CommData->FragFile%100 == 0)
				   printf("\nPKT: %d - BDY - LENTOTAL: %d\n",CommData->FragFile, CommData->PayloadLen);
#endif

			   RNumber = (uint16_t)rand();
			   if(CommData->FlagUpdateFWRun)
			   {

				   LoadRxBuffer(&BufferUSB->HP.Payload[0], CommData, (uint16_t)(CommData->PayloadLen)); // ignorar o ultimo byte
				   SizeRxFrame = AESDec(&CommData->BufferInfoConf[0], &CommData->Buffer_Rx[0], (uint16_t)(CommData->PayloadLen-2));
					if(!SizeRxFrame)
					{
#ifdef DEBUG_USB
						printf("\n ERRO CRIPT \n");
#endif
						return USB_RELISTEN;
					}
#ifdef DEBUG_USB
//					   printf("\n----------0x23-------------\n");
//					   for(int i = 0; i<= CommData->PayloadLen; i++)
//					   {
//						   printf(" %x",CommData->Buffer_Rx[i]);
//					   }
//					   printf("\n-----------------------\n");
#endif
				   DownloadFirmware(CommData, (uint16_t)RNumber);
				   ResetRxBuffer();
				   FrameBufferResponse(CommData, CNV_BDY_TXBUFF_OK);
			   }
			   else
			   {
				   LoadRxBuffer(&BufferUSB->HP.Payload[0], CommData,PayloadLength);
				   FrameBufferResponse(CommData, CNV_BDY_TXBUFF);
			   }
			   return USB_SEND;//USB_BUFFERING;
		   }
	   break;

	   case CNV_END_TXBUFF:
		   if (!Rx_Lim)
		   {
#ifdef DEBUG_USB
				printf("\n END BUFFER\n");
#endif

			   if(CommData->FlagUpdateFWRun)CommData->PayloadLen = PayloadLength;
			   else CommData->PayloadLen+= PayloadLength;

			   RNumber = (uint16_t)rand();

			   if(CommData->FlagUpdateFWRun)
			   {
				   if(CommData->PayloadLen < 16)
				   {

				   LoadRxBuffer(&BufferUSB->HP.Payload[0], CommData, 17); // ignorar o ultimo byte
				   SizeRxFrame = AESDec(&CommData->BufferInfoConf[0], &CommData->Buffer_Rx[0], 16);

				   }
				   else
				   {
					   LoadRxBuffer(&BufferUSB->HP.Payload[0], CommData, (uint16_t)(CommData->PayloadLen)); // ignorar o ultimo byte
					   SizeRxFrame = AESDec(&CommData->BufferInfoConf[0], &CommData->Buffer_Rx[0], (uint16_t)(CommData->PayloadLen-2));
					}
					if(!SizeRxFrame)
					{
#ifdef DEBUG_USB
						printf("\n ERRO CRIPT \n");
#endif
						return USB_RELISTEN;
					}

					padding = 0;

				   for(int idx = CommData->PayloadLen; idx > 0 ; idx--){
					   if(CommData->Buffer_Rx[idx] == CommData->Buffer_Rx[idx-1]){
						   padding++;
					   }
				   }
#ifdef DEBUG_USB
//				   printf("\n----------0x25-------------\n");
//				   for(int i = 0; i<= CommData->PayloadLen; i++)
//				   {
//					   printf(" %x",CommData->Buffer_Rx[i]);
//				   }
//				   printf("\n-----------------------\n");
#endif
				   DownloadFirmware(CommData, (uint16_t)RNumber);
				   ResetRxBuffer();

				   FrameBufferResponse(CommData, CNV_END_TXBUFF_OK);
			   }
			   else
			   {

				   	LoadRxBuffer(&BufferUSB->HP.Payload[0], CommData, (uint16_t)(CommData->PayloadLen)); // ignorar o ultimo byte
				    SizeRxFrame = AESDec(&CommData->BufferInfoConf[0], &CommData->Buffer_Rx[0], (uint16_t)(CommData->PayloadLen));

					if(!SizeRxFrame)
					{
#ifdef DEBUG_USB
						printf("\n ERRO CRIPT \n");
#endif
						return USB_RELISTEN;
					}
				   ProcessRxBuffer(CommData, StatusFlagsX);
				   FrameBufferResponse(CommData, CNV_END_TXBUFF_OK);
			   }

				ResetRxBuffer();
				CommData->PayloadLen = 0;
				return USB_SEND;
		   }
	   break;

	   //Implementação futura, se necessário
//	   case CNV_RXBUFF:
//		   ProcessRxBuffer(CommData);
//	   break;

	   //case CNV_FLUSH_USBBUFF:
	   //break;

	   //case CNV_RD_RXSIZE:
	   //break;
	}

	return USB_SEND;
}
void FrameBufferResponse(_COMM_CONTROL *CommData,uint8_t FrameBuffer)
{
	uint8_t headerResp = 0;

	if(FrameBuffer == CNV_INIT_TXBUFF)headerResp = CNV_TXPACKT_OK;
	else if(FrameBuffer == CNV_BDY_TXBUFF)headerResp = CNV_TXPACKT_OK;
	else if(FrameBuffer == CNV_END_TXBUFF)headerResp = CNV_TXPACKT_OK;
	else if(FrameBuffer == CNV_CRC_ERROR)headerResp = CNV_CRC_ERROR;
	else headerResp = CNV_TXPACKT_OK;

	memset(&CommData->BufferUSB.HeaderPayload[0], 0, sizeof(CommData->BufferUSB.HeaderPayload));

	CommData->BufferUSB.HeaderPayload[0] = headerResp;
	CommData->BufferUSB.HeaderPayload[1] = 0x00;
	CommData->BufferUSB.HeaderPayload[2] = 16;

	SendPacketUSB(CommData,19);
}

void ProcessRxBuffer( _COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX)
{
	uint16_t        PayloadLengthBuffer;

	Rx_Lim = Rx_Qtde;
	Rx_Cnt = 0;
	 int ret = -1;
	 int temp;
	uint8_t cnt = 0;


	if(CommData->SendPkts < 0) PayloadLengthBuffer = ProcessPayloadUSB(CommData, StatusFlagsX);
	else PayloadLengthBuffer = (uint16_t)CommData->PayloadLen;

	if(CommData->FlagUpdateFWRun) return;

	temp = PayloadLengthBuffer;
	while(( PayloadLengthBuffer % 16U ) != 0 ){
		CommData->Buffer_Tx[PayloadLengthBuffer++] = (uint8_t)temp;
	}

	if (( PayloadLengthBuffer % 16U ) != 0 )
	{
		uint8_t tmp;
		tmp = (uint8_t)((uint8_t)(PayloadLengthBuffer / 16U) + (uint8_t)1U);
		PayloadLengthBuffer = (uint8_t)(tmp*16U);
	}

	if(PayloadLengthBuffer <= SIZE_PAYLOAD_USB) // Payload len zero indica que nenhuma resposta deve ser enviada.
	{
		memset(&CommData->BufferUSB.HeaderPayload[0], 0, sizeof(CommData->BufferUSB.HeaderPayload));

		CommData->BufferUSB.HeaderPayload[cnt++] = CNV_TXPACKT_OK;
		CommData->BufferUSB.HeaderPayload[cnt++] =  (uint8_t)((PayloadLengthBuffer >>  8) & 0xFF);//(uint8_t)(PayloadLengthBuffer >> 8);
		CommData->BufferUSB.HeaderPayload[cnt++] =  (uint8_t)((PayloadLengthBuffer >>  0) & 0xFF);//(uint8_t)(PayloadLengthBuffer^0xFF);

		// Todos os pacotes sao criptografados com uma chave AES
		SizeRxFrame = AESEnc(&CommData->Buffer_Tx[0], &CommData->BufferUSB.HeaderPayload[cnt], PayloadLengthBuffer);
		ret = SendPacketUSB(CommData,(uint16_t)(SizeRxFrame+3U));
		CommData->SendPkts = -1;

	}
	else if(PayloadLengthBuffer > SIZE_PAYLOAD_USB)
	{
		uint16_t NumSendPackets = 0;
				 //num = 0; // Posicão do payload no CommControl.Buffer_Tx

		uint8_t  BufferEncTx[64]; // Buffer de envio encriptado
		CommData->PayloadLen = PayloadLengthBuffer;
		memset(&BufferEncTx[0], 0, sizeof(BufferEncTx));
		memset(&CommData->BufferUSB.HeaderPayload[0], 0, sizeof(CommData->BufferUSB.HeaderPayload));

		{
			cnt = 0;

			if(CommData->SendPkts == 0xFF)
			{
				pos = 0;
				CommData->SendPkts = NumSendPackets = (uint16_t)((PayloadLengthBuffer-pos)/48);
				CommData->BufferUSB.HeaderPayload[cnt++] = CNV_INIT_TXBUFF_OK;

				CommData->BufferUSB.HeaderPayload[cnt++] = (uint8_t)((48 >>  8) & 0xFF);//(uint8_t)(SIZE_PAYLOAD_USB >> 8);
				CommData->BufferUSB.HeaderPayload[cnt++] = (uint8_t)((48 >>  0) & 0xFF);//(uint8_t)(SIZE_PAYLOAD_USB^0xFF);

				memset(&BufferEncTx[0], 0, sizeof(BufferEncTx));

				//Encripta o payload de envio
				SizeRxFrame = AESEnc(&CommData->Buffer_Tx[0], &BufferEncTx[pos], 48);

			    memcpy(&CommControl.BufferUSB.HeaderPayload[cnt], &BufferEncTx[pos], SizeRxFrame);
			    pos = 48;
			    CommData->SendPkts--;

			    ret = SendPacketUSB(CommData,(uint16_t)(SIZE_HEADER_USB + SIZE_PAYLOAD_USB));
#ifdef DEBUG_USB
			    printf("SEND_FIRST_PKT: %d", ret);
#endif
			}
			else if (CommData->SendPkts == 0)
			{
				CommData->SendPkts = NumSendPackets = (uint16_t)((PayloadLengthBuffer-pos)/48);
				CommData->BufferUSB.HeaderPayload[cnt++] = CNV_END_TXBUFF_OK;
				CommData->BufferUSB.HeaderPayload[cnt++] = (uint8_t)((48 >>  8) & 0xFF);//(uint8_t)(SIZE_PAYLOAD_USB >> 8);
				CommData->BufferUSB.HeaderPayload[cnt++] = (uint8_t)((48 >>  0) & 0xFF);//(uint8_t)(SIZE_PAYLOAD_USB^0xFF);

				memset(&BufferEncTx[0], 0, sizeof(BufferEncTx));
				//Encripta o payload de envio
				SizeRxFrame = AESEnc(&CommData->Buffer_Tx[pos], &BufferEncTx[0], 48);

			    memcpy(&CommControl.BufferUSB.HeaderPayload[cnt], &BufferEncTx[0], SizeRxFrame);
			    CommData->SendPkts = -1;
				pos = 0;
				ret = SendPacketUSB(CommData,(uint16_t)(SIZE_HEADER_USB + SIZE_PAYLOAD_USB));

#ifdef DEBUG_USB
				printf("SEND_LAST_PKT: %d", ret);
#endif
			}
			else
			{
				CommData->SendPkts = NumSendPackets = (uint16_t)((PayloadLengthBuffer-pos)/48);
				CommData->BufferUSB.HeaderPayload[cnt++] = CNV_BDY_TXBUFF_OK;
				CommData->BufferUSB.HeaderPayload[cnt++] = (uint8_t)((48 >>  8) & 0xFF);//(uint8_t)(SIZE_PAYLOAD_USB >> 8);
				CommData->BufferUSB.HeaderPayload[cnt++] = (uint8_t)((48 >>  0) & 0xFF);//(uint8_t)(SIZE_PAYLOAD_USB^0xFF);

				memset(&BufferEncTx[0], 0, sizeof(BufferEncTx));
				//Encripta o payload de envio

				SizeRxFrame = AESEnc(&CommData->Buffer_Tx[pos], &BufferEncTx[0], 48);

			    memcpy(&CommControl.BufferUSB.HeaderPayload[cnt], &BufferEncTx[0], SizeRxFrame);

				pos = (uint16_t)(pos + 48);
				CommData->SendPkts--;
				ret = SendPacketUSB(CommData,(uint16_t)(SIZE_HEADER_USB + SIZE_PAYLOAD_USB));

#ifdef DEBUG_USB
				printf("SEND_%d_PKT: %d",CommData->SendPkts, ret);
#endif
			}
		}
	}
}


uint16_t ProcessPayloadUSB(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX)
{
	#define TypeCom 2
	uint16_t RNumber = (uint16_t)rand();
    uint16_t Size = 0;
#ifdef DEBUG_USB
    printf("\nCOMANDO: ");
#endif
	switch(CommData->Buffer_Rx[2])
	{
		case CMD_LOGIN_CONVERSOR:
#ifdef DEBUG_USB
			printf("CMD_LOGIN_CONVERSOR\n");
#endif
			Size = LoginDataResponse(CommData, (uint16_t)RNumber);
			break;

		case CMD_REINICIA_CONVERSOR: // REINICIA O CONVERSOR (REBOOT)
#ifdef DEBUG_USB
			printf("CMD_REINICIA_CONVERSOR\n");
#endif
			Size = AnswerDefault(CommData, (uint16_t)RNumber);
			StatusFlagsX->FlagRestartModule = true;
			CommData->TimerRestartModule = 3; //INICIALIZA A VARIAVEL QUE VAI RESETAR O CONVERSOR
			break;

		case CMD_CONF_FABRICA:
#ifdef DEBUG_USB
			printf("CMD_CONF_FABRICA\n");
#endif
			Size = AnswerDefault(CommData, (uint16_t)RNumber);
			StatusFlagsX->FlagRestoreFactory = true;
			break;

		case CMD_ATUALIZA_CONF:
#ifdef DEBUG_USB
			printf("CMD_ATUALIZA_CONF\n");
#endif
			Size = AtualizaInfoConfUSB(CommData,(uint16_t)RNumber);
			break;

		case CMD_CMD_REQ_CONF:
#ifdef DEBUG_USB
			printf("CMD_CMD_REQ_CONF\n");
#endif
			Size = RequesitaInfoConfUSB(CommData, (uint16_t)RNumber);
			break;

		case CMD_DESC_CONVERSOR:
#ifdef DEBUG_USB
			printf("CMD_DESC_CONVERSOR\n");
#endif
			Size = AnswerDefault(CommData, (uint16_t)RNumber);
		    tx_event_flags_set (&Cellular_Flags, CELL_FLAG_CELL_DISCONNECTED, TX_OR);
			break;

		case CMD_LIMPAR_DATALOGGER:
#ifdef DEBUG_USB
			printf("CMD_LIMPAR_DATALOGGER\n");
#endif
			StatusFlagsX->FlagEreaseDatalogger = true;
			Size = AnswerDefault(CommData, (uint16_t)RNumber);
			break;

		case CMD_LEITURA_LOG:
#ifdef DEBUG_USB
			printf("CMD_LEITURA_LOG\n");
#endif
			Size = ReadPgDatalogger(CommData, (uint16_t)RNumber);
			break;

		case CMD_AJUSTE_DATA_HORA:
#ifdef DEBUG_USB
			printf("CMD_AJUSTE_DATA_HORA\n");
#endif
			Size = UpdadeDateHour(CommData, (uint16_t)RNumber);
			StatusFlagsX->FlagUpdateDateTime = true;
			break;

		case CMD_STATUS_CONVERSOR:
#ifdef DEBUG_USB
			printf("CMD_STATUS_CONVERSOR\n");
#endif
			Size = GetStatus(CommData, (uint16_t)RNumber);
			break;

		case CMD_ATUALIZACAO_FW:
#ifdef DEBUG_USB
			printf("CMD_ATUALIZACAO_FW\n");
#endif
			Size = DownloadFirmware(CommData, (uint16_t)RNumber);
			break;

		case CMD_TESTE_FABRICA:
#ifdef DEBUG_USB
			printf("CMD_TESTE_FABRICA\n");
#endif
			Size = FactoryTest(CommData, (uint16_t)RNumber);
			break;

		default:
			CommData->Buffer_Tx[1] = 0;
			CommData->Buffer_Tx[2] = 0;
			Size = 0;
		    break;
	}

	return Size;
}

uint16_t ReadPgDatalogger(_COMM_CONTROL *CommData, uint16_t RandomNumber)
{
    /* Teste ...  08 00 03 00 00 01 08 08 08 08 08 08 08 08*/
    /* Pagina(d39 = 0x27) 08 00 03 00 27 01 08 08 08 08 08 08 08 08
    * */
    //print_to_console("\n\r [DM] SITRAD TCP -> 0x08 - READ_PG_DATALOGGER!");
    //if(S25FL064ChipEraseFinished() == 0)
    // 0   1   2   3  4  5 6   7  ....
    //RDM RDL 08  00  03 x y   z  (X || Y): Pagina a Ler  z: Qntidade de paginas.
    uint16_t Size = 0;
    uint16_t Temp;

    Temp = (uint16_t)((CommData->Buffer_Rx[5] <<  8) | (CommData->Buffer_Rx[6]));         //captura o numero da pagina a ler
    uint8_t QtdPages = CommData->Buffer_Rx[7];

    //-------------------Payload-----------------------
    CommData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber >> 8   );
    CommData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber ^  0xFF);
    CommData->Buffer_Tx[Size++] = CommData->Buffer_Rx[TypeCom] | 0x80;
    CommData->Buffer_Tx[Size++] = 0x00;
    CommData->Buffer_Tx[Size++] = 0x00;

    if((QtdPages > MAX_PAGES_TO_READ) || (QtdPages < 1))
    {
#if HANDLE_FLASH_ERRORS
    	CommData->Buffer_Tx[2] = CommData->Buffer_Rx[TypeCom] | 0xA0;

#endif
        Size = 5; //tamanho do payload
        CommData->Buffer_Tx[3] = 0; //ok
        CommData->Buffer_Tx[4] = 0; //ok
    }
    else // tudo ok, pode ler as paginas
    {

        static _DATA_LOGGER_DATA DataLoggerData;
        DataLoggerData.OpCod = REQUEST_PAG; //Sinaliza o Cod da operacao
        DataLoggerData.QntPg = QtdPages; //numero de paginas que deve ser lida
        DataLoggerData.SizeTemp = Temp; //primeira pagina a ser lida
        DataLoggerData.PayloadTcp = &(CommData->Buffer_Tx[Size]);

        tx_queue_send(&Datalogger_Flash, &DataLoggerData, TX_WAIT_FOREVER); //envia para a thread Datalogger
        tx_thread_relinquish();
        tx_queue_receive(&USB_Flash, &DataLoggerData, TX_WAIT_FOREVER); //recebe da thread Datalogger

#if HANDLE_FLASH_ERRORS
        if(!DataLoggerData.status)//erro de leitura da flash qspi
        {
        	CommData->Buffer_Tx[2] = CommData->Buffer_Rx[TypeCom] | 0xC0;

            Size = 8;
            CommData->Buffer_Tx[3] = 0; //ok
            CommData->Buffer_Tx[4] = 0; //ok
        }
        else
#endif
        {
            uint16_t u16LQtd;
            uint8_t     PayloadTcpCPY[256];    //4
            u16LQtd = (uint16_t)((uint16_t)(QtdPages*PAGE_SIZE) + (uint16_t)(CRC_SIZE));

            *PayloadTcpCPY = 0;
			memcpy(PayloadTcpCPY,DataLoggerData.PayloadTcp,256);
			for(int i = 0; i<= u16LQtd; i++)
			{

			if((PayloadTcpCPY[i] == DATALOGGER_HEADER_EVENT)&&(PayloadTcpCPY[i+3] == 0x0F)){

			CommData->Buffer_Tx[Size++] = PayloadTcpCPY[i+4];//DataLoggerData.PayloadTcp[i+4];	//DATA E HORA 22
			CommData->Buffer_Tx[Size++] = PayloadTcpCPY[i+5];//DataLoggerData.PayloadTcp[i+5];	//DATA E HORA 11
			CommData->Buffer_Tx[Size++] = PayloadTcpCPY[i+6];//DataLoggerData.PayloadTcp[i+6];	//DATA E HORA 99
			CommData->Buffer_Tx[Size++] = PayloadTcpCPY[i+7];//DataLoggerData.PayloadTcp[i+7];	//DATA E HORA 22
			CommData->Buffer_Tx[Size++] = PayloadTcpCPY[i+8];//DataLoggerData.PayloadTcp[i+8];	//DATA E HORA 11
			CommData->Buffer_Tx[Size++] = PayloadTcpCPY[i+9];//DataLoggerData.PayloadTcp[i+9];   //DATA E HORA 59
			CommData->Buffer_Tx[Size++] = 0;//PayloadTcpCPY[i+10];//DataLoggerData.PayloadTcp[i+10]; //EVENTO
			CommData->Buffer_Tx[Size++] = PayloadTcpCPY[i+1];//DataLoggerData.PayloadTcp[i+11]; //EVENTO
			}
		}

            CommData->SendPkts = 0xFF;
        }

    }
    return Size;
}
uint16_t DownloadFirmware(_COMM_CONTROL *CommData,  uint16_t RandomNumber)
{
    bool                FSend       = false;
    uint16_t            Size        = 0,
                        SizePayload = 0,
                        SizeFile    = 0,
                        CRC_Calc    = 0,
                        CompFile    = 0,
                        FragFile    = 0,
                        CRC_Recv    = 0;
    static uint32_t     AddressFirmwareL = 0;
    static uint16_t     ExpFragFileL = 0;
    UINT OldPrority;
    CommData->TimerTimeOutUpdateFW = 8;

    //ThreadMonitor_SendKeepAlive(T_MONITOR_STOP_MONITORING); //para o monitorando

    if(!CommData->FlagUpdateFWRun)
    {
    	CommData->CompFile = CompFile    = (uint16_t)((CommData->Buffer_Rx[3] <<  8) | (CommData->Buffer_Rx[4])); //captura o numero de partes do arqvivo
		tx_thread_priority_change(&USB_Thread, 1, &OldPrority);
    }

    SizePayload = (uint16_t)CommData->PayloadLen;//captura o tamanho do payload
    if(padding > 0) SizeFile    = (uint16_t)(SizePayload - (padding-1));
    else SizeFile    = (uint16_t)(SizePayload - 3);

    FragFile    = CommData->FragFile;//captura o fragmento do arquivo
    CompFile    = CommData->CompFile;
    CRC_Calc = (uint16_t)(CrcModbusCalc((uint8_t*)&CommData->Buffer_Rx[0], SizeFile, true));

    if(SizePayload < 16) SizePayload = 18;

    CRC_Recv = (uint16_t)((CommData->BufferInfoConf[SizePayload-2] <<  8) | CommData->BufferInfoConf[SizePayload-1]);

    if(!FragFile) //Sinaliza que vai comecar a enviar o firmware;
    {

#if RECORD_DATALOGGER_EVENTS
        DataLoggerRegisterEvent(DATALOGGER_EVENT_FOTA_STATUS_CHANGE, 0, true);
#endif
        _DATA_LOGGER_DATA DataLoggerData;
        DataLoggerData.OpCod = START_UPDATE_FW; //Avisa a thread datalogger que uma atualizacao vai comecar.
        tx_queue_send(&Datalogger_Flash, &DataLoggerData, TX_WAIT_FOREVER); //envia para a thread Datalogger
        CommData->FragFile = FragFile = ExpFragFileL = 1;
        AddressFirmwareL = 0;
        ClearSectorFlashQSPIFirm((uint16_t)AddressFirmwareL);
        CommData->FlagUpdateFWRun = 1;
        if(!CommData->FlagConnectSitrad)
        {
            FSend = true;
        }
        return 0;
    }

    else if((CRC_Calc == CRC_Recv) && (FragFile <= CompFile))
    {
       if(FragFile != ExpFragFileL)
       {
#ifdef DEBUG_USB
    	   printf("\nFRAGFILE %d != EXPFRAGFILE %d\n", FragFile , ExpFragFileL);
#endif
           g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_LOW);
           FSend = false;
       }
       else
       {
           g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_HIGH);

           CommData->FlashExternalPaylod->SizeData = (uint16_t)SizeFile;
           CommData->FlashExternalPaylod->FragFile = (uint16_t)FragFile;
           CommData->FlashExternalPaylod->CompFile = (uint16_t)CompFile;
           CommData->FlashExternalPaylod->DataFlash = &CommData->Buffer_Rx[0];

           AddressFirmwareL = SaveFirmwareQSPI(CommData->FlashExternalPaylod, AddressFirmwareL);
#ifdef DEBUG_USB
           if(ExpFragFileL == 1)
        	   printf("\nFrag(%d) End(%x)\n", ExpFragFileL,AddressFirmwareL);
#endif

           ExpFragFileL ++;
           CommData->FragFile++;
           FSend = true;
       }

       if(CompFile == FragFile+1)
       {
           if(!FSend)
           {

        	   CommData->TimerTimeOutUpdateFW = 3;
           }
           else
           {

        	   if(!CheckCRCFirmwareQSPI())
			   {
        		   CommData->TimerTimeOutUpdateFW = 1;
        		   CommData->FlagAttFW = 0;
			   }
			   else
			   {
				   CommData->TimerTimeOutUpdateFW = 2;
			   }
           }
       }
    }
    else
    {
#ifdef DEBUG_USB
		printf("ERRO AO SALVAR FRAG\n");
		if(CRC_Calc != CRC_Recv) printf("\nCRC_C(%d) == CRC_R(%d) ",CRC_Calc,CRC_Recv);
		if(FragFile > CompFile) printf("FF(%d) <= CF(%d)\n",FragFile, CompFile);
#endif
		return (uint16_t)-1;
    }
    //-------------------+ Header +------------------------
	CommControl.Buffer_Tx[Size++] = HEADER_TX_TCP;
    CommControl.Buffer_Tx[Size++] = 0x00;
    CommControl.Buffer_Tx[Size++] = 0x10;
    //------------------+ Payload +------------------------
    CommControl.Buffer_Tx[Size++] = (uint8_t)(RandomNumber   >>  8   );
    CommControl.Buffer_Tx[Size++] = (uint8_t)(RandomNumber   ^   0xFF);
    CommControl.Buffer_Tx[Size++] = (uint8_t)CommData->Buffer_Rx[TypeCom] | 0x80;
    CommControl.Buffer_Tx[Size++] = 0;//tamanho
    if(FSend)
    {
    	CommControl.Buffer_Tx[Size++] = 0;
        FSend = false;
    }
    else
    {
    	CommControl.Buffer_Tx[Size++] = 1;
    	CommControl.Buffer_Tx[Size++] = (uint8_t)ExpFragFileL;
    }

    g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_HIGH);

    return Size;
}

uint16_t FactoryTest(_COMM_CONTROL *CommData, uint16_t RandomNumber)
{
	bool     FSend = true;
	uint16_t Size = 0;

    //------------------+ Payload +------------------------
	CommData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber   >>  8   );
	CommData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber   ^   0xFF);
	CommData->Buffer_Tx[Size++] = (uint8_t)CommData->Buffer_Rx[TypeCom] | 0x80;
	CommData->Buffer_Tx[Size++] = 0x00;//06
	CommData->Buffer_Tx[Size++] = 0x02;//7
	CommData->Buffer_Tx[Size++] = CommData->Buffer_Rx[5]; //8 sub id
    Size++;

    switch(CommData->Buffer_Rx[5])
    {
		case FAC_TEST_START:
		{
			CommData->FlagFAC = true;

            _DATA_LOGGER_DATA DataLoggerData;
            DataLoggerData.OpCod = TEST_START; //Avisa a thread datalogger que uma atualizacao vai comecar.
            tx_queue_send(&Datalogger_Flash, &DataLoggerData, TX_WAIT_FOREVER); //envia para a thread Datalogger

			ThreadMainManagerSetFlag(MM_FAC_MODE);

		}break;

        case FAC_TEST_SUPER_CAP_ON:
        {
            g_ioport.p_api->pinWrite(RTC_FAC_PIN,IOPORT_LEVEL_HIGH);

            tx_thread_sleep(TIME_200MS);

            InitI2C_RTC();

            tx_thread_sleep(TIME_200MS);

            // inicializa o RTC Externo
            RX8571LC_Init();

            // verifica estado de ECLO
            if(FG_SUCCESS != RX8571LC_VerifyEclo())
            {
                FSend = false; //foi mal.
            }

            ThreadMonitor_SendKeepAlive(T_MONITOR_START_MONITORING);
        }break;

        case FAC_TEST_SUPER_CAP_OFF:
        {
        	ThreadMonitor_SendKeepAlive(T_MONITOR_STOP_MONITORING);

            CloseI2C_RTC();

            g_ioport.p_api->pinWrite(RTC_FAC_PIN,IOPORT_LEVEL_LOW);

        }break;

        case FAC_TEST_QSPI:
        {
            FSend = TestQSPI(); //ret=true ok, ret false, nok

        }break;

        case FAC_TEST_LEDS:
        {
        	ThreadMainManagerSetLedColor(CommData->Buffer_Rx[6]);
            ThreadMainManagerSetFlag(MM_FAC_TEST_LEDs);

        }break;

        case FAC_TEST_485:
        {
            FSend = TestSerial485(CommData->Buffer_Rx[6]);

        }break;

        case FAC_TEST_KEY:
        {
            FSend = ThreadMainManagerGetStatusButton();
        }break;

        case FAC_TEST_DO_FAC:
        {
        	_FLAGS_EEPROM FlagsEeprom;
        	FlagsEeprom.Bites.bCalibration = true;
        	SetValueFacEEPROM(&FlagsEeprom.Bytes);
            CommData->TimerRestartModule = 3; //INICIALIZA A VARIAVEL QUE VAI RESETAR O CONVERSOR
        }break;

        case FAC_TEST_END:
        {
        	CommData->FlagFAC = false;
        	ThreadMainManagerSetLedColor(12);
        	ThreadMainManagerSetFlag(MM_FAC_TEST_LEDs);
        }break;

		default:
			break;
	}

    if(FSend)
	{
    	CommData->Buffer_Tx[6] = 0;
	}
	else
	{
		CommData->Buffer_Tx[6] = 1;
	}

    return Size++;
}

//uint8_t TestStatusRssid(uint8_t Rssid_In)
//{
//
//        uint8_t Ret = 0;
//        if(Rssid_In > STATUS_EXCELLENT)
//        {
//            Ret = 4;
//        }
//        else if(Rssid_In> STATUS_GOOD)
//        {
//            Ret = 3;
//        }
//        else if(Rssid_In > STATUS_POOR)
//        {
//            Ret = 2;
//        }
//        else if(Rssid_In > STATUS_VERY_POOR)
//        {
//            Ret = 1;
//        }
//
//        return Ret;
//}

bool TestSerial485(uint8_t AddressInst485)
{
    ULONG SerialFlag = 0;
    uint32_t AddressInst_485;
    AddressInst_485 = AddressInst485;

    tx_queue_send(&QueueTest485, &AddressInst_485, TX_WAIT_FOREVER);   //envia para a thread Serial
    tx_thread_relinquish();

    tx_event_flags_get(&EventFlagsSerialTestF, 0x01, TX_OR_CLEAR, &SerialFlag, TIME_2S);

    return (bool)SerialFlag;
}

bool TestQSPI(void)
{
    uint8_t Tve[BUFFER_LENGTH-1];
    uint8_t InTve[BUFFER_LENGTH];
    bool FlagTestMem = false;

    memset(&InTve[0], 0xFF, BUFFER_LENGTH);

    FlashReadFirmwareData(0, 1, &InTve[0]);
    for(uint8_t i=0; i < sizeof(Tve); i++)
    {
        if(InTve[i] == 0xFF)
        {
            FlagTestMem = true; //se a flash vazia
        }
    }

    if(FlagTestMem)
    {
        ClearSectorFlashQSPIFirm(0);
        memset(&InTve, 0, sizeof(InTve));

        for(uint8_t i=0; i < sizeof(Tve); i++)
        {
            Tve[i] = (uint8_t)rand();
        }

        FlashWritePageData(QSPI_DEVICE_ADDRESS, sizeof(Tve), &Tve[0]);
        FlashReadFirmwareData(0, 1, &InTve[0]);
        for(uint8_t i=0; i < sizeof(Tve); i++)
        {
            if(InTve[i] != Tve[i])
            {
                return false;
            }
        }
    }

    return true;
}


uint16_t AtualizaInfoConfUSB(_COMM_CONTROL *CommData, uint16_t RandomNumber)
{
	uint16_t Temp = 0;
	uint16_t Size = 0;

    //-------------------Payload-----------------------
	CommData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber    >>     8);     //RANDOM1_IND
	CommData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber    ^   0xFF);     //RANDOM2_IND
	CommData->Buffer_Tx[Size++] = CommData->Buffer_Rx[2] |    0x80;         //COMMAND_IND
	CommData->Buffer_Tx[Size++] = 0x00;          //SIZE
	CommData->Buffer_Tx[Size++] = 0x00;          //SIZE

	if(CommData->Buffer_Rx[4] == 0xFD) //compatibilidade da versao antiga
	{
		Temp = (uint16_t)((CommData->Buffer_Rx[7] <<  8) | (CommData->Buffer_Rx[8]));           //captura o tempo de log
		if(Temp < 15)//se for menor que 15 segundos, seta em 15s;
			Temp = 15;

		DataloggerFlashSetCmdSitrad(Temp, &CommData->Buffer_Rx[11], CommData->Buffer_Rx[10]);
	}
	else
	{
		uint16_t SizeRequest = (uint16_t)( CommData->Buffer_Rx[3] << 8 | CommData->Buffer_Rx[4]);
		SetConfSitradUSB(&CommData->Buffer_Rx[5], CommData, SizeRequest); //envia os parametros recebidos do configurador para a memoria flash.
	}
    DataLoggerRegisterEvent(DATALOGGER_EVENT_CONFIG_STATUS_CHANGE, 0, true);
    return Size;
}

uint16_t RequesitaInfoConfUSB(_COMM_CONTROL *CommData, uint16_t RandomNumber)
{
    uint16_t Size = 0;

    bool     FlagReadConf = false;

    //-------------------Payload-----------------------
	CommData->Buffer_Tx[Size++]  = (uint8_t)(RandomNumber >> 8  );
	CommData->Buffer_Tx[Size++]  = (uint8_t)(RandomNumber ^ 0xFF);
	CommData->Buffer_Tx[Size++]  = CommData->Buffer_Rx[2] | 0x80;
	CommData->Buffer_Tx[Size++]  = 0x00;
	CommData->Buffer_Tx[Size++]  = 0x00;
    if((!CommData->Buffer_Rx[3]) && (!CommData->Buffer_Rx[4]))
    {
        //varre o intervalo
        uint16_t    TmpRet,
                    SizePayload = 0;

        for(uint8_t i = 0x16; i <= 0x17; i++)
        {
        	TmpRet = (uint16_t)GetConfSitradUSB(i, &CommData->Buffer_Tx[Size], CommData, FlagReadConf);
            FlagReadConf = true;
            if(TmpRet)
            {
                SizePayload = (uint16_t)(SizePayload + TmpRet);
                Size = (uint16_t)(TmpRet + Size);
            }
        }

        CommData->Buffer_Tx[3] = (uint8_t)(    ((SizePayload) >>  8) & 0xFF);
        CommData->Buffer_Tx[4] = (uint8_t)(    ((SizePayload) >>  0) & 0xFF);

        Size = (uint16_t)(SizePayload  + (uint16_t)5); //ajustar o payload

    }
    else
    {
        //varre o intervalo
        uint16_t    TmpRet,
                    SizePayload = 0,
                    SizeRequest;
        SizeRequest = (uint16_t)(CommData->Buffer_Rx[3]);

        if(!CommData->Buffer_Rx[5]) //verifica se eh range de IDs!
        {
            if(!CommData->Buffer_Rx[6] && !CommData->Buffer_Rx[7]) // 00 00 :> que todos os IDS
            {
            	CommData->Buffer_Rx[6] = ID_MODEL;    //Primeiro ID
            	CommData->Buffer_Rx[7] = ID_END;            //Ultimo ID
            }

            for(uint8_t i = CommData->Buffer_Rx[6]; i <= CommData->Buffer_Rx[7]; i++)
            {
                TmpRet = (uint16_t)GetConfSitradUSB(i, &CommData->Buffer_Tx[Size], CommData, FlagReadConf);
                FlagReadConf = true;
                if(TmpRet)
                {
                    SizePayload = (uint16_t)(SizePayload + TmpRet);
                    Size = (uint16_t)(TmpRet + Size);
                }
            }
        }
        else //Neste caso, o Sitrad quer os IDs individuais.
        {
            if(SizeRequest)
            {
                SizeRequest = (uint16_t)(SizeRequest - 1); //remove o sinalizador

                uint8_t IndexBufferInput = 6;
                for(uint8_t i = IndexBufferInput; i <= (SizeRequest+IndexBufferInput); i++)
                {
                    TmpRet = (uint16_t)GetConfSitradUSB(CommData->Buffer_Rx[i], &CommData->Buffer_Tx[Size], CommData, FlagReadConf);
                    FlagReadConf = true;
                    if(TmpRet)
                    {
                        SizePayload = (uint16_t)(SizePayload + TmpRet);
                        Size = (uint16_t)(TmpRet + Size);
                    }
                }
            }
        }


        CommData->Buffer_Tx[3] = (uint8_t)(    ((SizePayload) >>  8) & 0xFF); //CMD_SIZE1_IND
        CommData->Buffer_Tx[4] = (uint8_t)(    ((SizePayload) >>  0) & 0xFF); //CMD_SIZE2_IND

        Size = (uint16_t)(SizePayload + (uint16_t)5); //ajustar o payload

    }

    return Size;
}

uint16_t UpdadeDateHour(_COMM_CONTROL *CommData, uint16_t RandomNumber)
{
    uint16_t Size = 0;

    ClearTxBuffer(CommData);

    //-------------------Payload-----------------------
    CommData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber >> 0x08);
    CommData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber ^ 0xFF);
    CommData->Buffer_Tx[Size++] = (CommData->Buffer_Rx[2] | 0x80);
    CommData->Buffer_Tx[Size++] = 0x00;
    CommData->Buffer_Tx[Size++] = 0x01;
    CommData->Buffer_Tx[Size++] = 0x00;


    CommData->DateTimeValues[0] = CommData->Buffer_Rx[5];
    CommData->DateTimeValues[1] = CommData->Buffer_Rx[6];
    CommData->DateTimeValues[2] = CommData->Buffer_Rx[7];
    CommData->DateTimeValues[3] = CommData->Buffer_Rx[8];
    CommData->DateTimeValues[4] = CommData->Buffer_Rx[9];
    CommData->DateTimeValues[5] = CommData->Buffer_Rx[10];
    RtcPost(CommData);
    return Size;
}


uint16_t AnswerDefault(_COMM_CONTROL *CommData, uint16_t RandomNumber)
{
    uint16_t Size = 0;

    ClearTxBuffer(CommData);

    //-------------------Payload-----------------------
    CommData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber >> 0x08);
    CommData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber ^ 0xFF);
    CommData->Buffer_Tx[Size++] = CNV_TXPACKT_OK;//(CommData->Buffer_Rx[2] | 0x80 );
    CommData->Buffer_Tx[Size++] = 0x00;
    CommData->Buffer_Tx[Size++] = 0x01;
    CommData->Buffer_Tx[Size++] = 0x00;

    return Size;
}


uint16_t GetStatus(_COMM_CONTROL *CommData, uint16_t RandomNumber)
{
    uint8_t     DateTimeValue[DATE_TIME_SIZE];
    uint16_t    Temp;
    uint16_t    Size = 0;

    ClearTxBuffer(CommData);

    GetDateTimerX(DateTimeValue);

    //-------------------Payload-----------------------
    CommData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber >> 8);
    CommData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber ^ 0xFF);
    CommData->Buffer_Tx[Size++] = (uint8_t)(CommData->Buffer_Rx[TypeCom] | 0x80);
    CommData->Buffer_Tx[Size++] = (uint8_t)0x00;
    CommData->Buffer_Tx[Size++] = (uint8_t)0x10;
    CommData->Buffer_Tx[Size++] = (uint8_t)DataloggerGetFlags();
    CommData->Buffer_Tx[Size++] = (uint8_t)GetFlagEclo();
    CommData->Buffer_Tx[Size++] = (uint8_t)DateTimeValue[DATE_TIME_DAY];
    CommData->Buffer_Tx[Size++] = (uint8_t)DateTimeValue[DATE_TIME_MONTH];
    CommData->Buffer_Tx[Size++] = (uint8_t)DateTimeValue[DATE_TIME_YEAR];
    CommData->Buffer_Tx[Size++] = (uint8_t)DateTimeValue[DATE_TIME_HOUR];
    CommData->Buffer_Tx[Size++] = (uint8_t)DateTimeValue[DATE_TIME_MINUTE];
    CommData->Buffer_Tx[Size++] = (uint8_t)DateTimeValue[DATE_TIME_SECOND];
    Temp = DataloggerGetActualPage();
    CommData->Buffer_Tx[Size++] = (uint8_t)((Temp >>  8) & 0xFF);
    CommData->Buffer_Tx[Size++] = (uint8_t)((Temp >>  0) & 0xFF);
    CommData->Buffer_Tx[Size++] = (uint8_t)((PAGE_FLASH_SIZE >>  8) & 0xFF);
    CommData->Buffer_Tx[Size++] = (uint8_t)((PAGE_FLASH_SIZE >>  0) & 0xFF);
    CommData->Buffer_Tx[Size++] = (uint8_t)((NUMBER_OF_PAGES_DATALOGGER >>  8) & 0xFF);
    CommData->Buffer_Tx[Size++] = (uint8_t)((NUMBER_OF_PAGES_DATALOGGER >>  0) & 0xFF);

    CommData->Buffer_Tx[Size++] = (uint8_t)(CommData->FlagConnectGPRS); //novo

    return Size;
}

uint16_t GetConfSitradUSB(uint8_t id, uint8_t * GetConfData, _COMM_CONTROL *CommData, bool FlagReadConf) //(_PACKET_UDP *PacketUdp, _IPSTRUCT *PonterIpStruct)
{
    static _UDP_CONF ConfSitradUdp;
    uint16_t SizeReturn = 0;

    static _NET_STATIC_MODE NetStaticCellular;
    static _CONF_NET_BASIC ConfNetBasic;
    static _NET_CELLULAR_CONF NetCellularConf;
	_NET_STATIC_MODE     NetStatic;

	if(!FlagReadConf)
	{
      ReadPacketUdpCuston(&ConfSitradUdp);

      int_storage_read((uint8_t *)&NetStaticCellular, sizeof(_NET_STATIC_MODE), CONF_CELLULAR_STATIC_IP);
      int_storage_read((uint8_t *)&ConfNetBasic, sizeof(_CONF_NET_BASIC), CONF_NET_BASIC);
      int_storage_read((uint8_t *)&NetCellularConf, sizeof(_NET_CELLULAR_CONF), CONF_CELLULAR);
      int_storage_read((uint8_t *)&NetStatic, sizeof(_NET_STATIC_MODE), CONF_CELLULAR_STATIC_IP);
    }

	switch(id)
	{
//		case ID_MAC_ADDRESS://              = 1,
//			*GetConfData++ = id;
//			*GetConfData++ = MAC_ADDRESS_SIZE;
//			*GetConfData++ = (uint8_t)(CommData->IpStruct.IpMacAdressMs >> 8 ) & 0xFF;
//			*GetConfData++ = (uint8_t)(CommData->IpStruct.IpMacAdressMs      ) & 0xFF;
//			*GetConfData++ = (uint8_t)(CommData->IpStruct.IpMacAdressLs >> 24) & 0xFF;
//			*GetConfData++ = (uint8_t)(CommData->IpStruct.IpMacAdressLs >> 16) & 0xFF;
//			*GetConfData++ = (uint8_t)(CommData->IpStruct.IpMacAdressLs >> 8 ) & 0xFF;
//			*GetConfData++ = (uint8_t)(CommData->IpStruct.IpMacAdressLs      ) & 0xFF;
//			SizeReturn = (uint16_t) (MAC_ADDRESS_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
//			break;
		case ID_IP_ADDRESS://               = 2,
#ifdef DEBUG_USB
			printf("\nID_IP_ADDRESS\n");
#endif
			*GetConfData++ = id;
			*GetConfData++ = IP_ADDRESS_SIZE;
			*GetConfData++ = (uint8_t)(NetStaticCellular.AddIP >>24     ) & 0xFF;
			*GetConfData++ = (uint8_t)(NetStaticCellular.AddIP >>16     ) & 0xFF;
			*GetConfData++ = (uint8_t)(NetStaticCellular.AddIP >>8      ) & 0xFF;
			*GetConfData++ = (uint8_t)(NetStaticCellular.AddIP          ) & 0xFF;
			SizeReturn = (uint16_t) (IP_ADDRESS_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
			break;
//		case ID_MASK_ADDRESS://             = 3,
//			*GetConfData++ = id;
//			*GetConfData++ = MASK_ADDRESS_SIZE;
//			*GetConfData++ = (uint8_t)(CommData->IpStruct.IpMask >>24        ) & 0xFF;
//			*GetConfData++ = (uint8_t)(CommData->IpStruct.IpMask >>16        ) & 0xFF;
//			*GetConfData++ = (uint8_t)(CommData->IpStruct.IpMask >>8         ) & 0xFF;
//			*GetConfData++ = (uint8_t)(CommData->IpStruct.IpMask             ) & 0xFF;
//			SizeReturn = (uint16_t) (MASK_ADDRESS_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
//			break;
//		case ID_GETWAY_ADDRESS://           = 4,
//			*GetConfData++ = id;
//			*GetConfData++ = GTW_ADDRESS_SIZE;
//			*GetConfData++ = (uint8_t)(CommData->IpStruct.IpGateway >>24     ) & 0xFF;
//			*GetConfData++ = (uint8_t)(CommData->IpStruct.IpGateway >>16     ) & 0xFF;
//			*GetConfData++ = (uint8_t)(CommData->IpStruct.IpGateway >>8      ) & 0xFF;
//			*GetConfData++ = (uint8_t)(CommData->IpStruct.IpGateway          ) & 0xFF;
//			SizeReturn = (uint16_t) (GTW_ADDRESS_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
//			break;
        case ID_MODEL:
            *GetConfData++ = id;
            *GetConfData++ = (uint8_t)strlen((const char *)MODEL);
            strcpy((char *)GetConfData, (const char *)MODEL);
            SizeReturn = (uint16_t) (strlen((const char *)MODEL) + HEADER_CONF_SIZE); //size do comando + 2bytes header
            break;

        case ID_CONVERTER_NAME:
#ifdef DEBUG_USB
        	printf("\nID_CONVERTER_NAME\n");
#endif
            *GetConfData++ = id;
            *GetConfData++ = (uint8_t)strlen((const char *)ConfSitradUdp.ModelName);
            strcpy((char *)GetConfData, (const char *)ConfSitradUdp.ModelName);
            SizeReturn = (uint16_t) (strlen((const char *)ConfSitradUdp.ModelName) + HEADER_CONF_SIZE); //size do comando + 2bytes header
            break;

        case ID_ENABLE_PASSWORD:
            *GetConfData++ = id;
            *GetConfData++ = FLAG_PASSWORD_SIZE;
            *GetConfData++ = (uint8_t)ConfSitradUdp.EnablePassword;
            SizeReturn = (uint16_t) (FLAG_PASSWORD_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
            break;

        case ID_VERSION:
            *GetConfData++ = id;
            *GetConfData++ = (uint8_t)strlen((const char *)VERSION);//VERSION_SIZE;
            strcpy((char *)GetConfData, (const char *)VERSION);
            SizeReturn = (uint16_t) ((uint8_t)strlen((const char *)VERSION) + HEADER_CONF_SIZE); //size do comando + 2bytes header
            break;

        case ID_SETUP_PORT://               = 0x0C,
#ifdef DEBUG_USB
        	printf("\nID_SETUP_PORT\n");
#endif
        	*GetConfData++ = id;
            *GetConfData++ = SETUP_PORT_SIZE;
            *GetConfData++ = (uint8_t)(ConfSitradUdp.ConfigurationPort[0]);
            *GetConfData++ = (uint8_t)(ConfSitradUdp.ConfigurationPort[1]);
            SizeReturn =     (uint16_t) (SETUP_PORT_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
            break;
        case ID_COMUNITATION_PORT://        = 0x0E,
#ifdef DEBUG_USB
        	printf("\nID_COMUNITATION_PORT\n");
#endif
            *GetConfData++ = id;
            *GetConfData++ = COMUN_PORT_SIZE;
            *GetConfData++ = (uint8_t)(ConfSitradUdp.ComunicationPort[0]);
            *GetConfData++ = (uint8_t)(ConfSitradUdp.ComunicationPort[1]);
            SizeReturn =     (uint16_t) (COMUN_PORT_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
            break;

        case ID_BAUD_RATE:
            *GetConfData++ = id;
            *GetConfData++ = 4;
            *GetConfData++ = (uint8_t)(BAUD_RATE_RS485  >> 8 ) & 0xFF;
            *GetConfData++ = (uint8_t)(BAUD_RATE_RS485  >> 8 ) & 0xFF;
            *GetConfData++ = (uint8_t)(BAUD_RATE_RS485  >> 8 ) & 0xFF;
            *GetConfData++ = (uint8_t)(BAUD_RATE_RS485       ) & 0xFF;
            SizeReturn = (uint16_t) (4 + HEADER_CONF_SIZE); //size do comando + 2bytes header
            break;

        case ID_TIME_OUT_RS485://           = 0x10,
            *GetConfData++ = id;
            *GetConfData++ = TIME_OUT_485_SIZE;
            *GetConfData++ = (uint8_t)(TIMER_OUT_WAIT_RX_RS485 >> 8 ) & 0xFF;
            *GetConfData++ = (uint8_t)(TIMER_OUT_WAIT_RX_RS485      ) & 0xFF;
            SizeReturn = (uint16_t)(TIME_OUT_485_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
            break;

//        case ID_IP_MODE://                  = 0x11,
//            *GetConfData++ = id;
//            *GetConfData++ = FLAG_IP_MODE_SIZE;
//            *GetConfData++ = ConfNetBasic.AddrMode; ///ver isso depois
//            SizeReturn = (uint16_t)(FLAG_IP_MODE_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
//            break;
//
//        case ID_ENABLE_IP_FILTER://         = 0x13,
//            *GetConfData++ = id;
//            *GetConfData++ = FLAG_ENABLE_IP_FILTER;
//            *GetConfData++ = ConfSitradUdp.EnableIpFilter;
//            SizeReturn = (uint16_t)(FLAG_ENABLE_IP_FILTER + HEADER_CONF_SIZE); //size do comando + 2bytes header
//            break;
//
//        case ID_INIT_IP://                  = 0x14,
//            *GetConfData++ = id;
//            *GetConfData++ = INIT_FILTER_IP_SIZE;
//            *GetConfData++ = ConfSitradUdp.InitFilterIp[0];
//            *GetConfData++ = ConfSitradUdp.InitFilterIp[1];
//            *GetConfData++ = ConfSitradUdp.InitFilterIp[2];
//            *GetConfData++ = ConfSitradUdp.InitFilterIp[3];
//            SizeReturn = (uint16_t)(INIT_FILTER_IP_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
//            break;
//
//        case ID_END_IP://                   = 0x15,
//            *GetConfData++ = id;
//            *GetConfData++ = END_FILTER_IP_SIZE;
//            *GetConfData++ = ConfSitradUdp.EndFilterIp[0];
//            *GetConfData++ = ConfSitradUdp.EndFilterIp[1];
//            *GetConfData++ = ConfSitradUdp.EndFilterIp[2];
//            *GetConfData++ = ConfSitradUdp.EndFilterIp[3];
//            SizeReturn = (uint16_t)(END_FILTER_IP_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
//            break;

        case ID_TIME_LOG://                 = 0x16,
            *GetConfData++ = id;
            *GetConfData++ = TIME_OUT_LOG_SIZE;
            uint16_t TempS;
            TempS = DataloggerGetTimeToLog();
            *GetConfData++ = (uint8_t)(TempS  >> 8 ) & 0xFF;
            *GetConfData++ = (uint8_t)(TempS       ) & 0xFF;
            SizeReturn = (uint16_t)(TIME_OUT_LOG_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
            break;

        case ID_LIST_INSTRUMENTS:
            *GetConfData++ = id;
            uint8_t DataTemp[0xFF],
                    SizeTemp = 0;
            SizeTemp = (uint8_t)DataloggerGetListInstrumentsFG(&DataTemp[0]);
            if(!SizeTemp)
            {
                SizeTemp++;
                *GetConfData++ = SizeTemp;
                *GetConfData++ = 0;
            }
            else
            {
                *GetConfData++ = SizeTemp;
                memcpy(GetConfData, &DataTemp[0], SizeTemp);
            }
            SizeReturn = (uint16_t)(SizeTemp + HEADER_CONF_SIZE); //size do comando + 2bytes header
            break;

//        case ID_FLAG_DNS://  0x19  DNS
//            *GetConfData++ = id;
//            *GetConfData++ = FLAG_ONE_BYTE_SIZE;
//            *GetConfData++ = ConfNetBasic.FlagDNS; ///ver isso depois
//            SizeReturn = (uint16_t)(FLAG_ONE_BYTE_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
//            break;
//        case ID_DNS://  0x1A  DNS Primario
//            *GetConfData++ = id;
//            *GetConfData++ = END_DNS_IP_SIZE;
//            *GetConfData++ = (uint8_t)(NetStaticCellular.AddDNS >> 24   )   & 0xFF;
//            *GetConfData++ = (uint8_t)(NetStaticCellular.AddDNS >> 16   )   & 0xFF;
//            *GetConfData++ = (uint8_t)(NetStaticCellular.AddDNS >> 8    )   & 0xFF;
//            *GetConfData++ = (uint8_t)(NetStaticCellular.AddDNS         )   & 0xFF;
//            SizeReturn = (uint16_t)(END_DNS_IP_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
//            break;
//        case ID_DNS2://  0x1B  DNS Secundario
//            *GetConfData++ = id;
//            *GetConfData++ = END_DNS_IP_SIZE;
//            *GetConfData++ = (uint8_t)(NetStaticCellular.AddDNS2 >> 24  )  & 0xFF;
//            *GetConfData++ = (uint8_t)(NetStaticCellular.AddDNS2 >> 16  )  & 0xFF;
//            *GetConfData++ = (uint8_t)(NetStaticCellular.AddDNS2 >> 8   )  & 0xFF;
//            *GetConfData++ = (uint8_t)(NetStaticCellular.AddDNS2        )  & 0xFF;
//            SizeReturn = (uint16_t)(END_DNS_IP_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
//            break;
//        case ID_HIDDEN_NET:
//            *GetConfData++ = id;
//            *GetConfData++ = FLAG_ONE_BYTE_SIZE;
//            *GetConfData++ = ConfNetBasic.HiddenNet;
//            SizeReturn = (uint16_t)(FLAG_ONE_BYTE_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
//            break;
        case ID_PASSWORD:
#ifdef DEBUG_USB
			printf("\nID_PASSWORD\n");
#endif
			*GetConfData++ = id;
			*GetConfData++ = (uint8_t)strlen((const char *)PASSWORD_SIZE);
			strcpy((char *)GetConfData, (const char *)ConfSitradUdp.Password);
			SizeReturn = (uint16_t) (strlen((const char *)ConfSitradUdp.Password) + HEADER_CONF_SIZE); //size do comando + 2bytes header
			break;


		case ID_STATUS_SITRAD:
			*GetConfData++ = id;
			*GetConfData++ = (uint8_t)FLAG_T_CELLULAR_SIZE;
			*GetConfData++ = (uint8_t)CommData->FlagConnectGPRS;
			SizeReturn = (uint16_t) (FLAG_T_CELLULAR_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
			break;

		case ID_BASE_STATION:
#ifdef DEBUG_USB
			printf("\nID_BASE_STATION\n");
			printf("-> %s\n",  (const char *)NetCellularConf.BaseStation);
#endif
			*GetConfData++ = id;
			*GetConfData++ = (uint8_t)strlen((const char *)NetCellularConf.BaseStation);
			strcpy((char *)GetConfData, (const char *)NetCellularConf.BaseStation);
			SizeReturn = (uint16_t) (strlen((const char *)NetCellularConf.BaseStation) + HEADER_CONF_SIZE); //size do comando + 2bytes header
			break;

		case ID_STATUS_NET_CELL:
			*GetConfData++ = id;
			*GetConfData++ = (uint8_t)FLAG_T_CELLULAR_SIZE;
			*GetConfData++ = (uint8_t)CommData->CellularRssid;//NetCellularConf.StatusNetCell;
			SizeReturn = (uint16_t) (FLAG_T_CELLULAR_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
			break;

		case ID_VERSION_EXS82:
#ifdef DEBUG_USB
			printf("\nID_VERSION_EXS82\n");
#endif
			*GetConfData++ = id;
			*GetConfData++ = (uint8_t)strlen((const char *)NetCellularConf.VersionEXS82);
			strcpy((char *)GetConfData, (const char *)NetCellularConf.VersionEXS82);
			SizeReturn = (uint16_t) (strlen((const char *)NetCellularConf.VersionEXS82) + HEADER_CONF_SIZE); //size do comando + 2bytes header
			break;

		case ID_APN_MODE:
#ifdef DEBUG_USB
			printf("\nID_APN_MODE\n");
#endif
			*GetConfData++ = id;
			*GetConfData++ = (uint8_t)1;
			*GetConfData++ = (uint8_t)NetCellularConf.ApnMode;
			SizeReturn = (uint16_t) (1 + HEADER_CONF_SIZE); //size do comando + 2bytes header
			break;

		case ID_APN_ACTUAL:
#ifdef DEBUG_USB
			printf("\nID_APN_ACTUAL\n");
#endif
			*GetConfData++ = id;
			*GetConfData++ = (uint8_t)strlen((const char *)NetCellularConf.ApnActual);
			strcpy((char *)GetConfData, (const char *)NetCellularConf.ApnActual);
			SizeReturn = (uint16_t) (strlen((const char *)NetCellularConf.ApnActual) + HEADER_CONF_SIZE); //size do comando + 2bytes header
			break;

		case ID_APN_DEFAULT:
#ifdef DEBUG_USB
			printf("\nID_APN_DEFAULT\n");
#endif
			*GetConfData++ = id;
			*GetConfData++ = (uint8_t)strlen((const char *)NetCellularConf.ApnDefault);
			strcpy((char *)GetConfData, (const char *)NetCellularConf.ApnDefault);
			SizeReturn = (uint16_t) (strlen((const char *)NetCellularConf.ApnDefault) + HEADER_CONF_SIZE); //size do comando + 2bytes header
			break;

		case ID_APN_ALTERNATIVE:
#ifdef DEBUG_USB
			printf("\nID_APN_ALTERNATIVE\n");
#endif
			*GetConfData++ = id;
			*GetConfData++ = (uint8_t)strlen((const char *)NetCellularConf.ApnAlternative);
			strcpy((char *)GetConfData, (const char *)NetCellularConf.ApnAlternative);
			SizeReturn = (uint16_t) (strlen((const char *)NetCellularConf.ApnAlternative) + HEADER_CONF_SIZE); //size do comando + 2bytes header
			break;

		case ID_APN_MANUAL:
#ifdef DEBUG_USB
			printf("\nID_APN_MANUAL\n");
#endif
			*GetConfData++ = id;
			*GetConfData++ = (uint8_t)strlen((const char *)NetCellularConf.ApnManual);
			strcpy((char *)GetConfData, (const char *)NetCellularConf.ApnManual);
			SizeReturn = (uint16_t) (strlen((const char *)NetCellularConf.ApnManual) + HEADER_CONF_SIZE); //size do comando + 2bytes header
			break;

		case ID_APN_USER:
#ifdef DEBUG_USB
			printf("\nID_APN_USER\n");
#endif
			*GetConfData++ = id;
			*GetConfData++ = (uint8_t)strlen((const char *)NetCellularConf.ApnUser);
			strcpy((char *)GetConfData, (const char *)NetCellularConf.ApnUser);
			SizeReturn = (uint16_t) (strlen((const char *)NetCellularConf.ApnUser) + HEADER_CONF_SIZE); //size do comando + 2bytes header
			break;

		case ID_APN_PASSWORD:
#ifdef DEBUG_USB
			printf("\nID_APN_PASSWORD\n");
#endif
			*GetConfData++ = id;
			*GetConfData++ = (uint8_t)strlen((const char *)NetCellularConf.ApnPassword);
			strcpy((char *)GetConfData, (const char *)NetCellularConf.ApnPassword);
			SizeReturn = (uint16_t) (strlen((const char *)NetCellularConf.ApnPassword) + HEADER_CONF_SIZE); //size do comando + 2bytes header
			break;

		case ID_SITRAD:
#ifdef DEBUG_USB
			printf("\nID_SITRAD: %s\n",(const char *)NetCellularConf.IdSitrad);
#endif
			*GetConfData++ = id;
			*GetConfData++ = (uint8_t)strlen((const char *)NetCellularConf.IdSitrad);
			strcpy((char *)GetConfData, (const char *)NetCellularConf.IdSitrad);
			SizeReturn = (uint16_t) (strlen((const char *)NetCellularConf.IdSitrad) + HEADER_CONF_SIZE); //size do comando + 2bytes header
			break;

		case ID_IMEI:
#ifdef DEBUG_USB
			printf("\nID_IMEI\n");
#endif
			*GetConfData++ = id;
			*GetConfData++ = (uint8_t)strlen((const char *)NetCellularConf.Imei);
			strcpy((char *)GetConfData, (const char *)NetCellularConf.Imei);
			SizeReturn = (uint16_t) (strlen((const char *)NetCellularConf.Imei) + HEADER_CONF_SIZE); //size do comando + 2bytes header
			break;
        default:
            SizeReturn = 0;
            break;

	}

    return SizeReturn;
}


void SetConfSitradUSB(uint8_t * SetConfData, _COMM_CONTROL *CommData, uint16_t SizeRequest) //
{
	_UDP_CONF            ConfSitradUdp;
	_NET_STATIC_MODE     NetStatic;
	_CONF_NET_BASIC      ConfNetBasic;
	_NET_CELLULAR_CONF   NetCellularConf;
	_USB_CONF			 UsbConf;

    SSP_PARAMETER_NOT_USED(CommData);

    int_storage_read((uint8_t *)&ConfSitradUdp, sizeof(_UDP_CONF), CONF_UDP_SITRAD);
    int_storage_read((uint8_t *)&NetStatic, sizeof(_NET_STATIC_MODE), CONF_CELLULAR_STATIC_IP);
    int_storage_read((uint8_t *)&ConfNetBasic, sizeof(_CONF_NET_BASIC), CONF_NET_BASIC);
    int_storage_read((uint8_t *)&NetCellularConf, sizeof(_NET_CELLULAR_CONF), CONF_CELLULAR);


    uint16_t    SizeCmd = 0,
                SizeCmdHeader,
                TimeDatalogger = 0;
    uint8_t     Id,
                Temp,
                BufInstruments[MAX_NUM_INSTRUMENTS];

    for(SizeCmdHeader = 0; SizeCmdHeader < SizeRequest; )
    {
        Id = *SetConfData++;
        SizeCmd = *SetConfData++;
#ifdef DEBUG_USB
        printf("ID: %x Size:%x \n", Id, SizeCmd);
#endif
        switch(Id)
        {

            case ID_IP_ADDRESS://    - 0x02  Endereço IP
                NetStatic.AddIP = 0;
                NetStatic.AddIP =  (uint32_t)(NetStatic.AddIP | (uint32_t)(*SetConfData++ << 24));
                NetStatic.AddIP =  (uint32_t)(NetStatic.AddIP | (uint32_t)(*SetConfData++ << 16));
                NetStatic.AddIP =  (uint32_t)(NetStatic.AddIP | (uint32_t)(*SetConfData++ << 8));
                NetStatic.AddIP =  (uint32_t)(NetStatic.AddIP | *SetConfData++);
#ifdef DEBUG_USB
                printf("ID_IP_ADDRESS = %d.%d.%d.%d\r\n", (int)(NetStatic.AddIP>>24 & 0xFF),
                                                                    (int)(NetStatic.AddIP>>16 & 0xFF),
                                                                    (int)(NetStatic.AddIP>>8 &  0xFF),
                                                                    (int)(NetStatic.AddIP>>0 &  0xFF));
#endif
                break;
//        case ID_MASK_ADDRESS://   - 0x03  Máscara de Rede,
//            NetStatic.AddMask = 0;
//            NetStatic.AddMask =  (uint32_t)(NetStatic.AddMask | (uint32_t)(*SetConfData++ << 24));
//            NetStatic.AddMask =  (uint32_t)(NetStatic.AddMask | (uint32_t)(*SetConfData++ << 16));
//            NetStatic.AddMask =  (uint32_t)(NetStatic.AddMask | (uint32_t)(*SetConfData++ << 8));
//            NetStatic.AddMask =  (uint32_t)(NetStatic.AddMask | *SetConfData++);
//            break;
//        case ID_GETWAY_ADDRESS://    - 0x04  Gateway
//            NetStatic.AddGw = 0;
//            NetStatic.AddGw =  (uint32_t)(NetStatic.AddGw | (uint32_t)(*SetConfData++ << 24));
//            NetStatic.AddGw =  (uint32_t)(NetStatic.AddGw | (uint32_t)(*SetConfData++ << 16));
//            NetStatic.AddGw =  (uint32_t)(NetStatic.AddGw | (uint32_t)(*SetConfData++ << 8));
//            NetStatic.AddGw =  (uint32_t)(NetStatic.AddGw | *SetConfData++);
//            break;
        	case ID_MODEL:
        		memset(UsbConf.ModelConv,'\0', MODEL_CONV_SIZE );
        		memcpy((char *)UsbConf.ModelConv, (const char *)SetConfData, SizeCmd);
#ifdef DEBUG_USB
        		printf("MOD: %s\n",UsbConf.ModelConv);
#endif
        		SetConfData = SetConfData + SizeCmd;
        		break;

            case ID_CONVERTER_NAME:
                memset(ConfSitradUdp.ModelName,'\0', MODEL_NAME_SIZE );
                if(SizeCmd > MODEL_NAME_SIZE) SizeCmd = MODEL_NAME_SIZE-1;
                memcpy((char *)ConfSitradUdp.ModelName, (const char *)SetConfData, SizeCmd);
#ifdef DEBUG_USB
            	printf("NAME: %s\n",ConfSitradUdp.ModelName);
#endif
                SetConfData = SetConfData + SizeCmd;
                break;

            case ID_ENABLE_PASSWORD:
            	ConfSitradUdp.EnablePassword =  *SetConfData++;
                break;

            case ID_VERSION:
            	memset(UsbConf.Version,'\0', VERSION_SIZE );
            	memcpy((char *)UsbConf.Version, (const char *)SetConfData, SizeCmd);
            	SetConfData = SetConfData + SizeCmd;
            	break;

            case ID_SETUP_PORT://    - 0x0C  Porta de Configuracao
                ConfSitradUdp.ConfigurationPort[0] = (uint8_t)*SetConfData++;
                ConfSitradUdp.ConfigurationPort[1] = (uint8_t)*SetConfData++;
#ifdef DEBUG_USB
            	printf("\nSETUP_PORT: %d\n", ((ConfSitradUdp.ConfigurationPort[0] << 8)| (ConfSitradUdp.ConfigurationPort[1])));
#endif
                break;
            case ID_COMUNITATION_PORT://    - 0x0E  Porta de Comunicacao
                ConfSitradUdp.ComunicationPort[0] = (uint8_t)*SetConfData++;
                ConfSitradUdp.ComunicationPort[1] = (uint8_t)*SetConfData++;
#ifdef DEBUG_USB
                printf("\nCOMUNICATION_PORT: %d\n",((ConfSitradUdp.ComunicationPort[0] << 8)| (ConfSitradUdp.ComunicationPort[1])));
#endif
                break;

            case ID_BAUD_RATE://
            	ConfSitradUdp.BaudRate485[3] = (uint8_t)*SetConfData++;
            	ConfSitradUdp.BaudRate485[2] = (uint8_t)*SetConfData++;
            	ConfSitradUdp.BaudRate485[1] = (uint8_t)*SetConfData++;
            	ConfSitradUdp.BaudRate485[0] = (uint8_t)*SetConfData++;
                break;

            case ID_TIME_OUT_RS485:
            	ConfSitradUdp.TimeOutRs485[1] = (uint8_t)*SetConfData++;
            	ConfSitradUdp.TimeOutRs485[0] = (uint8_t)*SetConfData++;
				break;
			//TODO: checar necessidade
//            case ID_IP_MODE://                  = 0x11,
//                ConfSitradUdp.IpMode = (uint8_t)*SetConfData++;
//                ConfNetBasic.AddrMode = ConfSitradUdp.IpMode;
//                break;
            case ID_PASSWORD:
				memset(ConfSitradUdp.Password, 0 , PASSWORD_SIZE );
				if(SizeCmd > PASSWORD_SIZE) SizeCmd = PASSWORD_SIZE-1;
				memcpy((char *)ConfNetBasic.PasswordDevice, (const char *)SetConfData, (uint8_t)SizeCmd);
				SetConfData = SetConfData + SizeCmd;
                break;

//            case ID_ENABLE_IP_FILTER://         = 0x13,
//                ConfSitradUdp.EnableIpFilter = *SetConfData++;
//                break;
//            case ID_INIT_IP://                  = 0x14,
//                ConfSitradUdp.InitFilterIp[0] =  (uint8_t)*SetConfData++;
//                ConfSitradUdp.InitFilterIp[1] =  (uint8_t)*SetConfData++;
//                ConfSitradUdp.InitFilterIp[2] =  (uint8_t)*SetConfData++;
//                ConfSitradUdp.InitFilterIp[3] =  (uint8_t)*SetConfData++;
//                break;
//            case ID_END_IP://                   = 0x15,
//                ConfSitradUdp.EndFilterIp[0] =  (uint8_t)*SetConfData++;
//                ConfSitradUdp.EndFilterIp[1] =  (uint8_t)*SetConfData++;
//                ConfSitradUdp.EndFilterIp[2] =  (uint8_t)*SetConfData++;
//                ConfSitradUdp.EndFilterIp[3] =  (uint8_t)*SetConfData++;
//                break;

            case ID_TIME_LOG:
                TimeDatalogger = (uint16_t)(*SetConfData++ << 8);
                TimeDatalogger = (uint16_t)(TimeDatalogger | (uint8_t)*SetConfData++);
                if(TimeDatalogger < 15) //tempo minimo para log. em segundos
                    TimeDatalogger = 15;
                SetLogInterval(TimeDatalogger);
                break;

            case ID_LIST_INSTRUMENTS:
				Temp = (uint8_t)SizeCmd; //numero de instrumentos para log

				if(Temp > MAX_NUM_INSTRUMENTS)
					Temp = MAX_NUM_INSTRUMENTS;

				memset(&BufInstruments[0], 0xFF, MAX_NUM_INSTRUMENTS);

				for(uint8_t i=0; i < Temp; i++)
				{
					BufInstruments[i] = *SetConfData++;
				}

				DataloggerFlashSetCmdSitrad(0, &BufInstruments[0], Temp);
				break;

//            case ID_FLAG_DNS://  0x19  DNS? FLAG
//                ConfNetBasic.FlagDNS = *SetConfData++;
//                break;
//            case ID_DNS://  0x1A  DNS PRIMARIO
//                NetStatic.AddDNS = 0;
//                NetStatic.AddDNS =  (uint32_t)(NetStatic.AddDNS | (uint32_t)(*SetConfData++ << 24));
//                NetStatic.AddDNS =  (uint32_t)(NetStatic.AddDNS | (uint32_t)(*SetConfData++ << 16));
//                NetStatic.AddDNS =  (uint32_t)(NetStatic.AddDNS | (uint32_t)(*SetConfData++ << 8));
//                NetStatic.AddDNS =  (uint32_t)(NetStatic.AddDNS | *SetConfData++);
//                break;
//            case ID_DNS2://  0x1B  DNS2 SECUNDARIO
//                NetStatic.AddDNS2 = 0;
//                NetStatic.AddDNS2 =  (uint32_t)(NetStatic.AddDNS2 | (uint32_t)(*SetConfData++ << 24));
//                NetStatic.AddDNS2 =  (uint32_t)(NetStatic.AddDNS2 | (uint32_t)(*SetConfData++ << 16));
//                NetStatic.AddDNS2 =  (uint32_t)(NetStatic.AddDNS2 | (uint32_t)(*SetConfData++ << 8));
//                NetStatic.AddDNS2 =  (uint32_t)(NetStatic.AddDNS2 | *SetConfData++);
//                break;
//            case ID_HIDDEN_NET:
//                ConfNetBasic.HiddenNet = *SetConfData++; //(0-NaoOculta 1-Oculta)
//                break;

            case ID_STATUS_SITRAD:
            	//ConfSitradUdp.StatusSitrad =  *SetConfData++;
            	break;

            case ID_BASE_STATION:
            	memset(NetCellularConf.BaseStation,'\0', BASE_STATION_SIZE );
				memcpy((char *)NetCellularConf.BaseStation, (const char *)SetConfData, SizeCmd);
				SetConfData = SetConfData + SizeCmd;
				break;

            case ID_STATUS_NET_CELL:
            	NetCellularConf.StatusNetCell =  *SetConfData++;
            	break;

            case ID_VERSION_EXS82:
            	memset(NetCellularConf.VersionEXS82,'\0', VERSION_EXS82_SIZE );
				memcpy((char *)NetCellularConf.VersionEXS82, (const char *)SetConfData, SizeCmd);
				SetConfData = SetConfData + SizeCmd;
				break;

            case ID_APN_MODE:
            	NetCellularConf.ApnMode =  *SetConfData++;
            	break;

            case ID_APN_ACTUAL:
            	memset(NetCellularConf.ApnActual,'\0', APN_ACTUAL_SIZE );
            	if(SizeCmd > APN_ACTUAL_SIZE) SizeCmd = APN_ACTUAL_SIZE-1;
				memcpy((char *)NetCellularConf.ApnActual, (const char *)SetConfData, SizeCmd);
#ifdef DEBUG_USB
				printf("APN AT: %s\n",NetCellularConf.ApnActual);
#endif
				SetConfData = SetConfData + SizeCmd;
				break;

            case ID_APN_DEFAULT:
            	memset(NetCellularConf.ApnDefault,'\0', APN_DEFAULT_SIZE );
            	if(SizeCmd > APN_DEFAULT_SIZE) SizeCmd = APN_DEFAULT_SIZE-1;
				memcpy((char *)NetCellularConf.ApnDefault, (const char *)SetConfData, SizeCmd);
#ifdef DEBUG_USB
				 printf("APN PAD: %s\n",NetCellularConf.ApnDefault);
#endif

				 memset(NetCellularConf.ApnActual,'\0', APN_ACTUAL_SIZE );
				 memcpy((char *)NetCellularConf.ApnActual, (const char *)SetConfData, SizeCmd);
				 CommData->NetCellularConf.ValCod = FLASH_NUMBER_MAGIC;
				 NetCellularConf.ValCod = FLASH_NUMBER_MAGIC;

				 SetConfData = SetConfData + SizeCmd;
				break;

            case ID_APN_ALTERNATIVE:
            	memset(NetCellularConf.ApnAlternative,'\0', APN_ALTERNATIVE_SIZE );
            	if(SizeCmd > APN_ALTERNATIVE_SIZE) SizeCmd = APN_ALTERNATIVE_SIZE-1;
            	memcpy((char *)NetCellularConf.ApnAlternative, (const char *)SetConfData, SizeCmd);
#ifdef DEBUG_USB
            	 printf("APN alt: %s\n",NetCellularConf.ApnAlternative);
#endif
				SetConfData = SetConfData + SizeCmd;
				break;

            case ID_APN_MANUAL:
            	memset(NetCellularConf.ApnManual,'\0', APN_MANUAL_SIZE );
            	if(SizeCmd > APN_MANUAL_SIZE) SizeCmd = APN_MANUAL_SIZE-1;
				memcpy((char *)NetCellularConf.ApnManual, (const char *)SetConfData, SizeCmd);
#ifdef DEBUG_USB
				 printf("APN man: %s\n",NetCellularConf.ApnManual);
#endif
				SetConfData = SetConfData + SizeCmd;
				break;

            case ID_APN_USER:
            	memset(NetCellularConf.ApnUser,'\0', APN_USER_SIZE );
            	if(SizeCmd > APN_USER_SIZE) SizeCmd = APN_USER_SIZE-1;
				memcpy((char *)NetCellularConf.ApnUser, (const char *)SetConfData, SizeCmd);
				SetConfData = SetConfData + SizeCmd;
				break;

            case ID_APN_PASSWORD:
            	memset(NetCellularConf.ApnPassword,'\0', APN_PASSWORD_SIZE );
            	if(SizeCmd > APN_PASSWORD_SIZE) SizeCmd = APN_PASSWORD_SIZE-1;
				memcpy((char *)NetCellularConf.ApnPassword, (const char *)SetConfData, SizeCmd);
				SetConfData = SetConfData + SizeCmd;
				break;

            case ID_SITRAD:
            	memset(NetCellularConf.IdSitrad,'\0', ID_SITRAD_SIZE );
				memcpy((char *)NetCellularConf.IdSitrad, (const char *)SetConfData, SizeCmd);
				SetConfData = SetConfData + SizeCmd;
				break;

            case ID_IMEI:
            	memset(NetCellularConf.Imei,'\0', IMEI_SIZE );
				memcpy((char *)NetCellularConf.Imei, (const char *)SetConfData, SizeCmd);
				SetConfData = SetConfData + SizeCmd;
				break;

            default:
                //SizeReturn = 0;
                break;
        }

        SizeCmdHeader = (uint16_t)(SizeCmdHeader + (uint16_t)(SizeCmd + HEADER_CONF_SIZE));

        if(     (Id == ID_MODEL)            ||
                (Id == ID_CONVERTER_NAME)   ||
                (Id == ID_ENABLE_PASSWORD)  ||
                (Id == ID_VERSION)          ||
				(Id == ID_IP_ADDRESS)		||
				(Id == ID_SETUP_PORT)		||
				(Id == ID_COMUNITATION_PORT)||
                (Id == ID_BAUD_RATE)        ||
                (Id == ID_TIME_OUT_RS485)   ||
                (Id == ID_PASSWORD)         ||
                (Id == ID_STATUS_SITRAD)    ||
                (Id == ID_BASE_STATION)     ||
                (Id == ID_STATUS_NET_CELL)  ||
				(Id == ID_VERSION_EXS82)    ||
				(Id == ID_APN_MODE)         ||
				(Id == ID_APN_ACTUAL)       ||
				(Id == ID_APN_DEFAULT)      ||
				(Id == ID_APN_ALTERNATIVE)  ||
				(Id == ID_APN_MANUAL)       ||
				(Id == ID_APN_USER)         ||
				(Id == ID_APN_PASSWORD)     ||
				(Id == ID_SITRAD)           ||
                (Id == ID_IMEI))
        {
        	ConfSitradUdp.ConfDefault = FLASH_NUMBER_CUSTON;
        }

    }
    int_storage_write((uint8_t *)&ConfSitradUdp,    sizeof(_UDP_CONF),          CONF_UDP_SITRAD);
    int_storage_write((uint8_t *)&NetStatic,        sizeof(_NET_STATIC_MODE),   CONF_CELLULAR_STATIC_IP);
    int_storage_write((uint8_t *)&ConfNetBasic,     sizeof(_CONF_NET_BASIC),    CONF_NET_BASIC);
    int_storage_write((uint8_t *)&NetCellularConf,  sizeof(_NET_CELLULAR_CONF), CONF_CELLULAR);
}


uint16_t LoginDataResponse(_COMM_CONTROL *CommData, uint16_t RandomNumber)
{
    uint16_t Size = 0;
    uint8_t PasswordReceiv[PASSWORD_SIZE];//8bytes
    ClearTxBuffer(CommData);

    memcpy(&PasswordReceiv[0],&CommData->Buffer_Rx[5],CommData->Buffer_Rx[4]);
    //-------------------Payload-----------------------
    CommData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber >> 0x08);
    CommData->Buffer_Tx[Size++] = (uint8_t)(RandomNumber ^ 0xFF);
    CommData->Buffer_Tx[Size++] = ( CommData->Buffer_Rx[2] | 0x80 );
    CommData->Buffer_Tx[Size++] = 0x00;
    CommData->Buffer_Tx[Size++] = 0x01;

    if(memcmp(PasswordReceiv,CommData->ConfNetBasic.PasswordDevice, PASSWORD_SIZE) == 0){
    	CommData->Buffer_Tx[Size++] = true;
//    	CommData->ValidAccessUSB = true;
    }else
    	CommData->Buffer_Tx[Size++] = false;
//    	CommData->ValidAccessUSB = false;

//    CommData->Buffer_Tx[Size++] = CommData->ValidAccessUSB;

	return Size;
}


uint16_t AESEnc (uint8_t * PMessage, uint8_t *EncMessage, uint16_t InputOctets)
{
    uint32_t  MessageLen  = 0;

    if (( EncMessage == NULL ) || ( InputOctets <= 0 ))
    {
       return 0;
    }

    if (( InputOctets % 16U ) != 0 )
    {
       return 0;
    }

    MessageLen = (uint32_t)(InputOctets / 4U);

    g_sce_aes_0.p_api->open(g_sce_aes_0.p_ctrl, g_sce_aes_0.p_cfg); //Open AES_0

    g_sce_aes_0.p_api->encrypt(g_sce_aes_0.p_ctrl, (uint32_t *)aes_key2, (uint32_t *)IVc2, MessageLen, (uint32_t *)PMessage, (uint32_t *)EncMessage);

    // Close AES driver
    g_sce_aes_0.p_api->close(g_sce_aes_0.p_ctrl);

    return InputOctets;
}


uint16_t AESDec (uint8_t * PMessage, uint8_t *DecMessage, uint16_t InputOctets)
{
   //uint16_t NumBlocks;
   uint32_t  MessageLen  = 0;

   if (( DecMessage == NULL ) || ( InputOctets <= 0 ))
   {
      return 0;
   }

   if (( InputOctets % 16U ) != 0 )
   {
      return 0;
   }

   MessageLen = (uint32_t)(InputOctets / 4U);

   g_sce_aes_0.p_api->decrypt(g_sce_aes_0.p_ctrl, (uint32_t *)aes_key2, (uint32_t *)IVc2, MessageLen, (uint32_t *)PMessage, (uint32_t *)DecMessage);//(uint32_t *)tempOut);

   return InputOctets;
}

//------------------------------------------------------------------------------------------
// reseta buffer
//------------------------------------------------------------------------------------------
void ResetRxBuffer(void)
{
	Rx_Cnt = Rx_Lim = Rx_Qtde = 0;

}

//------------------------------------------------------------------------------------------
// Carrega Buffer
//------------------------------------------------------------------------------------------
void LoadRxBuffer(uint8_t *BufferInput,_COMM_CONTROL *CommData, uint16_t Len)
{
	uint32_t counter = 0;
	memcpy(&CommData->BufferInfoConf[Rx_Cnt], &BufferInput[counter], Len);
	Rx_Cnt = counter += Len;

	if(!CommData->FlagUpdateFWRun){
#ifdef DEBUG_USB
//	printf("\nBUFFER COMPLETO(%d): ",Rx_Cnt);
//	for(uint32_t i = 0; i <= (uint32_t)CommData->PayloadLen; i++){
//	if(i==0)printf("\n%x",CommData->BufferInfoConf[i]);
//			else if(i==Rx_Qtde)printf(" %x\n",CommData->BufferInfoConf[i]);
//			else printf(" %x",CommData->BufferInfoConf[i]);}
#endif
	}
}

void ClearRxBuffer(_COMM_CONTROL *CommData)
{
	memset(&CommData->Buffer_Rx[0], 0, sizeof(CommData->Buffer_Rx));
}

void ClearTxBuffer(_COMM_CONTROL *CommData)
{
	memset(&CommData->Buffer_Tx[0], 0, sizeof(CommData->Buffer_Tx));
}

void StatusUSB(_STATUS_FLAGS *StatusFlagsX)
{


	if(StatusFlagsX->FlagEreaseDatalogger) {
		CommControl.CommStatesMachine = SM_CLEAN_DATALLOGER;
		tx_event_flags_set (&Cellular_Flags, SM_CLEAN_DATALLOGER, TX_OR);
	}
	if(StatusFlagsX->FlagRestartModule){
		CommControl.CommStatesMachine = SM_RESTART_DEVICE;
		CommControl.CommStatesMachine = COMM_SM_RESTART_DEVICE;
		tx_event_flags_set (&Cellular_Flags, SM_RESTART_DEVICE, TX_OR);
		StatusFlagsX->FlagRestartModule = 0;
							RestartModule();

	}
	if(StatusFlagsX->FlagRestoreFactory){
		CommControl.CommStatesMachine = SM_RESTORE_FACTORY_DEVICE;
		tx_event_flags_set (&Cellular_Flags, SM_RESTORE_FACTORY_DEVICE, TX_OR);
	}
	if(StatusFlagsX->FlagUpdateDateTime){
	    	RtcPost(&CommControl);
			tx_thread_relinquish();

	}


}
