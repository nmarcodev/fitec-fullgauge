/*
 * ConfDefault.c
 *
 *  Created on: 29 de out de 2019
 *      Author: leonardo.ceolin
 */

#include "Includes/ConfDefault.h"
#include "Includes/DataLoggerFlash.h"
#include "Includes/common_util.h"
#include "Includes/ApplicationCellular.h"

#if RECORD_DATALOGGER_EVENTS
#include "Includes/Thread_Datalogger_entry.h"
#endif

void CommInitConf(_COMM_CONTROL *CommData);
bool CommFAC(bool BitFac, bool SetTGetF);
bool ConfFactory(_COMM_CONTROL *CommData);
/*
 * Retorna Status de FAC
 */
bool CommFAC(bool BitFac, bool SetTGetF)
{
    static bool FlagsEepromRam= false;

    if(SetTGetF)
    {
        FlagsEepromRam = BitFac;
    }

    return FlagsEepromRam;
}

void SetCommFAC(bool bFlagFac)
{

	CommFAC(bFlagFac, true);
}

bool GetCommFAC(void)
{

    return CommFAC(false, false);
}
bool ConfFactory(_COMM_CONTROL *CommData){
	bool ret = false;
	if(strcmp((const char *)APN_TIM,(char *)CommData->NetCellularConf.ApnDefault) == 0)
		ret = true;
	else if(strcmp((const char *)APN_CLARO,(char *)CommData->NetCellularConf.ApnDefault) == 0)
		ret = true;
	else if(strcmp((const char *)APN_VIVO,(char *)CommData->NetCellularConf.ApnDefault) == 0)
		ret = true;
	else if(strcmp((const char *)APN_OI,(char *)CommData->NetCellularConf.ApnDefault) == 0)
		ret = true;
	else
		ret = false;

	return ret;
}
/*
 * Funcao responsavel por inicializar o padrao de fabrica
 */
void CommInitConf(_COMM_CONTROL *CommData)
{

#if RECORD_DATALOGGER_EVENTS
    ssp_err_t result;
    result =
#endif
            int_storage_read((uint8_t *)&CommData->ConfNetBasic, sizeof(_CONF_NET_BASIC), CONF_NET_BASIC);

#if RECORD_DATALOGGER_EVENTS
    if (result != SSP_SUCCESS)
    {
        DataLoggerRegisterEvent(DATALOGGER_EVENT_FLASH_ERROR, result, false);
    }

    DataLoggerRegisterEvent(DATALOGGER_EVENT_NET_INIT,
    		(uint16_t)((CommData->ConfNetBasic.InterfaceType << 1) | (CommData->ConfNetBasic.AddrMode << 0)),
            CommData->ConfNetBasic.CodMagic == FLASH_NUMBER_CONF_BASIC);
#endif

    _NET_STATIC_MODE NetStaticCellular;
    int_storage_read((uint8_t *)&NetStaticCellular, sizeof(_NET_STATIC_MODE), CONF_CELLULAR_STATIC_IP);
    if(CommData->ConfNetBasic.CodMagic == FLASH_NUMBER_CONF_BASIC)
    {
        CommData->ConfNetBasic.InterfaceType = CELLULAR;
        CommData->ConfNetBasic.OldInterfaceType = CELLULAR;
        CommData->ConfNetBasic.FlagDNS = 0;
        CommData->ConfNetBasic.AddrMode = MODE_STATIC;
        CommData->ConfNetBasic.CodMagic = 0;
//        _NET_STATIC_MODE NetStaticCellular;
//        int_storage_read((uint8_t *)&NetStaticCellular, sizeof(_NET_STATIC_MODE), CONF_CELLULAR_STATIC_IP);
        NetStaticCellular.AddDNS = 0;
        NetStaticCellular.AddDNS2 = 0;
        NetStaticCellular.AddIP = 0;
        NetStaticCellular.AddGw = 0;
        NetStaticCellular.AddMask = 0;

        strcpy(CommData->ConfNetBasic.PasswordDevice ,"12345678");

        int_storage_write((uint8_t*)&CommData->ConfNetBasic, sizeof(_CONF_NET_BASIC), CONF_NET_BASIC);
        int_storage_write((uint8_t*)&NetStaticCellular, sizeof(_NET_STATIC_MODE), CONF_CELLULAR_STATIC_IP);
    }

    int_storage_read((uint8_t *)&CommData->IpStaticMode, sizeof(_NET_STATIC_MODE), CONF_CELLULAR_STATIC_IP);

    _UDP_CONF ConfSitradUdp;

    CommData->IpStruct.Tcp_Port_Com  = PORT_TCP_SITRAD;
    CommData->IpStruct.Udp_Port      = PORT_UDP_SITRAD;

    //Le a memoria em busca das configuracoes de UDP
    int_storage_read((uint8_t *)&ConfSitradUdp, sizeof(_UDP_CONF), CONF_UDP_SITRAD);
    if((ConfSitradUdp.ConfDefault == FLASH_NUMBER_CUSTON) || (ConfSitradUdp.ConfDefault == FLASH_NUMBER_DEFAULT))
    {
    	printf("\n CONFSITRAD_DEFAULT \n");
        CommData->IpStruct.Tcp_Port_Com = (uint16_t)((ConfSitradUdp.ComunicationPort[0] << 8) | ConfSitradUdp.ComunicationPort[1]) ;
        CommData->IpStruct.Tcp_Port_Conf = (uint16_t)((ConfSitradUdp.ConfigurationPort[0] << 8)| ConfSitradUdp.ConfigurationPort[1]) ;
        CommData->IpStaticMode.AddIP = IP_ADDRESS(NetStaticCellular.AddIP>>24 & 0xFF,NetStaticCellular.AddIP>>16 & 0xFF, NetStaticCellular.AddIP>>8 & 0xFF,NetStaticCellular.AddIP>>0 & 0xFF);
        //        ConfSitradUdp.ConfDefault = 0;
    }

     memset((char *)CommData->NetCellularConf.ApnActual, 0, sizeof(CommData->NetCellularConf.ApnActual));
     memset((char *)CommData->NetCellularConf.ApnAlternative, 0, sizeof(CommData->NetCellularConf.ApnAlternative));
     memset((char *)CommData->NetCellularConf.ApnDefault, 0, sizeof(CommData->NetCellularConf.ApnDefault));
     memset((char *)CommData->NetCellularConf.ApnManual, 0, sizeof(CommData->NetCellularConf.ApnManual));
     memset((char *)CommData->NetCellularConf.ApnPassword, 0, sizeof(CommData->NetCellularConf.ApnPassword));
     memset((char *)CommData->NetCellularConf.ApnUser, 0, sizeof(CommData->NetCellularConf.ApnUser));
     memset((char *)CommData->NetCellularConf.BaseStation, 0, sizeof(CommData->NetCellularConf.BaseStation));
     memset((char *)CommData->NetCellularConf.IdSitrad, 0, sizeof(CommData->NetCellularConf.IdSitrad));
     memset((char *)CommData->NetCellularConf.Imei, 0, sizeof(CommData->NetCellularConf.Imei));
     memset((char *)CommData->NetCellularConf.VersionEXS82, 0, sizeof(CommData->NetCellularConf.VersionEXS82));

     CommData->NetCellularConf.ApnMode = 0;
     CommData->NetCellularConf.StatusNetCell = 0;
     CommData->NetCellularConf.ValCod = 0;

#if RECORD_DATALOGGER_EVENTS
    result =
#endif
    int_storage_read((uint8_t *)&CommData->NetCellularConf, sizeof(_NET_CELLULAR_CONF), CONF_CELLULAR);
    printf("APN AT(6): %s\n",CommData->NetCellularConf.ApnActual);

#if RECORD_DATALOGGER_EVENTS
    if (result != SSP_SUCCESS)
    {
        DataLoggerRegisterEvent(DATALOGGER_EVENT_FLASH_ERROR, result, false);
    }

    DataLoggerRegisterEvent(DATALOGGER_EVENT_NET_INIT, 0x100, CommData->NetCellularConf.ValCod == FLASH_NUMBER_MAGIC);
#endif

    if(CommData->NetCellularConf.ValCod == 0|| !ConfFactory(CommData))//!= FLASH_NUMBER_MAGIC)
    {
    	printf("\nDIFF =  %d  != %d\n",CommData->NetCellularConf.ValCod, FLASH_NUMBER_MAGIC);
    	memset((char *)CommData->NetCellularConf.ApnActual, 0, sizeof(CommData->NetCellularConf.ApnActual));
        memset((char *)CommData->NetCellularConf.ApnDefault, 0, sizeof(CommData->NetCellularConf.ApnDefault));
        memset((char *)CommData->NetCellularConf.BaseStation, 0, sizeof(CommData->NetCellularConf.BaseStation));

        strcpy((char *)CommData->NetCellularConf.ApnActual, (const char *)APN_VIVO);
        strcpy((char *)CommData->NetCellularConf.ApnDefault, (const char *)APN_VIVO);
        strcpy((char *)CommData->NetCellularConf.BaseStation, (const char *)"VIVO");

        CommData->NetCellularConf.ValCod = FLASH_NUMBER_MAGIC;
        int_storage_write((uint8_t*)&CommData->NetCellularConf, sizeof(_NET_CELLULAR_CONF), CONF_CELLULAR);
    }
    else
    {
#if DEBUG_CELLULAR
    	printf("Leitura da APN na Flash\n");
    	printf("APN: %s\n", CommData->NetCellularConf.ApnActual);
#endif


    	strcpy((char *)CommData->SfProvisionCellular->apn, (char *)CommData->NetCellularConf.ApnActual);

    }
    if(CommData->FlagFAC)
    {
        CommData->ConfNetBasic.InterfaceType = CELLULAR;
        CommData->ConfNetBasic.FlagDNS = 0;
        CommData->ConfNetBasic.AddrMode = MODE_STATIC; //MODE_DHCP
        CommData->ConfNetBasic.HiddenNet = 0;
    }

//    CommData->IpStruct.Tcp_Port_Com  = PORT_TCP_SITRAD; //4000
//    //CommData->IpStruct.Tcp_Port_Conf = PORT_TCP_SITRAD_CONF;
//    CommData->IpStruct.Udp_Port      = PORT_UDP_SITRAD;

//    CommData->IpStaticMode.AddIP = IP_ADDRESS(192, 168, 248, 2); //IP_ADDRESS(192, 168, 2, 127);
//    CommData->IpStaticMode.AddGw = IP_ADDRESS(192, 168, 112, 1);
//    CommData->IpStaticMode.AddMask = IP_ADDRESS(255, 255, 255, 0);

//    printf("INIT-CONF: IP address = %d.%d.%d.%d\r\n", (int)(CommData->IpStaticMode.AddIP>>24 & 0xFF),
//                                                      (int)(CommData->IpStaticMode.AddIP>>16 & 0xFF),
//                                                      (int)(CommData->IpStaticMode.AddIP>>8 &  0xFF),
//                                                      (int)(CommData->IpStaticMode.AddIP>>0 &  0xFF));
//
//    printf("INIT-CONF: Gw address = %d.%d.%d.%d\r\n", (int)(CommData->IpStaticMode.AddGw>>24 & 0xFF),
//                                                      (int)(CommData->IpStaticMode.AddGw>>16 & 0xFF),
//                                                      (int)(CommData->IpStaticMode.AddGw>>8 &  0xFF),
//                                                      (int)(CommData->IpStaticMode.AddGw>>0 &  0xFF));
//
//    printf("INIT-CONF: Mask address = %d.%d.%d.%d\r\n", (int)(CommData->IpStaticMode.AddMask>>24 & 0xFF),
//                                                        (int)(CommData->IpStaticMode.AddMask>>16 & 0xFF),
//                                                        (int)(CommData->IpStaticMode.AddMask>>8 &  0xFF),
//                                                        (int)(CommData->IpStaticMode.AddMask>>0 &  0xFF));
//
//    printf("INIT-CONF: Netstatic ip = %d.%d.%d.%d\r\n", (int)(NetStaticCellular.AddIP>>24 & 0xFF),
//                                                        (int)(NetStaticCellular.AddIP>>16 & 0xFF),
//                                                        (int)(NetStaticCellular.AddIP>>8 &  0xFF),
//                                                        (int)(NetStaticCellular.AddIP>>0 &  0xFF));
//
//    printf("\nINIT-CONF:SETUP_PORT: %d\n", ((ConfSitradUdp.ConfigurationPort[0] << 8)| (ConfSitradUdp.ConfigurationPort[1])));
//
//    printf("\nINIT-CONF:COMUNICATION_PORT: %d\n", ((ConfSitradUdp.ComunicationPort[0] << 8)| (ConfSitradUdp.ComunicationPort[1])));
}
