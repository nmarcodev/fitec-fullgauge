/*
 * Eeprom25AA02E48.c
 *
 *  Created on: 5 de jun de 2019
 *      Author: leonardo.ceolin
 */
#include "Includes/TypeDefs.h"
#include "Includes/Eeprom25AA02E48.h"

bool MACValue(uint8_t *EepromMacAddress, uint8_t Select);
void SetMACValue(uint8_t *EepromMacAddress);
_FG_ERR GetMACValue(uint8_t *EepromMacAddress);

bool MACValue(uint8_t *EepromMacAddress, uint8_t Select)
{
    static uint8_t MacAddress[6];
    static bool Ret = false;

    for(uint8_t i=0; i < 6; i++)
    {
        if(!Select)//put
        {
            MacAddress[i] = EepromMacAddress[i];
            Ret = true; //sinaliza que em algum momento o MacAddress já foi carregado/lido
        }
        else//get
        {
            EepromMacAddress[i] = MacAddress[i];
        }
    }

    return Ret;
}

//Essa funcao escreve o endereco mac na ram.
void SetMACValue(uint8_t *EepromMacAddress)
{
    MACValue(&EepromMacAddress[0], 0U);
}

//Essa funcao le o endereco mac na ram.
_FG_ERR GetMACValue(uint8_t *EepromMacAddress)
{
    if(MACValue(&EepromMacAddress[0], 1U))
    {
        return FG_SUCCESS;
    }

    return FG_ERROR;
}


