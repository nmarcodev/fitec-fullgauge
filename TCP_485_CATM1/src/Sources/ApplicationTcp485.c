/*
 * ApplicationTcp485.c
 *
 *  Created on: 4 de nov de 2019
 *      Author: leonardo.ceolin
 *
 * Modulo principal da aplicacao, junto com ApplicationPayload.c
 *
 * Faz o controle das interfaces de rede, e do estado do equipamento, informando-o MainManagerThread.
 *
 * A configuracao do equipamento esta armazenada na memoria flash interna.
 */

#include "Includes/ApplicationTcp485.h"
#include "Includes/Thread_Monitor_entry.h"
#include "Includes/ApplicationIp.h"
#include "Cellular_Thread.h"
#include "Includes/ApplicationUSB.h"
#include "USB_HID_Device_Thread.h"
#include "Includes/Thread_Datalogger_entry.h"
#include "Includes/common_util.h"
#include "sf_cellular_common_private.h"

#if RECORD_DATALOGGER_EVENTS
#include "includes/Thread_Datalogger_entry.h"
#endif

void StatusSitrad(_STATUS_FLAGS *StatusFlagsX);
void ReadFlashNetDefault(_COMM_CONTROL *CommData);

void InitDhcp(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX);
void IdleInit(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX);
void InitCellular(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX);
void RestartIp(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX);
void DnsIp(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX);
void StatusIp(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX);
void CxAnyFlags(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX);
void NetStateMachine(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX);
void WaitPPP(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX);
void CellularStatusSignalCellular(_COMM_CONTROL *CommData, _MAIN_MANAGER_FLAGS *MainManagerFlag);
_MAIN_MANAGER_FLAGS NetStatusModule(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX);

void CheckConnectivity(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlags);

bool RamStatusSitrad(bool In, bool SetTGetF);
bool GetStatusSitrad(void);
void SetStatusSitrad(bool StatusSitradCon);


void StatusSitrad(_STATUS_FLAGS *StatusFlagsX)
{
    if(StatusFlagsX->StateSitrad != StatusFlagsX->FlagSitradConnected)
    {
        //Envia para a thread DataLoggerFlash a sinalizacao quanto ao status do Sitrad.
        _DATA_LOGGER_DATA FlagSitrad;
        FlagSitrad.OpCod = CONNECTED_SITRAD;
        FlagSitrad.SizeTemp = (uint16_t)StatusFlagsX->FlagSitradConnected;
        tx_queue_send(&Datalogger_Flash, &FlagSitrad, TX_WAIT_FOREVER); //envia para a thread Datalogger

        StatusFlagsX->StateSitrad = StatusFlagsX->FlagSitradConnected;

        SetStatusSitrad(StatusFlagsX->StateSitrad);
    }
}

bool RamStatusSitrad(bool In, bool SetTGetF)
{
    static bool RamStatusSitrad = false;

    if(SetTGetF)
    {
        RamStatusSitrad = In;
    }

    return RamStatusSitrad;
}

bool GetStatusSitrad(void)
{
    return RamStatusSitrad(false, false);
}

void SetStatusSitrad(bool StatusSitradCon)
{
    RamStatusSitrad(StatusSitradCon, true);
}


/**
 * Baseado na configuracao atual do equipmento e do estado, computa as flags para serem passsadas a thread
 * MainManager. Essa funcao tem muita liberdade para ser alterada, devendo manter as seguintes flags:
 * FlagFAC, FlagUpdateFWRun, FlagConnectedSitrad
 *
 * As demais devem ser alteradas ja que o caracteristica de operacao do conversor sera outra.
 * Essas flags funcionam em conjunto com a funcao StatusLed da MainManagerThread.
 *
 * @param CommData
 * @param StatusFlagsX
 * @return
 */
_MAIN_MANAGER_FLAGS NetStatusModule(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX)
{
    _MAIN_MANAGER_FLAGS StatusFlag = 0;

    if(!tx_semaphore_get(&CellularStatus_Semaphore, TX_NO_WAIT))
    {
    	if(CommData->ConfNetBasic.InterfaceType == CELLULAR)
    	{
    		StatusFlag |= MM_CELL_TYPE;

    		if(StatusFlagsX->FlagNetConnected)
			{
				StatusFlag |= MM_CONNECTED;
				CellularGetStatusIp(CommData);
				CellularStatusSignalCellular(CommData, &StatusFlag);//carrega_flag do sinal do Cellular
			}
			else
			{
				StatusFlag |= MM_DISCONNECTED;
				if((!CommData->FlagConnectSitrad)&& CommData->CellularRssid == 0) CommData->CellularRssid = 99;
				if(StatusFlagsX->FlagNetConnecting)
				{
					StatusFlag |= MM_NET_CONNECTING;
				}
			}

    	}


        if(StatusFlagsX->StateSitrad)
        {
            StatusFlag |= MM_CONNECTED_SITRAD;
            CommData->FlagConnectSitrad = true;
            printf("MM_CONNECTED_SITRAD\n");
        }
        else
        {
            StatusFlag |= MM_DISCONNECTED_SITRAD;
            CommData->FlagConnectSitrad = false;
        }

        /*Erros e Status Majoritarios*/
        if(StatusFlagsX->FlagErrorIp)
        {
            StatusFlag |= MM_ERRO_IP;
            printf("MM_ERRO_IP\n");
        }

        if(StatusFlagsX->FlagErrorPassWrd)
        {
            StatusFlag |= MM_ERRO_PWD;
            printf("MM_ERRO_PWD\n");
        }

        if(CommData->FlagUpdateFWRun)
        {
            StatusFlag |= MM_STATUS_UPDATE_FW;
        }

        if(CommData->FlagFAC)
        {
            StatusFlag |= MM_FAC_MODE;
        }
    }
    return StatusFlag;
}

void NetStateMachine(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX)
{
    //static uint8_t  NumAttmptsPwd = 0;
	uint8_t status;

    switch(CommData->CommStatesMachine)
    {
        case COMM_SM_IDLE:
        {
            IdleInit(CommData, StatusFlagsX);
        }break;

        case COMM_SM_INIT_CELL:
        {
        	//printf("COMM_SM_INIT_CELL\r\n");
            InitCellular(CommData, StatusFlagsX);
        }break;

        case COMM_SM_WAIT_PPP:
        {
            WaitPPP(CommData, StatusFlagsX);
        }break;

        case COMM_SM_DNS:
        {
            DnsIp(CommData, StatusFlagsX);
        }break;

        case COMM_SM_UPDATE_SERVER:
        {
        	status = UpdateIPServer(CommData);
        }break;

        case COMM_SM_DHCP:
        {
            InitDhcp(CommData, StatusFlagsX);
        }break;

        case COMM_SM_STATUS_IP:
        {
            StatusIp(CommData, StatusFlagsX);
        }break;

        case COMM_SM_RESTART_IP:
        {
#ifdef DEBUG_CELLULAR
        	printf("COMM_SM_RESTART_IP\r\n");
#endif
            RestartIp(CommData, StatusFlagsX);
        }break;

        case COMM_SM_RESTART_DEVICE:
        {
#ifdef DEBUG_CELLULAR
        	printf("COMM_SM_RESTART_DEVICE\r\n");
#endif
            RestartModule();
        }break;

        case COMM_SM_RESTORE_FACTORY_DEVICE:
        {
#ifdef DEBUG_CELLULAR
        	printf("COMM_SM_RESTORE_FACTORY_DEVICE\r\n");
#endif
        	ThreadMainManagerSetFlag(MM_RESTORE_FACTORY);
        }break;

        case COMM_SM_WAIT_RESET:
        {
        	//printf("COMM_SM_WAIT_RESET");
            tx_thread_sleep(TIME_200MS);
        }break;

        case COMM_SM_DONE:
        {
#ifdef DEBUG_CELLULAR
        	printf("COMM_SM_DONE\n");
#endif
            CheckConnectivity(CommData, StatusFlagsX);
            CommData->CommStatesMachine = COMM_SM_WAIT_QUEUE;
            StartTimerMain(); //Inicializa Timer Main.
        }break;

        case COMM_SM_WAIT_QUEUE:
        {
#ifdef DEBUG_CELLULAR
        	//printf("COMM_SM_WAIT_QUEUE\n");
#endif
            if(!tx_event_flags_get(&Cellular_Flags, CELL_FLAG_UDPTCP_ALL, TX_OR_CLEAR, &CommData->ActualFlags, 5))
            {
#ifdef DEBUG_CELLULAR
        	//printf("CxAnyFlags\n");
#endif
                CxAnyFlags(CommData, StatusFlagsX);
            }
            else
            {
#ifdef DEBUG_CELLULAR
        	//printf("Check Connectivity COMM_SM_WAIT_QUEUE\n");
#endif
                CheckConnectivity(CommData, StatusFlagsX);
            }

        }break;

        default:
        {
            CommData->CommStatesMachine = COMM_SM_IDLE;
        }break;
    }
}


void CheckConnectivity(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlags)
{
	if(CommData->ConfNetBasic.InterfaceType == CELLULAR)
    {
        if(!StatusFlags->FlagSitradConnected && !CommData->FlagUpdateFWRun)
        {
            CellularCheckConnectivity(CommData, StatusFlags);
        }
        else
        {// se conectado ao sitrad, remove erro de ip, que pode ocorrer no modo ip estatico
            StatusFlags->FlagErrorIp = false;
        }
    }
}

void IdleInit(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX)
{
    StopTimerMain(); //Para o timerMain.

    StatusFlagsX->FlagNetConnected    = false;
    StatusFlagsX->FlagNetConnecting   = false;
    StatusFlagsX->FlagSitradConnected = false;
    CommData->ValidAccess             = false;
//    CommData->ValidAccessUSB          = false;

    packet_pool_init0();

    CommData->CommStatesMachine = COMM_SM_INIT_CELL;

    tx_event_flags_set (&Cellular_Flags, CELL_FLAGS_NONE, TX_AND);
}


void InitCellular(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX)
{
    ULONG IpActualStatus = 0;
    UINT status;

    CommData->NxIP = &g_ip_Cell;
    CommData->NxPPP = &g_nx_ppp0;
    CommData->NxDHCP = &g_dhcp_Cell;

    status = nx_ip_interface_attach(&g_ip_Cell, "Cellular", IP_ADDRESS(0,0,0,0), IP_ADDRESS(0,0,0,0), g_sf_el_nx_cellular);
	/* NX_UNHANDLED_COMMAND is returned as the status if IPV6 support is enabled in NetX, (FEATURE_NX_IPV6 is defined) */
	/* and NX_DISABLE_ICMPV6_ROUTER_SOLICITATION is not defined, as multicast command is not supported by cellular NSAL */
	if ((NX_SUCCESS != status) && (NX_UNHANDLED_COMMAND != status))
	{
		printf("Erro printf nx_ip_interface_attach!!\n");
	}

    if(nx_ip_interface_status_check (&g_ip_Cell, 0, NX_IP_INITIALIZE_DONE, &IpActualStatus, 500))
    {

#if RECORD_DATALOGGER_EVENTS
        DataLoggerRegisterEvent(DATALOGGER_EVENT_CELLULAR_INIT, 0, false);
#endif

#if DEBUG_CELLULAR
        printf("IP interface status check!\n");
#endif
      ip_init_Cell();

#if RECORD_DATALOGGER_EVENTS
        DataLoggerRegisterEvent(DATALOGGER_EVENT_CELLULAR_INIT, 0, true);
#endif

      tx_thread_sleep(50);
    }

    CommData->CommStatesMachine = COMM_SM_WAIT_PPP;

    StatusFlagsX->FlagNetConnecting = true;
}


void WaitPPP(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX)
{
    uint32_t ActualFlags = 0;
    CommData->FlagConnectGPRS = 0;
    CommData->FlagAtCommand = 0;

    CommData->CommStatesMachine = COMM_SM_IDLE;

    if(!tx_event_flags_get(&Cellular_Flags, CELL_FLAG_STATUS_CELL, TX_OR_CLEAR, &ActualFlags, TX_NO_WAIT))
    {
        if(ActualFlags & CELL_FLAG_CELL_CONNECTED)
        {

#if DEBUG_CELLULAR
        	printf("WAITPPP: Celular conectado!\n");
#endif
        	StatusFlagsX->FlagNetConnecting = false;
        	CommData->FlagConnectGPRS = 1;

#if RECORD_DATALOGGER_EVENTS
            DataLoggerRegisterEvent(DATALOGGER_EVENT_CELLULAR_CONNECTION_STATUS_CHANGE, 0, true);
#endif

			CommData->CommStatesMachine = COMM_SM_DNS;//COMM_SM_DNS;//COMM_SM_DHCP;

            //CommData->FlagAtCommand = 1;
            CommData->NetCellularConf.ValCod = FLASH_NUMBER_MAGIC;
//            int_storage_write((uint8_t*)&CommData->NetCellularConf, sizeof(_NET_CELLULAR_CONF), CONF_CELLULAR);
        }
        else if(ActualFlags & CELL_FLAG_CELL_DISCONNECTED)
        {
        	printf("Celular Desconectado!\n");

        	nx_ppp_restart(CommData->NxPPP);

        	CommData->CommStatesMachine = COMM_SM_WAIT_PPP;
        }
    }
    else
    {
        tx_thread_sleep(50);
        CommData->CommStatesMachine = COMM_SM_WAIT_PPP;
    }
}

void DnsIp(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX)
{
    UINT      status;
    ULONG     dns_ip;
    ULONG     NeededStatus = 0;
    UINT      StatusRet = 0;

    SSP_PARAMETER_NOT_USED(StatusFlagsX);

    uint8_t retry_cnt = 0;

    dns_ip = 0;

//    if (NX_SUCCESS != nx_ip_interface_address_set(CommData->NxIP, 0, CommData->IpStruct.IpAddress, CommData->IpStruct.IpMask))
//    {
//    	printf("ERRO: nx_ip_interface_address_set\r\n");
//    }

    if(NX_SUCCESS != nx_ip_interface_status_check(CommData->NxIP, 0, NX_IP_LINK_ENABLED, &NeededStatus, TX_WAIT_FOREVER))
    {
    	printf("ERRO: nx_ip_interface_status_check\r\n");
    }

    dns_client_init0();

    /* Get the DNS IP assigned during the IPCP handshake */
    status = nx_ppp_dns_address_get(CommData->NxPPP, &dns_ip);
    if ((NX_SUCCESS != status) || (0 == dns_ip))
    {
        dns_ip = DNS_SERVER_1_ADDRESS;
    	printf("Erro DNS!\r\n");
    }


#ifdef DEBUG_CELLULAR
    printf("DNS IP address = %d.%d.%d.%d \r\n", (int)(dns_ip>>24 & 0xFF),
     										    (int)(dns_ip>>16 & 0xFF),
     										    (int)(dns_ip>>8  & 0xFF),
     										    (int)(dns_ip>>0  & 0xFF));
#endif

    if (0 != dns_ip)
    {
        /* Add an IPv4 server address to the Client list. */
        status = nx_dns_server_add(&g_dns0, dns_ip);
        if ((NX_SUCCESS != status) && (NX_DNS_DUPLICATE_ENTRY != status))
        {
            printf("nx_dns_server_add failed!\r\n");
        }
    }

//    do{
//         StatusRet = nx_ip_status_check(CommData->NxIP, NX_IP_ADDRESS_RESOLVED, &NeededStatus, 200);
//         retry_cnt++;
//    }while(StatusRet && (retry_cnt < 5));
//
//    if(StatusRet)
//    {
//    	printf("ERRO: StatusRet\r\n");
//    }


	CommData->CommStatesMachine = COMM_SM_UPDATE_SERVER;
}

void InitDhcp(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX)
{
    static uint8_t  NumAttemptsIp = 0;

    if(!CellularStartDhcpClient(CommData))
    {
        NumAttemptsIp = 0;
        CommData->CommStatesMachine = COMM_SM_STATUS_IP;
    }
    else
    {
        if(NumAttemptsIp < NUM_ATTEMPTS)
        {
            NumAttemptsIp++;
        }
        else
        {
            StatusFlagsX->FlagErrorIp = true;
        }

#if RECORD_DATALOGGER_EVENTS
//        DataLoggerRegisterEvent(DATALOGGER_EVENT_IP_ERROR, CommData->CommStatesMachine, CommData->ConfNetBasic.AddrMode);
#endif

        CommData->CommStatesMachine = COMM_SM_RESTART_IP;
    }
}

void CellularStatusSignalCellular(_COMM_CONTROL *CommData, _MAIN_MANAGER_FLAGS *MainManagerFlag)
{
    *MainManagerFlag |= MM_STATUS_CELL;

    if(CommData->CellularRssid >= STATUS_EXCELLENT)
    {
        *MainManagerFlag |= MM_S_CELL_EXCELLENT;
    }
    else if(CommData->CellularRssid >= STATUS_GOOD)
    {
        *MainManagerFlag |= MM_S_CELL_GOOD;
    }
    else if(CommData->CellularRssid == STATUS_POOR)
    {
        *MainManagerFlag |= MM_S_CELL_POOR;
    }
    else if(CommData->CellularRssid == STATUS_VERY_POOR)
    {
        *MainManagerFlag |= MM_S_CELL_VERY_POOR;
    }
    else
    {
        *MainManagerFlag |= MM_S_CELL_NO_SIGNAL;
    }
}

void StatusIp(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX)
{
    static uint8_t  NumAttemptsGetIp = 0;

    if(!CellularGetStatusIp(CommData))
    {

        if(!StatusFlagsX->FlagStartUdpTcp)
        {
            //UdpStart(CommData);
            if(!CommData->FlagFAC)
            {
                TcpStart(CommData);
            }

            StatusFlagsX->FlagStartUdpTcp = true;
        }

        CommData->FlagConnect           = true;
        StatusFlagsX->FlagNetConnected  = true;
        StatusFlagsX->FlagNetConnecting = false;

        CommData->CommStatesMachine = COMM_SM_DONE;
        NumAttemptsGetIp = 0;
        StatusFlagsX->FlagErrorIp = false;
        StatusFlagsX->FlagErrorPassWrd = false;

        printf("UDP e TCP inicializados!\r\n");
    }
    else
    {
        if(NumAttemptsGetIp < NUM_ATTEMPTS)
        {
            NumAttemptsGetIp++;
        }
        else
        {
            StatusFlagsX->FlagErrorIp = true;

#if RECORD_DATALOGGER_EVENTS
            DataLoggerRegisterEvent(DATALOGGER_EVENT_IP_ERROR, CommData->CommStatesMachine, CommData->ConfNetBasic.AddrMode);
#endif

        }
#ifdef DEBUG_CELLULAR
        printf("Erro no Get IP\r\n");
#endif
        CommData->CommStatesMachine = COMM_SM_RESTART_IP;
    }
}

void RestartIp(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX)
{

    CommData->FlagConnect = false;
    StatusFlagsX->FlagNetConnected = false;
    StatusFlagsX->FlagSitradConnected = false;

    //Envia para a thread DataLoggerFlash a sinalizacao quanto ao status do Sitrad.
    _DATA_LOGGER_DATA FlagSitrad;
    FlagSitrad.OpCod = CONNECTED_SITRAD;
    FlagSitrad.SizeTemp = (uint16_t)StatusFlagsX->FlagSitradConnected;
    tx_queue_send(&Datalogger_Flash, &FlagSitrad, TX_WAIT_FOREVER); //envia para a thread Datalogger

    if(StatusFlagsX->FlagStartUdpTcp)
    {
        StatusFlagsX->FlagStartUdpTcp = false;
        UdpFinish(CommData);
        if(!CommData->FlagFAC)
        {
            TcpFinish(CommData);
        }
    }

//    else
    {
        if(!NetStop(CommData))
        {
            CommData->CommStatesMachine = COMM_SM_IDLE;
        }
        else
        {
            CommData->CommStatesMachine = COMM_SM_RESTART_DEVICE;
        }

    }


#if RECORD_DATALOGGER_EVENTS
    DataLoggerRegisterEvent(DATALOGGER_EVENT_NET_RESTART, 0, CommData->CommStatesMachine == COMM_SM_RESTART_DEVICE);
#endif


}

void CxAnyFlags(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX)
{
    if(CELL_FLAG_TCP_CONNECT & CommData->ActualFlags)
    {
        if(!CommData->FlagUpdateFWRun)
        {
            if(!TcpAcceptConnect(CommData))
            {
            	printf("SITRAD CONNECTED! \r\n");
                StatusFlagsX->FlagSitradConnected = true;
                CommData->TimerConnectSitrad = TIME_SILENCE_SITRAD;
            }
        }
        else
        {
            TcpRefugeeConnection(CommData);
        }
    }

//    if(CELL_FLAG_TCP_CONNECT_CONF & CommData->ActualFlags)
//    {
//
//#if RECORD_DATALOGGER_EVENTS
//        DataLoggerRegisterEvent(DATALOGGER_EVENT_CONFIG_STATUS_CHANGE, 0, true);
//#endif
//        if(TcpAcceptConnect(CommData, CommData->IpStruct.Tcp_Port_Conf))
//        {
//            CommData->CommStatesMachine = COMM_SM_RESTART_IP;
//        }
//    }

    if(CELL_FLAG_TCP_RECEIVED & CommData->ActualFlags)
    {
        if(CommData->FlagUpdateFWRun)
        {
            CommData->ActualFlags |= CELL_FLAG_TCP_DELETE; //seta a flag para desconectar a tcp porta sitrad.
            TcpEvent(CommData); // le o pacote recebido para libear a pool
            nx_packet_release(CommData->PacketReceiveTCP);
        }

        else
        {
            if(CELL_TCP_REQUESTED == TcpEvent(CommData))
            {
                _TCP_RETURN Ret_temp = AppPacketTcp(CommData, CommData->IpStruct.Tcp_Port_Com);
                if(CELL_TCP_REQUESTED_485 == Ret_temp)
                {
                    SerialPost(CommData); //Envia pacote para a thread Serial 485
                }
//                else if(CELL_TCP_REQUESTED_CONF == Ret_temp)
//                {
//                    TcpSend(CommData, CommData->IpStruct.Tcp_Port_Com); //Envia para o Sitrad o pacote TCP recheado.
//                }
                nx_packet_release(CommData->PacketReceiveTCP);
                StatusFlagsX->FlagSitradConnected = true; ///seta flag de conexao com o Sitrad.
            }
            CommData->TimerConnectSitrad = TIME_SILENCE_SITRAD;
        }
    }

    if(CELL_FLAG_MFRAMEWORK & CommData->ActualFlags)
    {
        //trata-se de uma recepção de Message FW
        if(!PendSerial485(CommData) && StatusFlagsX->FlagSitradConnected)
        {
            if(!TcpInstrument(CommData))
            {
                //Envia para o Sitrad o pacote TCP recheado.
                TcpSend(CommData);
            }
        }

    }

//    if(CELL_FLAG_TCP_RECEIVED_CONF & CommData->ActualFlags)
//    {
//        if(CELL_TCP_REQUESTED == TcpEvent(CommData, CommData->IpStruct.Tcp_Port_Conf))
//        {
//            _TCP_RETURN Ret_temp = AppPacketTcp(CommData, CommData->IpStruct.Tcp_Port_Conf);
//            if(CELL_TCP_REQUESTED_CONF == Ret_temp)
//            {
//                TcpSend(CommData, CommData->IpStruct.Tcp_Port_Conf); //Envia para o Sitrad o pacote TCP recheado.
//            }
//        }
//        //nx_packet_release(CommData->PacketReceiveTCPConf);
//    }

    if(CELL_FLAG_TCP_DISCONNECT & CommData->ActualFlags)
    {
        TcpDisconnect(CommData);
        StatusFlagsX->FlagSitradConnected = false;
        CommData->ValidAccess = false;
        CommData->TimerConnectSitrad = 0;
        CommData->TimerInactivity = 0;
    }

//    if(CELL_FLAG_TCP_DISCONNECT_CONF & CommData->ActualFlags)
//    {
//
//#if RECORD_DATALOGGER_EVENTS
//        DataLoggerRegisterEvent(DATALOGGER_EVENT_CONFIG_STATUS_CHANGE, 0, false);
//#endif
//        //TcpDisconnect(CommData, CommData->IpStruct.Tcp_Port_Conf);
//        CommData->ValidAccessUSB = false;
//        CommData->TimerInactivity = 0;
//    }

    if(CELL_FLAG_TCP_DELETE & CommData->ActualFlags)
    {
        StopTimeoutTcp();//stop o timeout para conexao tcp
        if (NX_STILL_BOUND == nx_tcp_socket_delete(&CommData->TcpSocket))
        {
            TcpFinish(CommData);
        }
        StatusFlagsX->FlagSitradConnected = false;
    }

    if(CELL_FLAG_RTC_SEND & CommData->ActualFlags)
    {
        RtcPost(CommData);
        tx_thread_relinquish();
    }

    if(CELL_FLAG_CELL_DISCONNECTED & CommData->ActualFlags)
    {

#if RECORD_DATALOGGER_EVENTS
//        DataLoggerRegisterEvent(DATALOGGER_EVENT_WIFI_CONNECTION_STATUS_CHANGE, CommData->FlagConnect, false);
#endif

        if(CommData->FlagConnect)
        {
        	printf("CELLULAR DISCONNECTED! \r\n");
            CommData->CommStatesMachine = COMM_SM_RESTART_IP;
        }
    }

    if(CELL_FLAG_DETECTED_ACTIVITY & CommData->ActualFlags)
    {
        CommData->TimerInactivity = TIME_SILENCE_INACTIVITY;
    }

    if(CELL_FLAG_TIMEOUT_TIMER_MAIN & CommData->ActualFlags)
    {
        EventTimerMain(CommData, StatusFlagsX);
    }

    if(SM_CLEAN_DATALLOGER & CommData->ActualFlags){

    }

    if(SM_RESTART_DEVICE & CommData->ActualFlags){
    	printf("\n\n VAI RESETAR\n\n");
    	RestartModule();
    }

    if(SM_RESTORE_FACTORY_DEVICE & CommData->ActualFlags){
    	CommData->NetCellularConf.ValCod = 0;
    	CommData->TimerRestoreFactory = 3;
    	ThreadMainManagerSetFlag(MM_RESTORE_FACTORY);
    }

    if(SM_UPDATE_DATE_TIME & CommData->ActualFlags){
    	printf("=\n=\n VAI ATUALIZAR HORA =\n=\n");
    	RtcPost(CommData);
		tx_thread_relinquish();

    }
}
