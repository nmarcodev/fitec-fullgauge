#include <Includes/Cellular_Thread_Entry.h>
#include "common_data.h" // include obrigatório

#include "Includes/UdpTcpTypes.h"

#include "Includes/ApplicationFlash.h"
#include "Includes/internal_flash.h"

#include "Includes/ApplicationUdp.h"
#include "Includes/ApplicationTcp485.h"
#include "Includes/common_util.h"


void ReadPacketUdpCuston(_UDP_CONF *ConfSitradUdp);
void XatPacketUdp(_PACKET_UDP *PacketUdp, _IPSTRUCT *PonterIpStruct);
uint8_t XatPacketUdpDin(uint8_t *PacketUdp);
void GetUdpIp(_IPSTRUCT *ConfSitradIPUdp);
void SetUdpIp(_IPSTRUCT *ConfSitradIPUdp);
void PacketUdpIp(_IPSTRUCT *ConfSitradIPUdp, bool Set1_Get0);

void UdpStart(_COMM_CONTROL *CommData);
void UdpUnbind(_COMM_CONTROL *CommData);
void UdpRelease(_COMM_CONTROL *CommData);
void UdpFinish(_COMM_CONTROL *CommData);
void PacketUdpCuston(_UDP_CONF *ConfSitradUdp, bool Set1Get0);
void GetPacketUdpCuston(_UDP_CONF *ConfSitradUdp);
void SetPacketUdpCuston(_UDP_CONF *ConfSitradUdp);

void UdpSendOnDemand(NX_UDP_SOCKET *UdpX);

bool    FlagUdpRam;

void ReadPacketUdpCuston(_UDP_CONF *ConfSitradUdp)
{
    _UDP_CONF ConfSitrad_UDP;
    //Le a memoria em busca das configuracoes de UDP
    int_storage_read((uint8_t *)&ConfSitrad_UDP, sizeof(_UDP_CONF), CONF_UDP_SITRAD);

    if((ConfSitrad_UDP.ConfDefault != FLASH_NUMBER_CUSTON) && (ConfSitrad_UDP.ConfDefault != FLASH_NUMBER_DEFAULT))
    {   //Caso nunca tenha sido adicionado uma conf, usa o default.
        SetConfigDefault(&ConfSitrad_UDP);

        ConfSitrad_UDP.ConfDefault = FLASH_NUMBER_DEFAULT;
        int_storage_write((uint8_t *)&ConfSitrad_UDP, sizeof(_UDP_CONF), CONF_UDP_SITRAD);
    return;
    }

    memcpy(ConfSitradUdp, &ConfSitrad_UDP, sizeof(_UDP_CONF));
}

void GetPacketUdpCuston(_UDP_CONF *ConfSitradUdp)
{
    PacketUdpCuston(ConfSitradUdp, false);
}

void SetPacketUdpCuston(_UDP_CONF *ConfSitradUdp)
{
    PacketUdpCuston(ConfSitradUdp, true);
}

void PacketUdpCuston(_UDP_CONF *ConfSitradUdp, bool Set1Get0)
{
    static _UDP_CONF ConfSitradUdpRAM;
    if(Set1Get0)
    {
        memcpy(&ConfSitradUdpRAM, ConfSitradUdp, sizeof(_UDP_CONF));
    }
    else
    {
        memcpy(ConfSitradUdp, &ConfSitradUdpRAM, sizeof(_UDP_CONF));
    }
}

void GetUdpIp(_IPSTRUCT *ConfSitradIPUdp)
{
    PacketUdpIp(ConfSitradIPUdp, false);
}

void SetUdpIp(_IPSTRUCT *ConfSitradIPUdp)
{
    PacketUdpIp(ConfSitradIPUdp, true);
}

void PacketUdpIp(_IPSTRUCT *ConfSitradIPUdp, bool Set1_Get0)
{
    static _IPSTRUCT ConfSitradIPUdpRAM;

    if(Set1_Get0)
    {
        memcpy(&ConfSitradIPUdpRAM, ConfSitradIPUdp, sizeof(_IPSTRUCT));
    }
    else
    {
        memcpy(ConfSitradIPUdp, &ConfSitradIPUdpRAM, sizeof(_IPSTRUCT));
    }
}

uint8_t XatPacketUdpDin(uint8_t *PacketUdp)
{
    static bool FlagReadConf = false;
    static _UDP_CONF ConfSitradUdp;
    uint8_t SizeFrameUDP = 0;
    _IPSTRUCT ConfSitradIPUdp;

    if(!FlagReadConf)
    {
        ReadPacketUdpCuston(&ConfSitradUdp);
        SetPacketUdpCuston (&ConfSitradUdp);
        FlagReadConf = true;
    }
/*ID_MAC_ADDRESS              = 1,
    ID_IP_ADDRESS               = 2,
    ID_MASK_ADDRESS             = 3,
    ID_GETWAY_ADDRESS           = 4,
    ID_MODEL_NAME               = 7,
    ID_ENABLE_PASSWORD          = 8,
    ID_VERSION                  = 0x0A,
    ID_SETUP_PORT               = 0x0C,
    ID_COMUNITATION_PORT        = 0x0E,
    ID_BAUD_RATE                = 0x0F,
    ID_TIME_OUT_RS485           = 0x10,
    ID_IP_MODE                  = 0x11,
    ID_PASSWORD                 = 0x12,
    ID_ENABLE_IP_FILTER         = 0x13,
    ID_INIT_IP                  = 0x14,
    ID_END_IP                   = 0x15,
    ID_TIME_LOG                 = 0x16,
    ID_LIST_INSTRUMENTS         = 0x17,
    ID_SSID_WIRI_CLIENT         = 0x18,
    ID_PWD_WIFI_CLIENT          = 0x19,
    ID_SSID_WIFI_SERVER         = 0x1A,
    ID_PWD_WIFI_SERVER          = 0x1B,
    ID_STATUS_SITRAD            = 0x29
 *
 *
 */

    GetUdpIp(&ConfSitradIPUdp); //Carrega as confs IP

    *PacketUdp++ = FIRST_VAL_UDP;//0xBB;

    *PacketUdp++ = ID_MAC_ADDRESS; //1
    *PacketUdp++ = MAC_ADDRESS_SIZE;
    *PacketUdp++ = (uint8_t)((ConfSitradIPUdp.IpMacAdressMs >>  8)  & 0xFF );
    *PacketUdp++ = (uint8_t)( ConfSitradIPUdp.IpMacAdressMs         & 0xFF );
    *PacketUdp++ = (uint8_t)((ConfSitradIPUdp.IpMacAdressLs >>  24) & 0xFF );
    *PacketUdp++ = (uint8_t)((ConfSitradIPUdp.IpMacAdressLs >>  16) & 0xFF );
    *PacketUdp++ = (uint8_t)((ConfSitradIPUdp.IpMacAdressLs >>   8) & 0xFF );
    *PacketUdp++ = (uint8_t)( ConfSitradIPUdp.IpMacAdressLs         & 0xFF );
    SizeFrameUDP = (uint8_t)(SizeFrameUDP + MAC_ADDRESS_SIZE + HEADER_ID_SIZE);

    *PacketUdp++ = ID_IP_ADDRESS; //2
    *PacketUdp++ = IP_ADDRESS_SIZE;
    *PacketUdp++ = (uint8_t)((ConfSitradIPUdp.IpAddress >>  24)  & 0xFF );//(PonterIpStruct->IpAddress  >>24  );
    *PacketUdp++ = (uint8_t)((ConfSitradIPUdp.IpAddress >>  16)  & 0xFF );//(PonterIpStruct->IpAddress  >>16  )&0xFF;
    *PacketUdp++ = (uint8_t)((ConfSitradIPUdp.IpAddress >>   8)  & 0xFF );//(PonterIpStruct->IpAddress  >>8   )&0xFF;
    *PacketUdp++ = (uint8_t)((ConfSitradIPUdp.IpAddress       )  & 0xFF );//(PonterIpStruct->IpAddress        )&0xFF;
    SizeFrameUDP = (uint8_t)(SizeFrameUDP + IP_ADDRESS_SIZE + HEADER_ID_SIZE);

    *PacketUdp++ = ID_MASK_ADDRESS; //3
    *PacketUdp++ = MASK_ADDRESS_SIZE;
    *PacketUdp++ = (uint8_t)((ConfSitradIPUdp.IpMask >>  24)  & 0xFF );  //(PonterIpStruct->IpMask     >>24    );
    *PacketUdp++ = (uint8_t)((ConfSitradIPUdp.IpMask >>  16)  & 0xFF );  //(PonterIpStruct->IpMask     >>16    )&0xFF;
    *PacketUdp++ = (uint8_t)((ConfSitradIPUdp.IpMask >>   8)  & 0xFF );  //(PonterIpStruct->IpMask     >>8     )&0xFF;
    *PacketUdp++ = (uint8_t)((ConfSitradIPUdp.IpMask >>   0)  & 0xFF );  //(PonterIpStruct->IpMask             )&0xFF;
    SizeFrameUDP = (uint8_t)(SizeFrameUDP + MASK_ADDRESS_SIZE + HEADER_ID_SIZE);

    *PacketUdp++ = ID_GETWAY_ADDRESS; //4
    *PacketUdp++ = GTW_ADDRESS_SIZE;
    *PacketUdp++ = (uint8_t)((ConfSitradIPUdp.IpGateway >>  24)  & 0xFF );//(PonterIpStruct->IpGateway  >>24    );
    *PacketUdp++ = (uint8_t)((ConfSitradIPUdp.IpGateway >>  16)  & 0xFF );//(PonterIpStruct->IpGateway  >>16    )&0xFF;
    *PacketUdp++ = (uint8_t)((ConfSitradIPUdp.IpGateway >>   8)  & 0xFF );//(PonterIpStruct->IpGateway  >>8     )&0xFF;
    *PacketUdp++ = (uint8_t)((ConfSitradIPUdp.IpGateway >>   0)  & 0xFF );//(PonterIpStruct->IpGateway          )&0xFF;
    SizeFrameUDP = (uint8_t)(SizeFrameUDP + GTW_ADDRESS_SIZE + HEADER_ID_SIZE);
    //5
    *PacketUdp++ = ID_MODEL; //6
    *PacketUdp++ = (uint8_t)strlen((const char *)MODEL);
    strcpy((char *)PacketUdp, (const char *)MODEL);
    SizeFrameUDP = (uint8_t)(SizeFrameUDP + (uint8_t)strlen((const char *)MODEL) + HEADER_ID_SIZE);
    PacketUdp += (uint8_t)strlen((const char *)MODEL); //atualiza o endereco do ponteiro

    *PacketUdp++ = ID_CONVERTER_NAME; //7
    *PacketUdp++ = (uint8_t)strlen((const char *)ConfSitradUdp.ModelName);
    memcpy((char *)PacketUdp, (const char *)ConfSitradUdp.ModelName, (uint8_t)strlen((const char *)ConfSitradUdp.ModelName));
    SizeFrameUDP = (uint8_t)(SizeFrameUDP + (uint8_t)strlen((const char *)ConfSitradUdp.ModelName) + HEADER_ID_SIZE);
    PacketUdp += (uint8_t)strlen((const char *)ConfSitradUdp.ModelName); //atualiza o endereco do ponteiro

    *PacketUdp++ = ID_ENABLE_PASSWORD; //8
    *PacketUdp++ = FLAG_PASSWORD_SIZE;
    *PacketUdp++ = (uint8_t)ConfSitradUdp.EnablePassword;
    SizeFrameUDP = (uint8_t)(SizeFrameUDP + FLAG_PASSWORD_SIZE + HEADER_ID_SIZE);
    //9
    *PacketUdp++ = ID_VERSION; //A
    *PacketUdp++ = (uint8_t)strlen((const char *)VERSION); //TAMANHO
    strcpy((char *)PacketUdp, (const char *)VERSION);
    SizeFrameUDP = (uint8_t)(SizeFrameUDP + (uint8_t)strlen((const char *)VERSION) + HEADER_ID_SIZE);
    PacketUdp += (uint8_t)strlen((const char *)VERSION); //atualiza o endereco do ponteiro

    //B

    *PacketUdp++ = ID_SETUP_PORT; //C
    *PacketUdp++ = SETUP_PORT_SIZE;
    *PacketUdp++ = ConfSitradUdp.ConfigurationPort[1];
    *PacketUdp++ = ConfSitradUdp.ConfigurationPort[0];
    SizeFrameUDP = (uint8_t)(SizeFrameUDP + SETUP_PORT_SIZE + HEADER_ID_SIZE);

    //13

    *PacketUdp++ = ID_COMUNITATION_PORT; //E
    *PacketUdp++ = COMUN_PORT_SIZE;
    *PacketUdp++ = ConfSitradUdp.ComunicationPort[1];
    *PacketUdp++ = ConfSitradUdp.ComunicationPort[0];
    SizeFrameUDP = (uint8_t)(SizeFrameUDP + COMUN_PORT_SIZE + HEADER_ID_SIZE);


//    *PacketUdp++ = ID_BAUD_RATE; //F
//    *PacketUdp++ = BAUD_RATE_485_SIZE;
//    *PacketUdp++ = (uint8_t)(BAUD_RATE_RS485  >> 8 ) & 0xFF;
//    *PacketUdp++ = (uint8_t)(BAUD_RATE_RS485       ) & 0xFF;
//    SizeFrameUDP = (uint8_t)(SizeFrameUDP + BAUD_RATE_485_SIZE + HEADER_ID_SIZE);

//    *PacketUdp++ = ID_TIME_OUT_RS485; //16
//    *PacketUdp++ = TIME_OUT_485_SIZE;
//    *PacketUdp++ = ConfSitradUdp.TimeOutRs485[1];
//    *PacketUdp++ = ConfSitradUdp.TimeOutRs485[0];
//    SizeFrameUDP = (uint8_t)(SizeFrameUDP + TIME_OUT_485_SIZE + HEADER_ID_SIZE);

    *PacketUdp++ = ID_IP_MODE; //17
    *PacketUdp++ = FLAG_IP_MODE_SIZE;
    *PacketUdp++ = 1;
    SizeFrameUDP = (uint8_t)(SizeFrameUDP + FLAG_IP_MODE_SIZE + HEADER_ID_SIZE);

//    *PacketUdp++ = ID_PASSWORD; //18
//    *PacketUdp++ = PASSWORD_SIZE;
//    memcpy((char *)PacketUdp, (const char *)ConfSitradUdp.Password, PASSWORD_SIZE);
//    SizeFrameUDP = (uint8_t)(SizeFrameUDP + PASSWORD_SIZE + HEADER_ID_SIZE);
//    PacketUdp += (uint8_t)PASSWORD_SIZE; //atualiza o endereco do ponteiro

    *PacketUdp++ = ID_ENABLE_IP_FILTER;//19
    *PacketUdp++ = FLAG_ENABLE_IP_FILTER;
    *PacketUdp++ = ConfSitradUdp.EnableIpFilter;
    SizeFrameUDP = (uint8_t)(SizeFrameUDP + FLAG_ENABLE_IP_FILTER + HEADER_ID_SIZE);

//    *PacketUdp++ = ID_INIT_IP; //20
//    *PacketUdp++ = INIT_FILTER_IP_SIZE;
//    *PacketUdp++ = 0;
//    *PacketUdp++ = 0;
//    *PacketUdp++ = 0;
//    *PacketUdp++ = 0;
//    SizeFrameUDP = (uint8_t)(SizeFrameUDP + INIT_FILTER_IP_SIZE + HEADER_ID_SIZE);

//    *PacketUdp++ = ID_END_IP; //21
//    *PacketUdp++ = END_FILTER_IP_SIZE;
//    *PacketUdp++ = 0;
//    *PacketUdp++ = 0;
//    *PacketUdp++ = 0;
//    *PacketUdp++ = 0;
//    SizeFrameUDP = (uint8_t)(SizeFrameUDP + END_FILTER_IP_SIZE + HEADER_ID_SIZE);

    *PacketUdp++ = ID_STATUS_SITRAD;
    *PacketUdp++ = FLAG_ENABLE_IP_FILTER;
    *PacketUdp++ = 0;
    SizeFrameUDP = (uint8_t)(SizeFrameUDP + FLAG_ENABLE_IP_FILTER + HEADER_ID_SIZE);

    *PacketUdp++ = LAST_VAL_UDP;//0x55;

    SizeFrameUDP = (uint8_t)(SizeFrameUDP + HEADER_ID_SIZE); //soma o FIRST_VAL_UDP + LAST_VAL_UDP

    return (uint8_t)SizeFrameUDP;
}


void UdpStart(_COMM_CONTROL *CommData)
{
	UINT status;

    status = nx_udp_socket_create(CommData->NxIP, CommData->UdpSocket, "UDP Socket", NX_IP_NORMAL, NX_FRAGMENT_OKAY, 0x80U, 30UL);
    if (NX_SUCCESS != status)
    {
    	printf("ERRO: nx_udp_socket_create\r\n");
    }

    printf("PORTA UDP: %d \r\n",CommData->IpStruct.Udp_Port);

    status = nx_udp_socket_bind(CommData->UdpSocket, (UINT)CommData->IpStruct.Udp_Port, 300);
    if (NX_SUCCESS != status)
    {
    	printf("ERRO: nx_udp_socket_bind\r\n");
    }

    status = nx_udp_socket_receive_notify(CommData->UdpSocket, Cellular_UDP_Notify);
    if (NX_SUCCESS != status)
    {
    	printf("ERRO: nx_udp_socket_receive_notify\r\n");
    }

    FlagUdpRam = false;

    _UDP_CONF ConfSitradUdp;
    ReadPacketUdpCuston(&ConfSitradUdp);
    SetPacketUdpCuston (&ConfSitradUdp);
}

//Funcao que carrega na ram o pacote UDP. Esta funcao carregada uma unica vez os dados na ram.
void UdpSendOnDemand(NX_UDP_SOCKET *UdpSocketX)
{
    char    received_str[4];
    char    *Ptr;
    const char ReplyBroadcastRx[]  = { (char)0x55, (char)0x01, (char)0xAA };

    static uint8_t  PacketUdpCuston[SIZE_PACKET_UDP];
    static uint8_t  SizeFrameUDP = 0;

    UINT    InterfaceIndex,
            ProtocolSitrad,
            IpPortSitrad;
    ULONG   IpSitrad;

    NX_PACKET   *PacketUdp;
    NX_PACKET   *PacketUdpSend;

    if(TX_SUCCESS != nx_udp_socket_receive(UdpSocketX, &PacketUdp, TX_NO_WAIT))
    {
        /*Libera o buffer da recepção*/
        //nx_packet_release(PacketUdp); // todo release test
        return;
    }

    Ptr = (char *)PacketUdp->nx_packet_prepend_ptr;
    memcpy(received_str, (char *)Ptr, (size_t)4U);

    if(TX_SUCCESS != nx_udp_packet_info_extract(PacketUdp, &IpSitrad, &ProtocolSitrad, &IpPortSitrad, &InterfaceIndex))
    {
        /*Libera o buffer da recepção*/
        nx_packet_release(PacketUdp);
        return;
    }

    //carrega para a RAM o pacote UDP.
    if(!FlagUdpRam)
    {
        SizeFrameUDP = XatPacketUdpDin(&PacketUdpCuston[0]);
        FlagUdpRam = true;
    }

    PacketUdpCuston[(SizeFrameUDP-2)] = GetStatusSitrad();

    // Libera o buffer da recepção
    nx_packet_release(PacketUdp);

    if(     (ReplyBroadcastRx[0] == received_str[0]) &&
            (ReplyBroadcastRx[1] == received_str[1]) &&
            (ReplyBroadcastRx[2] == received_str[2]))
    {

        // Alocando Memoria para o Protocolo UDP.
        if(NX_SUCCESS != nx_packet_allocate(&g_packet_pool0, &PacketUdpSend, NX_UDP_PACKET, 5)) //50
        {
            return;
        }

        if(NX_SUCCESS != nx_packet_data_append(PacketUdpSend, &PacketUdpCuston[0], SizeFrameUDP, &g_packet_pool0, 5)) //50
        {
            nx_packet_release(PacketUdpSend);
            return;
        }

        /* Enviando o Dado para o Sitrad - Protocolo UDP. */
        if(NX_SUCCESS != nx_udp_socket_send(UdpSocketX, PacketUdpSend, IpSitrad, IpPortSitrad))
        {
            nx_packet_release(PacketUdpSend);
        }

//        while(TX_SUCCESS == nx_udp_socket_receive(UdpSocketX, &PacketUdp, TX_NO_WAIT))
//        {
//            /*Libera o buffer da recepção*/
//            nx_packet_release(PacketUdp);
//        }
    }
}

void UdpUnbind(_COMM_CONTROL *CommData)
{
    nx_udp_socket_unbind(CommData->UdpSocket);
}

void UdpRelease(_COMM_CONTROL *CommData)
{
    //Libera o buffer da recepção
    nx_packet_release(CommData->PacketUdp);
}


void UdpFinish(_COMM_CONTROL *CommData)
{
    UINT port;
    //verifica se tem alguma porta ativa.
    if(!nx_udp_socket_port_get(CommData->UdpSocket, &port))
    {
        nx_udp_socket_unbind(CommData->UdpSocket);

        nx_udp_socket_delete(CommData->UdpSocket);
    }
}
