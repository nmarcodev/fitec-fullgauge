/*
 * ApplicationMeFramw.c
 *
 *  Created on: 7 de jun de 2019
 *      Author: leonardo.ceolin
 */

#include <Cellular_Thread.h>
#include "Includes/Cellular_Thread_Entry.h"
#include "common_data.h" // include obrigatório
#include "Includes/ApplicationIp.h"
#include "Includes/ApplicationMeFramw.h"
#include "USB_HID_Device_Thread.h"
#include "Includes/ApplicationUSB.h"

void RtcPost(_COMM_CONTROL *CommData);
void SerialPost(_COMM_CONTROL *CommData);
bool PendSerial485(_COMM_CONTROL *CommData);

void RtcPost(_COMM_CONTROL *CommData)
{
    CommData->DateTimeMessagePayload->header.event_b.class_instance = 0;
    CommData->DateTimeMessagePayload->header.event_b.class_code = SF_MESSAGE_EVENT_CLASS_DATE_TIME_MESSAGE;
    CommData->DateTimeMessagePayload->header.event_b.code = SF_MESSAGE_EVENT_NEW_DATA;

    CommData->DateTimeMessagePayload->SetDateTime = true;
    memcpy(&CommData->DateTimeMessagePayload->DateTimeValues[0], &CommData->DateTimeValues[0], DATE_TIME_SIZE);

    g_sf_message0.p_api->post(g_sf_message0.p_ctrl,
                                           &CommData->DateTimeMessagePayload->header,
                                           &CommData->MessageCfg.postCfg,
                                           &CommData->MessageCfg.err_postcFG,
                                           300);
}

void SerialPost(_COMM_CONTROL *CommData)
{
    CommData->SerialDataInPayload->header.event_b.class_instance = 0;
    CommData->SerialDataInPayload->header.event_b.class_code = SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_IN; //symbol da engrenagem
    CommData->SerialDataInPayload->header.event_b.code = SF_MESSAGE_EVENT_S485_TX; //Symbol Events da engrenagem

    g_sf_message0.p_api->post(g_sf_message0.p_ctrl,
                                           &CommData->SerialDataInPayload->header,//DataIpPayloadMessagePayload->header,
                                           &CommData->MessageCfg.postCfg,         //global que esta no incio desta thread;
                                           &CommData->MessageCfg.err_postcFG,     //global que esta no incio desta thread;
                                           TX_NO_WAIT);
}

bool PendSerial485(_COMM_CONTROL *CommData)
{
    ssp_err_t SSP_status;
    bool RetTemp = true;

    _SERIAL_DATA_OUT_PAYLOAD         *SerialDataOutPayloadMP;

    //SSP_status = g_sf_message0.p_api->pend(g_sf_message0.p_ctrl,
//                                           &Cellular_Thread_message_queue,
 //                                          (sf_message_header_t **) &CommData->Header,
 //                                          TX_NO_WAIT);
    if(SSP_status == SSP_SUCCESS)
    {
        //recebeu menssagem de retorno da 485
        if(CommData->Header->event_b.class_code == SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_OUT)
        {
            SerialDataOutPayloadMP = (_SERIAL_DATA_OUT_PAYLOAD*)CommData->Header;

            CommData->SerialDataOutPayload.num_of_bytes = SerialDataOutPayloadMP->num_of_bytes;
            CommData->SerialDataOutPayload.pointer = SerialDataOutPayloadMP->pointer;
            if(SerialDataOutPayloadMP->num_of_bytes)
            {
                RetTemp = false;
            }
        }
    }

    return RetTemp;
}
