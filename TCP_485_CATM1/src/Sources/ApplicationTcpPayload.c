/*
 * ApplicationTcpPayload.c

 *
 *  Created on: 6 de jun de 2019
 *      Author: leonardo.ceolin
 *
 * Modulo principal da aplicacao, junto com ApplicationTcp485.c
 *
 * Trata os pacotes TCP da comunicacao com o SITRAD, com o aplicativo TCP485Config e com o software de teste TCP485Tester.
 * Identifica os comandos presentes nos pacotes e os envia para o modulo responsavel, seja ele
 * RTC, Datalogger ou Serial.
 *
 * A configuracao do equipamento esta armazenada na memoria flash interna.
 *
 */
#include "Includes/TypeDefs.h"
#include "Cellular_Thread.h"
#include "USB_HID_Device_Thread.h"
#include "Includes/ApplicationCommonModule.h"
#include "Includes/ApplicationTcpPayload.h"
#include "Includes/DataLoggerFlash.h"
#include "Includes/Thread_Datalogger_entry.h"
#include "Includes/CRCModbus.h"
#include "Includes/ApplicationUdp.h"
#include "Includes/DateTime.h"
#include "Includes/internal_flash.h"
#include "Includes/Define_IOs.h"
#include "Includes/FlashQSPI.h"
#include "Includes/MainManagerThreadEntry.h"
#include "Includes/Serial_thread_entry.h"
#include "Includes/Thread_Monitor_entry.h"
#include "Includes/RX_8571LC.h"
#include "Includes/Eeprom25AA02E48.h"

#define HANDLE_FLASH_ERRORS (0)

TX_THREAD   Eth_Wifi_Thread;

#if FIRMWARE_VERSION == 99
const uint8_t aes_key[]   = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F};
#else
#error no firmware version defined!
#endif
const uint8_t IVc[]   = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

uint16_t    AES_Dec (uint8_t * PMessage, uint8_t *DecMessage, uint16_t InputOctets);
uint16_t    AES_Enc (uint8_t * PMessage, uint8_t *EncMessage, uint16_t InputOctets);

uint16_t    AppCmdModule(uint8_t *BufferInput, _BUFFER_TCP *BufferTcp, _COMM_CONTROL *CommData, uint16_t PortSitrad);
uint16_t    GetConfSitrad(uint8_t id, uint8_t * GetConfData, _COMM_CONTROL *CommData, bool FlagReadConf);
void        SetConfSitrad(uint8_t * SetConfData, _COMM_CONTROL *CommData, uint16_t SizeRequest);
_TCP_RETURN AppPacketTcp(_COMM_CONTROL *CommData, uint16_t PortSitrad);
uint8_t     TcpInstrument(_COMM_CONTROL *CommControl);
void        StartTimerReset(void);

uint16_t    RequesitaInfoConf(_COMM_CONTROL *CommDataX, _BUFFER_TCP *BufferTcpX, uint8_t * BufferInputX, uint16_t RandomNumber, uint16_t PortSitradX);
uint16_t    AtualizaInfoConf(_COMM_CONTROL *CommData,  uint8_t * BufferInput, uint16_t RandomNumber);
uint16_t    TCPUpdadeDateHour(uint8_t * BufferInput, uint8_t * GetConfData, _COMM_CONTROL *CommControl, uint16_t RandomNumber);
uint16_t    TCPValidateAccess(uint8_t Id, uint8_t * GetConfData, uint16_t RandomNumber, bool ValidAccess);
uint16_t    TCPReadPgDatalogger(_BUFFER_TCP *BufferTcp, uint8_t * BufferInput, uint16_t RandomNumber);
uint16_t    TcpDisconnectConv(uint8_t * BufferInput, uint8_t * GetConfData, uint16_t RandomNumber);
uint16_t    TCPEraseDatalogger(uint8_t Id, uint8_t * GetConfData, uint16_t RandomNumber);
uint16_t    TCPGetStatus(uint8_t id, uint8_t * GetConfData, _COMM_CONTROL *CommControl, uint16_t RandomNumber);
uint16_t    TCPAnswerDefault(uint8_t * BufferInput, uint8_t * GetConfData, uint16_t RandomNumber);
void        TCPBufferTcpH(_BUFFER_TCP *BufferTcp);

/**
 * Trata um pacote TCP recebido.
 *
 * Protocolo detalhado no documento "Informacoes Sobre Protocolo e Configuracao - Conversor Ethernet para RS-485"
 *
 * @param CommData tem praticamente todas as informacoes processadas, desde o buffer com o pacote
 * ate as configuracoes da operacao do equipamento.
 * @param PortSitrad define o comportamento de acordo com a funcao da porta tcp. Ainda, a porta de comunicacao pode receber pacotes
 * maiores que o MTU, entao um tratamento especial e feito dentro dessa funcao.
 * @return a acao demandada pelo pacote recebido. TCP_RELISTEN significa que a operacao foi abortada
 */
_TCP_RETURN AppPacketTcp(_COMM_CONTROL *CommData, uint16_t PortSitrad)
{
    _BUFFER_TCP     *BufferTcp;
    ConvBytesToInt  CmdSize;
    static uint16_t offset = 0;
    uint8_t         BufferRxTCPSitradOut[SIZE_HEADER_TCP + SIZE_PAYLOAD_TCP]; ///> Buffer de envio
    uint16_t        PayloadLength,
                    SizeRxFrame = 0;

    
    BufferTcp = &CommData->BufferTcp;

	memcpy((BufferTcp->HeaderPayload), CommData->PacketReceiveTCP->nx_packet_prepend_ptr, CommData->PacketReceiveTCP->nx_packet_length );

	if(CommData->FlagFAC)
	{
		return CELL_TCP_RELISTEN;
	}

    if(BufferTcp->HP.Header[0] >= DATA_UNKNOWN) //comando invalido;
    {
        CommData->BigFrameTCP = 0;
        return CELL_TCP_RELISTEN;
    }

    CmdSize.Bytes[0] = BufferTcp->HP.Header[2];
    CmdSize.Bytes[1] = BufferTcp->HP.Header[1];
    PayloadLength    = CmdSize.Word16;
    if(!PayloadLength) //se o payload tem tamanho zero
    {
        CommData->BigFrameTCP = 0;
        return CELL_TCP_RELISTEN;
    }

    // Todos os pacotes sao criptografados com uma chave AES
    memset(&BufferRxTCPSitradOut[0], 0, sizeof(BufferRxTCPSitradOut));
    SizeRxFrame = AES_Dec(&BufferTcp->HP.Payload[0], &BufferRxTCPSitradOut[0], PayloadLength);
    if(!SizeRxFrame)
    {
        CommData->BigFrameTCP = false;
        return CELL_TCP_RELISTEN;//return EW_TCP_NONE;//return CELL_TCP_RELISTEN;
    }

    switch(BufferTcp->HP.Header[0])
    {
       case DATA_RS485_SPACE://implementacao futura para modbus (8 bits)
			break;
		//Comandos requisitando comunicacao serial somente podem ser enviados pela porta de comunicacao (SITRAD)
       case DATA_RS485_MARK:
           if(PortSitrad == CommData->IpStruct.Tcp_Port_Com)
           {
               memcpy(&CommData->BufferTcp.HP.Payload, BufferRxTCPSitradOut+2, (size_t)(SizeRxFrame-2));
               //tamanho menos os numeros randomicos
               CommData->SerialDataInPayload->num_of_bytes = (uint32_t)(SizeRxFrame-2);
               //payload menos os 2 bytes de num. randomico.
               CommData->SerialDataInPayload->pointer = (uint16_t *)CommData->BufferTcp.HP.Payload;
               CommData->BigFrameTCP = false;

               return CELL_TCP_REQUESTED_485;
           }
       break;

       // Comandos de configuracao / status podem ser enviados por ambas as portas, de preferencia pela porta de configuracao
       case DATA_CELL_MODULE:

           // Processa o pacote recebido e ja executa a acao. Payload Length possui o tamanho do pacote a ser enviado de volta ao requerente.
           PayloadLength = AppCmdModule(&BufferRxTCPSitradOut[0], BufferTcp, CommData, PortSitrad);
//           if(PayloadLength) // Payload len zero indica que nenhuma resposta deve ser enviada.
//           {
//               if (( PayloadLength % 16U ) != 0 )
//               {
//                    uint8_t tmp;
//                    tmp = (uint8_t)((uint8_t)(PayloadLength / 16U) + (uint8_t)1U);
//                    PayloadLength = (uint8_t)(tmp*16U);
//               }
//
//               memset(&BufferRxTCPSitradOut[0], 0, sizeof(BufferRxTCPSitradOut));
//
//               // Todos os pacotes sao criptografados com uma chave AES
//               SizeRxFrame = AES_Enc(&BufferTcp->HP.Payload[0], &BufferRxTCPSitradOut[0], PayloadLength);
//
//               memcpy(&BufferTcp->HP.Payload[0], &BufferRxTCPSitradOut[0], SizeRxFrame);
//
//               memcpy(&CommData->BufferTcp, BufferTcp, SizeRxFrame+3U);
//
//               CommData->BigFrameTCP = 0;
//               return CELL_TCP_REQUESTED_CONF;
//           }
       break;
    }

//    if(CommData->BigFrameTCP)
//    { //caso de erro geral
//        TCPBufferTcpH(&CommData->BufferTcp);
//        CommData->BigFrameTCP = false;
//        return CELL_TCP_REQUESTED_CONF;
//    }

    CommData->BigFrameTCP = 0;
    return CELL_TCP_RELISTEN;
}


/**
 * Trata os pacotes de configiracao / status
 *
 * Sempre que um socket e aberto, o cliente deve autenticar, com o comando VALIDA_ACESSO, mesmo que a senha de acesso nao esteja configurada.
 * Se nao for feita a autenticacao, as demais requisicoes serao negadas.
 *
 * @param BufferInput
 * @param BufferTcp
 * @param CommData
 * @param PortSitrad
 * @return
 */
//trata comando recebido para o modulo wifi (tipo 0x02)
uint16_t AppCmdModule(uint8_t *BufferInput, _BUFFER_TCP *BufferTcp, _COMM_CONTROL *CommData, uint16_t PortSitrad)
{
    #define TypeCom 2
    int RNumber = rand();
    uint16_t    Size        = 0;
    uint16_t    SizeTemp0,
                SizeTemp1 = 0;
    uint8_t     Dif = 0;



#if RECORD_DATALOGGER_EVENTS
    if((PortSitrad == CommData->IpStruct.Tcp_Port_Conf) || (BufferInput[TypeCom] != REQUISITA_INFO_CONF && BufferInput[TypeCom] != REQUISITA_STATUS))
    {
        DataLoggerRegisterEvent(DATALOGGER_EVENT_TCP_COMMAND_RECEIVED, BufferInput[TypeCom], PortSitrad == CommData->IpStruct.Tcp_Port_Com);
    }
#endif

    if(BufferInput[TypeCom] == VALIDA_ACESSO)
    {
        _UDP_CONF Udp_Conf;
        GetPacketUdpCuston(&Udp_Conf);
        bool ValidAcessTemp = true;
        if(PortSitrad == CommData->IpStruct.Tcp_Port_Com)
        {
            CommData->ValidAccess = true;
        }


        // Se nao houver senha o comando valida acesso sempre retorna positivo. Mesmo assim, e obrigatorio.
        if ( Udp_Conf.EnablePassword )
        {
            for(uint8_t Index = 0; Index < PASSWORD_SIZE; Index++)
            {
                if ( BufferInput[5 + Index] != Udp_Conf.Password[Index] )
                {
                    if(PortSitrad == CommData->IpStruct.Tcp_Port_Com)
                    {
                        CommData->ValidAccess = false;
                    }

                    ValidAcessTemp = false;
                    Index = 8;                                       // Forca saida
                }
            }
        }

        Size = TCPValidateAccess(BufferInput[TypeCom], &BufferTcp->HeaderPayload[0], (uint16_t)RNumber, ValidAcessTemp);
    }

    ///Vefifica se a porta que esta solicitando comunicacao foi validada
    else if(((PortSitrad == CommData->IpStruct.Tcp_Port_Com) && CommData->ValidAccess))
    {
        switch(BufferInput[TypeCom]) //2 - tipo de comando recebido
        {

            case ATUALIZA_INFO_CONF: //04 configura quais os instrumentos vao ser logados e informacoes do conversor (configs)
            {
//                Size = AtualizaInfoConf(CommData, &BufferTcp[0], &BufferInput[0], (uint16_t)RNumber);
            }break;

            case REQUISITA_INFO_CONF: //0x05 OK -< Coleta as configuracoes do log e do conversor
            {
                Size = RequesitaInfoConf(CommData, &BufferTcp[0], &BufferInput[0], (uint16_t)RNumber, PortSitrad);
            }break;

            case ERASE_DATALOGGER://0x07 - ERASE_DATALOGGER!!!
            {
                tx_event_flags_set (&DataloggerEventFlags, DL_FLAG_CLEAR, TX_OR); // Seta a flag para a thread datalogger apagar o registro de logs.
                Size = TCPAnswerDefault(&BufferInput[0], &BufferTcp->HeaderPayload[0], (uint16_t)RNumber);
            }break;

            case READ_PG_DATALOGGER: // Requisita a leitura de paginas (maximo 16) do datalogger pela queue, e aguarda retorno
            {
               Size = TCPReadPgDatalogger(&BufferTcp[0], &BufferInput[0], (uint16_t)RNumber);

            }break;

            case WRITE_DATE_HOUR: //0x09
            {
                Size = TCPUpdadeDateHour(&BufferInput[0], &BufferTcp->HeaderPayload[0], CommData, (uint16_t)RNumber);
                //seta o flag primeiro.
                tx_event_flags_set (&Cellular_Flags, CELL_FLAG_RTC_SEND, TX_OR); // Seta a flag para a thread do RTC ajustar o horario
            }break;

            case REQUISITA_STATUS: //0x0A // envia data/hora, estado de conexao com sitrad, variaveis do datalogger, situacao do rtc
            {
                Size = TCPGetStatus(BufferInput[TypeCom], &BufferTcp->HeaderPayload[0], CommData, (uint16_t)RNumber);
            }break;

            default:
            {
                BufferTcp->HeaderPayload[1] = 0;
                BufferTcp->HeaderPayload[2] = 0;
                Size = 0;
            }break;

        }
    }

    else
    {
        BufferTcp->HeaderPayload[Size++] = 0x13;                                        //TYPE_IND
        BufferTcp->HeaderPayload[Size++] = 0x00;                                        //SIZE1_IND
        BufferTcp->HeaderPayload[Size++] = 0x10;//0x01;                                 //SIZE2_IND
        BufferTcp->HeaderPayload[Size++] = (uint8_t)(RNumber >> 8);                     //RANDOM1_IND
        BufferTcp->HeaderPayload[Size++] = (uint8_t)(RNumber^0xFF);                     //RANDOM2_IND
        BufferTcp->HeaderPayload[Size++] = 0xA1;                                        //COMMAND_IND
    }

    //Completa o payload com a diferenca....
    //Exemplo abaixo: 0010 - 0001 -5 = 0A
    //Exemplo: 12 00 10 F8 CB 81 00 01 01 0A 0A 0A 0A 0A 0A 0A 0A 0A 0A
    //

    SizeTemp0 = BufferTcp->HeaderPayload[1];
    SizeTemp0 = (uint16_t)((SizeTemp0 << 8) | BufferTcp->HeaderPayload[2]);
    if((Size > 6) && (BufferTcp->HeaderPayload[0] == HEADER_TX_TCP))
    {
        SizeTemp1 = BufferTcp->HeaderPayload[6];
        SizeTemp1 = (uint16_t)((SizeTemp1 << 8) | BufferTcp->HeaderPayload[7]);

        Dif = (uint8_t)(SizeTemp0 - SizeTemp1);
        if(Dif >= 5)
        {
            Dif = (uint8_t)(Dif - 5);
        }
    }
    else if(Size > 0)
    {
        Dif = (uint8_t)SizeTemp0;
        Dif = (uint8_t)(Dif - 3);
    }
    else
    {
        Dif = 0;
        SizeTemp0 = 0;
    }

    for(uint16_t i=0; i<=Dif; i++)
    {
        BufferTcp->HeaderPayload[i+Size] = Dif;
    }



    return SizeTemp0;
}

uint16_t TCPReadPgDatalogger(_BUFFER_TCP *BufferTcp, uint8_t * BufferInput, uint16_t RandomNumber)
{
    /* Teste ...  08 00 03 00 00 01 08 08 08 08 08 08 08 08*/
    /* Pagina(d39 = 0x27) 08 00 03 00 27 01 08 08 08 08 08 08 08 08
    * */
    //print_to_console("\n\r [DM] SITRAD TCP -> 0x08 - READ_PG_DATALOGGER!");
    //if(S25FL064ChipEraseFinished() == 0)
    // 0   1   2   3  4  5 6   7  ....
    //RDM RDL 08  00  03 x y   z  (X || Y): Pagina a Ler  z: Qntidade de paginas.
    uint16_t Size = 0;
    uint16_t Temp;

    Temp = (uint16_t)((BufferInput[5] <<  8) | (BufferInput[6]));         //captura o numero da pagina a ler
    uint8_t QtdPages = BufferInput[7];

        //-------------------Header------------------------
    BufferTcp->HeaderPayload[Size++] = HEADER_TX_TCP;
    BufferTcp->HeaderPayload[Size++] = 0x00;
    BufferTcp->HeaderPayload[Size++] = 0x00;
    //-------------------Payload-----------------------
    BufferTcp->HeaderPayload[Size++] = (uint8_t)(RandomNumber >> 8   );
    BufferTcp->HeaderPayload[Size++] = (uint8_t)(RandomNumber ^  0xFF);
    BufferTcp->HeaderPayload[Size++] = BufferInput[TypeCom] | 0x80;
    BufferTcp->HeaderPayload[Size++] = 0x00;
    BufferTcp->HeaderPayload[Size++] = 0x00;

    if((QtdPages > MAX_PAGES_TO_READ) || (QtdPages < 1))
    {
#if HANDLE_FLASH_ERRORS
        BufferTcp->HeaderPayload[5] = BufferInput[TypeCom] | 0xA0;

#endif
        Size = 8;
        BufferTcp->HeaderPayload[6] = 0; //ok
        BufferTcp->HeaderPayload[7] = 0; //ok

    }
    else // tudo ok, pode ler as paginas
    {


        static _DATA_LOGGER_DATA DataLoggerData;
        DataLoggerData.OpCod = REQUEST_PAG; //Sinaliza o Cod da operacao
        DataLoggerData.QntPg = QtdPages; //numero de paginas que deve ser lida
        DataLoggerData.SizeTemp = Temp; //primeira pagina a ser lida
        DataLoggerData.PayloadTcp = &(BufferTcp->HeaderPayload[Size]);

        tx_queue_send(&Datalogger_Flash, &DataLoggerData, TX_WAIT_FOREVER); //envia para a thread Datalogger
        tx_thread_relinquish();
        tx_queue_receive(&Cellular_Flash, &DataLoggerData, TX_WAIT_FOREVER); //recebe da thread Datalogger

#if HANDLE_FLASH_ERRORS
        if(!DataLoggerData.status)//erro de leitura da flasj qspi
        {
            BufferTcp->HeaderPayload[5] = BufferInput[TypeCom] | 0xC0;

            Size = 8;
            BufferTcp->HeaderPayload[6] = 0; //ok
            BufferTcp->HeaderPayload[7] = 0; //ok
        }
        else
#endif
        {
            uint16_t u16LQtd;
            u16LQtd = (uint16_t)((uint16_t)(QtdPages*PAGE_SIZE) + (uint16_t)(CRC_SIZE));

            BufferTcp->HeaderPayload[6] = (uint8_t)((u16LQtd >>  8) & 0xFF); //ok
            BufferTcp->HeaderPayload[7] = (uint8_t)((u16LQtd >>  0) & 0xFF); //ok

            u16LQtd = (uint16_t)(u16LQtd + (uint16_t)(HEADER_SIZE));

            Size = (uint16_t)(u16LQtd - (uint16_t)CRC_SIZE);

            Temp = (uint16_t)(CrcModbusCalc((uint8_t*)&BufferTcp->HeaderPayload[5], Size, false));

            Size = (uint16_t)(Size + (uint16_t)5);

            BufferTcp->HeaderPayload[Size++] = (uint8_t)((Temp >>  8) & 0xFF);
            BufferTcp->HeaderPayload[Size++] = (uint8_t)((Temp >>  0) & 0xFF);
        }
    }

    /*Ajusta o tamanho do payload*/
    uint16_t DifTemp = (Size % 16);
    BufferTcp->HeaderPayload[1] = (uint8_t)(((uint16_t)(Size + (16 - DifTemp)) >>  8) & 0xFF);
    BufferTcp->HeaderPayload[2] = (uint8_t)(((uint16_t)(Size + (16 - DifTemp)) >>  0) & 0xFF);

    return Size;
}



uint16_t AtualizaInfoConf(_COMM_CONTROL *CommData,  uint8_t * BufferInput, uint16_t RandomNumber)
{
//	_BUFFER_TCP *BufferTcp
    //0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28
    //32 92 04 00 18 26 0c 54 50 2d 4c 49 4e 4b 5f 44 36 38 32 27 08 38 30 31 30 31 30 36 32
    //print_to_console("\n\r [DM] SITRAD TCP -> 0x04 - ATUALIZ_INFO_CONF!");
    //0  1  2  3  4  5 6  7  8  9  10 11 12 13 14 15 ....
    //0d 20 04 00 fd 16 02 00 0f 17 f7 1e 20 21 22 00
    uint16_t Temp = 0;
    uint16_t Size = 0;
//    -------------------Header------------------------
//    BufferTcp->HeaderPayload[Size++] = HEADER_TX_TCP;                           //TYPE_IND
//    BufferTcp->HeaderPayload[Size++] = 0x00;                                    //SIZE1_IND
//    BufferTcp->HeaderPayload[Size++] = 0x10;//0x01;                             //SIZE2_IND
//    //-------------------Payload-----------------------
//    BufferTcp->HeaderPayload[Size++] = (uint8_t)(RandomNumber    >>     8);     //RANDOM1_IND
//    BufferTcp->HeaderPayload[Size++] = (uint8_t)(RandomNumber    ^   0xFF);     //RANDOM2_IND
//    BufferTcp->HeaderPayload[Size++] = BufferInput[TypeCom] |    0x80;          //COMMAND_IND
//    BufferTcp->HeaderPayload[Size++] = 0x00;          //SIZE
//    BufferTcp->HeaderPayload[Size++] = 0x00;          //SIZE

    if(BufferInput[4] == 0xFD) //compatibilidade da versao antiga
    {
        Temp = (uint16_t)((BufferInput[7] <<  8) | (BufferInput[8]));           //captura o tempo de log
        if(Temp < 15)//se for menor que 15 segundos, seta em 15s;
            Temp = 15;

        DataloggerFlashSetCmdSitrad(Temp, &BufferInput[11], BufferInput[10]);
    }
    else
    {
        uint16_t SizeRequest = (uint16_t)( BufferInput[3] << 8 | BufferInput[4]);
        SetConfSitradUSB(&BufferInput[5], CommData, SizeRequest); //envia os parametros recebidos do configurador para a memoria flash.
    }
    DataLoggerRegisterEvent(DATALOGGER_EVENT_CONFIG_STATUS_CHANGE, 0, true);
    return Size;
}

uint16_t RequesitaInfoConf(_COMM_CONTROL *CommDataX, _BUFFER_TCP *BufferTcpX, uint8_t * BufferInputX, uint16_t RandomNumber, uint16_t PortSitradX)
{
    // exemplo 85 00 fd 16 02 00 1e 17 f7
    //A principio o sitrad deve enviar o range dos comandos que o conversor de responder.
    //Caso A: 0x00 0x00 = Respondo tudo, estilo UDP
    //Caso B: 0x01 0x05 = Respondo o intervalo, do 0x01 ao 0x05
    //Caso C: 0x03 0x03 = Respondo apenas o 0x03

    uint16_t    Size = 0;

    bool        FlagReadConf = false;

    //-------------------Header------------------------
    BufferTcpX->HeaderPayload[Size++]  = HEADER_TX_TCP;
    BufferTcpX->HeaderPayload[Size++]  = 0;
    BufferTcpX->HeaderPayload[Size++]  = 0;
    //-------------------Payload-----------------------
    BufferTcpX->HeaderPayload[Size++]  = (uint8_t)(RandomNumber >> 8  );
    BufferTcpX->HeaderPayload[Size++]  = (uint8_t)(RandomNumber ^ 0xFF);
    BufferTcpX->HeaderPayload[Size++]  = BufferInputX[2] | 0x80;
    BufferTcpX->HeaderPayload[Size++]  = 0x00;
    BufferTcpX->HeaderPayload[Size++]  = 0x00;

    if(PortSitradX == CommDataX->IpStruct.Tcp_Port_Com) //torna compativel com a versao do sitrad antiga
    {
        BufferInputX[3] = 0;
        BufferInputX[4] = 0;
    }

    if((!BufferInputX[3]) && (!BufferInputX[4]))
    {
        //varre o intervalo
        uint16_t    TmpRet,
                    SizePayload = 0;
        for(uint8_t i = 0x16; i <= 0x17; i++)
        {
            TmpRet = (uint16_t)GetConfSitrad(i, &BufferTcpX->HeaderPayload[Size], CommDataX, FlagReadConf);
            FlagReadConf = true;
            if(TmpRet)
            {
                SizePayload = (uint16_t)(SizePayload + TmpRet);
                Size = (uint16_t)(TmpRet + Size);
            }
        }

        BufferTcpX->HeaderPayload[6] = (uint8_t)(    ((SizePayload) >>  8) & 0xFF);
        BufferTcpX->HeaderPayload[7] = (uint8_t)(    ((SizePayload) >>  0) & 0xFF);

        Size = (uint16_t)(SizePayload  + (uint16_t)5); //ajustar o payload

        uint8_t TempSize = (uint8_t)(Size / 16);

        TempSize++;

        BufferTcpX->HeaderPayload[2] = (uint8_t)(0x10 * TempSize);

        Size = (uint16_t)(Size + (uint16_t)3);
    }
    else
    {
        //varre o intervalo
        uint16_t    TmpRet,
                    SizePayload = 0,
                    SizeRequest;
        SizeRequest = (uint16_t)(BufferInputX[3]<<8 | BufferInputX[4]);

        if(!BufferInputX[5]) //verifica se eh range de IDs!
        {
            if(!BufferInputX[6] && !BufferInputX[7]) // 00 00 :> que todos os IDS
            {
                BufferInputX[6] = ID_MAC_ADDRESS;    //Primeiro ID
                BufferInputX[7] = ID_END;            //Ultimo ID
            }

            for(uint8_t i = BufferInputX[6]; i <= BufferInputX[7]; i++)
            {
                //TmpRet = (uint16_t)GetConfSitrad(i, &BufferTcpX->HeaderPayload[Size], CommDataX, FlagReadConf);
                FlagReadConf = true;
                if(TmpRet)
                {
                    SizePayload = (uint16_t)(SizePayload + TmpRet);
                    Size = (uint16_t)(TmpRet + Size);
                }
            }
        }
        else //Neste caso, o Sitrad quer os IDs individuais.
        {
            if(SizeRequest)
            {
                SizeRequest = (uint16_t)(SizeRequest - 1); //remove o sinalizador

                uint8_t IndexBufferInput = 6;
                for(uint8_t i = IndexBufferInput; i <= (SizeRequest+IndexBufferInput); i++)
                {
                    //TmpRet = (uint16_t)GetConfSitrad(BufferInputX[i], &BufferTcpX->HeaderPayload[Size], CommDataX, FlagReadConf);
                    FlagReadConf = true;
                    if(TmpRet)
                    {
                        SizePayload = (uint16_t)(SizePayload + TmpRet);
                        Size = (uint16_t)(TmpRet + Size);
                    }
                }
            }
        }


        BufferTcpX->HeaderPayload[6] = (uint8_t)(    ((SizePayload) >>  8) & 0xFF); //CMD_SIZE1_IND
        BufferTcpX->HeaderPayload[7] = (uint8_t)(    ((SizePayload) >>  0) & 0xFF); //CMD_SIZE2_IND

        Size = (uint16_t)(SizePayload + (uint16_t)5); //ajustar o payload

        uint16_t DifTemp = (Size % 16);
        BufferTcpX->HeaderPayload[1] = (uint8_t)(((uint16_t)(Size + (16 - DifTemp)) >>  8) & 0xFF);
        BufferTcpX->HeaderPayload[2] = (uint8_t)(((uint16_t)(Size + (16 - DifTemp)) >>  0) & 0xFF);
        Size = (uint16_t)(Size + (uint16_t)3);
    }

    return Size;
}

//Está funcao formata o Dado recebido da 485 envio para tcp;
uint8_t TcpInstrument(_COMM_CONTROL *CommControl)
{
    uint8_t Dif,
            RetTemp = 0U;

    memcpy(&CommControl->BufferTcp.HP.Payload[2], CommControl->SerialDataOutPayload.pointer, CommControl->SerialDataOutPayload.num_of_bytes);

    CommControl->SerialDataOutPayload.num_of_bytes = CommControl->SerialDataOutPayload.num_of_bytes+2;//adiciona 2 para o num. aleatorio
    Dif = (uint8_t)(CommControl->SerialDataOutPayload.num_of_bytes % 16);

    if(Dif < 16)
    {
        Dif = (uint8_t)(16 - Dif);
    }

    for(uint8_t i=0; i<Dif; i++)
    {
        CommControl->BufferTcp.HP.Payload[i+CommControl->SerialDataOutPayload.num_of_bytes] = Dif;
    }
    CommControl->SerialDataOutPayload.num_of_bytes = CommControl->SerialDataOutPayload.num_of_bytes + Dif;

    CommControl->BufferTcp.HP.Header[0] = 0x11; //sinalizacao que eh uma resposta com dados do instrumento
    CommControl->BufferTcp.HP.Header[1] = (uint8_t)((CommControl->SerialDataOutPayload.num_of_bytes & 0xFF00) >> 8); //tamanho do frame Ms
    CommControl->BufferTcp.HP.Header[2] = CommControl->SerialDataOutPayload.num_of_bytes & 0x00FF;//tamanho do frame Ls

    int RNumber = rand();
    CommControl->BufferTcp.HP.Payload[0] = (uint8_t)(RNumber >> 8);
    CommControl->BufferTcp.HP.Payload[1] = (uint8_t)(RNumber^0xFF);

    uint16_t ret = 1U;
    ret = AES_Enc (CommControl->BufferTcp.HP.Payload, &CommControl->BufferTcp.HP.Payload[0], (uint16_t)CommControl->SerialDataOutPayload.num_of_bytes);

    if(ret % 16)
    {
        RetTemp = 1U;//nao Ok;
    }

    return RetTemp;
}


uint16_t TCPValidateAccess(uint8_t Id, uint8_t * GetConfData, uint16_t RandomNumber, bool ValidAccess)
{
    uint8_t    *AddInput, *AddOut ;

    AddInput = GetConfData;
    //-------------------Header------------------------
    *GetConfData++ = HEADER_TX_TCP;
    *GetConfData++ = 0x00;
    *GetConfData++ = 0x10;
    //-------------------Payload-----------------------
    *GetConfData++ = (uint8_t)(RandomNumber >>   8);
    *GetConfData++ = (uint8_t)(RandomNumber ^ 0xFF);
    *GetConfData++ =  Id | 0x80;
    *GetConfData++ = 0;
    *GetConfData++ = 1;
    *GetConfData++ = ValidAccess == false ? 0x00 : 0x01;

    AddOut = GetConfData;

    return (uint16_t)(AddOut - AddInput);
}

void TCPBufferTcpH(_BUFFER_TCP *BufferTcp)
{
    BufferTcp->HP.Header[0] = 0x12;
    BufferTcp->HP.Header[1] = 0x00;
    BufferTcp->HP.Header[2] = 0x10;
    //***** payload ****//
    BufferTcp->HP.Payload[0] = 0x5C;
    BufferTcp->HP.Payload[1] = 0x35;
    BufferTcp->HP.Payload[2] = 0x5F;
    BufferTcp->HP.Payload[3] = 0xE7;
    BufferTcp->HP.Payload[4] = 0x13;
    BufferTcp->HP.Payload[5] = 0x83;
    BufferTcp->HP.Payload[6] = 0xA4;
    BufferTcp->HP.Payload[7] = 0x21;
    BufferTcp->HP.Payload[8] = 0x52;
    BufferTcp->HP.Payload[9] = 0x8D;
    BufferTcp->HP.Payload[10] = 0x84;
    BufferTcp->HP.Payload[11] = 0x5B;
    BufferTcp->HP.Payload[12] = 0x22;
    BufferTcp->HP.Payload[13] = 0x67;
    BufferTcp->HP.Payload[14] = 0xE8;
    BufferTcp->HP.Payload[15] = 0xFD;
}

uint16_t TCPUpdadeDateHour(uint8_t * BufferInput, uint8_t * GetConfData, _COMM_CONTROL *CommControl, uint16_t RandomNumber)
{
    //Pacote sitrad: 11 2d 09 00 07 19 02 13 02 11 39 0f 04 04 04 04
    //                              D  M  Y  DW H  M  S  x  x  x  x
    uint8_t    *AddInput, *AddOut ;

    AddInput = GetConfData;

    //-------------------Header------------------------
    *GetConfData++ = HEADER_TX_TCP;
    *GetConfData++ = 0x00;
    *GetConfData++ = 0x10;
    //-------------------Payload-----------------------
    *GetConfData++ = (uint8_t)(RandomNumber >> 0x08);
    *GetConfData++ = (uint8_t)(RandomNumber ^ 0xFF);
    *GetConfData++ = ( BufferInput[2] | 0x80 );
    *GetConfData++ = 0x00;
    *GetConfData++ = 0x01;
    *GetConfData++ = 0x00;

    AddOut = GetConfData;

    CommControl->DateTimeValues[0] = BufferInput[5];
    CommControl->DateTimeValues[1] = BufferInput[6];
    CommControl->DateTimeValues[2] = BufferInput[7];
    CommControl->DateTimeValues[3] = BufferInput[9];
    CommControl->DateTimeValues[4] = BufferInput[10];
    CommControl->DateTimeValues[5] = BufferInput[11];


    return (uint16_t)(AddOut - AddInput);
}


uint16_t TCPAnswerDefault(uint8_t * BufferInput, uint8_t * GetConfData, uint16_t RandomNumber)
{
    uint8_t    *AddInput, *AddOut ;

    AddInput = GetConfData;

    //-------------------Header------------------------
    *GetConfData++ = HEADER_TX_TCP;
    *GetConfData++ = 0x00;
    *GetConfData++ = 0x10;
    //-------------------Payload-----------------------
    *GetConfData++ = (uint8_t)(RandomNumber >> 0x08);
    *GetConfData++ = (uint8_t)(RandomNumber ^ 0xFF);
    *GetConfData++ = ( BufferInput[2] | 0x80 );
    *GetConfData++ = 0x00;
    *GetConfData++ = 0x01;
    *GetConfData++ = 0x00;

    AddOut = GetConfData;
    return (uint16_t)(AddOut - AddInput);
}


uint16_t TCPGetStatus(uint8_t id, uint8_t * GetConfData, _COMM_CONTROL *CommControl, uint16_t RandomNumber)
{
    uint8_t     DateTimeValue[DATE_TIME_SIZE];
    uint16_t    Temp;
    uint8_t    *AddInput, *AddOut ;

    AddInput = GetConfData;

    GetDateTimerX(DateTimeValue);

    //-------------------Header------------------------
    *GetConfData++ = HEADER_TX_TCP;
    *GetConfData++ = 0x00;
    *GetConfData++ = 0x20;
    //-------------------Payload-----------------------
    *GetConfData++ = (uint8_t)(RandomNumber >> 8);
    *GetConfData++ = (uint8_t)(RandomNumber ^ 0xFF);
    *GetConfData++ = (uint8_t)(id | 0x80);
    *GetConfData++ = (uint8_t)0x00;
    *GetConfData++ = (uint8_t)0x10;
    *GetConfData++ = (uint8_t)DataloggerGetFlags();
    *GetConfData++ = (uint8_t)GetFlagEclo();
    *GetConfData++ = (uint8_t)DateTimeValue[DATE_TIME_DAY];
    *GetConfData++ = (uint8_t)DateTimeValue[DATE_TIME_MONTH];
    *GetConfData++ = (uint8_t)DateTimeValue[DATE_TIME_YEAR];
    *GetConfData++ = (uint8_t)0x00;//WeekOfTheMonth
    *GetConfData++ = (uint8_t)DateTimeValue[DATE_TIME_HOUR];
    *GetConfData++ = (uint8_t)DateTimeValue[DATE_TIME_MINUTE];
    *GetConfData++ = (uint8_t)DateTimeValue[DATE_TIME_SECOND];
    Temp = DataloggerGetActualPage();
    *GetConfData++ = (uint8_t)((Temp >>  8) & 0xFF);
    *GetConfData++ = (uint8_t)((Temp >>  0) & 0xFF);
    *GetConfData++ = (uint8_t)((PAGE_FLASH_SIZE >>  8) & 0xFF);
    *GetConfData++ = (uint8_t)((PAGE_FLASH_SIZE >>  0) & 0xFF);
    *GetConfData++ = (uint8_t)((NUMBER_OF_PAGES_DATALOGGER >>  8) & 0xFF);
    *GetConfData++ = (uint8_t)((NUMBER_OF_PAGES_DATALOGGER >>  0) & 0xFF);

    *GetConfData++ = (uint8_t)(CommControl->FlagConnectSitrad); //novo

    AddOut = GetConfData;

    return (uint16_t)(AddOut - AddInput);
}


uint16_t GetConfSitrad(uint8_t id, uint8_t * GetConfData, _COMM_CONTROL *CommData, bool FlagReadConf) //(_PACKET_UDP *PacketUdp, _IPSTRUCT *PonterIpStruct)
{
	//TODO: Adaptar para comunicação com Sitrad
	return 0;
}

void SetConfSitrad(uint8_t * SetConfData, _COMM_CONTROL *CommData, uint16_t SizeRequest) //
{
	//TODO: Adaptar para comunicação com Sitrad
}

//Enc
uint16_t AES_Enc (uint8_t * PMessage, uint8_t *EncMessage, uint16_t InputOctets)
{
    uint32_t  MessageLen  = 0;

    if (( EncMessage == NULL ) || ( InputOctets <= 0 ))
    {
       return 0;
    }

    if (( InputOctets % 16U ) != 0 )
    {
       return 0;
    }

    MessageLen = (uint32_t)(InputOctets / 4U);

    g_sce_aes_0.p_api->open(g_sce_aes_0.p_ctrl, g_sce_aes_0.p_cfg); //Open AES_0

    g_sce_aes_0.p_api->encrypt(g_sce_aes_0.p_ctrl, (uint32_t *)aes_key, (uint32_t *)IVc, MessageLen, (uint32_t *)PMessage, (uint32_t *)EncMessage);

    // Close AES driver
    g_sce_aes_0.p_api->close(g_sce_aes_0.p_ctrl);

    return InputOctets;
}


 uint16_t AES_Dec (uint8_t * PMessage, uint8_t *DecMessage, uint16_t InputOctets)
 {
    uint16_t NumBlocks;
    uint32_t  MessageLen  = 0;

    if (( DecMessage == NULL ) || ( InputOctets <= 0 ))
    {
       return 0;
    }

    if (( InputOctets % 16U ) != 0 )
    {
       return 0;
    }

    NumBlocks = ( InputOctets / 16U );//280219

    MessageLen = (uint32_t)(InputOctets / 4U);

    g_sce_aes_0.p_api->open(g_sce_aes_0.p_ctrl, g_sce_aes_0.p_cfg); //Open AES_0

    g_sce_aes_0.p_api->decrypt(g_sce_aes_0.p_ctrl, (uint32_t *)aes_key, (uint32_t *)IVc, MessageLen, (uint32_t *)PMessage, (uint32_t *)DecMessage);//(uint32_t *)tempOut);

    /* Close AES driver */
    g_sce_aes_0.p_api->close(g_sce_aes_0.p_ctrl);

    uint16_t padLen = DecMessage[(NumBlocks*16) - 1];

    if (( padLen <= 0 ) || ( padLen > 16 ))
    {
        return 0;
    }

    //Verifica o padding do pacote
    for ( uint16_t i = (uint16_t)(16 - padLen); i < 16; i++ )
    {
        if(DecMessage[((NumBlocks*16)-16) + i] != padLen)
        {
              return 0;
        }
    }

    uint16_t OutSizePayload;
    OutSizePayload = (uint16_t)(( NumBlocks*16 ) - padLen);

    return OutSizePayload;
 }


