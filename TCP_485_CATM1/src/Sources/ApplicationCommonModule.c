/*
 * ApplicationCommunModule.c
 *
 *  Created on: 21 de mai de 2019
 *      Author: leonardo.ceolin
 */

#include "Includes/ApplicationCommonModule.h"
#include "Includes/Define_IOs.h"
#include "Includes/Thread_Monitor_entry.h"
#include "includes/internal_flash.h"

#if RECORD_DATALOGGER_EVENTS
#include "Includes/Thread_Datalogger_entry.h"
#endif

void RestartModule(void);

void RestartModule()
{
    uint32_t TempTime = 0x05000000;
    //printf("\nREINICIANDO...\n");
#if RECORD_DATALOGGER_EVENTS
    DataLoggerRegisterEvent(DATALOGGER_EVENT_RESET, 0, true);
#endif

    ThreadMonitor_SendKeepAlive(T_MONITOR_STOP_MONITORING);
    tx_thread_sleep(100);

    int_storage_deinit();

    g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_HIGH); // DESLIGA O LED DE ENVIO
    g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_HIGH); // DESLIGA O LED DE RECEBIMENTO

    g_ioport.p_api->pinWrite(LED_ON_R, IOPORT_LEVEL_LOW);
    g_ioport.p_api->pinWrite(LED_ON_G, IOPORT_LEVEL_LOW);
    g_ioport.p_api->pinWrite(LED_ON_B, IOPORT_LEVEL_LOW);

    g_ioport.p_api->pinWrite(LED_ST_R, IOPORT_LEVEL_LOW);
    g_ioport.p_api->pinWrite(LED_ST_G, IOPORT_LEVEL_LOW);
    g_ioport.p_api->pinWrite(LED_ST_B, IOPORT_LEVEL_LOW);


    do{
        TempTime--;
    }while(TempTime);


    NVIC_SystemReset();
}


