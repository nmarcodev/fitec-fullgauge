#include "Includes/ApplicationCellular.h"
#include "Includes/Cellular_Thread_Entry.h"
#include "common_data.h"
#include "Includes/ApplicationCommonModule.h"
#include "Includes/internal_flash.h"
#include "Includes/DateTime.h"
#include "Includes/ApplicationLedsStatus.h"
#include "Includes/Define_IOs.h"
#include "Cellular_Thread.h"
#include "cellular_thread.h"
#include "Includes/common_util.h"
#include "sf_cellular_common_private.h"

#define BUFFER_UPDATE_REQUEST 300
#define BUFFER_ID_REQUEST     150

char IP_Adress[30];

uint8_t ModuleProvision(_COMM_CONTROL *CommData);
void GetStatusCellular(_COMM_CONTROL *CommData);
uint8_t GetIDSitrad(_COMM_CONTROL *CommData);
uint8_t UpdateIPServer(_COMM_CONTROL *CommData);
extern uint16_t AESEnc(uint8_t * PMessage, uint8_t *EncMessage, uint16_t InputOctets);
extern uint16_t AESDec(uint8_t * PMessage, uint8_t *DecMessage, uint16_t InputOctets);
uint8_t CellularInfoTest(_COMM_CONTROL *CommData);

void SaveNet(sf_cellular_provisioning_t *ProvisionCellular);
bool MacAddressList(uint8_t *MacAdd, bool AddTRemoveF);
void StrToHex(char *dest, char * src);

void sf_cellular_debug_log(uint8_t const * const p_cmd_buf, uint32_t cmd_buf_len, sf_cellular_log_buffer_type_t cmd_buf_type);

ssp_err_t cellular_setup(UINT interface_index);

void EXS82_init(void);

ULONG get_response(char * receive_buffer, ULONG size_of_buffer, ULONG *recieved_bytes);
char identifierValidationstring[40] = "";

ULONG get_response(char * receive_buffer, ULONG size_of_buffer, ULONG *recieved_bytes)
{
    /* This function assumes the size of the buffer passed in is big enough to hold the complete response */
    ULONG       get_status = NX_SUCCESS;
    NX_PACKET   *receive_packet;
    ULONG       status;

    /* At this point, we need to handle the response from the server */
    /*by repeatedly calling nx_web_http_client_response_body_get()   */
    /*until the entire response is retrieved. */

    do{
      get_status = nx_web_http_client_response_body_get(&g_web_http_client0,
&receive_packet, NETWORK_TIMEOUT);
      if (NX_SUCCESS == get_status)
      {
          /* Process response packets */
          *recieved_bytes = 0;
          status = nx_packet_data_extract_offset(receive_packet, 0,
                                                 receive_buffer,
                                                 size_of_buffer - 1,
                                                 recieved_bytes );
          nx_packet_release (receive_packet);
          if (NX_SUCCESS != status)
          {
             return status;
          }
      }
      else if (get_status != NX_WEB_HTTP_GET_DONE)
      {
          return get_status;
      }

    }while(get_status != NX_WEB_HTTP_GET_DONE);

    return NX_SUCCESS;
}


/* Provisioning information for SIM */
sf_cellular_provisioning_t cellular_prov_info =
{
    .airplane_mode = (SF_CELLULAR_AIRPLANE_MODE_OFF),
    .auth_type      = (SF_CELLULAR_AUTH_TYPE_NONE),
    .username       = "",
    .password       = "",
    .apn            = "",
    .context_id     = (1),
    .pdp_type       = (SF_CELLULAR_PDP_TYPE_IP),
};


void EXS82_init(void)
{
	tx_thread_sleep(TIME_3S);
	g_ioport.p_api->pinWrite(ON_EXS82, IOPORT_LEVEL_HIGH);
}

void sf_cellular_debug_log(uint8_t const * const p_cmd_buf, uint32_t cmd_buf_len, sf_cellular_log_buffer_type_t cmd_buf_type)
{
    char buffer[BUFFER_SIZE];
    SSP_PARAMETER_NOT_USED(cmd_buf_type);

    if(BUFFER_SIZE < cmd_buf_len)
    {
        cmd_buf_len = BUFFER_SIZE;
    }
    memset(buffer, 0, sizeof(buffer));
    snprintf(buffer, cmd_buf_len, (char *)p_cmd_buf);
    printf("%s\r\n", buffer);
}

void ppp_link_down_callback(NX_PPP * ppp_ptr)
{
    SSP_PARAMETER_NOT_USED(ppp_ptr);

    printf("PPP Link down\r\n");
    tx_event_flags_set(&g_cellular_event_flags, PPP_LINK_DOWN, TX_OR);

    tx_event_flags_set (&Cellular_Flags, CELL_FLAG_CELL_DISCONNECTED, TX_OR);

    //nx_ppp_restart(ppp_ptr);
}



void ppp_link_up_callback(NX_PPP * ppp_ptr)
{
    SSP_PARAMETER_NOT_USED(ppp_ptr);

    printf("PPP Link up\r\n");
    tx_event_flags_set(&g_cellular_event_flags, PPP_LINK_UP, TX_OR);

    tx_event_flags_set (&Cellular_Flags, CELL_FLAG_CELL_CONNECTED, TX_OR);

}


void invalid_packet_handler(NX_PACKET *packet_ptr)
{
    /* Print out the non-PPP byte. In Windows, the string "CLIENT" will
       be sent before Windows PPP starts. Once CLIENT is received, we need
       to send "CLIENTSERVER" to establish communication. It's also possible
       to receive modem commands here that might need some response to
       continue.  */
    nx_packet_release(packet_ptr);
}

ssp_err_t celr_prov_callback(sf_cellular_callback_args_t * p_args)
{
    ssp_err_t err = SSP_SUCCESS;

#if DEBUG_CELLULAR
    printf("Cellular Provisioning Callback\n");
#endif

    if(SF_CELLULAR_EVENT_PROVISIONSET == p_args->event)
    {
        memset((char *)CommControl.NetCellularConf.ApnActual, 0, sizeof(CommControl.NetCellularConf.ApnActual));
        int_storage_read((uint8_t *)&CommControl.NetCellularConf, sizeof(_NET_CELLULAR_CONF), CONF_CELLULAR);
        strcpy((char *)cellular_prov_info.apn, (const char *)CommControl.NetCellularConf.ApnActual);


        sf_cellular_ctrl_t * p_ctrl = (sf_cellular_ctrl_t *)(p_args->p_context);


//#ifdef TIM /* Force the modem to GSM RAT */
        sf_cellular_cmd_resp_t  input_at_command;
        sf_cellular_cmd_resp_t  output;
        uint8_t                 response_buffer[64];
        uint8_t                 atcommand_buffer[64];
        int                     result;

        output.p_buff = response_buffer;
        output.buff_len = sizeof(response_buffer);

        input_at_command.p_buff = atcommand_buffer;
        input_at_command.buff_len = sizeof(atcommand_buffer);

        /* RAT search sequence GSM>LTE Cat M1>LTE Cat NB1 */
        memset(response_buffer, 0, sizeof(response_buffer));
        memset(atcommand_buffer, 0, sizeof(atcommand_buffer));
        snprintf((char *)atcommand_buffer, sizeof(atcommand_buffer), NETWORK_SCAN_SEQ_CMD);

        err = g_sf_cellular0.p_api->commandSend(p_ctrl, &input_at_command , &output, CMD_TIMEOUT);
        if (SSP_SUCCESS != err)
        {
            return err;
        }

        output.p_buff[output.buff_len] = 0;
        result = strcmp((char *)output.p_buff, CMD_RESPONSE);
        if (0 != result)
        {
            return SSP_ERR_INTERNAL;
        }

//#endif

        err = g_sf_cellular0.p_api->provisioningSet (p_ctrl, &cellular_prov_info);
        if (SSP_SUCCESS != err)
        {
            printf ("Modem Provisioning Failed\r\n");
            p_args->event = SF_CELLULAR_EVENT_PROVISIONSET;
            CommControl.CommStatesMachine = COMM_SM_INIT_CELL;
        }
        else
        {
        	GetStatusCellular(&CommControl);
            printf ("Modem Provisioning Successful\r\n");
        }
    }
    return err;
}

void user_rts_callback(uint32_t channel, uint32_t level)
{
    __NOP();
    if(channel == g_uart_cell.p_cfg->channel)
    {
        switch(level)
        {
            case 0U:
                g_ioport.p_api->pinWrite((ioport_port_pin_t)IOPORT_PORT_06_PIN_08, IOPORT_LEVEL_LOW);
                break;
            case 1U:
                g_ioport.p_api->pinWrite((ioport_port_pin_t)IOPORT_PORT_06_PIN_08, IOPORT_LEVEL_HIGH);
                break;
            default:
                break;
        }
    }
}

//Atualiza o servidor da rede com as informações de IP e porta para conexão com o Sitrad
uint8_t UpdateIPServer(_COMM_CONTROL *CommData)
{
	char buf[4096];
	ULONG packet_len = 0;
	UINT status;
	NX_WEB_HTTP_CLIENT *http;
	NX_DNS *dns;
	ULONG ip;

	char *host = "hlg-introducer.sitrad.com";

	NX_PACKET *http_packet;
	CHAR http_request[BUFFER_UPDATE_REQUEST];
	char resourceID[70];

	memset(buf, '\0', sizeof buf);

	memset(resourceID, 0, sizeof(resourceID));
	snprintf(resourceID, sizeof(resourceID), "/api/v1/converters/%s/endpoints", CommData->NetCellularConf.IdSitrad);

	//printf("\n%s\n",resourceID);
	if(!CellularInfoTest(CommData))
	{

		web_http_client_init0();

		http = &g_web_http_client0;
		dns = &g_dns0;

		//printf("\nIp celular: %s\n", CommData->ip_addr);

		NXD_ADDRESS address = {.nxd_ip_version = NX_IP_VERSION_V4};

		// busca o ip a partir do host. se falhar, usa o endereco ja conhecido.
		status = nx_dns_host_by_name_get(dns, (UCHAR *)host, &ip, NETWORK_TIMEOUT);
		if (NX_SUCCESS != status)
		{
			printf("DNS host by name ger error\n");
			address.nxd_ip_address.v4 = IP_ADDRESS(192, 185, 7, 163);
		}
		else
		{
			address.nxd_ip_address.v4 = ip;
		}

		//printf("\nip upadteip: %s\n", IP_Adress);

		/* Set up the strings for the HTTP requests */
		memset(http_request, 0, sizeof(http_request));
		snprintf(http_request, BUFFER_UPDATE_REQUEST,
				"{\"identifier\":\"%s\",\"identifierValidation\":\"%s\",\"endPoints\":[{\"address\":%s,\"port\":11000,\"accessMode\":1},{\"address\":%s,\"port\":11000,\"accessMode\":0}]}"
				,CommData->NetCellularConf.Imei, identifierValidationstring, IP_Adress, IP_Adress);

		// inicializa um request do tipo get.
		status = nx_web_http_client_request_initialize(http, NX_WEB_HTTP_METHOD_POST, resourceID, host,strlen(http_request), NX_FALSE, NX_NULL, NX_NULL, NETWORK_TIMEOUT);
		if(status != NX_SUCCESS)
		{
			printf("nx_web_http_client_request_initialize error\r\n");
			return 1U;
		}

		// adiciona header para forcar resposta em json.
		status = nx_web_http_client_request_header_add(http, "content-type", strlen("content-type"), "application/json", strlen("application/json"), TX_NO_WAIT);
		if(status != NX_SUCCESS)
		{
			printf("SITRAD: Erro HTTP Client request header add!\n");
			return 1U;
		}

		status = nx_web_http_client_request_header_add(&g_web_http_client0, "Accept", strlen("Accept"), "application/json", strlen("application/json"), NETWORK_TIMEOUT);
		if(status != NX_SUCCESS)
		{
			printf("SITRAD: Erro HTTP Client request header add Accept!\n");
			return 1U;
		}

		// abre o socket.
		status = nx_web_http_client_connect(http, &address, NX_WEB_HTTP_SERVER_PORT, NETWORK_TIMEOUT);
		if (status != NX_SUCCESS)
		{
			printf("SITRAD: Erro HTTP Client Connect!\n");
			return 1U;
		}

		// envia o request
		status = nx_web_http_client_request_send(http, NETWORK_TIMEOUT);
		if(status != NX_SUCCESS)
		{
			printf("SITRAD: Erro HTTP Client request send 1!\n");
			return 1U;
		}

		/* Create a new data packet request on the HTTP(S) client instance. */
		status = nx_web_http_client_request_packet_allocate(http, &http_packet, NETWORK_TIMEOUT);
		if (NX_SUCCESS != status)
		{
			printf("Erro nx_web_http_client_request_packet_allocate!\n");
			return 1U;
		}

		status = nx_packet_data_append(http_packet, http_request, strlen(http_request), &g_packet_pool0, NETWORK_TIMEOUT);
		if (NX_SUCCESS != status)
		{
			printf("Erro nx_packet_data_append!\n");
			return 1U;
		}

		status = nx_web_http_client_request_packet_send(http, http_packet, 0, NETWORK_TIMEOUT);
		if (NX_SUCCESS != status)
		{
			printf("Erro nx_web_http_client_request_packet_send!\n");
			return 1U;
		}

		NX_PACKET *packet_pointer;
		// recebe o body da resposta.
		do
		{
			status = nx_web_http_client_response_body_get(http, &packet_pointer, NETWORK_TIMEOUT);
			if(status == NX_SUCCESS || status == NX_WEB_HTTP_GET_DONE)
			{
				memcpy(&buf[packet_len], packet_pointer->nx_packet_prepend_ptr, packet_pointer->nx_packet_length);
				packet_len += packet_pointer->nx_packet_length;
				nx_packet_release(packet_pointer);
			}
		} while (status == NX_SUCCESS);

		CommData->CommStatesMachine = COMM_SM_STATUS_IP;

		return 0U;
	}
	else
	{
		CommData->CommStatesMachine = COMM_SM_IDLE;
		return 1U;
	}
}


uint8_t GetIDSitrad(_COMM_CONTROL *CommData)
{
	char buf[4096];
	ULONG packet_len = 0;
	UINT status;
	NX_WEB_HTTP_CLIENT *http;
	NX_DNS *dns;
	ULONG ip;

	char *host = "hlg-introducer.sitrad.com";
	char *resourceID = "/api/v1/converters";

	char ID_Sitrad[10];
	NX_PACKET    *http_packet;
	CHAR         http_request[150];

	memset(buf, '\0', sizeof buf);

	web_http_client_init0();

	http = &g_web_http_client0;
	dns = &g_dns0;

	if(!CellularInfoTest(CommData))
	{
		NXD_ADDRESS address = {.nxd_ip_version = NX_IP_VERSION_V4};

		status = nx_dns_server_add(dns, IP_ADDRESS(192, 168, 137, 1));
		if (NX_SUCCESS != status)
		{
			printf("Erro nx_dns_server_add\n");
			return 1U;
		}

		// busca o ip a partir do host. se falhar, usa o endereco ja conhecido.
		status = nx_dns_host_by_name_get(dns, (UCHAR *)host, &ip, NETWORK_TIMEOUT);
		if (NX_SUCCESS != status)
		{
			printf("DNS host by name ger error\n");
			address.nxd_ip_address.v4 = IP_ADDRESS(192, 185, 7, 163);
		}
		else
		{
			address.nxd_ip_address.v4 = ip;
		}


//#ifdef DEBUG_CELLULAR
//		printf("Host IP address = %d.%d.%d.%d \r\n", (int)(address.nxd_ip_address.v4>>24 & 0xFF),
//													 (int)(address.nxd_ip_address.v4>>16 & 0xFF),
//													 (int)(address.nxd_ip_address.v4>>8  & 0xFF),
//													 (int)(address.nxd_ip_address.v4>>0  & 0xFF));
//#endif

	    /* Set up the strings for the HTTP requests */
	    memset(http_request, 0, sizeof(http_request));
 	    snprintf(http_request, 150, "{\"identifier\": \"%s\", \"identifierValidation\": \"%s\"}",CommData->NetCellularConf.Imei, identifierValidationstring); //todo: Criar define pro buffer de 150 de http

//	    printf("\n%s%\n",http_request);

		// inicializa um request do tipo get.
	    status = nx_web_http_client_request_initialize(http, NX_WEB_HTTP_METHOD_POST, resourceID, host,strlen(http_request), NX_FALSE, NX_NULL, NX_NULL, NETWORK_TIMEOUT);
		if(status != NX_SUCCESS)
		{
			printf("nx_web_http_client_request_initialize error\r\n");
			return 1U;
		}

		// adiciona header para forcar resposta em json.
		status = nx_web_http_client_request_header_add(http, "content-type", strlen("content-type"), "application/json", strlen("application/json"), TX_NO_WAIT);
		if(status != NX_SUCCESS)
		{
			printf("SITRAD: Erro HTTP Client request header add!\n");
			return 1U;
		}

		// abre o socket.
		status = nx_web_http_client_connect(http, &address, NX_WEB_HTTP_SERVER_PORT, NETWORK_TIMEOUT);
		if (status != NX_SUCCESS)
		{
			printf("SITRAD: Erro HTTP Client Connect!\n");
			return 1U;
		}

		// envia o request
		status = nx_web_http_client_request_send(http, NETWORK_TIMEOUT);
		if(status != NX_SUCCESS)
		{
			printf("SITRAD: Erro HTTP Client request send 1!\n");
			return 1U;
		}

		//START EXEMPLO DO HTTPS EXAMPLE
		/* Create a new data packet request on the HTTP(S) client instance. */
		status = nx_web_http_client_request_packet_allocate(http, &http_packet, NETWORK_TIMEOUT);
		if (NX_SUCCESS != status)
		{
	    	printf("Erro nx_web_http_client_request_packet_allocate!\n");
			return 1U;
		}

	    status = nx_packet_data_append(http_packet, http_request, strlen(http_request), &g_packet_pool0, NETWORK_TIMEOUT);
	    if (NX_SUCCESS != status)
	    {
	    	printf("Erro nx_packet_data_append!\n");
			return 1U;
	    }

	    status = nx_web_http_client_request_packet_send(http, http_packet, 0, NETWORK_TIMEOUT);
	    if (NX_SUCCESS != status)
	    {
	    	printf("Erro nx_web_http_client_request_packet_send!\n");
			return 1U;
	    }
	    //END EXEMPLO DO HTTPS EXAMPLE

		NX_PACKET *packet_pointer;
		// recebe o body da resposta.
	    do
	    {
	    	status = nx_web_http_client_response_body_get(http, &packet_pointer, NETWORK_TIMEOUT);

	        if(status == NX_SUCCESS || status == NX_WEB_HTTP_GET_DONE)
	        {
	            memcpy(&buf[packet_len], packet_pointer->nx_packet_prepend_ptr, packet_pointer->nx_packet_length);
	            packet_len += packet_pointer->nx_packet_length;
	            nx_packet_release(packet_pointer);
	        }
	    } while (status == NX_SUCCESS);
	}

	if(packet_len)
	{
		char *ptr;
		// tokeniza o json e recupera os campos. Para parsear o json, podem ser usada uma bibliiotecan como a jsmn, por exemplo.
		ptr = strtok(buf, "{,}");
		while(ptr)
		{
			if(strstr(ptr, "\"id\":") == ptr)
			{
				strncpy(ID_Sitrad, ptr + strlen("\"id\":"), sizeof ID_Sitrad);
			}

			ptr = strtok(NULL, "{,}");
		}
	}

	memset((char *)CommData->NetCellularConf.IdSitrad, 0, sizeof(CommData->NetCellularConf.IdSitrad));
	strcpy((char *)CommData->NetCellularConf.IdSitrad, (const char *)ID_Sitrad);

	int_storage_write((uint8_t*)&CommData->NetCellularConf, sizeof(_NET_CELLULAR_CONF), CONF_CELLULAR);

	return 0U;
}


uint8_t CellularInfoTest(_COMM_CONTROL *CommData)
{
	sf_crypto_data_handle_t data;
	uint8_t identifier[16] = {0};
	uint8_t AES_message_digest[16]  = {0};
	uint8_t SHA1_message_digest[20] = {0};
	uint32_t digest_length;
	ssp_err_t status = SSP_SUCCESS;
    char temp[20];

	//Define o IMEI como senha padrão no teste de fábrica
	memcpy(&CommData->ConfNetBasic.PasswordDevice[0], &CommData->NetCellularConf.Imei[7], sizeof(CommData->ConfNetBasic.PasswordDevice));

	//Codifica o IMEI para AES
	memcpy(&identifier[0], &CommData->NetCellularConf.Imei[0], sizeof(identifier));

	for(uint8_t i = 0; i< SF_CELLULAR_IMEI_LEN; i++) {
		if(identifier[i] == 0) identifier[i] = 0x01;
	}

	AESEnc(&identifier[0], &AES_message_digest[0], SF_CELLULAR_IMEI_LEN);

	sf_crypto_hash_init0();

	//Cofifica o AES anterior para SHA1
    status = g_sf_crypto_hash0.p_api->hashInit(g_sf_crypto_hash0.p_ctrl);
    if (SSP_SUCCESS != status)
	{
		printf("Erro no hashinit\r\n");
		return 1U;
	}

    data.p_data = (uint8_t *)AES_message_digest;
    data.data_length = sizeof(AES_message_digest);
    status = g_sf_crypto_hash0.p_api->hashUpdate(g_sf_crypto_hash0.p_ctrl, &data);
    if (SSP_SUCCESS != status)
    {
    	printf("Erro no hashUpdate\r\n");
    	return 1U;
    }

    data.p_data = SHA1_message_digest;
	data.data_length = sizeof(SHA1_message_digest);
    status = g_sf_crypto_hash0.p_api->hashFinal(g_sf_crypto_hash0.p_ctrl, &data, &digest_length);
    if (SSP_SUCCESS != status)
    {
    	printf("Erro no hashFinal\r\n");
    	return 1U;
    }


    memcpy(&CommData->identifierValidation[0], &SHA1_message_digest[0], sizeof(SHA1_message_digest));
    for(int i=0; i<20; i++){
    	*temp = 0;
    	sprintf(temp, "%.2x", CommData->identifierValidation[i]);
    	strcat(identifierValidationstring, temp);
    }
	return 0U;
}

void GetStatusCellular(_COMM_CONTROL *CommData)
{
	sf_cellular_info_t CellularInfo;
	sf_cellular_network_status_t NetworkInfo;
	ssp_err_t	status = SSP_SUCCESS;

	memset(&CellularInfo, 0, sizeof(CellularInfo));
	memset(&NetworkInfo, 0, sizeof(NetworkInfo));
	int_storage_read((uint8_t *)&CommData->NetCellularConf, sizeof(_NET_CELLULAR_CONF), CONF_CELLULAR);
	status = g_sf_cellular0.p_api->infoGet(g_sf_cellular0.p_ctrl, &CellularInfo);
	if(SSP_SUCCESS == status)
	{
		CommData->CellularRssid = (uint8_t)(CellularInfo.rssi);
        CommData->NetCellularConf.ValCod = FLASH_NUMBER_MAGIC;

        // Leitura do IMEI do EXS82-W
    	memset(&CommData->NetCellularConf.Imei[0], 0, IMEI_SIZE);
        memcpy(&CommData->NetCellularConf.Imei[0], &CellularInfo.imei[0], SF_CELLULAR_IMEI_LEN);

        //Leitura do endereço IP do conversor
        memset(&IP_Adress[0], 0, 30);
        memcpy(&IP_Adress[0], &CellularInfo.ip_addr[0], 30);
	}

	status = g_sf_cellular0.p_api->networkStatusGet(g_sf_cellular0.p_ctrl, &NetworkInfo);
	if(SSP_SUCCESS == status)
	{
		memset(&CommData->NetCellularConf.BaseStation[0], 0, sizeof(CommData->NetCellularConf.BaseStation));
		memcpy(&CommData->NetCellularConf.BaseStation[0], &NetworkInfo.op_name[0], BASE_STATION_SIZE);
	}

//#ifdef DEBUG_CELLULAR
//        printf("SENHA DO CONVERSOR: %s\r\n", CommData->ConfNetBasic.PasswordDevice);
//        printf("IMEI: %s\r\n", CommData->NetCellularConf.Imei);
//        printf("OPERADORA: %s\r\n", CommData->NetCellularConf.BaseStation);
//#endif
        int_storage_write((uint8_t*)&CommData->NetCellularConf, sizeof(_NET_CELLULAR_CONF), CONF_CELLULAR);
}


//Salva na ram 5 mac de redes ocultas
bool MacAddressList(uint8_t *MacAdd, bool AddTRemoveF)
{
    static uint8_t MacAddress[MAX_NUM_MAC_LIST][6],
                   SizeList,
                   NumAtt;
    static bool FlagErase = false;

    if(SizeList == MAX_NUM_MAC_LIST)
    {
        FlagErase = 0;
    }

    if(!FlagErase)
    {
        memset(&MacAddress, 0, sizeof(MacAddress));
        SizeList = 0;
        NumAtt = 0;
        FlagErase = true;
    }

    if(AddTRemoveF)
    {
        uint8_t j=0;
        for(uint8_t i=0; i<10 && i<SizeList ; i++)
        {
            if( (MacAddress[i][j]   == MacAdd[j]) &&
                (MacAddress[i][j+1] == MacAdd[j+1]) &&
                (MacAddress[i][j+2] == MacAdd[j+2]) &&
                (MacAddress[i][j+3] == MacAdd[j+3]) &&
                (MacAddress[i][j+4] == MacAdd[j+4]) &&
                (MacAddress[i][j+5] == MacAdd[j+5]))
            {
                NumAtt++;
                if(NumAtt > MAX_NUM_MAC_LIST)
                {
                    NumAtt = 0;
                    SizeList = MAX_NUM_MAC_LIST;
                }
                return true; //caso ja esteja salvo o mac
            }
        }
        for(uint8_t i=0; i<6; i++)
        {
            MacAddress[SizeList][i] = *MacAdd++;
        }
        SizeList++;
    }
    else
    {
        memset(&MacAddress, 0, sizeof(MacAddress));
        SizeList = 0;
    }

    return false;
}

void SaveNet(sf_cellular_provisioning_t *ProvisionCellular)
{
    bool WriteFlash = true;
    _NET_CELLULAR_CONF NetCellularConf;
    int_storage_read((uint8_t *)&NetCellularConf, sizeof(_NET_CELLULAR_CONF), CONF_CELLULAR);
//    if(strlen((char *)NetWifiConf.ApSsid) == strlen((char *)ProvisionCellular->ssid)) //compara o tamanho da atual com a ssid da memoria
//    {
//       if(!strcmp((char *)NetWifiConf.ApSsid, (char *)ProvisionCellular->ssid)) //compara o igualdade das SSID
//       {
//           if(NetWifiConf.SecurityType == ProvisionCellular->security)
//           {
//               WriteFlash = false;
//           }
//       }
//    }
//
//    if(WriteFlash)
//    {
//        strcpy((char *)NetWifiConf.ApSsid, (char*)ProvisionCellular->ssid);
//        strcpy((char *)NetWifiConf.ApPwd, (char*)ProvisionCellular->key);
//        NetWifiConf.SecurityType = ProvisionCellular->security;
//
//        int_storage_write((uint8_t *)&NetWifiConf,      sizeof(_NET_WIFI_CONF),     CONF_WIFI_CLIENT);
//    }
}



void StrToHex(char *dest, char * src)
{
    size_t len = strlen(src);
    strcpy(dest,"");
    for(size_t i = 0; i < len; i++){
        char aux[3];
        sprintf(aux, "%.2x", src[i]);
        strcat(dest, aux);
    }
}
