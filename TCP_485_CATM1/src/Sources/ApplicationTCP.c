/*
 * ApplicationTCP.c
 *
 *  Created on: 5 de jun de 2019
 *      Author: leonardo.ceolin
 */

#include "common_data.h" // include obrigatório
#include "Includes/Cellular_Thread_Entry.h"
#include "Cellular_Thread.h"
#include "Includes/ApplicationTcp.h"
#include "Includes/ApplicationUdp.h"
#include "Includes/Define_IOs.h"
#include "Includes/common_util.h"

#if RECORD_DATALOGGER_EVENTS
#include "Includes/Thread_Datalogger_entry.h"
#endif

#define SIZE_BUFFER_TCP 2100
#define TCP_SOCKET_DISCONNECTION_TIMEOUT (100)

void TcpStart(_COMM_CONTROL *CommData);
uint8_t TcpAcceptConnect(_COMM_CONTROL *CommData);
_TCP_RETURN TcpEvent(_COMM_CONTROL *CommData);
void TcpSend(_COMM_CONTROL *CommData);
void TcpRelisten(_COMM_CONTROL *CommData, NX_TCP_SOCKET * SocketTCP, UINT timeout);
void TcpFinish(_COMM_CONTROL *CommData);
void TcpDisconnect(_COMM_CONTROL *CommData);
bool FullgaugeValidaIP_Remoto(uint32_t RemoteIP);
void TcpRefugeeConnection(_COMM_CONTROL *CommData);


void TcpStart(_COMM_CONTROL *CommData)
{
    UINT status;
	NX_TCP_SOCKET *SocketTCP;


	if(CommData->FlagFAC) //não inicializa a porta de comunicacao em caso de FAC.
		return;

	SocketTCP = & CommData->TcpSocket;

//	status = nx_tcp_socket_create(CommData->NxIP, SocketTCP, "TCP Socket", NX_IP_NORMAL, NX_FRAGMENT_OKAY, NX_IP_TIME_TO_LIVE, SIZE_BUFFER_TCP, NX_NULL,
//                         Cellular_TCP_DisconnectRequest);
	status = nx_tcp_socket_create(&g_ip_Cell, SocketTCP, "TCP Socket", NX_IP_NORMAL, NX_FRAGMENT_OKAY, NX_IP_TIME_TO_LIVE, SIZE_BUFFER_TCP, NX_NULL,
                         Cellular_TCP_DisconnectRequest);
    if (NX_SUCCESS != status)
    {
    	printf("ERRO: nx_tcp_socket_create\r\n");
    }

    //TODO: Retirada desta etapa, colocado pra teste
    CommData->IpStruct.Tcp_Port_Com = 4000;
    //printf("PORTA TCP: %d\r\n",CommData->IpStruct.Tcp_Port_Com);

    // Setup server listening on port Sitrad.
	//status = nx_tcp_server_socket_listen(CommData->NxIP, (UINT)CommData->IpStruct.Tcp_Port_Com, SocketTCP, 0, Cellular_TCP_ConnectRequest );
	status = nx_tcp_server_socket_listen(&g_ip_Cell, 4000, SocketTCP, 0, Cellular_TCP_ConnectRequest);
    if (NX_SUCCESS != status)
    {
    	printf("ERRO: nx_tcp_server_socket_listen\r\n");
    }

    //Setup a receive packet callback function for the "client_socket" socket.
	status = nx_tcp_socket_receive_notify(SocketTCP, Cellular_TCP_ReceiveNotify);
    if (NX_SUCCESS != status)
    {
    	printf("ERRO: nx_tcp_socket_receive_notify\r\n");
    }
}

uint8_t TcpAcceptConnect(_COMM_CONTROL *CommData)
{
    UINT    StatusRet;
    NX_TCP_SOCKET *SocketTCP;

    //porta de comunicacao TCP -> Apenas essa tem direito de refugar IP
	SocketTCP = & CommData->TcpSocket;
	if(!FullgaugeValidaIP_Remoto((uint32_t)(SocketTCP->nx_tcp_socket_connect_ip.nxd_ip_address.v4)))
	{
		// Se o cliente deve ser negado por causa do IP, o unico jeito e aceitar a conexao e depois fechar o socket imediatamente.
		// https://stackoverflow.com/questions/16590847/how-can-i-refuse-a-socket-connection-in-c
		// O sitrad, mesmo recebendo o FIN, tenta enviar comandos. O Nx Duo vai dar Ack nesse comando e vai chamar a callback de recepcao
		// Ao tentar receber o pacote, vai retornar erro Not Bound.
		StatusRet = nx_tcp_server_socket_accept(SocketTCP, 200);
		StatusRet = 1U;
	}
	else
	{
		StatusRet = nx_tcp_server_socket_accept(SocketTCP, 200);
	}

    nx_tcp_server_socket_unlisten(CommData->NxIP, (UINT)CommData->IpStruct.Tcp_Port_Com);
    if (StatusRet != NX_SUCCESS)
    {
        TcpRelisten(CommData, SocketTCP, 2 * TCP_SOCKET_DISCONNECTION_TIMEOUT);

        return 1U; //reporta problema
    }

    return 0U; //reporta sucesso
}

void TcpRefugeeConnection(_COMM_CONTROL *CommData)
{
    NX_TCP_SOCKET *SocketTCP;

    // Send to client. verifica a porta para a resposta
    SocketTCP = & CommData->TcpSocket;

    TcpRelisten(CommData, SocketTCP, 2 * TCP_SOCKET_DISCONNECTION_TIMEOUT);
}

bool FullgaugeValidaIP_Remoto(uint32_t RemoteIP)
{
   bool Result = true;
   _UDP_CONF   UdpConfFG;
   uint32_t    IpInit = 0,
               IpEnd  = 0;

   GetPacketUdpCuston(&UdpConfFG);

   if ( UdpConfFG.EnableIpFilter )
   {
       IpInit =                      (uint32_t)(UdpConfFG.InitFilterIp[0] << 24  );
       IpInit =  (uint32_t)IpInit |  (uint32_t)(UdpConfFG.InitFilterIp[1] << 16  );
       IpInit =  (uint32_t)IpInit |  (uint32_t)(UdpConfFG.InitFilterIp[2] << 8   );
       IpInit =  (uint32_t)IpInit |  (uint32_t)(UdpConfFG.InitFilterIp[3]        );

       IpEnd =                        (uint32_t)(UdpConfFG.EndFilterIp[0] << 24  );
       IpEnd =  (uint32_t)IpEnd     | (uint32_t)(UdpConfFG.EndFilterIp[1] << 16  );
       IpEnd =  (uint32_t)IpEnd     | (uint32_t)(UdpConfFG.EndFilterIp[2] << 8   );
       IpEnd =  (uint32_t)IpEnd     | (uint32_t)(UdpConfFG.EndFilterIp[3]        );


      Result = (( RemoteIP >= IpInit ) &&
                ( RemoteIP <= IpEnd ));

   }

   return Result;
}

////Funcao que trata do recebimento TCP e valida o pacote recebido comunicacao
_TCP_RETURN TcpEvent(_COMM_CONTROL *CommData)
{
    NX_TCP_SOCKET   * SocketTCP;
    NX_PACKET       * PacketReceive;
    UINT Status;

	SocketTCP       = &CommData->TcpSocket;
	PacketReceive   = (NX_PACKET*) &CommData->PacketReceiveTCP;

    Status = nx_tcp_socket_receive(SocketTCP, (NX_PACKET**)PacketReceive, 1);

    if (NX_SUCCESS == Status)
    {
        return CELL_TCP_REQUESTED;
    }
    else if(NX_NOT_BOUND == Status)
    {
        // Caso o a conexao tenha sido negada, o socket era aberto com accept e fechado logo em seguida.
        // Mesmo assim o sitrad pode enviar um pacote, e vai cair aqui com esse erro. A conexao ja esta fechada, e nao pode ser fechada novamente.
    }
    else if(NX_NO_PACKET != Status)
    {
        TcpRelisten(CommData, SocketTCP, 5 * TCP_SOCKET_DISCONNECTION_TIMEOUT);
    }

#if RECORD_DATALOGGER_EVENTS
    DataLoggerRegisterEvent(DATALOGGER_EVENT_TCP_ERROR, (uint16_t)(0x100 + Status), CommData->IpStruct.Tcp_Port_Com);
#endif

   return CELL_TCP_NONE;
}



void TcpSend(_COMM_CONTROL *CommData)
{
    ULONG SizeTcpSend;
    static NX_PACKET *PacketTCP_Temp;
    NX_TCP_SOCKET *SocketTCP;

    // Allocate a packet for the Send message.
    if (NX_SUCCESS != nx_packet_allocate(&g_packet_pool0, &PacketTCP_Temp, NX_TCP_PACKET, 1))//50
    {
        return;
    }

    SizeTcpSend = (ULONG)(SIZE_HEADER_TCP + ((CommData->BufferTcp.HP.Header[1] << 8) | CommData->BufferTcp.HP.Header[2])); //parte +signficativa do tamanho

    if (NX_SUCCESS != nx_packet_data_append(PacketTCP_Temp, CommData->BufferTcp.HeaderPayload, SizeTcpSend, &g_packet_pool0, 1))//50
    {
        nx_packet_release(PacketTCP_Temp);
        return;
    }

    // Send to client. verifica a porta para a resposta
	SocketTCP = & CommData->TcpSocket;

    UINT result = nx_tcp_socket_send(SocketTCP, PacketTCP_Temp, 5); //500
    if (NX_SUCCESS != result)
    {

#if RECORD_DATALOGGER_EVENTS
        DataLoggerRegisterEvent(DATALOGGER_EVENT_TCP_ERROR, (uint16_t)result, CommData->IpStruct.Tcp_Port_Com);
#endif

        if (NX_WINDOW_OVERFLOW == result)
        {
            // tenta de novo
            result = nx_tcp_socket_send(SocketTCP, PacketTCP_Temp, 5); //500
        }
        if (NX_SUCCESS != result)
        {

#if RECORD_DATALOGGER_EVENTS
        DataLoggerRegisterEvent(DATALOGGER_EVENT_TCP_ERROR, (uint16_t)result, CommData->IpStruct.Tcp_Port_Com);
#endif

            nx_packet_release(PacketTCP_Temp);
            TcpRelisten(CommData, SocketTCP, 5 * TCP_SOCKET_DISCONNECTION_TIMEOUT);
        }
    }

}

void TcpRelisten(_COMM_CONTROL *CommData, NX_TCP_SOCKET * SocketTCP, UINT timeout)
{

#if RECORD_DATALOGGER_EVENTS
    DataLoggerRegisterEvent(DATALOGGER_EVENT_SOCKET_SHUTDOWN, (uint16_t)timeout, CommData->IpStruct.Tcp_Port_Com);
#endif

    // Now disconnect the server socket from the client.
    nx_tcp_socket_disconnect(SocketTCP, timeout);

    // Unaccept the server socket. Note that unaccept is called even if disconnect or accept fails.
    nx_tcp_server_socket_unaccept(SocketTCP);

    /* Setup server socket for listening with this socket again. */
    //nx_tcp_server_socket_listen(   CommData->NxIP, (UINT)PortSitrad, SocketTCP, 0,
                                       //(PortSitrad == CommData->IpStruct.Tcp_Port_Conf) ? Cellular_TCP_ConnectRequestConf : Cellular_TCP_ConnectRequest );
    nx_tcp_server_socket_listen(   CommData->NxIP, (UINT)CommData->IpStruct.Tcp_Port_Com, SocketTCP, 0,
                                   Cellular_TCP_ConnectRequest );
}

void TcpFinish(_COMM_CONTROL *CommData)
{
    UINT port;
    NX_TCP_SOCKET *SocketTCP;

    // Send to client. verifica a porta para a resposta
	SocketTCP = & CommData->TcpSocket;

    nx_tcp_server_socket_unlisten(CommData->NxIP, CommData->IpStruct.Tcp_Port_Com);

    if(!nx_tcp_client_socket_port_get(SocketTCP, &port))
    {
        nx_tcp_socket_disconnect(SocketTCP, 100);

        nx_tcp_server_socket_unaccept(SocketTCP);
    }

    nx_tcp_socket_delete(SocketTCP);
}

void TcpDisconnect(_COMM_CONTROL *CommData)
{
    UINT port;

    NX_TCP_SOCKET *SocketTCP;

    // Send to client. verifica a porta para a resposta
	SocketTCP = & CommData->TcpSocket;

    if(!nx_tcp_client_socket_port_get(SocketTCP, &port))
    {
        TcpRelisten(CommData, SocketTCP, 15 * TCP_SOCKET_DISCONNECTION_TIMEOUT);

    }

}

