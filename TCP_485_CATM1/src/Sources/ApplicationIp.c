/*
 * ApplicationIp.c
 *
 *  Created on: 7 de jun de 2019
 *      Author: leonardo.ceolin
 */
#include "Includes/MacAdress.h"
#include "Includes/Cellular_Thread_Entry.h"
#include "common_data.h" // include obrigatório
#include "Cellular_Thread.h"
#include "Includes/ApplicationUSB.h"
//#include "Includes/Eeprom25AA02E48.h"
#include "Includes/ApplicationIp.h"
#include "Includes/Define_IOs.h"
#include "Includes/ApplicationUdp.h"
//#include "Includes/ApplicationAP.h"
#include "Includes/ApplicationCellular.h"
#include "Includes/common_util.h"
#include <string.h>

#if RECORD_DATALOGGER_EVENTS
#include "Includes/Thread_Datalogger_entry.h"
#endif

void CommOpen(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX);
uint8_t CellularStartDhcpClient (_COMM_CONTROL *CommData);
uint8_t CellularStartStaticIP(NX_IP * NxIP, ULONG Address, ULONG Mask, ULONG Gtw, ULONG Dns);
uint8_t CellularGetStatusIp(_COMM_CONTROL *CommData);
UINT NetStop (_COMM_CONTROL *CommData);

bool MacAddressValueCell(uint8_t *MacAddressCellular, uint8_t Select);
void SetMACValueCell(uint8_t *MacAddressCell);
_FG_ERR GetMACValueCell(uint8_t *MacAddressCell);


void mydhcp_Cellular(CHAR *name);
void ReplaceNonAlphanumericByDash(CHAR *Str);

#define ARP_RENEWAL_PERIOD (60 * _CLOCKS_PER_SEC_)
#define ARP_CHECK_PERIOD (30 * _CLOCKS_PER_SEC_)

// Inicialização da comunicação (cellular/usb)
void CommOpen(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX)
{
    //Message Framework: Used in Acquire
    CommData->MessageCfg.acquireCfg.buffer_keep = true;   // não desaloca o buffer

    //Message Framework: Used in Post
    CommData->MessageCfg.postCfg.priority = SF_MESSAGE_PRIORITY_NORMAL;
    CommData->MessageCfg.postCfg.p_callback = NULL; // não cria callback

    //Message Framework: Cria BufferAcquire para postar para Serial 485
    g_sf_message0.p_api->bufferAcquire( g_sf_message0.p_ctrl,
                                        (sf_message_header_t **) &CommData->SerialDataInPayload,
                                        &CommData->MessageCfg.acquireCfg,
                                        300);

    //Message Framework: Cria BufferAcquire para postar para RTC
    g_sf_message0.p_api->bufferAcquire(  g_sf_message0.p_ctrl,
                                         (sf_message_header_t **) &CommData->DateTimeMessagePayload,
                                         &CommData->MessageCfg.acquireCfg,
                                         300);

    //Message Framework: Cria BufferAcquire para postar para Firmware
    g_sf_message0.p_api->bufferAcquire(  g_sf_message0.p_ctrl,
                                         (sf_message_header_t **) &CommData->FlashExternalPaylod,
                                         &CommData->MessageCfg.acquireCfg,
                                         300);

    //Cria a notificacao entre uma mensagem recebida com o Data_Manager_Message_Framework_NotifyCallback
    tx_queue_send_notify (&Cellular_Thread_message_queue, Cellular_Message_Framework_NotifyCallback);

    StatusFlagsX->StateSitrad           = false;
    StatusFlagsX->FlagNetConnecting     = false;
    //StatusFlagsX->FlagNumAttempts       = false;
    StatusFlagsX->FlagStartUdpTcp       = false;
    StatusFlagsX->FlagNetConnected      = false;
    StatusFlagsX->FlagSitradConnected   = false;

    StatusFlagsX->FlagErrorIp           = false;
    StatusFlagsX->FlagErrorPassWrd      = false;

    CommData->FlagConnect            = false;
    CommData->ValidAccess            = false;
//    CommData->ValidAccessUSB         = false;
    CommData->BigFrameTCP            = false;
    CommData->FlagUpdateFWFinished   = false;
    CommData->FlagUpdateFWRun        = false;

    CommData->TimerBigFrameTCP       = 0;
    CommData->TimerRestartModule     = 0;
    CommData->TimerUpdateFWCheckCRC  = 0;
    CommData->TimerTimeOutUpdateFW   = 0;
    CommData->TimerRestoreFactory    = 0;
    CommData->TimerInactivity        = 0;

    g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_HIGH); // DESLIGA O LED DE RECEBIMENTO
    g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_HIGH); // DESLIGA O LED DE ENVIO

    CommData->CommStatesMachine = COMM_SM_IDLE;
}


/***********************************************************************************************************************
 * @brief  start_dhcp_client function
 *
 * This function starts the dhcp client and obtaines the IP address
 ***********************************************************************************************************************/
uint8_t CellularStartDhcpClient (_COMM_CONTROL *CommData)
{
   ULONG   CellularIpAddress = 0,
           CellularMaskAddress = 0;
   ULONG   NeededStatus = 0;
   UINT    StatusRet = 0;

   uint8_t retry_cnt = 0;

   static _UDP_CONF ConfSitradUdp;
   ReadPacketUdpCuston(&ConfSitradUdp);
   CHAR NameForDhcp[sizeof ConfSitradUdp.ModelName];
   strcpy(NameForDhcp, (CHAR*)ConfSitradUdp.ModelName);
   ReplaceNonAlphanumericByDash(NameForDhcp);

   mydhcp_Cellular(NameForDhcp);

   if(NX_SUCCESS != nx_dhcp_stop(CommData->NxDHCP))
   {
	   printf("\nErro DHCP\n");
       return 1U;
   }

   if(NX_SUCCESS != nx_dhcp_reinitialize(CommData->NxDHCP))
   {
	   printf("\nErro DHCP\n");
       return 1U;
   }

   CellularMaskAddress = 0;
   CellularIpAddress = 0;

   if (NX_SUCCESS != nx_ip_interface_address_set(CommData->NxIP, 0, CellularIpAddress, CellularMaskAddress))
   {
	   printf("\nErro DHCP\n");
       return 1U;
   }

   if(NX_SUCCESS != nx_ip_interface_status_check(CommData->NxIP, 0, NX_IP_LINK_ENABLED, &NeededStatus, TX_WAIT_FOREVER))
   {
	   printf("\nErro DHCP\n");
       return 1U;
   }

   if(NX_SUCCESS != nx_dhcp_start(CommData->NxDHCP))
   {
	   printf("\nErro DHCP\n");
       return 1U;
   }

   do{
        StatusRet = nx_ip_status_check(CommData->NxIP, NX_IP_ADDRESS_RESOLVED, &NeededStatus, 200);
        retry_cnt++;
   }while(StatusRet && (retry_cnt < 5));

   if(StatusRet)
   {
	   printf("\nErro DHCP\n");
       return 1U;
   }

   return 0U;
}


/*********************************************************************************************************************
 * @brief CellularStartStaticIP
 *
 * This function sets the IP mode to static mode
 ********************************************************************************************************************/
uint8_t CellularStartStaticIP(NX_IP * NxIP, ULONG Address, ULONG Mask, ULONG Gtw, ULONG Dns)
{
    UINT status;
    SSP_PARAMETER_NOT_USED(Dns);
    SSP_PARAMETER_NOT_USED(Gtw);
    status = nx_ip_address_set(NxIP, Address, Mask);
    if (NX_SUCCESS != status) {
#ifdef DEBUG_CELLULAR
    	printf("ERRO: nx_ip_address_set\r\n");
#endif
        return 1U;
    }

//    status = nx_ip_gateway_address_set(NxIP, Gtw);
//    if (NX_SUCCESS != status) {
//#ifdef DEBUG_CELLULAR
//    	printf("ERRO: nx_ip_gateway_address_set\r\n");
//#endif
//        return 1U;
//    }

    return 0;
}

uint8_t CellularGetStatusIp(_COMM_CONTROL *CommData)
{
    uint8_t MacAdressCellular[6];
    UINT status;
    CommData->IpStruct.IpGateway = 0;
    CommData->IpStruct.IpAddress = 0;
    CommData->IpStruct.IpMask = 0;

    /* retrieve the IP address */
    status = nx_ip_interface_address_get(CommData->NxIP, 0, &CommData->IpStruct.IpAddress, &CommData->IpStruct.IpMask);
    if (NX_SUCCESS != status)
        return 1U;

#if DEBUG_CELLULAR
    printf("IP address = %d.%d.%d.%d\r\n", (int)(CommData->IpStruct.IpAddress>>24 & 0xFF),
                                           (int)(CommData->IpStruct.IpAddress>>16 & 0xFF),
                                           (int)(CommData->IpStruct.IpAddress>>8 &  0xFF),
                                           (int)(CommData->IpStruct.IpAddress>>0 &  0xFF));

    printf("IP Mask = %d.%d.%d.%d\r\n", (int)(CommData->IpStruct.IpMask>>24 & 0xFF),
                                        (int)(CommData->IpStruct.IpMask>>16 & 0xFF),
                                        (int)(CommData->IpStruct.IpMask>>8 &  0xFF),
                                        (int)(CommData->IpStruct.IpMask>>0 &  0xFF));
#endif

    //status = nx_ppp_ip_address_assign(CommData->NxPPP,CommData->IpStruct.IpAddress,CommData->IpStruct.IpAddress);
    //if (NX_SUCCESS != status) printf("Erro nx_ppp_ip_address_assign\r\n");

    if(NX_SUCCESS != nx_ip_gateway_address_get(CommData->NxIP, &CommData->IpStruct.IpGateway))
    {
        if(!CommData->IpStruct.IpGateway)
        {
        	CommData->IpStruct.IpGateway = CommData->IpStruct.IpAddress & CommData->IpStruct.IpMask;
        	CommData->IpStruct.IpGateway++;
        }
    }

#if DEBUG_CELLULAR
    printf("IP Gateway = %d.%d.%d.%d\r\n", (int)(CommData->IpStruct.IpGateway>>24 & 0xFF),
                                           (int)(CommData->IpStruct.IpGateway>>16 & 0xFF),
                                           (int)(CommData->IpStruct.IpGateway>>8 &  0xFF),
                                           (int)(CommData->IpStruct.IpGateway>>0 &  0xFF));
#endif



    if(NX_SUCCESS != nx_ip_interface_physical_address_get(CommData->NxIP, 0UL, &CommData->IpStruct.IpMacAdressMs, &CommData->IpStruct.IpMacAdressLs))
    {
        return 1U;
    }

    MacAdressCellular[5] = (uint8_t)((CommData->IpStruct.IpMacAdressMs >>8)   & 0xFF);
    MacAdressCellular[4] = (uint8_t)((CommData->IpStruct.IpMacAdressMs)       & 0xFF);
    MacAdressCellular[3] = (uint8_t)((CommData->IpStruct.IpMacAdressLs>>24)   & 0xFF);
    MacAdressCellular[2] = (uint8_t)((CommData->IpStruct.IpMacAdressLs>>16)   & 0xFF);
    MacAdressCellular[1] = (uint8_t)((CommData->IpStruct.IpMacAdressLs>>8)    & 0xFF);
    MacAdressCellular[0] = (uint8_t)( CommData->IpStruct.IpMacAdressLs        & 0xFF);

    SetMACValueCell(&MacAdressCellular[0]); //Seta o MACAddress na RAM

//    if(CommData->IpStruct.IpGateway == CommData->IpStruct.IpAddress)
//    {
//        return 1U;
//    }

    //seta a strutura do IP para a ram -> UdpOndemand
    SetUdpIp(&CommData->IpStruct);

    return 0U;
}

UINT NetStop (_COMM_CONTROL *CommData)
{
    if(CommData->ConfNetBasic.AddrMode != MODE_STATIC)
    {
        nx_dhcp_stop(CommData->NxDHCP);
        nx_dhcp_delete(CommData->NxDHCP);
    }

    memset(&g_dhcp_Cell, 0, sizeof(g_dhcp_Cell));

    nx_ip_delete(CommData->NxIP);

    memset(&g_ip_Cell, 0, sizeof(g_ip_Cell));

    nx_packet_pool_delete(&g_packet_pool0);

    memset(&g_packet_pool0, 0, sizeof(g_packet_pool0));

    return NX_SUCCESS;
}


///*********************************************************************************************************************
// * @brief  set_mac_address function
// *
// * Sets the unique Mac Address of the device using the FMI unique ID.
// ********************************************************************************************************************/
//void set_mac_address (nx_mac_address_t*_pMacAddress)
//{
//    fmi_unique_id_t id;
//    _FG_ERR Status;
//    ULONG lowerHalfMac;
//    uint8_t ZEepromMacAdress[6];
//
//    Status = GetMACValue(&ZEepromMacAdress[0]);
//
//    if(FG_ERROR == Status)
//    {
//        /* Read FMI unique ID */
//        g_fmi.p_api->uniqueIdGet(&id);
//
//        /* REA's Vendor MAC range: 00:30:55:xx:xx:xx */
//        lowerHalfMac = ((0x55000000) | (id.unique_id[0] & (0x00FFFFFF)));
//
//        /* Return the MAC address */
//        _pMacAddress->nx_mac_address_h=0x0030;
//        _pMacAddress->nx_mac_address_l=lowerHalfMac;
//    }
//    else
//    {
//        /* Return the MAC address */
//        _pMacAddress->nx_mac_address_h = (ULONG)(   ((ZEepromMacAdress[0]&(0x000000FF)) << 8) | (ZEepromMacAdress[1]&(0x000000FF)));
//        _pMacAddress->nx_mac_address_l = (ULONG)(   ((ZEepromMacAdress[2]&(0x000000FF)) << 24) | ((ZEepromMacAdress[3]&(0x000000FF)) << 16) |
//                                                    ((ZEepromMacAdress[4]&(0x000000FF)) << 8) | (ZEepromMacAdress[5]));
//    }
//}

void mydhcp_Cellular(CHAR *name)
{

    UINT g_dhcp_Cell_err;
    /* Create DHCP client. */
    g_dhcp_Cell_err = nx_dhcp_create (&g_dhcp_Cell, &g_ip_Cell, name);
    if (NX_SUCCESS != g_dhcp_Cell_err)
    {
        g_dhcp_Cell_err_callback ((void *) &g_dhcp_Cell, &g_dhcp_Cell_err);
    }

#if DHCP_USR_OPT_ADD_ENABLE_g_dhcp_Cell
    /* Set callback function to add user options.  */
    g_dhcp_Cell_err = nx_dhcp_user_option_add_callback_set(&g_dhcp_Cell,
            dhcp_user_option_add_Cell);

    if (NX_SUCCESS != g_dhcp_Cell_err)
    {
        g_dhcp_Cell_err_callback((void *)&g_dhcp_Cell,&g_dhcp_Cell_err);
    }
#endif
}

void ReplaceNonAlphanumericByDash(CHAR *Str)
{
    CHAR *ptr = Str;
    while(*ptr)
    {
        if((!isalnum(*ptr)) && (*ptr != '-') && (*ptr != '.') )
        {
            *ptr = '-';
        }
        ptr++;
    }

}

void CellularCheckConnectivity(_COMM_CONTROL *CommControl, _STATUS_FLAGS *StatusFlagsX)
{
    static ULONG LastCheck = 0;
    static bool Sent = false;
    static bool IpIsValid = false; // controle para ver se o ip foi validado ao menos uma vez. Se essa flag esta, nao pode gerar erro de ip por aqui

    ULONG Now = tx_time_get();

    if(CommControl->CommStatesMachine == COMM_SM_DONE)
    {
        // limpa controle
        Sent = false;
        IpIsValid = false;

        if(CommControl->ConfNetBasic.AddrMode == MODE_STATIC)
        {
            LastCheck = Now - ARP_RENEWAL_PERIOD;
        }
        else
        {
            LastCheck = Now;
        }
        return;
    }
    if((Now - LastCheck) > ARP_RENEWAL_PERIOD)
    {
        LastCheck = Now;
        Sent = true;
        nx_arp_dynamic_entries_invalidate(CommControl->NxIP);
        nx_arp_dynamic_entry_set(CommControl->NxIP, CommControl->IpStruct.IpGateway, 0, 0);
        nx_arp_gratuitous_send(CommControl->NxIP, NULL);
    }
    else if(Sent && ((Now - LastCheck) > (ARP_CHECK_PERIOD)))
    {
        Sent = false;
        ULONG msb, lsb;
//        if (NX_SUCCESS != nx_arp_hardware_address_find(CommControl->NxIP, CommControl->IpStruct.IpGateway, &msb, &lsb))
//        {
//            // no modo estatico, acusa erro de ip
//            if ((CommControl->ConfNetBasic.AddrMode == MODE_STATIC) && (!IpIsValid)) // se ainda nao validou ip no modo estatico, assume erro de ip
//            {
//                StatusFlagsX->FlagErrorIp = true;
//            }
//
//#if RECORD_DATALOGGER_EVENTS
//            DataLoggerRegisterEvent(DATALOGGER_EVENT_IP_ERROR, CommControl->CommStatesMachine, CommControl->ConfNetBasic.AddrMode);
//#endif
//#if DEBUG_CELLULAR
//            printf("Erro no Check Connectivity\r\n");
//#endif
//            CommControl->CommStatesMachine = COMM_SM_RESTART_IP;
//        }
//        else
        {
            StatusFlagsX->FlagErrorIp = false;//limpa erro de ip independente do caso
            IpIsValid = true; // marca para que nao assumir mais erro de ip
        }
    }
}

//Essa funcao escreve o endereco mac na ram.
void SetMACValueCell(uint8_t *MacAddressCell)
{
    MacAddressValueCell(&MacAddressCell[0], 0U);
}

//Essa funcao le o endereco mac na ram.
_FG_ERR GetMACValueCell(uint8_t *MacAddressCell)
{
    if(MacAddressValueCell(&MacAddressCell[0], 1U))
    {
        return FG_SUCCESS;
    }

    return FG_ERROR;
}


bool MacAddressValueCell(uint8_t *MacAddressCellular, uint8_t Select)
{
    static uint8_t MacAddress[6];
    static bool Ret = false;

    for(uint8_t i=0; i < 6; i++)
    {
        if(!Select)//put
        {
            MacAddress[i] = MacAddressCellular[i];
            Ret = true; //sinaliza que em algum momento o MacAddress já foi carregado/lido
        }
        else//get
        {
        	MacAddressCellular[i] = MacAddress[i];
        }
    }

    return Ret;
}


