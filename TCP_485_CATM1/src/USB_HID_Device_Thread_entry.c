#include "USB_HID_Device_Thread.h"
#include "Includes/common_util.h"
#include "Includes/ApplicationUSB.h"
#include "Includes/ApplicationCellular.h"
#include "Includes/ConfDefault.h"
#include "Includes/Thread_Monitor_entry.h"
#include "Includes/MainManagerThreadEntry.h"

extern void g_ux_device_class_register_hid(void);
extern void StatusUSB(_STATUS_FLAGS *StatusFlagsX);
extern _USB_RETURN AppPacketUSB(_COMM_CONTROL *CommData, _STATUS_FLAGS *StatusFlagsX);
void USB_Message_Framework_NotifyCallback(TX_QUEUE *USB_HID_Device_Thread_message_queue);


void  usb_activate(void *hid_instance);
void  usb_deactivate(void *hid_instance);

#define HID_FLAG ((ULONG)0x0001)

ULONG status;

UX_SLAVE_CLASS_HID* g_hid;

UX_SLAVE_CLASS_HID_EVENT hid_event;
extern aes_instance_t g_sce_aes_0;

void  usb_activate(void *hid_instance)
{
    /* Save the CDC instance.  */
	g_hid = (UX_SLAVE_CLASS_HID *)hid_instance;
    tx_event_flags_set(&USB_Flags, HID_FLAG, TX_OR);
#ifdef DEBUG_USB
	printf("USB actived\n");
#endif
}

void  usb_deactivate(void *hid_instance)
{
	SSP_PARAMETER_NOT_USED(hid_instance);
    tx_event_flags_set(&USB_Flags, ~HID_FLAG, TX_AND);
    g_hid = UX_NULL;
#ifdef DEBUG_USB
	printf("USB deactivate\n");
#endif
}

_USB_RETURN SendPacketUSB(_COMM_CONTROL *CommData, uint16_t len)
{
	SSP_PARAMETER_NOT_USED(len);

	if(g_hid != NULL)
	{
		memset(&hid_event.ux_device_class_hid_event_buffer[0], 0, sizeof(hid_event.ux_device_class_hid_event_buffer));
		hid_event.ux_device_class_hid_event_length = 64;
		memcpy(&hid_event.ux_device_class_hid_event_buffer[0], &CommData->BufferUSB.HeaderPayload[0], len);
		if(hid_event.ux_device_class_hid_event_buffer[0] == 0x26)return UX_SUCCESS;
		status = ux_device_class_hid_event_set(g_hid, &hid_event);
		if(status != TX_SUCCESS)
		{
			printf("Error at ux_device_class_hid_event_set: %d", status);
			while(1){}
		}
		return UX_SUCCESS;
	}
	return UX_ERROR;

}

/* USB HID Device Thread entry function */
void USB_HID_Device_Thread_entry(void) {

	ULONG actual_flags;
	static _STATUS_FLAGS StatusFlags;

	g_ux_device_class_register_hid();
#ifdef DEBUG_USB
	printf("Thread USB inicializada\n");
#endif

	g_sce_0.p_api->open(g_sce_0.p_ctrl, g_sce_0.p_cfg);
	g_sce_aes_0.p_api->open(g_sce_aes_0.p_ctrl, g_sce_aes_0.p_cfg); //Open AES_0

	CommControl.SendPkts = -1;

    while (1)
	{
    	ux_utility_memory_set(&hid_event, 0, sizeof(UX_SLAVE_CLASS_HID_EVENT));
    	/* Is the device configured ? */
    	/* Check if a HID device is connected */
		status = tx_event_flags_get(&USB_Flags, HID_FLAG,  TX_OR, &actual_flags,  TX_WAIT_FOREVER);
		if(status != TX_SUCCESS)
		{
			printf("Error at tx_event_flags_get: %d", status);
			while(1){}
		}


		if (g_hid == NULL)
		{
#ifdef DEBUG_USB
			printf("\nZEROU HID\n");
#endif
			g_ux_device_class_register_hid();
		}

		/* Until the device stays configured.  */
		if (g_hid != NULL)
		{
			tx_thread_sleep(10);

		    AppPacketUSB(&CommControl, &StatusFlags);
		    StatusUSB(&StatusFlags);
		}
	}
}
