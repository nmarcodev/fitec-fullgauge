/*
 * bootloader.c
 *
 *  Created on: Dec 19, 2016
 *      Author: LAfonso01
 */
#include "bsp_api.h"
#include "hal_data.h"
#include "utilities.h"
#include "flash_handler.h"
#include "bootloader.h"
#include "Define_IOs.h"

void Leds(bool LedOnR, bool LedOnG, bool LedOnB, bool LedStR, bool LedStG, bool LedStB);

typedef void ( *function_ptr_T ) ( void );

function_ptr_T function_ptr;
uint32_t  *ISR_Ptr;
uint32_t  *booMagicNum;

const fl_image_header_t *AppInfo;
const fl_image_header_t *AppExtInfo;

void jumptoApplication(_ERROR_CODE* errorCode)
{
    uint8_t temp[4];
    uint32_t MagicCod = 0;
    // habilita watchdog
    //g_wdt0.p_api->open(g_wdt0.p_ctrl, g_wdt0.p_cfg);
    Leds(true, true, true, true, true, true);

    g_flash0.p_api->open(g_flash0.p_ctrl, g_flash0.p_cfg); //open internal flash
    AppInfo = (fl_image_header_t *)APP_INFO_ADDRESS;
    AppExtInfo = (fl_image_header_t *)APP_EXT_INFO_ADDRESS;
    booMagicNum = (uint32_t *)FLASH_DF_BOOTLOADER_CODE_ADDRRESS;

    __disable_irq ();
    g_flash0.p_api->read(g_flash0.p_ctrl, &temp[0], (uint32_t)FLASH_DF_BOOTLOADER_CODE_ADDRRESS,sizeof(uint32_t));
    __enable_irq ();
    g_flash0.p_api->close(g_flash0.p_ctrl);

    MagicCod =  (uint32_t)(MagicCod | (uint32_t)(temp[3] << 24 ));
    MagicCod =  (uint32_t)(MagicCod | (uint32_t)(temp[2] << 16 ));
    MagicCod =  (uint32_t)(MagicCod | (uint32_t)(temp[1] << 8  ));
    MagicCod =  (uint32_t)(MagicCod | (uint32_t)(temp[0]       ));

    if(MagicCod != BOOTLOADER_VALID_CODE)
    {
        /*Check if app CRC CCITT match with the calculated CRC */
        if(fl_check_application() ==  AppInfo->raw_crc)
        {
            if(AppInfo->valid_mask == 0xAAAA)
            {
                uint32_t *msp = (uint32_t *)APP_RESET;
                ISR_Ptr = (uint32_t  *)(APP_RESET+4);
                function_ptr = (function_ptr_T)*ISR_Ptr;
                SCB->VTOR = (uint32_t)APP_RESET;

//                /** Set stack here. */
//                __set_MSP(*((uint32_t*)(AppAddr))-0x10);

                //Isso é obrigatorio na versao 1.7.0 para debug
                R_SPMON->MSPMPUCTL = (uint16_t)0x0000; //Desabilita monitor stack
                __NOP();
                __set_MSP(*msp);
                g_flash0.p_api->close(g_flash0.p_ctrl);
                __NOP();
                __NOP();
                g_wdt0.p_api->refresh(g_wdt0.p_ctrl);
                Leds(true, false, false, true, true, false);
                function_ptr();
                __NOP();
            }
            else
            {
                *errorCode = CODE_MASK;
                Leds(true, true, true, true, false, false);
            }
        }
        else
        {
            *errorCode = CODE_CRC;
            Leds(true, true, true, true, false, false);
        }
    }
    else
    {
        bootloader(CODE_USER);//*errorCode = CODE_USER;
    }
}

void bootloader(_ERROR_CODE errorCode)
{
    ssp_err_t SSP_Status;

    Leds(true, true, true, true, true, false);

    SSP_Status = g_qspi0.p_api->open(g_qspi0.p_ctrl, g_qspi0.p_cfg);
    uint16_t CalcCrc;

    AppInfo = (fl_image_header_t *)APP_INFO_ADDRESS;
    g_flash0.p_api->open(g_flash0.p_ctrl, g_flash0.p_cfg); //opne flash interna

    g_flash0.p_api->close(g_flash0.p_ctrl);

    AppExtInfo = (fl_image_header_t *)APP_EXT_INFO_ADDRESS;
    CalcCrc = fl_check_ext_application();
    g_wdt0.p_api->refresh(g_wdt0.p_ctrl);
    if(CalcCrc ==  AppExtInfo->raw_crc)
    {
        flashInit();
        g_wdt0.p_api->refresh(g_wdt0.p_ctrl);
        flashWrite((unsigned char *)BSP_PRV_QSPI_DEVICE_PHYSICAL_ADDRESS, APP_LAST_BLOCK_ADDRESS);
        g_wdt0.p_api->refresh(g_wdt0.p_ctrl);

        // apaga requisicao de atualizacao
        g_wdt0.p_api->refresh(g_wdt0.p_ctrl);
        __disable_irq ();
        g_flash0.p_api->erase(g_flash0.p_ctrl, FLASH_DF_BOOTLOADER_CODE_ADDRRESS, 1);
        __enable_irq ();

        NVIC_SystemReset();
    }
    else
    {
        for(uint32_t i = 0; i < 0xFFFFFF; i++);

        jumptoApplication(&errorCode);
    }
    SSP_PARAMETER_NOT_USED(SSP_Status);
}


void Leds(bool LedOnR, bool LedOnG, bool LedOnB, bool LedStR, bool LedStG, bool LedStB)
{
    g_ioport.p_api->pinWrite(LED_ON_R,  LedOnR?IOPORT_LEVEL_LOW:IOPORT_LEVEL_HIGH);
    g_ioport.p_api->pinWrite(LED_ON_G,  LedOnG?IOPORT_LEVEL_LOW:IOPORT_LEVEL_HIGH);
    g_ioport.p_api->pinWrite(LED_ON_B,  LedOnB?IOPORT_LEVEL_LOW:IOPORT_LEVEL_HIGH);

    g_ioport.p_api->pinWrite(LED_ST_R,  LedStR?IOPORT_LEVEL_LOW:IOPORT_LEVEL_HIGH);
    g_ioport.p_api->pinWrite(LED_ST_G,  LedStG?IOPORT_LEVEL_LOW:IOPORT_LEVEL_HIGH);
    g_ioport.p_api->pinWrite(LED_ST_B,  LedStB?IOPORT_LEVEL_LOW:IOPORT_LEVEL_HIGH);
}



